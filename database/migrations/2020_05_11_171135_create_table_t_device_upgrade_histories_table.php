<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTDeviceUpgradeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_device_upgrade_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_first_id');
            $table->integer('device_last_id');
            $table->tinyInteger('level_upgrade');
            $table->integer('user_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_device_upgrade_histories');
    }
}
