<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTProvidedDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_provided_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_id');
            $table->tinyInteger('role');
            $table->integer('user_id');
            $table->string('specification', 1000);
            $table->string('note', 1000);
            $table->date('return_date');
            $table->string('approval_manager', 1000);
            $table->integer('manager_id');
            $table->tinyInteger('status');
            $table->date('approved_at');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_provided_devices');
    }
}
