<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTDevicesStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_device_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_id');
            $table->integer('import_user_id')->nullable();
            $table->integer('import_quantity');
            $table->date('import_date')->nullable();
            $table->tinyInteger('status');
            $table->text('note')->nullable();
            $table->integer('stock_number')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_device_stocks');
    }
}
