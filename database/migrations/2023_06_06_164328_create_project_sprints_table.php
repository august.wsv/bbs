<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_sprints', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->comment('Mô tả');
            $table->integer('project_id')->comment('Dự án')->index();
            $table->timestamp('start_date')->comment('Bắt đầu');
            $table->timestamp('end_date')->comment('Kết thúc');
            $table->integer('status')->comment('0: Mới tạo; 1: Đang kích hoạt; 2: Đã kết thúc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_sprints');
    }
}
