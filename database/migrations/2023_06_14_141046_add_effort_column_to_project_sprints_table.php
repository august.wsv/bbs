<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEffortColumnToProjectSprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_sprints', function (Blueprint $table) {
            $table->float('effort')->nullable()->comment('Công số');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_sprints', function (Blueprint $table) {
            $table->dropColumn('effort');
        });
    }
}
