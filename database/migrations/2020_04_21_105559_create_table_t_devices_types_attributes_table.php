<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTDevicesTypesAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_device_types_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_type_id');
            $table->string('name', 255);
            $table->string('field_name', 255);
            $table->tinyInteger('is_required');
            $table->tinyInteger('is_mutiple');
            $table->string('note', 255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_device_types_attributes');
    }
}
