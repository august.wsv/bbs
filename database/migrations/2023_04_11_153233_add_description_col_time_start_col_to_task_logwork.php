<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionColTimeStartColToTaskLogwork extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_logwork', function (Blueprint $table) {
            $table->string('description', 255)->nullable()->after('time_text');
            $table->dateTime('time_start')->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_logwork', function () {
            //
        });
    }
}
