<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumCheckDayoffsMonthToRemainDayoffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remain_dayoffs', function (Blueprint $table) {
            $table->integer('add_day_offs_month')->default(DEFAULT_VALUE)
                ->comment('nếu value nhỏ hơn tháng hiện tại tức là user đó chưa được cộng ngày nghỉ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remain_dayoffs', function () {
            //
        });
    }
}
