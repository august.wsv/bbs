<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarOffsOfJapanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_offs_of_japan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date_name');
            $table->date('date_off_from');
            $table->date('date_off_to');
            $table->tinyInteger('is_repeat')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_offs_of_japan');
    }
}
