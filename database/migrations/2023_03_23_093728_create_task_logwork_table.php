<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskLogworkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_logwork', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id');
            $table->integer('time')->default(0)->comment('Thời gian log task tính theo giờ');
            $table->integer('creator_id');
            $table->string('time_text', 50);
            $table->timestamps();

            $table->index(['task_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_logwork');
    }
}
