<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaborCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labor_calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id');
            $table->text('content');
            $table->text('note')->nullable();
            $table->char('status', 1)->default(0);
            $table->dateTime('labor_date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labor_calendars');
    }
}
