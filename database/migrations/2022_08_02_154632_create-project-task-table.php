<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('project_id');
            $table->integer('user_id');
            $table->integer('creator_id');
            $table->integer('updator_id');

            $table->tinyInteger('status')->default(0)->comment("0: Open, 99: Closed, and more");
            $table->string('task_id')->nullable()->comment("Task/Issue/Bug Id");
            $table->date('deadline')->nullable();
            $table->integer('progress')->default(0)->comment("Tiến độ công việc");

            $table->text('content')->nullable()->comment("Nội dung báo cáo");
            $table->text('issue')->nullable()->comment("Vấn đề gặp phải");
            $table->timestamps();

            $table->index(['project_id', 'user_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_tasks');
    }
}
