<?php

return [
    'no_info' => 'không có thông tin chấm công',
    'lately' => 'đi muộn',
    'lately_morning' => 'đi muộn buổi sáng',
    'lately_afternoon' => 'đi muộn buổi chiều',
    'early' => 'về sớm',
    'early_morning' => 'về sớm buổi sáng',
    'early_afternoon' => 'về sớm buổi chiều',
    'ot' => 'làm thêm giờ',
    'ot_weekend' => 'làm thêm giờ cuối tuần',
    'off' => 'nghỉ cả ngày',
    'off_morning' => 'nghỉ sáng',
    'off_afternoon' => 'nghỉ chiều',
    'late_over_haft_morning' => 'nghỉ sáng (đi muộn quá nửa buổi)',
    'early_over_haft_morning' => 'nghỉ sáng (về sớm quá nửa buổi)',
    'late_over_haft_afternoon' => 'nghỉ chiều (đi muộn quá nửa buổi)',
    'early_over_haft_afternoon' => 'nghỉ chiều (về sớm quá nửa buổi)',
];
