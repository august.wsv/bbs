<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }} - Authorization</title>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset_ver('bootstrap/css/bootstrap.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset_ver('css/passport-authorize.css') }}">
</head>
<body class="passport-authorize">
    <div class="container">
        <div class="row justify-content-center">
            <div class="card card-default">
                <div class="card-header">
                    Yêu cầu ủy quyền
                </div>
                <div class="card-body">
                    <!-- Introduction -->
                    <p><strong>{{ $client->name }}</strong> đang yêu cầu truy cập vào tài khoản của bạn.</p>
                    <div class="buttons">
                        <!-- Authorize Button -->
                        <form method="post" action="{{ route('passport.authorizations.approve') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="state" value="{{ $request->state }}">
                            <input type="hidden" name="client_id" value="{{ $client->id }}">
                            <button type="submit" class="btn btn-success btn-approve">Chấp nhận</button>
                        </form>

                        <!-- Cancel Button -->
                        <form method="post" action="{{ route('passport.authorizations.deny') }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <input type="hidden" name="state" value="{{ $request->state }}">
                            <input type="hidden" name="client_id" value="{{ $client->id }}">
                            <button class="btn btn-danger">Hủy</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
