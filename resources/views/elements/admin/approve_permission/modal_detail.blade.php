<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"">Nôi dung xin phép
            </h4>
        </div>
        <div class="modal-body" class="container">
            <div class="row">
                <p style="font-weight: bold;" class="col-sm-3 col-xs-6">Tên nhân viên: </p>
                <span id="name-worktimeex" class="col-sm-9 col-xs-6">{{ $workTimesExplanation->creator->name ?? '' }}</span>
            </div>
            <div class="row">
                <p style="font-weight: bold;" class="col-sm-3 col-xs-6">Ngày: </p>
                <span id="day-worktimeex" class="col-sm-9 col-xs-6">{{ $workTimesExplanation->work_day ?? '' }}</span>
            </div>
            <div class="row">
                <p style="font-weight: bold;" class="col-sm-3 col-xs-6">Hình thức: </p>
                <span id="type-worktimeex" class="col-sm-9 col-xs-6">
                    @if($workTimesExplanation->type == array_search('Đi muộn',WORK_TIME_TYPE))
                    Đi muộn
                    @elseif($workTimesExplanation->type == array_search('Về sớm',WORK_TIME_TYPE))
                    Về sớm
                    @endif
                </span>
            </div>
            <div class="row">
                <p style="font-weight: bold;" class="col-sm-12 col-xs-12">Lý do: </p>
                <p class="col-sm-12 col-xs-12"><span id="note-worktimeex">{{ $workTimesExplanation->note ?? '' }}</span></p>
            </div>
            <div class="row">
                <p style="font-weight: bold;" class="col-sm-3 col-xs-6">Trạng thái phê duyệt: </p>
                <span id="status-worktimeex" class="col-sm-9 col-xs-6">
                    @if($workTimesExplanation->status == array_search('Đã duyệt',OT_STATUS))
                    Đã duyệt
                    @elseif($workTimesExplanation->status == array_search('Chưa duyệt',OT_STATUS))
                    Chưa duyệt
                    @elseif($workTimesExplanation->status == array_search('Từ chối',OT_STATUS))
                    Từ chối
                    @endif
                </span>
            </div>
            <div class="row">
                <p style="font-weight: bold;" class="col-sm-3 col-xs-6">Người duyệt: </p>
                <span id="approver-worktimeex" class="col-sm-9 col-xs-6">{{ $workTimesExplanation->approver->name ?? '' }}</span>
            </div>
            <div class="row">
                <p style="font-weight: bold;" class="col-sm-3 col-xs-6">Ngày duyệt: </p>
                <span id="approverat-worktimeex" class="col-sm-9 col-xs-6">{{ $workTimesExplanation->approver_at ?? '' }}</span>
            </div>
            <div class="row">
                <p style="font-weight: bold;" class="col-sm-12 col-xs-12">Nội dung phản hồi: </p>
                <p class="col-sm-12 col-xs-12"><span id="reason-worktimeex">{{ $workTimesExplanation->reason_reject ?? '' }}</span></p>
            </div>
        </div>
        <div class="modal-footer" id="footer-approval">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>
    </div>
</div>