<?php
$dayOfWeek = now()->dayOfWeek;
$urlImage = $laborMeme ?? LIST_URL_LABOR_EVERY_DAY[$dayOfWeek % 5];
?>
<a href="{{route('labor_calendar_index')}}"}}>
    <div class="mb-4 wow fadeIn" id="labor">
        <div class="border border-light card text-center animated fadeInRight">
            <div class="card-body blue lighten-5">
                <div id="memeArea" class="text-center" title="Enjoy your memes @.@ !!!">
                    <img id="labor-image" alt="Lịch trực nhật" src="{{asset($urlImage)}}" class="image my-5"
                         style="max-width: 100%; max-height: 400px;margin-top: 0px !important;margin-bottom: auto !important;"/>
                </div>
                <p style="margin-top: 15px; font-size: 1.5rem; color: #37392f">Lịch trực nhật ngày</p>
                <p><b>{{$laborUserName}}</b></p>
            </div>
        </div>
    </div>
</a>
@push('extend-js')
    <script>
      $(function () {
        const upload_meme = (file) => {
          const formData = new FormData();
          formData.append('image', file);

          $.ajax('/change-meme-labor', {
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            error: function (e) {
              console.error("error", e);
            },
            success: function (res) {
              if (res.status) {
                $("#labor-image").attr('src', res.path);
              }
            }
          });
        };

        const handlePaste = (pasteEvent) => {
          // consider the first item (can be easily extended for multiple items)
          const item = pasteEvent.clipboardData.items[pasteEvent.clipboardData.items.length - 1];
          if (item.type.indexOf("image") === 0) {
            upload_meme(item.getAsFile());
          }
        };

        $("#memeArea").mouseover(() => {
          document.onpaste = handlePaste
        });

        $("#memeArea").mouseout(() => {
          document.removeEventListener('onpaste', handlePaste)
        });
      })
    </script>
@endpush
