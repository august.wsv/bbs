<div class="text-right">
    <span class="btn btn-success btn-table" style="margin-right: 0" id="exportOTGrid">Xuất danh sách làm thêm giờ</span>
    <span class="btn btn-info btn-table" style="margin-right: 0" id="exportExcelGrid">Xuất bảng chấm công</span>
    <span class="btn btn-danger btn-table" style="margin-right: 0"
          id="exportExcelLatelyGrid">Xuất danh sách đi muộn</span>
    <span class="btn btn-primary btn-table" id="exportExcel">Xuất file excel</span>
</div>
<div class="table-responsive list-records">
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th style="width: 30px">
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
            </th>
            <th style="width: 150px">Ngày
                {{__admin_sortable('work_day')}}
            </th>
            <th style="width: 150px">Nhân viên</th>
            <th style="width: 170px">Checkin</th>
            <th style="width: 170px">Checkout</th>
            <th style="width: 90px">Tính công</th>
            <th>Chú thích</th>
            <th>Giải trình</th>
            <th style="width: 100px">Chức năng</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($records as $record)
            <?php
            $editLink = route($resourceRoutesAlias . '.edit', $record->id);
            $dayLink = route($resourceRoutesAlias . '.index', paginate_links(['work_day' => $record->work_day]));
            $userLink = route($resourceRoutesAlias . '.index', paginate_links(['user_id' => $record->user_id]));

            $deleteLink = route($resourceRoutesAlias . '.destroy', $record->id);
            $formId = 'formDeleteModel_' . $record->id;
            $currentLink = url()->full();
            session(['links' => $currentLink]);
            ?>
            <tr>
                <td class="text-center"><input type="checkbox" name="ids[]" value="{{ $record->id }}"
                                               class="square-blue chkDelete"></td>
                <td class="text-right">
                    @php($day = date_format(date_create($record->work_day) , 'N') + 1)

                    <a href="{{ $dayLink }}">{{ $record->work_day }}
                        ({{ $day == 8 ? 'Chủ nhật' : ('Thứ ' . $day) }}) </a>
                </td>
                <td class="table-text">
                    <a href="{{ $userLink }}">{{ $record->user->name ?? '' }}</a>
                </td>
                <td class="input_time" class="text-right">
                    <span >{{ $record->start_at }}</span>
                    <input style="display: none" type="time" class="form-control pull-right time_at" autocomplete="off"
                               value="{{ $record->start_at }}"
                               id="start_at_{{$record->id}}"
                               id_user="{{ $record->id }}" >
                </td>
                <td class="input_time" class="text-right">
                    <span>{{ $record->end_at }}</span>
                    <input style="display: none"  type="time" class="form-control pull-right time_at " autocomplete="off"
                               value="{{ $record->end_at }}"
                               id="end_at_{{$record->id}}"
                               id_user="{{ $record->id }}" >
                </td>
                <td class="text-right cost">{{ $record->cost }}</td>
                <td class="note">
                    <?php
                    switch ($record->type) {
                        case 5:
                        case 4:
                            $typeClass = 'success';
                            break;
                        case 2:
                            $typeClass = 'warning';
                            break;
                        case 1:
                            $typeClass = 'danger';
                            break;
                        case -1:
                            $typeClass = 'disable';
                            break;
                        case -2:
                            $typeClass = 'info';
                            break;
                    }
                    ?>
                    @if(isset($typeClass))
                        <span class="label label-{{$typeClass}}">{{ $record->note }}</span>
                    @endif
                </td>
                <td>{{ $record->explanation($record->work_day)->note ?? '' }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ $editLink }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete"
                           data-form-id="{{ $formId }}"><i class="fa fa-trash-o"></i></a>
                    </div>

                    <!-- Delete Record Form -->
                    <form id="{{ $formId }}" action="{{ $deleteLink }}" method="POST"
                          style="display: none;" class="hidden form-inline">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@push('footer-scripts')
    <script>
        $(function () {
            $("#exportExcelGrid").click(function () {
                $("#searchForm").append('<input id="is_export" type="hidden" name="is_export" value="1" />');
                $("#searchForm").append('<input id="is_grid" type="hidden" name="is_grid" value="1" />');
                $("#searchForm").submit();
                $("#is_export").remove();
                $("#is_grid").remove();
            });
            $("#exportExcelLatelyGrid").click(function () {
                $("#searchForm").append('<input id="is_export" type="hidden" name="is_export" value="1" />');
                $("#searchForm").append('<input id="is_lately" type="hidden" name="is_lately" value="1" />');
                $("#searchForm").submit();
                $("#is_export").remove();
                $("#is_lately").remove();
            });
            $("#exportOTGrid").click(function () {
                $("#searchForm").append('<input id="is_export" type="hidden" name="is_export" value="1" />');
                $("#searchForm").append('<input id="is_ot" type="hidden" name="is_ot" value="1" />');
                $("#searchForm").submit();
                $("#is_export").remove();
                $("#is_ot").remove();
            });
        });

        $(".content").on("dblclick", ".input_time", function(){
            let timeInput = $(this);
            let firstInput = timeInput.children().first();
            let lastInput = timeInput.children().last();
            firstInput.toggle();
            lastInput.toggle();
            lastInput.focus();
            lastInput.on('blur', function () {
                let id = $(this).attr( "id_user" );
                let start_at = "#start_at_" + id;
                let end_at = "#end_at_" + id;
                $.ajax({
                    type: "get",
                    url: "{{route('admin::work_time.update_time')}}",
                    data: {
                        start_at:$(start_at).val(),
                        end_at :$(end_at).val(),
                        id : id
                    },
                    success: function (response) {
                        if(response.status){
                            let trTable = timeInput.parent();
                            trTable.children('.input_time').first().find('span').text(response.start_at);
                            trTable.children('.input_time').last().find('span').text(response.end_at);
                            trTable.children('.cost').text(response.cost);
                            trTable.children('.note').find('span').text(response.note);
                            if(!$('.alert-success').length){
                                $( ".alert-info" ).remove();
                                $('.content').prepend(
                                    `<div class="alert alert-success" role="alert">
                                            Cập nhật thành công.
                                    </div>`)
                            }
                        }
                        else{
                            if(!$('.alert-info').length){
                                $( ".alert-success" ).remove();
                                $('.content').prepend(
                                    `<div class="alert alert-info" role="alert">
                                            Cập nhật thất bại.
                                    </div>`)
                            }
                        }
                        firstInput.toggle();
                        lastInput.toggle();
                        lastInput.unbind();

                    }
                });
            });
        });

    </script>
@endpush