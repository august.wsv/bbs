<div class="input-group input-group-sm margin-r-5 pull-left" style="width: 600px;">
    <input type="text" name="search" class="mr-1 w-22 form-control" value="{{ $search }}"
           placeholder="Search...">
    {{ Form::select('jobtitle', ['' => 'Chức vụ'] + JOB_TITLES, request('jobtitle'), ['class'=>'mr-1 w-22 form-control jobtitle']) }}
    {{ Form::select('position', ['' => 'Chức danh'] + POSITIONS, request('position'), ['class'=>'mr-1 w-22 form-control position']) }}
    {{ Form::select('contract_type', ['' => 'Loại hợp đồng'] + CONTRACT_TYPES_NAME, request('contract_type'), ['class'=>'mr-1 w-22 form-control contract_type']) }}

    <div class="input-group-btn">
        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
    </div>
</div>
