<!-- Delete Confirm - Helper. Show a modal box -->
<div id="modalConfirmModelDelete" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="modalConfirmModelDeleteLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-red color-palette">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalConfirmModelDeleteLabel">Bạn có chắc chắn <i class="fa fa-question"></i>
                </h4>
            </div>
            <div class="modal-body">
                <p> Bạn có chắc chắn muốn xóa bản ghi này không<i class="fa fa-question"></i></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnModalConfirmModelDelete" data-form-id="">Xác nhận
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCancelModelDelete">
                    Hủy bỏ
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Delete Confirm - Helper. Show a modal box -->
<div id="modalConfirmMutipleDelete" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="modalConfirmMutipleDeleteLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-red color-palette">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalConfirmMutipleDeleteLabel">Bạn có chắc chắn<i class="fa fa-question"></i>
                </h4>
            </div>
            <div class="modal-body">
                <p> Bạn có chắc chắn muốn xóa <span id="itemCount"></span> bản ghi được chọn?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnModalConfirmMutipleDelete" data-form-id="">Xác nhận
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCancelMutipleDelete">
                    Hủy bỏ
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@if ($resourceRoutesAlias == 'admin::user_trash')
    <div id="modalConfirmModelRestore" class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="modalConfirmModelRestoreLabel">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="POST" action="{{route('admin::user_trash.restore')}}">
                @csrf
                @method('PATCH')
                <input type="hidden" name="user_ids[]">
                <div class="modal-header bg-light-blue color-palette">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalConfirmModelRestoreLabel">Bạn có chắc chắn <i class="fa fa-question"></i>
                    </h4>
                </div>
                <div class="modal-body">
                    <p> Bạn có chắc chắn muốn khôi phục tài khoản <a href="#" class="text-info name_user"></a> không<i class="fa fa-question"></i></p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info" id="btnModalConfirmModelRestore">Xác nhận
                    </button>
                    <button type="button" class="btn btn-default"
                        data-dismiss="modal" id="btnModalCancelModelRestore"
                        onclick="$(this).parents('form').find(`[name='user_ids[]']`).val('')">
                        Hủy bỏ
                    </button>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="modalConfirmMultipleRestore" class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="modalConfirmMutipleDeleteLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue color-palette">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Bạn có chắc chắn<i class="fa fa-question"></i>
                    </h4>
                </div>
                <div class="modal-body">
                    <p> Bạn có chắc chắn muốn khôi phục <span id="itemCountRestore"></span> bản ghi được chọn?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnModalConfirmMutipleRestore" data-form-id="">Xác nhận
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCancelMutipleRestore">
                        Hủy bỏ
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endif
<!-- Page scripts -->

{{-- Initiate Confirm Delete --}}
<script type="text/javascript">
    $(function () {
        var $mutipleDeleteForm = $('#formDeleteMutiple');
        var $modalConfirmMutipleDelete = $('#modalConfirmMutipleDelete');
        $(document).on('click', '.btnOpenerModalConfirmModelDelete', function (e) {
            e.preventDefault();
            var formId = $(this).attr('data-form-id');
            var btnConfirm = $('#modalConfirmModelDelete').find('#btnModalConfirmModelDelete');
            if (btnConfirm.length) {
                btnConfirm.attr('data-form-id', formId);
            }
            $('#modalConfirmModelDelete').modal('show');
        });
        // Modal Button Confirm Delete
        $(document).on('click', '#btnModalConfirmModelDelete', function (e) {
            e.preventDefault();
            var formId = $(this).attr('data-form-id');
            var form = $(document).find('#' + formId);
            if (form.length) {
                form.submit();
            }
            $('#modalConfirmModelDelete').modal('hide');
        });
        // Modal Button Cancel Delete
        $(document).on('click', '#modalConfirmModelDelete #btnModalCancelModelDelete', function (e) {
            e.preventDefault();
            var btnConfirm = $('#modalConfirmModelDelete').find('#btnModalConfirmModelDelete');
            if (btnConfirm.length) {
                btnConfirm.attr('data-form-id', "");
            }
            $('#modalConfirmModelDelete').modal('hide');
        });

        $(document).on('click', '#btnDeleteMutiple', function (e) {
            e.preventDefault();
            var $items = $(".chkDelete:checked");
            if ($items.length > 0) {
                $modalConfirmMutipleDelete.find('#itemCount').text($items.length);

                $idList = $mutipleDeleteForm.find('#id-list');
                $.each($items, function () {
                    $idList.append('<input name="ids[]" value="' + this.value + '" />');
                });
                $modalConfirmMutipleDelete.modal('show');
            }
        });
        // Modal Button Confirm Delete
        $(document).on('click', '#btnModalConfirmMutipleDelete', function (e) {
            e.preventDefault();

            $mutipleDeleteForm.submit();
            $modalConfirmMutipleDelete.modal('hide');
        });
        // Modal Button Cancel Delete
        $(document).on('click', '#modalConfirmMutipleDelete #btnModalCancelMutipleDelete', function (e) {
            e.preventDefault();
            $mutipleDeleteForm.find('#id-list').empty();
            $modalConfirmMutipleDelete.modal('hide');
        });
    });
</script>

<script>
    var $mutipleRestoreForm = $('#formRestoreMutiple');
    var $modalConfirmMutipleRestore = $('#modalConfirmMultipleRestore');
    $(document).on('click', '.btnOpenerModalConfirmModelRestore', function(e) {
        e.preventDefault();
        const $name = $(this).parents('tr').find('.name_user');
        
        $('#modalConfirmModelRestore').find('.name_user').text($name.text()).attr('href', $name.find('a').attr('href'));
        $('#modalConfirmModelRestore').find('[name="user_ids[]"]').val($(this).data('id'));
        $('#modalConfirmModelRestore').modal('show');
    });
    $(document).on('click', '#btnRestoreMutiple', function (e) {
        e.preventDefault();
        var $items = $(".chkDelete:checked");
        if ($items.length > 0) {
            $modalConfirmMutipleRestore.find('#itemCountRestore').text($items.length);

            $idList = $mutipleRestoreForm.find('#id-restore-list');
            $.each($items, function () {
                $idList.append('<input name="user_ids[]" value="' + this.value + '" />');
            });
            $modalConfirmMutipleRestore.modal('show');
        }
    });

    // Modal Button Confirm Restore
    $(document).on('click', '#btnModalConfirmMutipleRestore', function (e) {
        e.preventDefault();

        $mutipleRestoreForm.submit();
        $modalConfirmMutipleRestore.modal('hide');
    });
    // Modal Button Cancel Restore
    $(document).on('click', '#modalConfirmMutipleRestore #btnModalCancelMutipleRestore', function (e) {
        e.preventDefault();
        $mutipleRestoreForm.find('#id-restore-list').empty();
        $modalConfirmMutipleRestore.modal('hide');
    });
</script>
<!-- End Delete Confirm - Helper. Show a modal box -->

<script type="text/javascript">
    $(function () {
        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
            var clicks = $(this).data('clicks');
            if (clicks) {
                //Uncheck all checkboxes
                $(".list-records input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
            } else {
                //Check all checkboxes
                $(".list-records input[type='checkbox']").iCheck("check");
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });

        // Search
        $('ul.custom-data-changer li a').on('click', function (e) {
            e.preventDefault();
            var showValue = $(this).attr('data-value');
            var dataChanger = $(this).parent('li').parent('ul.custom-data-changer');
            if (dataChanger.length) {
                var inputSelector = dataChanger.attr('data-change');
                if (inputSelector == '#search-show-input') {
                    showValue = (!isNaN(showValue) && showValue > 0 && showValue <= 100) ? showValue : 10;
                } else if (inputSelector == '#search-sort-input') {
                    showValue = (showValue == 'asc' || showValue == 'desc') ? showValue : '';
                } else if (inputSelector == '#search-sortby-input') {
                    @if (count($sortByParams)) <?php $strTmp = ''; ?>
                            @foreach ($sortByParams as $sortByParam)
                    <?php $strTmp .= (empty($strTmp)) ? "showValue == '$sortByParam'" : " || showValue == '$sortByParam'"; ?>
                            @endforeach
                        showValue = ({!! $strTmp !!}) ? showValue : '';
                    @endif
                }
                var inputElem = $(document).find(inputSelector);
                if (inputElem.length) {
                    inputElem.val('');
                    inputElem.val(showValue);
                }
            }
        });
    });
</script>
