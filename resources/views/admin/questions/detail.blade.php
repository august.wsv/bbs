{{-- Extends Layout --}}
@extends('layouts.admin.master')

<?php
$quizId = request()->query->all()['id'];
$_pageTitle = (isset($addVarsForView['_pageTitle']) && !empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : ($resourceTitle));
$_pageSubtitle = (isset($addVarsForView['_pageSubtitle']) && !empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : "Chi Tiết  " . str_singular($_pageTitle));
$_formFiles = isset($addVarsForView['formFiles']) ? $addVarsForView['formFiles'] : false;
$_listLink = route($resourceRoutesAlias . '.index', ['id' => $quizId]);
$_createLink = route($resourceRoutesAlias . '.create', ['id' => $quizId]);
$_updateLink = route($resourceRoutesAlias . '.edit', [$record->id, 'id' => $quizId]);
$_printLink = false;
?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('detail_question', $quizId, $record->id) !!}
@endsection

{{-- Page Title --}}
@section('page-title', $_pageTitle)

{{-- Page Subtitle --}}
@section('page-subtitle', $_pageSubtitle)

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12">

            <!-- Edit Form -->
            <div class="box box-info" id="wrap-edit-box">

                <form class="form" role="form" method="POST"
                      action="{{ $_updateLink }}" {{ $_formFiles === true ? 'enctype="multipart/form-data"' : ''}}>
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $_pageTitle }}</h3>

                        <div class="box-tools">
                            {{--<a href="#" class="btn btn-sm btn-default margin-r-5 margin-l-5" onclick="history.go(-1)">--}}
                            {{--<i class="fa fa-caret-left"></i> <span>Trở về</span>--}}
                            {{--</a>--}}
                            <a href="{{ $_listLink }}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                                <i class="fa fa-search"></i> <span>Danh sách</span>
                            </a>
                            <a href="{{ $_createLink }}" class="btn btn-sm btn-success margin-r-5 margin-l-5">
                                <i class="fa fa-plus"></i> <span>Thêm mới</span>
                            </a>
                            @if ($_printLink)
                                <a href="{{ $_printLink }}" target="_blank"
                                   class="btn btn-sm btn-default margin-r-5 margin-l-5">
                                    <i class="fa fa-print"></i> <span>Print</span>
                                </a>
                            @endif
                            <a href="{{ $_updateLink }}" class="btn btn-sm btn-info margin-r-5 margin-l-5">
                                <i class="fa fa-plus"></i> <span>Sửa thông tin</span>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <div class="row" >
                            <div class="col-md-6 col-md-offset-3">
                                <div class="margin-t-15"><b>Câu hỏi: </b>{{ $record->question }}</div>
                                @if($record->type == 0)
                                    <div class="answer_sample">
                                        <?php $id = 0 ?>
                                        @foreach($answers as $value)
                                            @if($value->is_correct == 1)
                                                <div class="form-check margin-t-5 padding-10-5 correct-answer-quiz">
                                                    <input class="form-check-input" type="radio" id="flexRadioDisabled{{$id}}" value="" style="padding-left:5px;" checked disabled>
                                                    <label class="form-check-label label-answer" for="exampleRadios">{{$value->answer}}</label>
                                                </div>
                                            @else 
                                                <div class="form-check margin-t-5 padding-10-5" style="border-radius: 4px;">
                                                    <input class="form-check-input" type="radio" id="flexRadioDisabled{{$id}}" value="" style="padding-left:5px;" disabled>
                                                    <label class="form-check-label label-answer" for="exampleRadios">{{$value->answer}}</label>
                                                </div>
                                            @endif    
                                        @endforeach
                                    </div>
                                @else
                                    <div class="md-form">
                                        <input type="text" id="form1" class="form-control" name="input-text" style="width:60% ;background: transparent;border: none;border-bottom: 1px solid #000000;" placeholder="Câu trả lời của bạn..." disable>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /End Edit Form -->
        </div>
    </div>
    <!-- /.row -->
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')
    @include('admin._resources._list-footer-extras', ['sortByParams' => []])
@endsection