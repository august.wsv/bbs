<!-- @extends('layouts.admin.master') -->

<?php
$quizId = request()->route('id');
$_pageTitle = (isset($addVarsForView['_pageTitle']) && !empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : ($resourceTitle));
$_pageSubtitle = (isset($addVarsForView['_pageSubtitle']) && !empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : 'Danh sách');
$_listLink = route($resourceRoutesAlias . '.index', ['id' => $quizId]);
$_createLink = route($resourceRoutesAlias . '.create', ['id' => $quizId]);
$_mutipleDeleteLink = route($resourceRoutesAlias . '.deletes');
$tableCounter = 0;
$total = 0;
if (count($records) > 0) {
    if ($records[0]->total) {
        foreach ($records as $record) {
            $total += $record->total;
        }
    } else {
        $total = $records->total();
        $tableCounter = ($records->currentPage() - 1) * $records->perPage();
        $tableCounter = $tableCounter > 0 ? $tableCounter : 0;
    }
}
?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::questions.index', $quizId) !!}
@endsection

{{-- Page Title --}}
@section('page-title', $_pageTitle)

{{-- Page Subtitle --}}
@section('page-subtitle', $_pageSubtitle)

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
<!-- Default box -->
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $_pageSubtitle }}</h3>
        <!-- Search -->
        <div class="box-tools pull-right">
            <form id="searchForm" class="form" role="form" method="GET" action="{{ $_listLink }}">
                @if( isset($resourceSearchExtend))
                @include($resourceSearchExtend, ['search' => $search, '$createLink' => $_createLink])
                @else
                <div class="input-group input-group-sm margin-r-5 pull-left" style="width: 200px;">
                    <input type="text" name="search" class="form-control" value="{{ $search }}" placeholder="Search...">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <a href="{{ $_createLink }}" class="btn btn-sm btn-primary pull-right toggle-create">
                    <i class="fa fa-plus"></i> <span>Thêm mới</span>
                </a>
                @endif
            </form>

        </div>
        <!-- END Search -->

    </div>

    <div class="box-body no-padding">
        @if (count($records) > 0)
        <div class="padding-5">
            <div class="row">
                <div class="col-sm-6">
                    <a href="#" class="btn btn-sm btn-danger" id="btnDeleteMutiple">

                        <i class="fa fa-close"></i> <span>Xóa bản ghi được chọn</span>

                    </a>
                    <form style="display: none" method="post" action="{{$_mutipleDeleteLink}}" id="formDeleteMutiple">
                        @csrf
                        <div id="id-list">

                        </div>
                    </form>
                </div>
                <div class="col-sm-6 text-right">

                    <span class="text-green padding-l-5">Tất cả: {{ $total }} bản ghi.</span>&nbsp;

                </div>
            </div>
        </div>

        <div class="table-responsive list-records">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th style="width: 40px;">
                            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                            </button>
                        </th>
                        <th class="text-center" style="width: 300px">Loại</th>
                        <th class="text-center">Câu hỏi</th>

                        <th class="text-center" style="width: 120px;">chức năng</th>
                    </tr>
                </thead>
                <tbody id="tablecontents">
                    @php
                    $i = 1;
                    @endphp
                    @foreach ($records as $record)
                    <?php
                    $tableCounter++;
                    $editLink = route($resourceRoutesAlias . '.edit', [$record->id, 'id' => $quizId]);
                    $showLink = route($resourceRoutesAlias . '.show', [$record->id, 'id' => $quizId]);
                    $deleteLink = route($resourceRoutesAlias . '.destroy', $record->id);
                    $formId = 'formDeleteModel_' . $record->id;
                    ?>
                    <tr class="row-record" data-id="{{ $record->id }}">
                        <td><input type="checkbox" name="ids[]" value="{{ $record->id }}" class="square-blue chkDelete"></td>

                        <td>
                            @if ( $record->type == array_search('Chọn đáp án đúng', QUESTION_TYPES))
                                Chọn đáp án đúng
                            @else
                                Tự luận
                            @endif
                        </td>
                        <td>{{ $record->question }}</td>

                        <!-- we will also add show, edit, and delete buttons -->
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{ $showLink }}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                <a href="{{ $editLink }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                <a href="#" class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete" data-form-id="{{ $formId }}"><i class="fa fa-trash-o"></i></a>
                            </div>

                            <!-- Delete Record Form -->
                            <form id="{{ $formId }}" action="{{ $deleteLink }}" method="POST" style="display: none;" class="hidden form-inline">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        @else

        <p class="margin-l-5 lead text-green">Không có dữ liệu.</p>

        @endif
    </div>
    <!-- /.box-body -->
    @if (count($records) > 0)
    @include('common.paginate', ['records' => $records])
    @endif

</div>
<!-- /.box -->


@endsection
@section('footer-extras')
    @include('admin._resources._list-footer-extras', ['sortByParams' => []])
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')

@endsection
@push('footer-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tablecontents").sortable({
            items: "tr",
            cursor: 'move',
            opacity: 0.6,
            update: function() {
                sendOrderToServer();
            }
        });

        function sendOrderToServer() {
            var order = [];
            $('tr.row-record').each(function(index,element) {
                order.push({
                    id: $(this).attr('data-id'),
                    position: index+1
                });
            });
            $.ajax({
                type: "POST", 
                dataType: "json", 
                url: '../../sortable',
                data: {
                    'order': order,
                },
                success: function(response) {
                    if (response.status == "success") {
                        console.log(response);
                    } else {
                        console.log(response);
                    }
                }
            });
        }
    })
</script>
@endpush