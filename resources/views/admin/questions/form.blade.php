{{-- Extends Layout --}}
@extends('layouts.admin.master')

<?php
$quizId = request()->query->all()['id'];
$_pageTitle = (isset($addVarsForView['_pageTitle']) && !empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : ($resourceTitle));
$_pageSubtitle = (isset($addVarsForView['_pageSubtitle']) && !empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : "Thêm " . str_singular($_pageTitle));
$_formFiles = isset($addVarsForView['formFiles']) ? $addVarsForView['formFiles'] : false;
$_listLink = route($resourceRoutesAlias . '.index', ['id' => $quizId]);
$_createLink = route($resourceRoutesAlias . '.create', ['id' => $quizId]);
$_storeLink = route($resourceRoutesAlias . '.store');

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render($resourceRoutesAlias.'.create') !!}
@endsection

{{-- Page Title --}}
@section('page-title', $_pageTitle)

{{-- Page Subtitle --}}
@section('page-subtitle', $_pageSubtitle)

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12">

            <!-- Edit Form -->
            <div class="box box-info" id="wrap-edit-box">

                <form class="form" role="form" method="POST" id="question_form" action="{{ $_storeLink }}"
                      enctype="multipart/form-data" {!! $_formFiles === true ? 'enctype="multipart/form-data"' : '' !!}>
                    {{ csrf_field() }}

                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm mới</h3>

                        <div class="box-tools">
                            <a href="{{ $_listLink }}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                                <i class="fa fa-search"></i> <span>Danh sách</span>
                            </a>
                            <a href="{{ $_createLink }}" class="btn btn-sm btn-success margin-r-5 margin-l-5">
                                <i class="fa fa-plus"></i> <span>Thêm mới</span>
                            </a>
                            <button class="btn btn-sm btn-info margin-r-5 margin-l-5 btn-save">
                                <i class="fa fa-save"></i> <span>Lưu</span>
                            </button>
                            @yield('more-buttons')
                        </div>
                    </div>
					<div class="container">
						<div class="header">
							<ul>
								<li class="active form_1_progessbar">
									<div>
										<p>1</p>
									</div>
								</li>
								<li class="form_2_progessbar">
									<div>
										<p>2</p>
									</div>
								</li>
								<li class="form_3_progessbar">
									<div>
										<p>3</p>
									</div>
								</li>
							</ul>
						</div>
						<div class="form_wrap">
							<div class="form_1 data_info">
								<p>Chọn loại câu hỏi</p>
									<div class="form_container" >
										<div class="input_wrap" style="text-align: center;">
												<label class="radio-inline">
													<input id="multiple-choice" type="radio" name="type" value="0" class="required" title="*" checked>
														Chọn đáp án đúng
												</label>
												<label class="radio-inline" style="margin-left: 80px;">
													<input id="essay" type="radio" name="type" value="1" class="required" title="*">
														Tự luận
												</label>
										</div>
									</div>
							</div>
							<div class="form_2 data_info" style="display: none;">
								<p>Thêm câu hỏi dạng chọn đáp án đúng</p>
									<div class="form_container add_question">
										<div class="input_wrap">
											<div class="form-group margin-b-5 margin-t-5{{ $errors->has('question') ? ' has-error' : '' }}">
												<label for="question">Câu hỏi *</label>
												<textarea name="question" rows="5"
														class="form-control question">{{ old('question', $record->question) }}</textarea>
											</div>
										</div>
										<div class="input_wrap add_answer">
											<label for="answer">Câu trả lời *</label>
											<div style="margin-left: 10px;">
												<div class="form-group margin-b-5 margin-t-5{{ $errors->has('answers.*') ? ' has-error' : '' }} group-answer">
													<div class="form-check" style="margin-top: 10px;">
														<input class="form-check-input correct-answer" type="radio" name="is_correct" value="0" id="radio1">
														<input  class="form-control answer" type="text" name="answers[]" id="answer1">
													</div>
													<div class="form-check " style="margin-top: 10px;">
														<input class="form-check-input correct-answer" type="radio" name="is_correct" value="1" id="radio2">
														<input  class="form-control answer" type="text" name="answers[]" id="answer2">
													</div>
													<div class="form-check" style="margin-top: 10px;">
														<input class="form-check-input correct-answer" type="radio" name="is_correct" value="2" id="radio3">
														<input  class="form-control answer" type="text" name="answers[]" id="answer3">
														<a href="#" class="btn btn-danger btn-sm btn-delete" data-id="2" style="margin-left: 10px;"><i class="fa fa-trash-o"></i></a>
													</div>
													<div class="form-check" style="margin-top: 10px;">
														<input class="form-check-input correct-answer" type="radio" name="is_correct" value="3" id="radio4">
														<input  class="form-control answer" type="text" name="answers[]" id="answer4">
														<a href="#" class="btn btn-danger btn-sm btn-delete" data-id="3" style="margin-left: 10px;"><i class="fa fa-trash-o"></i></a>
													</div>
												</div>
											</div>
											<div style="margin-left: 10px;"><button type="button" class="btn btn-default btn-add-answer">Thêm câu trả lời</button></div>
										</div>
									</div>
							</div>
							<div class="form_3 data_info" style="display: none;">
								<p>Kết quả</p>
								<div class="form_container">
									<div class="input_wrap question_sample">
										<div class="question"><span style="font-weight: 700;">Câu hỏi: &nbsp</span></div>
									</div>
									<div class="input_wrap answers_sample">
									</div>
								</div>
							</div>
						</div>
						<input name="quiz_id" hidden value="{{request()->query->all()['id']}}"/>
						<div class="btns_wrap">
							<div class="common_btns form_1_btns">
								<button type="button" class="btn_next">Tiếp<span class="icon"><i class="fa fa-chevron-right"></i></span></button>
							</div>
							<div class="common_btns form_2_btns" style="display: none; width:730px">
								<button type="button" class="btn_back"><span class="icon"><i class="fa fa-chevron-left"></i></span>Trước</button>
								<button type="button" class="btn_next">Tiếp<span class="icon"><i class="fa fa-chevron-right"></i></span></button>
							</div>
							<div class="common_btns form_3_btns" style="display: none;">
								<button type="button" class="btn_back"><span class="icon"><i class="fa fa-chevron-left"></i></span>Trước</button>
								<button type="submit" class="btn_save" name="save_question" value="save-continue">Lưu và tiếp tục</button>
								<button type="submit" name="save_question" value="save">Lưu</button>
							</div>
						</div>
					</div>
				</form>
            </div>
            <!-- /.box -->
            <!-- /End Edit Form -->
        </div>
    </div>
    <!-- /.row -->
@endsection
{{-- Footer Extras to be Included --}}
@section('footer-extras')

@endsection

@push('footer-scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
@endpush