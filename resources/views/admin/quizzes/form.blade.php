<div class="row" >
    <div class="col-md-6 col-md-offset-3">
        <div class="row">
            <div class="col-md-10">
                <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Chủ đề *</label>
                    <input type="text" name="name" class="form-control"
                           value="{{ old('name', $record->name) }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group margin-b-5 margin-t-15{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description">Mô tả</label>
                    <textarea name="description" rows="5"
                            class="form-control">{{ old('description', $record->description) }}</textarea>
                    <!-- /.form-group -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group margin-b-5 margin-t-15{{ $errors->has('type') ? ' has-error' : '' }}">
                    <label for="type">Loại</label>
                    <select class="form-control" name="type">
                        <option value="0" {{ old('type', $record->type ?? 0) == 0 ? 'selected' : '' }}>Định kì</option>
                        <option value="1" {{ old('type', $record->type ?? 1) == 1 ? 'selected' : '' }}>Một lần</option>
                    </select>
                    @if ($errors->has('type'))
                        <span class="help-block">
                        <strong>{{ $errors->first('type') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group margin-b-5 margin-t-15{{ $errors->has('participants') ? ' has-error' : '' }}" >
                    <label> Chọn đối tượng *</label>
                    <select class="selectpicker form-control" value="" multiple data-live-search="true" style="z-index: 3 !important" name="participants[]" data-none-selected-text data-dropup-auto="false">
                    @foreach($groups as $group => $users)
                        <optgroup label="{{$group}}">
                            @foreach($users as $key => $value)
                                <option value="{{$key}}" {{ is_array(old('participants', $record->participants )) && in_array($key, old('participants', $record->participants )) ? 'selected' : '' }} >{{$value}}</optio>
                            @endforeach
                        </optgroup>
                    @endforeach
                    </select>
                    @if ($errors->has('participants'))
                        <span class="help-block">
                        <strong>{{ $errors->first('participants') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-7">
                <div class="form-group margin-b-5 margin-t-15">
                    <label for="status"> Kích hoạt </label>
                        <input type="hidden" name="status" id="status" value="0">
                        <input type="checkbox" class="square-blue" name="status" id="status" value="1" {{ old('status', $record->status ?? 1) == 1 ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group margin-b-5 margin-t-15{{ $errors->has('start_date') ? ' has-error' : '' }}">
                    <label for="start_date">Thời gian bắt đầu *</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right datepicker"
                            name="start_date" data-date-format='yyyy-mm-dd' readonly
                            value="{{ old('start_date', $record->start_date) }}" id="start_date" style="z-index: 1 !important">
                    </div>
                    @if ($errors->has('start_date'))
                        <span class="help-block"><strong>{{ $errors->first('start_date') }}</strong></span>
                    @endif
                </div>
                
            </div>
            <!-- <div class="col-md-5">
                <div class="form-group margin-b-5 margin-t-15{{ $errors->has('end_date') ? ' has-error' : '' }}">
                    <label for="end_date">Thời gian kết thúc *</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right datepicker"
                            name="end_date" data-date-format='yyyy-mm-dd' readonly
                            value="{{ old('end_date', $record->end_date) }}" id="end_date" style="z-index: 1 !important">
                    </div>
                    @if ($errors->has('end_date'))
                        <span class="help-block"><strong>{{ $errors->first('end_date') }}</strong></span>
                    @endif
                </div>
            </div>
            <a href="#" class="margin-t-40 btn btn-success btn-sm reset-end-date"><i class="fa fa-refresh"></i></a> -->
        </div>
    </div>
</div>
@push('footer-scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script>
    $(function () {
        myDatePicker($("#start_date, #end_date"));
        $('.selectpicker').selectpicker();
    })

</script>
@endpush