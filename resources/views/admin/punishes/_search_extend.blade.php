<?php
$rules = ['0' => 'Đi muộn'] + \App\Models\Rules::all()->pluck('name', 'id')->toArray();
?>

<div class="input-group input-group-sm margin-r-5 pull-left" style="width: 800px;">
    <input type="text" name="search" class="mr-1 w-18 form-control" value="{{ $search }}"
           placeholder="Search...">
    {{ Form::select('year', get_years(), request('year'), ['class'=>'mr-1 w-18 form-control']) }}
    {{ Form::select('month', get_months(), request('month'), ['class'=>'mr-1 w-18 form-control']) }}
    {{ Form::select('rule_id', ['' => 'Tên vi phạm'] + $rules, request('rule_id'), ['class'=>'mr-1 w-18 form-control']) }}
    {{ Form::select('is_confirmed', ['' => 'Trạng thái'] + PUNISH_CONFIRMED_NAME, request('is_confirmed'), ['class'=>'w-18 form-control']) }}
    <input type="hidden" name="user_id" value="{{request('user_id')}}">
{{--    <input type="hidden" name="rule_id" value="{{request('rule_id')}}">--}}

    <div class="input-group-btn">
        <button type="submit" class="btn btn-primary">Tìm kiếm</button>
    </div>
</div>
<a href="{{ $_createLink }}" class="btn btn-sm btn-primary pull-right">
    <i class="fa fa-plus"></i> <span>Thêm mới</span>
</a>
