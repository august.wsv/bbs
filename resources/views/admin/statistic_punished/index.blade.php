<?php
$teams = \App\Models\Team::select('id', 'name', 'color')
    ->withCount('members')
    ->get();
$admin = \App\Facades\AuthAdmin::user();
?>

@extends('layouts.admin.master')
@section('page-title', 'Thống kê tiền phạt')
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::admins') !!}
@endsection
@section('content')

    @if ($admin->can('admin'))

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thống kê tiền phạt {{ $startDate ?? '6 tháng gần nhất' }}
                                {{ $endDate ? ' đến ' . $endDate : '' }}</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-9">
                                        <form class="d-flex"
                                            class="d-flex " action="{{ route('admin::statistic_punished.index') }}"
                                            method="get">
                                            <label style="min-width: 60px" for="start_day">Từ ngày:</label><br>
                                            <input value="{{ $startDate ?? '' }}" style="max-width:200px" type="date"
                                                class="form-control margin-r-5 margin-l-5" name="start_day"
                                                placeholder="Ngày bắt đầu" id="start_day">

                                            <label style="min-width: 70px" for="end_day">Đến ngày:</label><br>
                                            <input value="{{ $endDate ?? '' }}" style="max-width:200px" type="date"
                                                class="form-control margin-r-5 margin-l-5" name="end_day"
                                                placeholder="Ngày kết thúc" id="end_day">
                                            <label for="group">Group: </label>
                                            <select name="group" class=" margin-r-5 margin-l-5 form-control">
                                                <option value="" selected>Tất cả</option>
                                                @foreach ($groups as $group)
                                                    <option @if (isset($groupId) && $group->id == $groupId) selected @endif
                                                        value="{{ $group->id }}">{{ $group->name }}</option>
                                                @endforeach
                                            </select>
                                            <button type="submit" class="btn btn-sm btn-info margin-r-5 margin-l-5">
                                                <i class="fa fa-save"></i> <span>Thống kê</span>
                                            </button>

                                        </form>

                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    <div class=" mg-top-10 chart-responsive ">
                                        <canvas id="pieChart2" class="w-60 h-60"></canvas>
                                    </div>

                                    <!-- ./chart-responsive -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-3 ">
                                    <a href="{{ route('admin::export-punishes', ['startDate' => $startDate ?? '', 'endDate' => $endDate ?? '', 'group' => $groupId ?? '']) }}"
                                        type="button" class="btn btn-sm btn-success margin-r-5 margin-l-5"><i
                                            class="fa fa-download"> Xuất dữ liệu</i>
                                    </a>
                                    <ul class="chart-legend clearfix">
                                        <li>
                                            <i class="fa fa-circle-o" style="color: red"></i> Phạt đi muộn:
                                            {{ number_format($totalLate) }} vnđ
                                        </li>
                                        @foreach ($rules as $key => $rule)
                                            @php
                                                $rule->color = COLORS[$key % count(COLORS)];
                                            @endphp
                                            <li @if ($punishTypes[$key] == 0) hidden @endif>
                                                <i class="fa fa-circle-o" style="color: {{ $rule->color }}"></i>
                                                {{ $rule->name }}: {{ number_format($punishTypes[$key]) }} vnđ
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- /.col -->

                            </div>

                            <div class="row mg-top-10">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr style="background-color:red;color:white">
                                                <th scope="col">#</th>
                                                <th scope="col">Top vi phạm</th>
                                                <th scope="col">Group</th>
                                                <th scope="col">Tiền phạt</th>
                                                <th scope="col">Chi tiết</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($punishUsers as $key => $punishUser)
                                                <tr>
                                                    <th scope="row">{{ $key + 1 }}</th>
                                                    <td>{{ $punishUser->name }}</td>
                                                    <td><a
                                                            href="{{ route('admin::statistic_punished.index', [
                                                                'startDate' => $startDate ?? '',
                                                                'endDate' => $endDate ?? '',
                                                                'group' => $groupId ?? $punishUser->user->getGroupIdAttribute(),
                                                            ]) }}">
                                                            {{ $punishUser->user->getGroupNameAttribute() }}
                                                        </a>
                                                    </td>
                                                    <td>{{ number_format($punishUser->total) }}</td>
                                                    <td class="text-center">
                                                        <a target="_blank"
                                                            href="{{ route('admin::punishes.index', [
                                                                'user_id' => $punishUser->user_id,
                                                                'year' => $endDate ? date('Y', strtotime($endDate)) : date('Y'),
                                                                'month' => $endDate ? date('m', strtotime($endDate)) : date('m'),
                                                            ]) }}"
                                                            class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="text-center">
                                        {!! $punishUsers->links() !!}
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <!-- /.footer -->
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@push('footer-scripts')
    <script>
        window.PieData = [
            @foreach ($teams as $team)
                {
                    value: '{{ $team->members_count }}',
                    color: '{{ $team->color }}',
                    highlight: '{{ $team->color }}',
                    label: '{{ $team->name }}'
                },
            @endforeach
        ];

        window.PieDataPunish = [{
                value: '{{ $totalLate }}',
                color: 'red',
                highlight: 'red',
                label: 'Phạt đi muộn'
            },
            @foreach ($rules as $key => $rule)
                {
                    value: '{{ $punishTypes[$key] }}',
                    color: '{{ $rule->color }}',
                    highlight: '{{ $rule->color }}',
                    label: '{{ $rule->name }}'
                },
            @endforeach

        ];

        $(function() {

            'use strict';

            var piePunishChartCanvas = $('#pieChart2').get(0).getContext('2d');
            var piePunishChart = new Chart(piePunishChartCanvas);

            var pieOptions = {
                // Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                // String - The colour of each segment stroke
                segmentStrokeColor: '#fff',
                // Number - The width of each segment stroke
                segmentStrokeWidth: 1,
                // Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                // Number - Amount of animation steps
                animationSteps: 100,
                // String - Animation easing effect
                animationEasing: 'easeOutBounce',
                // Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                // Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                // Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: false,
                // String - A legend template
                legendTemplate: '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                // String - A tooltip template
                tooltipTemplate: '<%=value %> <%=label%> users'
            };
            // Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            piePunishChart.Doughnut(PieDataPunish, pieOptions);
            $('.sparkbar').each(function() {
                var $this = $(this);
                $this.sparkline('html', {
                    type: 'bar',
                    height: $this.data('height') ? $this.data('height') : '30',
                    barColor: $this.data('color')
                });
            });

        });
    </script>
    <script src="{{ asset_ver('js/libs/chart.js') }}"></script>
@endpush
