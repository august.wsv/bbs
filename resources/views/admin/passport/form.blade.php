<div class="col-md-3"></div>
<div class="col-md-9">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Tên *</label>
                <input type="text" id="name" class="form-control" name="name" placeholder="Tên"
                       value="{!! old('name', $record->name) !!} " required>

                @if ($errors->has('name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group margin-b-5 margin-t-5{{ $errors->has('redirect') ? ' has-error' : '' }}">
                <label for="redirect">Redirect *</label>
                <input type="text" id="redirect" class="form-control" name="redirect" placeholder="Redriect"
                    value="{!! old('redirect', $record->redirect) !!} " required>

                @if ($errors->has('redirect'))
                    <span class="help-block">
                    <strong>{{ $errors->first('redirect') }}</strong>
                </span>
                @endif
            </div>
        </div>
    </div>
</div>

