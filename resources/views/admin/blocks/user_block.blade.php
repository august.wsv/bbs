{{-- Extends Layout --}}
@extends('layouts.admin.master')

<?php
$_pageTitle = isset($addVarsForView['_pageTitle']) && !empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : ucwords($resourceTitle);
$_pageSubtitle = isset($addVarsForView['_pageSubtitle']) && !empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : 'Quản lý thành viên  ' . str_singular($_pageTitle);
$_formFiles = isset($addVarsForView['formFiles']) ? $addVarsForView['formFiles'] : false;
$_listLink = route($resourceRoutesAlias . '.index');
$_createLink = route($resourceRoutesAlias . '.create');
$_updateLink = route($resourceRoutesAlias . '.edit', $record->id);
$_printLink = false;
?>

@section('breadcrumbs')
    {!! Breadcrumbs::render($resourceRoutesAlias . '.show', $record->id) !!}
@endsection

{{-- Page Title --}}
@section('page-title', $_pageTitle)

{{-- Page Subtitle --}}
@section('page-subtitle', $_pageSubtitle)

{{-- Header Extras to be Included --}}
@section('head-extras')
    <link rel="stylesheet" href="{{ asset_ver('vendor/Drop-Down-Combo-Tree/style.css') }}">
    <script src="{{ asset_ver('vendor/Drop-Down-Combo-Tree/comboTreePlugin.js') }}"></script>

@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-info" id="wrap-edit-box">

                <form class="form" role="form" method="POST" action="{{ asset_ver('admin/blocks/save-member') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $record->id }}">
                    <div class="box-header with-border">
                        <h3 class="box-title">Quản lý thành viên</h3>

                        <div class="box-tools">
                            <a href="{{ $_listLink }}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                                <i class="fa fa-search"></i> <span>Danh sách</span>
                            </a>
                            <a href="{{ $_createLink }}" class="btn btn-sm btn-success margin-r-5 margin-l-5">
                                <i class="fa fa-plus"></i> <span>Thêm mới</span>
                            </a>
                            <button class="btn btn-sm btn-info margin-r-5 margin-l-5">
                                <i class="fa fa-save"></i> <span>Lưu</span>
                            </button>
                            @yield('more-buttons')
                        </div>
                    </div>

                    <div id="wrap" class="container">
                        <div class="row">
                            <h3>Đăng ký chỗ ngồi</h3>
                            <input name="member_ids[]" multiple="multiple" type="text" id="justAnInputBox1"
                                placeholder="Select" autocomplete="off" />
                            <br>
                            <div class="text-center margin-bottom margin-t-5">
                                <a href="#" class="btn btn-sm btn-default margin-r-5 margin-l-5"
                                    onclick="history.go(-1)">
                                    <i class="fa fa-caret-left"></i> <span>Quay lại</span>
                                </a>
                                <button type="submit" class="btn btn-sm btn-info margin-r-5 margin-l-5">
                                    <i class="fa fa-save"></i> <span>Lưu</span>
                                </button>

                                <a href="{{ $_listLink }}" class="btn btn-sm btn-default margin-r-5 margin-l-5">
                                    <i class="fa fa-ban"></i> <span>Hủy</span>
                                </a>
                            </div>
                            <br>
                            <h3>Thành viên đã có trong khu vực</h3>
                            @include('admin.blocks.table_user_block')
                        </div>


                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')
    <script type="text/javascript">
        var SampleJSONData2 = {!!  $listUserInTeam !!};
        
        jQuery(document).ready(function($) {
            var comboTree3 = $('#justAnInputBox1').comboTree({
                source: SampleJSONData2,
                isMultiple: true,
                cascadeSelect: true,
                collapse: false
            });
            comboTree3.setSelection({!!  $listUserIdOfTeam !!}) 
        });
    </script>
@endsection
