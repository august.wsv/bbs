<div class="col-md-5">
    <div class="row">
        <div class="col-md-8">
            <br />
            <p>Tên khu vực: <b>{{ $record->name }}</b></p>
            <p>Ngày tạo: <b>{{ $record->created_at }}</b></p>
            <p>Số lượng thành viên: <b>{{ $record->members->count() }}</b></p>
            <p>Mô tả: <b>
                {!! nl2br($record->description) !!}
                </b></p>
        </div>
    </div>
</div>

<div class="col-md-8">
    <br />
    <br />
    <hr />

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group margin-b-5 margin-t-5">
                        <label>Danh sách thành viên</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group margin-b-5 margin-t-5">
                        <a href="{{ asset_ver('admin/blocks/manage-member/' . $record->id) }}"
                            class="btn btn-sm btn-primary pull-right">
                            <i class="fa fa-plus"></i> <span>Quản lý thành viên</span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.form-group -->
        </div>
    </div>
    @include('admin.blocks.table_user_block')

</div>
