<div class="table-responsive list-records">
    <table class="table table-hover table-bordered">
        <thead>
            <th style="width: 20px" class="text-center">STT</th>
            <th style="width: 200px">Tên thành viên</th>
            <th style="width: 120px">Tên group</th>
            <th style="width: 120px">Ngày sắp xếp</th>
        </thead>
        <tbody>
            @foreach ($record->members as $key=>$member)
                <tr>
                    <td class="text-center">{{ ++$key }}</td>
                    <td class="table-text">
                        {{ $member->name }}
                    </td>
                    <td class="table-text">
                        {{ $member->team()->name ?? '' }}
                    </td>
                    <td class="table-text">
                        {{ $member->userBlock->created_at }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
