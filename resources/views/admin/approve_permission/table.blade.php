@extends('layouts.admin.master')
{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render($resourceRoutesAlias) !!}
@endsection

{{-- Page Title --}}
@section('page-title', $_pageTitle)

{{-- Page Subtitle --}}
@section('page-subtitle', $_pageSubtitle)

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $_pageSubtitle }}</h3>
            <!-- Search -->
            <div class="box-tools pull-right">
                <form id="searchForm" class="form" role="form" method="GET" action="{{ $_listLink }}">
                    @if( isset($resourceSearchExtend))
                        @include($resourceSearchExtend, ['search' => $search, '$createLink' => $_createLink])
                    @else
                        <div class="input-group input-group-sm margin-r-5 pull-left" style="width: 200px;">
                            <input type="text" name="search" class="form-control" value="{{ $search }}"
                                   placeholder="Search...">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    @endif
                </form>

            </div>
            <!-- END Search -->
        </div>

        <div class="text-right">
            <button class="btn btn-primary btn-export-over-time" id="exportExcel">Xuất file excel</button>
        </div>
        <div class="table-responsive list-records table-over-time">
            <table class="table table-hover table-bordered">
                <thead>
                <th style="padding: 15px">STT</th>
                <th style="padding: 15px">Mã nhân viên</th>
                <th style="padding: 15px">Tên nhân viên</th>
                <th style="padding: 15px">Tên group</th>
                <th style="padding: 15px">Ngày</th>
                <th style="padding: 15px">Hình thức</th>
                <th style="padding: 15px">Lý do</th>
                <th style="padding: 15px">Ngày tạo</th>
                <th style="padding: 15px;">Trạng thái phê duyệt</th>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($records as $record)
                    @php
                        $attrArray = ['search' => $search, 'type' => request()->get('type'), 'year' => request()->get('year'), 'month' => request()->get('month')];
                        $dayLink = route($resourceRoutesAlias . '.index', ['work_day' => $record->work_day] + $attrArray);
                        $userLink = route($resourceRoutesAlias . '.index', ['user_id' => $record->user_id] + $attrArray);
                    @endphp
                    <tr>
                        <td class="table-text" style="padding: 15px">
                            {{ $i++ }}
                        </td>
                        <td class="table-text text-center" style="padding: 15px">
                            {{ $record->creator->staff_code ?? '' }}
                        </td>
                        <td class="table-text" style="padding: 15px"><a href="{{ $userLink }}">{{ $record->creator->name ?? '' }}</a></td>
                        <td class="table-text" style="padding: 15px">
                        @php
                            $team = $record->creator->team();
                            if($team):
                                $group = $team->group;
                                if($group):
                                    $groupLink = route($resourceRoutesAlias . '.index', ['group_name' => $group->name]  + $attrArray);
                        @endphp
                                    <a href="{{ $groupLink }}">{{ $group->name ?? '' }}</a>
                        @php
                                endif;
                            endif;
                            
                        @endphp
                        </td>
                        <td class="table-text" style="padding: 15px"><a href="{{ $dayLink }}">{{ $record->work_day }}</a></td>
                        <td class="table-text" style="padding: 15px">
                            @if($record->type == array_search('Đi muộn',WORK_TIME_TYPE))
                                Đi muộn
                            @elseif($record->type == array_search('Về sớm',WORK_TIME_TYPE))
                                Về sớm
                            @endif
                        </td>
                        <td class="table-text" style="padding: 15px">{!! $record->note !!} </td>
                        <td class="table-text" style="padding: 15px">{{ $record->created_at }} </td>
                        <td class="table-text text-center" style="padding: 15px">
                            @if($record['status'] == array_search('Đã duyệt',OT_STATUS))
                                <span data-toggle="tooltip"
                                      data-placement="right"
                                      title="{{$record->approver->name ?? ''}} - {{$record->approver_at ?? ''}}"
                                      class="label label-info show-detail"
                                      attr="{{ $record->id }}">
                                    Đã duyệt
                                </span>
                            @elseif($record['status'] == array_search('Chưa duyệt',OT_STATUS))
                                <span class="label label-warning show-detail" attr="{{ $record->id }}">Chưa duyệt</span>
                            @elseif($record['status'] == array_search('Từ chối',OT_STATUS))
                                <span class="label label-danger show-detail" attr="{{ $record->id }}">Từ chối</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @if (count($records) > 0)
            @include('common.paginate', ['records' => $records])
        @endif
    </div>
<div class="modal fade" id="detailEx" tabindex="-1" role="dialog" aria-labelledby="modalConfirmSubmitLabel"
     aria-hidden="true">
</div>
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')
    @include('admin._resources._list-footer-extras', ['sortByParams' => []])
@endsection

@push('footer-scripts')
    <script>
        $(document).ready(function () {
            $('.show-detail').on('click', function () {
                var id = $(this).attr('attr');
                $.ajax({
                    url: '{{ route("admin::approve_permission.modal_detail") }}' + '/' + id,
                    type: 'GET',
                    success: function(response) {
                        $('#detailEx').empty();
                        $('#detailEx').append(response);
                        $('#detailEx').modal('show')
                    }
                });
            });
        })
    </script>
@endpush
