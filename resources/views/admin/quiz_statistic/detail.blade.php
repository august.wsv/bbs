    
@extends('layouts.admin.master')

<?php

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('detail_quiz_statistic',$quizId) !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Quiz')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Thống kê chi tiết')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
    
<section class="content">
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Danh sách
                </h3>
            </div>
            <div class="box-body no-padding">
                <div class="padding-5">

                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif

                <div class="table-responsive list-records col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center" style="width:10%">Mã nhân viên</th>
                                <th class="text-center" style="width:50%">Tên nhân viên</th>
                                <th class="text-center">Số câu trả lời đúng</th>
                                <th class="text-center">Thời gian nộp bài</th>
                                <th class="text-center">Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($users as $user)
                            @if ($user['name'])
                                <tr>
                                    <td class="text-center">{{$user['staff_code']}}</td>
                                    <td>
                                        {{$user['name']}}
                                    </td>
                                    @if (isset($user['submited_at']))
                                        <td class="text-center">
                                            {{$amountCorrectAnswers[$user['user_id']]}}/{{$amountQuestion}}
                                        </td>
                                        <td class="text-center">
                                            {{$user['submited_at']}}
                                        </td>
                                    @else
                                        <td class="text-center">
                                            Chưa trả lời
                                        </td>
                                        <td class="text-center"></td>
                                    @endif
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('admin::quiz.answer_user', ['id' => $user['user_id'], 'quizId' => $quizId]) }}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        @endforeach   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</section>
<script>

</script>
@endsection
