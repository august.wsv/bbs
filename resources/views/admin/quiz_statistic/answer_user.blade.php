    
@extends('layouts.admin.master')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('answers_user', $quizId, $userId) !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Quiz')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Thống kê chi tiết')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
    
<section class="content">
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Danh sách
                </h3>
            </div>
            <div class="container" style="">
                <div class="box-body no-padding">
                    @php
                        $count = 1; 
                    @endphp
                    @foreach ($questions as $value)
                        <div class="wrap-question" style="margin-top: 10px;">
                            <div class="question_sample"><span style="font-weight: 700;">Câu {{$count}}:</span> {{$value->question}}</div>
                            @if ($value->type == 1)
                                <div class="margin-t-5">
                                    <div class="form-group padding-10-5">
                                        <textarea class="form-control textarea-answer" name="questions['+ value.id +']" rows="2" cols="48" style="max-width: 100%" readonly>{{ $value->answer }}</textarea>
                                    </div>
                                </div>
                            @else
                                <div class="answer_sample">
                                    <?php $id=0 ?>
                                    @foreach($value->answers as $answers)
                                        @if($value->answer == $answers->answer && $answers->is_correct == 1)
                                            <div class="form-check padding-10-5 margin-t-5 correct-answer-quiz">
                                                <input class="form-check-input" type="radio" name="question{{$value->id}}" id="flexRadioDisabled{{$id}}" value="" style="padding-left:5px;" checked disabled>
                                                <label class="form-check-label label-answer" for="exampleRadios">{{$answers->answer}}</label>
                                            </div> 
                                        @elseif($value->answer == $answers->answer && $answers->is_correct == 0)
                                            <div class="form-check padding-10-5 margin-t-5 wrong-answer-quiz">
                                                <input class="form-check-input" type="radio" name="question{{$value->id}}" id="flexRadioDisabled{{$id}}" value="" style="padding-left:5px;" checked disabled>
                                                <label class="form-check-label label-answer" for="exampleRadios">{{$answers->answer}}</label>
                                            </div> 
                                        @elseif($value->answer != $answers->answer && $answers->is_correct == 1)
                                            <div class="form-check padding-10-5 margin-t-5 correct-answer-quiz">
                                                <input class="form-check-input" type="radio" name="question{{$value->id}}" id="flexRadioDisabled{{$id}}" value="" style="padding-left:5px;" disabled>
                                                <label class="form-check-label label-answer" for="exampleRadios">{{$answers->answer}}</label>
                                            </div> 
                                        @elseif($value->answer != $answers->answer && $answers->is_correct == 0)
                                            <div class="form-check padding-10-5 margin-t-5" style="border-radius: 4px;">
                                                <input class="form-check-input" type="radio" name="question{{$value->id}}" id="flexRadioDisabled{{$id}}" value="" style="padding-left:5px;" disabled>
                                                <label class="form-check-label label-answer" for="exampleRadios">{{$answers->answer}}</label>
                                            </div> 
                                                
                                        @endif
                                        <?php $id++; ?>
                                    @endforeach

                                </div>
                            @endif
                            
                        </div>
                        <?php $count++; ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    
</section>
@endsection
