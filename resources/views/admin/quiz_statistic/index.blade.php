    
@extends('layouts.admin.master')

<?php

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('quiz_statistic') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Quiz')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Thống kê quiz')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
    
<section class="content">
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Danh sách
                </h3>
            </div>
            <div class="box-body no-padding">
                <div class="padding-5">

                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif

                <div class="table-responsive list-records col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center" style="width:5%">STT</th>
                                <th class="text-center" style="width:50%">Chủ đề</th>
                                <th class="text-center">Số lượng trả lời</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="text-center">Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $count = 1;
                        ?>
                        @foreach($quizzes as $quiz)
                            <tr>
                                <td class="text-center">{{$count++}}</td>
                                <td>{{$quiz->name}}</td>
                                <td class="text-center">
                                    @if (isset($sumUserAnswers[$quiz->id]))
                                        {{ $sumUserAnswers[$quiz->id] }} / {{ $quiz->participants }}
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if ( $quiz->status == array_search('Đã kích hoạt', STATUS_QUIZ))
                                        <span style="color:blue">Đã kích hoạt</span>
                                    @else
                                        <span style="color:red">Chưa kích hoạt</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{route('admin::quiz.get_user', ['id' => $quiz->id])}}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</section>
<script>

</script>
@endsection
