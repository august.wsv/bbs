

<button class="btn btn-primary btn-export-over-time" id="exportExcel">Xuất file excel</button>
<div class="table-responsive list-records table-over-time">
    <table class="table table-hover table-bordered">
        <thead>
        <th style="">#</th>
        <th style="">Group/Team</th>
        <th style="">Tên nhân viên</th>
        <th style="">Thời gian</th>
        <th style="">Hình thức OT</th>
        <th style="">Nội dung</th>
        <th style="">Người duyệt</th>
        <th style="">Trạng thái</th>
        <th style=""></th>
        </thead>
        <tbody>
        @foreach ($records as $idx => $record)
            <?php
            $undoLink = route($resourceRoutesAlias . '.undo', $record->id);
            $deleteLink = route($resourceRoutesAlias . '.destroy', $record->id);
            $formId = 'formDeleteModel_' . $record->id;
            ?>
            <tr>
                <td class="table-text text-center" style="">
                    {{ $idx + 1 }}
                </td>
                <td class="table-text" style="">{{ $record->creator->group_name ?? '' }}</td>
                <td class="table-text" style="">{{ $record->creator->name ?? '' }}</td>
                <td class="table-text" style="">{{ $record->work_day }} <br />{{ $record->description_time }}</td>
                <td class="table-text" style="">
                    <b>{{ $record->ot_type == array_search('Dự án', OT_TYPE) ? 'OT dự án' : 'Lý do cá nhân' }}</b>
                    <br/> 
                    {{ $record->project->name ?? '' }}</td>
                <td class="table-text" style=""> {{ $record->reason }}</td>
                <td class="table-text" style="">{{ $record->approver->name ?? '' }}</td>
                <td class="table-text text-center" style="">
                    @if($record['status'] == 0)
                        <span class="label label-warning">Chưa duyệt</span>
                    @elseif($record['status'] == 1)
                        <span class="label label-success">Đã duyệt</span>
                    @else
                        <span class="label label-danger">Từ chối</span>
                    @endif
                </td>
                <td class="text-right">
                    <div class="btn-group" style="min-width: 80px;">
                        @if( $record->status != OT_STATUS_NEW)
                        <a href="{{ $undoLink }}" class="btn btn-warning btn-sm" title="Hủy duyệt"><i class="fa fa-undo"></i></a>
                        @endif
                        <a href="#" class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete"
                            data-form-id="{{ $formId }}" title="Xóa"><i class="fa fa-trash-o"></i></a>
                    </div>

                    <!-- Delete Record Form -->
                    <form id="{{ $formId }}" action="{{ $deleteLink }}" method="POST"
                            style="display: none;" class="hidden form-inline">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@push('extend-css')
<style>
#btnDeleteMutiple{
    display: none;
}
#exportExcel {
    position: absolute;
    top: 30px;
}
</style>
@endpush