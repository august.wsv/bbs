<div class="col-lg-3">
    <ul class="chart-legend clearfix">
        <li class="margin"><img src="{{asset_ver('img/statistics/on_time.png')}}" alt="Đi làm đúng giờ"> Đi làm đúng giờ</li>
        <li class="margin"><img src="{{asset_ver('img/statistics/early.png')}}" alt="Đi làm muộn/sớm"> Đi làm muộn/sớm</li>
        <li class="margin"><img src="{{asset_ver('img/statistics/ot.png')}}" alt="Đi làm muộn/sớm"> OT</li>
        <li class="margin"><img src="{{asset_ver('img/statistics/late.png')}}" alt="Đi làm muộn + OT"> Đi làm muộn + OT</li>
        <li class="margin"><img src="{{asset_ver('img/statistics/leave.png')}}" alt="Xin nghỉ"> Xin nghỉ</li>
    </ul>
</div>
<style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    .doughnut{
        width: 500px !important;
    }
</style>
