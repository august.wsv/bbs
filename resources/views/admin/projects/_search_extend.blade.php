@php
    use App\Models\Project;
    use App\Models\Group;
    $groups = Group::select('id', 'name')->get();
    $categories = Project::select('category')
        ->whereNotNull('category')
        ->distinct('category')
        ->get()
        ->pluck('category');
@endphp
<div class="input-group input-group-sm margin-r-5 pull-left" style="width: 600px;">
    <input type="text" name="search" class="mr-1 w-22 form-control" value="{{ $search }}" placeholder="Search...">
    <select name="group_id" id="" class="mr-1 w-22 form-control">
        <option value="">Group quản lý</option>
        @foreach ($groups as $group)
            <option @if (request('group_id') == $group->id) selected @endif value="{{ $group->id }}">
                {{ $group->name }}
            </option>
        @endforeach
    </select>
    <select name="category" id="" class="mr-1 w-22 form-control">
        <option value="">Lĩnh vực</option>
        @foreach ($categories as $category)
            <option @if (request('category') == $category) selected @endif value="{{ $category }}">
                {{ $category }}
            </option>
        @endforeach
    </select>
    <select name="status" id="" class="mr-1 w-22 form-control">
        <option value="">Trạng thái</option>
        @foreach (STATUS_PROJECT as $key => $status)
            <option @if (request('status') != '' && request('status') == $key) selected @endif value="{{ $key }}">
                {{ $status }}
            </option>
        @endforeach
    </select>
    <div class="input-group-btn">
        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
    </div>
</div>
<a href="{{ $_createLink }}" class="btn btn-sm btn-primary pull-right">
    <i class="fa fa-plus"></i> <span>Thêm mới</span>
</a>
