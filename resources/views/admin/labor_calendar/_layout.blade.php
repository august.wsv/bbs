{{-- Extends Layout --}}
@extends('layouts.admin.master')

<?php

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::labor_calendar') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Lịch trực nhật')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Danh sách trực nhật trong tháng')

{{-- Header Extras to be Included --}}
@section('head-extras')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.css">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/interaction/main.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.min.js"></script>

@endsection
@push('extend-css')
    <style>
        .fc-content {
            margin: 5px;
            text-align: center;
        }

        .text-center th {
            text-align: center !important;
        }

        #labor_calendar_table thead {
            display: table;
            width: 100%;
            table-layout: fixed;
        }

        #labor_calendar_table tbody {
            display: block;
            max-height: 300px;
            overflow-y: overlay;
        }

        .margin-right-10 {
            margin-right: 10px;
        }
        .margin-bottom-20 {
            margin-bottom: 20px;
        }

        @media only screen and (min-width: 1024px) and (max-width: 1366px){
            .modal-content-user {
                width: 800px;
            }
        }
        @media only screen and (min-width: 1367px){
            .modal-content-user {
                width: 1020px;
            }
        }
        @media only screen and (max-width: 768px){
            #labor_calendar_table thead {
                width: auto;
            }
            .fc-view-container {
                overflow: scroll;
            }
            .fc-view, .fc-view>table {
                width: 800px;
            }
            .fc-dayGrid-view .fc-body .fc-row {
                min-height: 8em;
            }
            .box-body .fc {
                margin-top: 20px;
            }
            #pair {
                margin-right: -6px;
            }
            .fc-content {
                margin: 2px;
            }
        }

    </style>
@endpush

@section('content')
    <!-- Edit Form -->
    <div class="box box-info" id="wrap-edit-box">

        <div class="box-header with-border">
            <h3 class="box-title">Danh sách</h3>
            <div class="box-tools">
                <form id="labor-calendar" class="form" role="form" style="display: inline-block"
                      action="{{ route('admin::labor_calendar.index') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('admin.labor_calendar._search_extend')
                    <a id="pair" class="btn btn-sm btn-primary pull-right" @if($searchYear <= now()->year && $searchMonth <= now()->month) style="opacity: 0.5" @else onclick="submit()" @endif>
                        <i class="fa fa-plus"></i> <span>Ghép cặp</span>
                    </a>
                    <input type="hidden" name="content">
                </form>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            @include($resourceAlias.'.index')
        </div>
        <!-- /.box-body -->

        <div class="box-footer clearfix">
            <!-- Edit Button -->
        </div>
        <!-- /.box-footer -->
        <form id="update-work-content" action="{{route('admin::labor.update.work.content')}}" method="post">
            @csrf
            <div class="margin-bottom-20">
                <label class="mt-3" for="content" style="margin-left: 10px;">Nội dung công việc</label>
                <button type="button" class="btn btn-sm btn-primary pull-right margin-right-10" onclick="updateWorkContent()"> Cập nhật công việc</button>
            </div>
            <div class="md-form mt-1 mb-0">
                <i class="fas fa-pencil-alt prefix grey-text"></i>
                <textarea type="text" id="content" name="content"
                          class="md-textarea form-control">{{$contentWork}}</textarea>
            </div>
            <input type="hidden" name="month" value="{{$searchMonth}}">
            <input type="hidden" name="year" value="{{$searchYear}}">
        </form>
    </div>
    <form action="{{route('admin::labor.update.pair.user')}}" method="post">
        @csrf
        <div id="MyPopup" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content modal-content-user">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            Chi tiết trực nhật ngày: <span id="date-labor"></span>
                        </h4>
                    </div>
                    <div class="modal-body" style="overflow: auto;">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="leader_id">Tên người trực nhật</label>
                                        <div style="display: block">
                                        <select id="user_1" class="form-control user_1" name="userID_1"
                                                onchange="focusInfoUser(this)">
                                            <optgroup label="Chưa đăng kí trực nhật">
                                                @foreach($listLaborUserInMonthUnChose as $unChose)
                                                    <option value="{{$unChose->id}}">{{$unChose->name}}</option>
                                                @endforeach
                                            </optgroup>
                                            <optgroup label="Đã có lịch trực nhật trong tháng">
                                                @foreach($listLaborUserInMonthChose as $chose)
                                                    <option value="{{$chose->id}}">{{$chose->name}}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="leader_id">Tên người trực nhật</label>
                                        <select id="user_2" class="form-control user_2" name="userID_2"
                                                onchange="focusInfoUser(this)">
                                            <optgroup label="Chưa đăng kí trực nhật">
                                                @foreach($listLaborUserInMonthUnChose as $unChose)
                                                    <option value="{{$unChose->id}}">{{$unChose->name}}</option>
                                                @endforeach
                                            </optgroup>
                                            <optgroup label="Đã có lịch trực nhật trong tháng">
                                                @foreach($listLaborUserInMonthChose as $chose)
                                                    <option value="{{$chose->id}}">{{$chose->name}}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                        <input type="hidden" name="labor_calendar_id">
                                        <input type="hidden" name="month_search" value="{{$searchMonth}}">
                                        <input type="hidden" name="year_search" value="{{$searchYear}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px">
                                <div class="col-md-12">
                                    <div class="table-responsive list-records">
                                        <div class="container-table">
                                            <table id="labor_calendar_table"
                                                   class="table table-hover table-bordered dataTable">
                                                <thead>
                                                <tr class="text-header-center">
                                                    <th rowspan="2">
                                                        Họ và tên
                                                    </th>
                                                    <th colspan="12" style="text-align: center">
                                                        Lịch trực nhật trong năm {{$searchYear}}
                                                    </th>
                                                </tr>
                                                <tr style="text-align: center" class="text-header-center text-center">
                                                    <th>Tháng 1</th>
                                                    <th>Tháng 2</th>
                                                    <th>Tháng 3</th>
                                                    <th>Tháng 4</th>
                                                    <th>Tháng 5</th>
                                                    <th>Tháng 6</th>
                                                    <th>Tháng 7</th>
                                                    <th>Tháng 8</th>
                                                    <th>Tháng 9</th>
                                                    <th>Tháng 10</th>
                                                    <th>Tháng 11</th>
                                                    <th>Tháng 12</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listLaborCalendarOfAllEmployee as $employee)
                                                    <tr id="position_{{$employee['id']}}"
                                                        style="display: table; width: 100%;  table-layout: fixed;">
                                                        <td>{{$employee['name']}}</td>
                                                        @for($i = 1; $i <=12; $i++)
                                                            <th style="text-align: center">@if($employee['month_'.$i])
                                                                    <i class='fa fa-check'></i> @endif</th>
                                                        @endfor
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Update
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form action="{{route('admin::labor.update.pair.status')}}" method="post">
        @csrf
        <div id="labor_status" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            Trạng thái trực nhật ngày: <span id="status-labor"></span>
                        </h4>
                    </div>
                    <div class="modal-body" style="overflow: auto">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="leader_id">Trạng thái</label>
                                        <select id="status" class="form-control" name="status">
                                            <option value="0" selected="">Chưa kiểm tra</option>
                                            <option value="1">Đã hoàn thành</option>
                                            <option value="2">Chưa hoàn thành</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6"></div>
                            </div>

                            <!-- /.col-md-12 -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="description">Lý do chưa hoàn thành</label>
                                        <textarea id="note" class="form-control" name="note" rows="5"
                                                  placeholder="Mô tả">
                                        </textarea>
                                    </div>
                                    <!-- /.form-group -->

                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="labor_calendar_id">
                    <input type="hidden" name="month_search" value="{{$searchMonth}}">
                    <input type="hidden" name="year_search" value="{{$searchYear}}">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">
                            update
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
{{-- Footer Extras to be Included --}}
@section('footer-extras')
    <script>
        $(document).ready(function() {
            $('.user_1').select2({width: '100%'});
        });
        $(document).ready(function() {
            $('.user_2').select2({width: '100%'});
        });
      myEditor($('#content'));
      $(document).ready(function () {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: ['interaction'],
          plugins: ['dayGrid'],
          locale: 'vi',
          header: {
            // left: 'title',
            // left: 'prev,next today',
            // center: 'title',
            // right: 'month,agendaWeek,agendaDay,listWeek'
            right: null
          },
          views: {
            dayGridMonth: {
              titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'}
            }
          },
          defaultDate: '{{$nowDate}}',
          navLinks: true,
          eventLimit: true,
          eventOrder: 'start',
          events: [
                  @foreach($laborCalendars as $laborCalendar)
                  @if ($laborCalendar['check_type'] !== 'button' || $laborCalendar['check_type'] == 'button' && $laborCalendar['start_date'] < now()->setTime(0, 0, 0))
            {
              id: '@if($laborCalendar['check_type'] == 'user') user @else button @endif',
              title: '@if($laborCalendar['check_type'] == 'user') {{$laborCalendar['title']}} @elseif($laborCalendar['check_type'] == 'button' && $laborCalendar['status'] == 1) Hoàn thành @elseif($laborCalendar['check_type'] == 'button' && $laborCalendar['status'] == 2) Chưa hoàn thành @else Chưa kiểm tra @endif',
              start: '{{$laborCalendar['start_date']}}',
              allDay: true,
              backgroundColor: '@if($laborCalendar['check_type'] == 'user' && $laborCalendar['gender'] == 0)#F5C55D @elseif($laborCalendar['check_type'] == 'user' && $laborCalendar['gender'] == 1) #C872F2 @elseif($laborCalendar['check_type'] == 'button' && $laborCalendar['status'] == 1) #00ff00 @elseif($laborCalendar['check_type'] == 'button' && $laborCalendar['status'] == 2) #ff5656 @else #4c4c4c @endif',
              groupId: '{{$laborCalendar['group_id']}}',
            },
              @endif
              @endforeach
          ],
          eventClick: function (info) {
            let laborCalendarID = info.event.groupId;
            let date = info.event.start;
            let dateChose = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
            let now = new Date();
            let dateCurrent = now.getFullYear() + '/' + (now.getMonth() + 1) + '/' + now.getDate();
            if (info.event.id.trim() == 'user' && new Date(dateChose) >= new Date(dateCurrent)) {
              $.ajax({
                url: '{{route('admin::labor.pair.chose')}}',
                type: 'post',
                data: {
                  laborCalendarID: laborCalendarID
                },
                success: function (data) {
                    $('.user_1').val(data.id[0].user_id);
                    $('.user_1').trigger('change');
                    $('.user_2').val(data.id[1].user_id);
                    $('.user_2').trigger('change');
                  $('#date-labor').html(data.date);
                  $('#MyPopup input[name=labor_calendar_id]').val(data.laborCalendarID);
                  $('#MyPopup').modal("show");
                }
              });
            } else {
              $.ajax({
                url: '{{route('admin::labor.pair.status')}}',
                type: 'post',
                data: {
                  laborCalendarID: laborCalendarID
                },
                success: function (data) {
                  if (data.status_response == 200) {
                    $('#status option[value=' + data.status + ']').attr('selected', 'selected');
                    $('#note').html(data.note);
                    $('#status-labor').html(data.date);
                    $('#labor_status input[name=labor_calendar_id]').val(data.laborCalendarID);
                    $('#labor_status').modal("show");
                  } else {
                    alert('Không tìm thấy trạng thái làm việc trong ngày!')
                  }
                }
              });
            }
          },
        });


        calendar.render();

      });

      function submit() {
        let contentWork = tinymce.activeEditor.getContent();
        if (contentWork == '') {
          alert('Nội dung công việc chưa nhập!')
        } else {
          let now = new Date();
          let monthNow = now.getMonth() + 1;
          let yearNow = now.getFullYear();
          let monthChose = $('select[name=month]').val();
          let yearChose = $('select[name=year]').val();
          if (yearChose > yearNow || yearChose == yearNow && monthChose >= monthNow) {
            $('input[name=content]').val(contentWork);
            $('#labor-calendar').attr('action', '{{route('admin::labor.pairing')}}');
            $('#labor-calendar').submit();
            $('#pair').prop("onclick", null).off("click");
          } else {
            alert('Không được cập nhật lich trực trong quá khứ!');
          }
        }
      }

      function focusInfoUser(e) {
        let idUser = $(e).val();
        $("#position_" + idUser)[0].scrollIntoView();
        $("#position_" + idUser).css("background-color", '#C9C1C1');
        setTimeout(function () {
          $("#position_" + idUser).css("background-color", '');
        }, 4000);
      }

      let now = new Date();
      let monthNow = now.getMonth() + 1;
      let yearNow = now.getFullYear();
      $('select[name=month]').change(function () {
        let monthSelect = $(this).val();
        let yearSelect = $('select[name=year]').val();
        if (yearSelect == yearNow && monthSelect < monthNow) {
          $('#pair').removeAttr('onclick');
          $('#pair').css('opacity', '0.5');
        } else {
          $('#pair').attr('onclick', 'submit()');
          $('#pair').css('opacity', '');
        }
      });

      $('select[name=year]').change(function () {
        let yearSelect = $(this).val();
        let monthSelect = $('select[name=month]').val();
        if (yearSelect == yearNow && monthSelect < monthNow) {
          $('#pair').removeAttr('onclick');
          $('#pair').css('opacity', '0.5');
        } else {
          $('#pair').attr('onclick', 'submit()');
          $('#pair').css('opacity', '');
        }
      });
        function updateWorkContent() {
            $("#update-work-content").submit();
        }

    </script>
@endsection
