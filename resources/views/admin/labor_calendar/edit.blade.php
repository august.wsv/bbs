{{-- Extends Layout --}}
@extends('layouts.admin.master')

<?php
$_pageTitle = (isset($addVarsForView['_pageTitle']) && !empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : $resourceTitle);
$_pageSubtitle = (isset($addVarsForView['_pageSubtitle']) && !empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : "Sửa thông tin " . str_singular($_pageTitle));
$_formFiles = isset($addVarsForView['formFiles']) ? $addVarsForView['formFiles'] : false;
$_listLink = route($resourceRoutesAlias . '.index');
$_createLink = route($resourceRoutesAlias . '.create');

$_updateLink = route($resourceRoutesAlias . '.update', $record->id);
$_printLink = false;
?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render($resourceRoutesAlias.'.edit', $record->id) !!}
@endsection
{{-- Page Title --}}
@section('page-title', $_pageTitle)

{{-- Page Subtitle --}}
@section('page-subtitle', $_pageSubtitle)

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12">

            <!-- Edit Form -->
            <div class="box box-info" id="wrap-edit-box">

                <form class="form" role="form" method="POST"
                      action="{{ $_updateLink }}"
                      enctype="multipart/form-data"{{ $_formFiles === true ? 'enctype="multipart/form-data"' : ''}}>
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="box-header with-border">
                        <h3 class="box-title">Sửa thông tin </h3>

                        <div class="box-tools">
                            {{--<a href="#" class="btn btn-sm btn-default margin-r-5 margin-l-5" onclick="history.go(-1)">--}}
                            {{--<i class="fa fa-caret-left"></i> <span>Back</span>--}}
                            {{--</a>--}}
                            <a href="{{ $_listLink }}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                                <i class="fa fa-search"></i> <span>Danh sách</span>
                            </a>
                            <a href="{{ $_createLink }}" class="btn btn-sm btn-success margin-r-5 margin-l-5 toggle-create">
                                <i class="fa fa-plus"></i> <span>Thêm mới</span>
                            </a>
                            @if ($_printLink)
                                <a href="{{ $_printLink }}" target="_blank"
                                   class="btn btn-sm btn-default margin-r-5 margin-l-5">
                                    <i class="fa fa-print"></i> <span>Print</span>
                                </a>
                            @endif
                            <button class="btn btn-sm btn-info margin-r-5 margin-l-5">
                                <i class="fa fa-save"></i> <span>Lưu</span>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="col-md-9">
                            @foreach($record->laborCalendarDetail as $detail)
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="name">Tên người trực nhật </label>
                                        <input type="text" readonly class="form-control" name="name" placeholder="Tên nhóm" value="{{\App\Models\User::find($detail->user_id)->name}}" required="">

                                    </div>

                                </div>
                                <div class="col-md-6"></div>
                            </div>
                            @endforeach

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="leader_id">Ngày trực nhật </label>
                                        <input type="text" readonly class="form-control" value="{{Carbon::parse($record->labor_date)->format(DATE_FORMAT)}}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.col-md-12 -->
                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group margin-b-5 margin-t-5">
                                            <label for="leader_id">Trạng thái *</label>
                                            <select class="form-control" name="status">
                                                @foreach(LABOR_STATUS_NAME as $val => $statusName)
                                                    <option value="{{$val}}" {{$val == $record->status ? 'selected' : ''}}>{{$statusName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group margin-b-5 margin-t-5">
                                        <label for="description">Lý do chưa hoàn thành</label>
                                        <textarea id="note" class="form-control" name="note" rows="5"
                                                  placeholder="Mô tả">
                                            {{$record->note}}
                                        </textarea>
                                    </div>
                                    <!-- /.form-group -->

                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    @if($resourceRoutesAlias != 'admin::day_offs')

                        <div class="box-footer clearfix">
                            <div class="col-xs-12 option-with">
                                <div align="center">
                                    <button class="btn btn-info mr-2">
                                        <i class="fa fa-save"></i> <span>Lưu</span>
                                    </button>
                                    <a href="{{ $_listLink }}" class="btn btn-default">
                                        <i class="fa fa-ban"></i> <span>Hủy</span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.col-xs-6 -->
                        </div>
                @endif
                <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /End Edit Form -->
        </div>
    </div>
    <!-- /.row -->
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')
    <script src="{{asset_ver('js/tinymce/tinymce.min.js')}}"></script>
    <script !src="">
        tinymce.init({
            selector: 'textarea',
            paste_data_images: true,
            height: '350px',
            width: '99%',
            language: 'vi',
            plugins: [
                "advlist autolink lists charmap preview hr anchor pagebreak",
            ],
        });
    </script>
@endsection
