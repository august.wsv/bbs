<div style="display: inline-block;margin-right: 5px">
    <select class="mr-1  form-control" name="month" style="height: 32px">
            @foreach(get_months() as  $key => $month)
                @if(!empty($key))
                <option value="{{$key}}" {{$month == $searchMonth ? 'selected' : ''}} >Tháng {{$month}}</option>
                 @endif
            @endforeach
    </select>
</div>
<div style="display: inline-block;margin-right: 5px">
    <select class="mr-1  form-control" name="year" style="height: 32px">
        @foreach(LABOR_YEAR as  $year)
            <option value="{{$year}}" {{$year == $searchYear ? 'selected' : ''}}>Năm {{$year}}</option>
        @endforeach
    </select>
</div>
<div class="input-group-btn" style="display: inline-block">
    <button type="submit" class="btn btn-sm btn-primary" style="height: 30px; margin-bottom: 7px;margin-right: 5px;"><i class="fa fa-search"></i> Tìm kiếm</button>
</div>
