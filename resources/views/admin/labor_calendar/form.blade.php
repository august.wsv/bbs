<div style="margin-bottom: 10px;font-weight: bold">Số lượng chọn: <span class="rate" total-user ='{{$countUserChose}}'>0/{{$countUserChose}}</span></div>
<div class="table-responsive list-records">
    <div class="container-table" style="overflow-y: auto; max-height: 400px;margin-bottom: 20px">
        <table class="table table-hover table-bordered dataTable">
        <tr class="text-header-center">
            <th rowspan="2">
                Mã nhân viên
            </th>
            <th rowspan="2">
                Họ và tên
            </th>
            <th colspan="6" style="text-align: center">
                Lịch trực nhật 6 tháng gần nhất
            </th>
            <th rowspan="2">
                Chọn
            </th>
        </tr>
        <tr style="text-align: center" class="text-header-center">
            @foreach($listBeforeSixMonth as $month)
                <th>Tháng {{$month}}</th>
            @endforeach
        </tr>
        <tbody>
        @if(!empty($laborUserList))
            @foreach ($laborUserList as $infoUser)
                <tr>
                    <td>{{$infoUser['user']->staff_code}}</td>
                    <td>{{$infoUser['user']->name}}</td>
                    @foreach($infoUser['info'] as $checkLabor)
                        <td>@if($checkLabor) <i class='fa fa-check'></i>@endif</td>
                    @endforeach
                    <td style="text-align: center"><input type="checkbox" name="list_user_id[]"
                                                          onchange="changeChose(this)"
                                                          value="{{$infoUser['user']->id}}"></td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9" style="padding: 10px;text-align: center">Không có dữ liệu</td>
            </tr>
        @endif
        </tbody>
    </table>
    </div>
    <label class="mt-3" for="content">Nội dung công việc</label>
    <div class="md-form mt-1 mb-0">
        <i class="fas fa-pencil-alt prefix grey-text"></i>
        <textarea type="text" id="content" name="content"
                  class="md-textarea form-control"></textarea>
    <input type="hidden" name="month" value="{{$currentMonth}}">
    <input type="hidden" name="year" value="{{$currentYear}}">
</div>
@push('footer-scripts')
        <script src="{{asset_ver('js/tinymce/tinymce.min.js')}}"></script>
        <script !src="">
            tinymce.init({
                selector: 'textarea',
                language: 'vi',
                paste_data_images: true,
                height: '350px',
                width: '99%',
                plugins: [
                    "advlist autolink lists charmap preview hr anchor pagebreak",
                ],
            });
        </script>

    <script>
        function changeChose(e) {
            let countUserChecked = 0;
            $.each($('input[type=checkbox]'),function (index,value) {
                if($(this).is(':checked'))
                {
                    countUserChecked++;
                }
            });
            let totalUser = $('.rate').attr('total-user');
            if(countUserChecked > totalUser)
            {
                alert('Không được chọn quá số người quy định');
                $(e).prop("checked", false);
            }else{
                $('.rate').html(countUserChecked+'/'+totalUser);
            }

        }
    </script>


@endpush