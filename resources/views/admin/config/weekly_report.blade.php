<div class="row" id="reportConfig">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <div id="chkEnableGroup" class="form-group margin-b-5 margin-t-5">
                    <label for="has_notify">
                        <input id="chkEnable" type="checkbox" class="square-blue" name="enable_weekly_report_check"
                               @if($record->enable_weekly_report_check)
                               checked
                               @endif
                               value="1"
                        >
                        Check báo cáo tuần
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group margin-b-5 margin-t-5">
                    <label for="has_notify">
                        <input type="checkbox" class="square-blue" name="must_comfirm_weekly_report_check"
                               @if($record->must_comfirm_weekly_report_check)
                               checked
                               @endif
                               value="1"
                        >
                        Xác nhận trước khi tính phạt
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group margin-b-5 margin-t-5">
            <label for="date_off_from">Ngày kiểm tra</label>
            <div class="input-group date">
                <ul class="list-group list-inline">
                    <?php $days = get_day_of_week();?>
                    @foreach($days as $dayOff => $day)
                        <?php
                        $inputId = 'weekly_report_check_day_' . $dayOff;
                        ?>
                        <li class="">
                            <input type="checkbox" id="{{$inputId}}"
                                   @if(in_array($dayOff, $record->weekly_report_check_day))
                                   checked
                                   @endif
                                   name="weekly_report_check_day[]"
                                   value="{{$dayOff}}">
                            <label
                                    for="{{$inputId}}">{{$day}}</label>
                        </li>
                    @endforeach
                </ul>
            </div>
            @if ($errors->has('weekly_report_check_day'))
                <span class="help-block">
                    <strong>{{ $errors->first('weekly_report_check_day') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group margin-b-5 margin-t-5">
            <label>Giờ kiểm tra</label>
            <input type="time" class="form-control" style="width: 200px"
                   autocomplete="off"
                   name="weekly_report_check_time"
                   value="{{old('weekly_report_check_time', $record->weekly_report_check_time)}}">
        </div>
    </div>
</div>

@push('footer-scripts')
    <script>
      $(function () {
        var $otherWraps = $("#reportConfig .form-group:not('#chkEnableGroup')");
        $otherWraps.hide();
        var $chkEnable = $("#chkEnable");
        var checkEnable = function () {
          if ($chkEnable.is(':checked')) {
            $otherWraps.show();
          } else {
            $otherWraps.hide();
          }
        };
        checkEnable();
        $chkEnable.on('ifChanged', function () {
          checkEnable();
        });
      })
    </script>
@endpush
