    
@extends('layouts.admin.master')

<?php

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::configs') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Chi tiết thiết bị')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Chi tiết thiết bị')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
    
<section class="content">
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border" style="margin-bottom: 20px">
                <h3 class="box-title">Nhân viên : {{$user->staff_code}} - {{$user->name}}</h3>
                <div class="box-tools">
                    <a href="{{route('admin::t_devices.t_list_devices')}}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                        <i class="fa fa-search"></i> <span>Danh sách</span>
                    </a>
                    <a href="{{route('admin::t_devices.t_add_devices', ['user_id' => $user->id])}}" class="btn btn-sm btn-success margin-r-5 margin-l-5">
                        <i class="fa fa-plus"></i> <span>Thêm mới</span>
                    </a>
                </div>
                
            </div>
            <div class="box-body">
                <div class="tab-content clearfix">
                    <div class="col-md-12">
                        <?php
                            $count = 1;
                        ?>
                        @foreach($user->user_devices as $item)
                        <div class="border">
                            <div class="col-md-12" style="border: 1px solid #cccccc; padding-top: 12px; padding-bottom: 12px; margin-bottom: 12px">
                            <input type="hidden" name="" value="1">
                                <div>
                                    <label for="staticEmail" class="">Thiết bị {{$count++}} - {{$item->devices->device_name->name}} : {!!$item->devices->name!!}<a href="#"></a></label>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="form-group row col-md-6">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Loại thiết bị :</label>
                                        <div class="col-sm-8">
                                            <span>{{$item->devices->device_name->name}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group row col-md-6">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Tên hãng :</label>
                                        <div class="col-sm-8">
                                            <span>{{$item->devices->name}}</span>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div>
                                @foreach($item->devices->specification as $key=>$value)
                                    <div class="row">   
                                        @foreach($value as $key_v=>$value_v)
                                            @if(is_array($value_v))
                                            <div class="form-group row col-md-6">
                                                <label for="staticEmail" class="col-sm-4 col-form-label">{{$key}} :</label>
                                                <div class="col-sm-8">
                                                    <span>{{$value_v['name']}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row col-md-6">
                                                <label for="staticEmail" class="col-sm-4 col-form-label">Ghi chú :</label>
                                                <div class="col-sm-8">
                                                @if(isset($value_v['note']))
                                                    <span>{{$value_v['note']}}</span>
                                                @endif
                                                </div>
                                            </div>
                                            @else
                                            @if($key_v == 'name')
                                            <div class="form-group row col-md-6">
                                                <label for="staticEmail" class="col-sm-4 col-form-label">{{$key}} :</label>
                                                <div class="col-sm-8">
                                                    <span>{{$value_v}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row col-md-6">
                                                <label for="staticEmail" class="col-sm-4 col-form-label">Ghi chú :</label>
                                                <div class="col-sm-8">
                                                    @if(isset($value['note']))
                                                    <span>{{$value['note']}}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            @endif
                                            @endif
                                        @endforeach
                                        
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
