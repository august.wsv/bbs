    
@extends('layouts.admin.master')

<?php

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::configs') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Thiết bị nhân viên')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Thiết bị nhân viên')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
    
<section class="content">
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Danh sách
                </h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('admin::t_devices.t_add_attribute_devices') }}" class="btn btn-sm btn-primary mr-5">
                        <span>Thêm mới +</span>
                    </a>
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="padding-5">

                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif

                <div class="table-responsive list-records col-md-8">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th>Loại thiết bị</th>
                                <th>Thuộc tính</th>
                                <th class="text-center">Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $count = 1;
                        ?>
                        @foreach($device_attributes as $item)
                            <tr>
                                <td class="text-center">{{$count++}}</td>
                                <td>
                                    {{$item->name}}
                                </td>
                                <td>
                                    <?php
                                        $name_attr = '';
                                        
                                        foreach($item->attribute as $key_device_item=>$device_item)
                                        {
                                            
                                            if(count($item->attribute) - 1 == $key_device_item){
                                                $note = '';
                                            }else
                                            {
                                                $note = ' | ';
                                            }
                                            $name_attr .= $device_item->name . $note;  
                                        }

                                    ?>
                                    {{$name_attr}}
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{route('admin::t_devices_add_attr', ['id' => $item->id])}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</section>
<script>

</script>
@endsection
