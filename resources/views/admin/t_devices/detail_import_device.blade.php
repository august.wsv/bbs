    
@extends('layouts.admin.master')

<?php

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::configs') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Chi tiết thiết bị')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Chi tiết thiết bị')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="box box-info">
            <form id="formSubmit" method="POST" action="{{route('admin::t_devices.edit_name_import_device')}}">
            @csrf
            <div class="box-header with-border" style="margin-bottom: 20px">
                <h3 class="box-title">Thiết bị : {{$device_import->name}}</h3>
                <input type="hidden" name="id" value="{{$device_import->id}}">
                <div class="box-tools">
                    <a href="{{route('admin::t_devices.t_import_device_index')}}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                        <i class="fa fa-search"></i> <span>Danh sách</span>
                    </a>
                </div>
                
            </div>
            <div class="box-body">
                <div class="tab-content clearfix">
                    <div class="col-md-12">
                        <div class="border">
                            <div class="col-md-12" style="border: 1px solid #cccccc; padding-top: 12px; padding-bottom: 12px; margin-bottom: 12px">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <label class="col-md-12" for="staticEmail" class="">Thiết bị : {!!$device_import->name!!}<a href="#"></a></label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="form-group row col-md-12">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Loại thiết bị :</label>
                                        <div class="col-sm-8">
                                            <span>{{$device_import->device->name}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12 error">

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group row col-md-12">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Tên thiết bị :</label>
                                        <div class="col-sm-6">
                                            <input id="edit_name" type="text" name="name" value="{{$device_import->name}}" class="form-control">
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group row col-md-12">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Mã thiết bị :</label>
                                        <div class="col-sm-6">
                                            <input readonly type="text" value="{{$device_import->code}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group row col-md-12">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Bảo hành :</label>
                                        <div class="col-sm-6">
                                            <input readonly type="text" value="{{$device_import->guarantee}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group row col-md-12">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Ngày mua :</label>
                                        <div class="col-sm-6">
                                        <input readonly type="text" value="{{$device_import->date_import}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                @foreach($device_import->specification as $key=>$value)
                                    <div class="row">   
                                        @foreach($value as $key_v=>$value_v)
                                            @if(is_array($value_v))
                                            <div class="form-group row col-md-12">
                                                <label for="staticEmail" class="col-sm-2 col-form-label">{{$key}} :</label>
                                                <div class="col-sm-6">
                                                    <input readonly type="text" value="{{$value_v['name']}}" class="form-control">
                                                </div>
                                            </div>
                                            @else
                                            @if($key_v == 'name')
                                            <div class="form-group row col-md-12">
                                                <label for="staticEmail" class="col-sm-2 col-form-label">{{$key}} :</label>
                                                <div class="col-sm-8">
                                                    <input readonly type="text" value="{{$value_v}}" class="form-control">
                                                </div>
                                            </div>
                                            @endif
                                            @endif
                                        @endforeach
                                        
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="box-footer clearfix">
                <div class="col-xs-12 option-with" style="margin-top: 15px">
                    <div align="center">
                        <button id="btn_submit" class="btn btn-info mr-2">
                            <i class="fa fa-save"></i> <span>Lưu</span>
                        </button>
                        <a href="{{route('admin::t_devices.t_import_device_index')}}" class="btn btn-default">
                            <i class="fa fa-ban"></i> <span>Hủy</span>
                        </a>
                    </div>
                </div>
                <!-- /.col-xs-6 -->
            </div>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $("#formSubmit").submit(function(e) {

                var error_name =  $('#edit_name').val()

                if (error_name == '') {
                    e.preventDefault();
                    $('.error').find('.error-notification').remove()
                    $('.error').append(`
                        <p class="text-danger error-notification">Bạn không được bỏ trống tên thiết bị!</p>
                    `)
                }
                return true;

            });
        })
    </script>
</section>
@endsection
