
@extends('layouts.admin.master')

<?php

use App\Models\TUserDevice;

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::configs') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Thiết bị nhân viên')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Thiết bị nhân viên')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')

<section class="content">
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Danh sách
                </h3>
                <div class="box-tools pull-right">
                    <form id="searchForm" class="form" role="form" method="GET" action="{{route('admin::t_devices.t_import_device_index')}}">
                        <a href="{{ route('admin::t_devices.export.almost_expire') }}" class="btn btn-sm btn-primary toggle-create margin-r-5">
                            <i class="fa fa-file-excel-o" aria-hidden="true"></i> <span>Thông tin thiết bị</span>
                        </a>
                        <div class="input-group input-group-sm margin-r-5 pull-left" style="width: 200px;">
                            <input type="text" name="search" class="form-control" value="" placeholder="Nhập tên hoặc mã thiết bị...">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <a href="{{ route('admin::t_devices.add_device_import') }}" class="btn btn-sm btn-primary pull-right toggle-create">
                            <i class="fa fa-plus"></i> <span>Thêm mới </span>
                        </a>
                    </form>
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="padding-5">

                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                <div class="table-responsive list-records">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th>Loại thiết bị</th>
                                <th>Mã thiết bị</th>
                                <th class="text-center">Tên thiết bị</th>
                                <th class="text-center">Tổng số</th>
                                <th class="text-center">Tồn kho</th>
                                <th class="text-center">Ngày nhập</th>
                                <th class="text-center">Hạn bảo hành</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $count = 1;
                        ?>
                       @foreach($devices as $item)
                       <?php
                            $check_item_used = TUserDevice::where('device_import_id', $item->id)->count();
                       ?>
                        <tr>
                            <td class="text-center">{{$count++}}</td>
                            <td><a href="{{route('admin::t_devices.t_import_device_index', ['device' => $item->device_type_id])}}">{{$item->device_name}}</a></td>
                            <td>{{$item->code}}</td>
                            <td style="width:450px"><a href="{{route('admin::t_devices.device_import_detail', ['id' => $item->id])}}">{!!$item->device_import_name!!}</a></td>
                            <td class="text-center">{{$item->sum_total}}</td>
                            <td class="text-center">{{$item->quantity <= 0 ? 0 : $item->quantity}}</td>
                            <td class="text-center">{{$item->date_import}}</td>
                            <?php
                            $dateImport = Carbon::parse($item->date_import);
                            $dateExpireGuarantee = $dateImport->addMonth($item->guarantee);
                            ?>
                            <td class="text-center">{{$dateExpireGuarantee->toDateString()}}</td>
                            <td style="cursor:pointer">
                                <a  data-quantity="{{$item->quantity}}" data-id="{{$item->id}}" class="thanh-ly-thiet-bi" type="button">Thanh lý</a> @if($check_item_used <= 0)| <a href="#" data-id="{{$item->id}}" class="xoa-thiet-bi" type="button">Xóa</a>@endif | <a data-import-device-id="{{$item->id}}" data-import-device-name="{{$item->device_import_name}}" class="list-user">Người sử dụng</a></td>
                        </tr>
                       @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if (count($devices) > 0)
                @include('common.paginate_device', ['records' => $devices])
            @endif
            </div>
    </div>

    <div id="modalConfirmModelDelete" class="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-red color-palette">
                    <button type="button" class="close close_model" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="modalConfirmModelDeleteLabel">Bạn có chắc chắn <i class="fa fa-question"></i>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Hành động này sẽ xóa hết tất cả thiết bị liên quan!!!</p>
                    <p> Bạn có chắc chắn muốn xóa không<i class="fa fa-question"></i></p>
                </div>
                <div class="modal-footer">
                    <a type="button" href="#" class="btn btn-danger" id="btnRemoveDevice">Xác nhận
                    </a>
                    <button type="button" class="btn btn-default close_model" id="btnModalCancelModelDelete">
                        Hủy bỏ
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div id="modalThanhLy" class="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="formThanhLy" action="" method="POST">
                    @csrf
                <div class="modal-header bg-red color-palette">
                    <button type="button" class="close close_model_thanh_ly" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="">Bạn có chắc chắn <i class="fa fa-question"></i>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Chọn số lượng muốn thanh lý!</p>
                        <input id="quantity" name="quantity">
                    <div class="error">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Xác nhận
                    </button>
                    <button type="button" class="btn btn-default close_model_thanh_ly" id="btnModalCancelModelDelete">
                        Hủy bỏ
                    </button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div id="MyPopup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content modal-content-user">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        <b id="device-name"></b>
                    </h4>
                </div>
                <div class="modal-body" style="overflow: auto; max-height: 400px">
                    <div class="col-md-12" id="table-user">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){

        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };

        $('#quantity').inputFilter(function(value) {
            return /^\d*$/.test(value);
        });

        $('.xoa-thiet-bi').click(function(){
            $('#modalConfirmModelDelete').show();
            var id_xoa_thiet_bi = $(this).attr('data-id');
            $('#btnRemoveDevice').attr('href', '../remove-import-device/' + id_xoa_thiet_bi);
        })

        $('.close_model').click(function(){
            $('#modalConfirmModelDelete').hide();
        })




        $('.thanh-ly-thiet-bi').click(function(){
            $('#modalThanhLy').show();
            var id_thanh_ly = $(this).attr('data-id');
            var quantity = $(this).attr('data-quantity');
            $('#formThanhLy').attr('data-quantity', quantity);
            $('#formThanhLy').attr('action', '../thanh-ly-thiet-bi/' + id_thanh_ly);
        })

        $('.close_model_thanh_ly').click(function(){
            $('.error').find('.error-notification').remove()
            $('#quantity').val('')
            $('#modalThanhLy').hide();
        })

        $("#formThanhLy").submit(function(e) {
            var quantity_submit = $(this).attr('data-quantity')
            var quantity_input = $('#quantity').val()
            if (Number(quantity_input) > Number(quantity_submit)) {
                e.preventDefault();
                $('.error').find('.error-notification').remove()
                $('.error').append(`
                    <p class="text-danger error-notification">Số sản phẩm thanh lý phải nhỏ hơn hoặc bằng số lượng còn trong kho!</p>
                `)
            }
            if (quantity_input == null) {
                e.preventDefault();
                $('.error').find('.error-notification').remove()
                $('.error').append(`
                    <p class="text-danger error-notification">Số sản phẩm thanh lý không đúng!</p>
                `)
            }

        return true;

        });

        $('.list-user').click(function () {
            let import_device_name = $(this).attr('data-import-device-name')
            let import_device_id = $(this).attr('data-import-device-id')
            $('#device-name').html('Thiết bị: '+ import_device_name)
            $.ajax({
                url: "{{route('admin::t_device.list_user')}}",
                type: 'post',
                data: {
                    import_device_id: import_device_id
                },
                success: function (response) {
                    if (response.status == 200) {
                        $('#table-user').html(response.table_user)
                        $('#MyPopup').modal("show")
                    } else {
                        alert('Lỗi xử lý hệ thống')
                    }
                }
            })
        })

    })
</script>
@endsection
