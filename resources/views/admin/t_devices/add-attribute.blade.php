    
@extends('layouts.admin.master')

<?php

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::configs') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Thêm mới thiết bị')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Thêm mới thiết bị')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Thiết bị : {{$device->name}}
                </h3>
                <div class="box-tools">
                    <a href="{{ route('admin::t_devices.attribute') }}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                        <i class="fa fa-search"></i> <span>Danh sách</span>
                    </a>
                    <button data-count="{{count($device_attributes)}}" style="margin-right: 12px" type="button" class="btn btn-primary btn-sm add_input">Thêm thuộc tính +</button>
                </div>
            </div>
            <div class="box-body">
                <div class="tab-content clearfix">
                    <div class="col-md-12">
                        <form id="formSubmit" action="{{route('admin::t_devices.t_save_attribute', ['id' => $device->id])}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-12" id="append_item">
                                @if(Session::has('message'))
                                    <div class="alert alert-success" role="alert">
                                        {{ Session::get('message') }}
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <label class="col-md-9" for="staticEmail" class=""></label>
                                            <div class="col-md-3">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="error col-md-12">

                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-12 row">
                                        <div class="form-group row col-md-12">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">Loại thiết bị</label>
                                            <div class="col-sm-5">
                                                <input name="name_device" type="text" value="{{$device->name}}" class="form-control type_input_readonly validate">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 row append_input">
                                    @foreach($device_attributes as $key_attr=>$attr_device)
                                        <div class="form-group row col-md-12">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">Thuộc tính</label>
                                            <div class="col-sm-5">
                                                <input name="name_attribute[{{$key_attr}}]" type="text" value="{{$attr_device['name']}}" class="form-control type_input_readonly validate">
                                                <input type="hidden" name="id_attribute[{{$key_attr}}]" value="{{$attr_device['id']}}">
                                            </div>
                                            <div class="col-sm-2 form-check-inline">
                                                <input @if($attr_device['is_mutiple'] == 0) checked  @endif  class="form-check-input type_input_disabled input_mutiple_edit" data-id="{{$attr_device['id']}}" type="checkbox">
                                                <input id="value_request_mutiple_{{$attr_device['id']}}" type="hidden" value="{{$attr_device['is_mutiple']}}" name="is_mutiple_edit[{{$key_attr}}]">
                                                <label class="form-check-label" for="">
                                                    Mutiple
                                                </label>
                                            </div>
                                            <div class="col-sm-2 form-check-inline">
                                                <input @if($attr_device['is_required'] == 0) checked @endif data-id="{{$attr_device['id']}}" class="form-check-input type_input_disabled input_required_edit" type="checkbox">
                                                <input id="value_request_required_{{$attr_device['id']}}" type="hidden" value="{{$attr_device['is_required']}}" name="is_required_edit[{{$key_attr}}]">
                                                <label class="form-check-label" for="">
                                                    Bắt buộc 
                                                </label>
                                            </div>
                                            <div class="col-sm-1">
                                                <button data-id="{{$attr_device['id']}}" type="button" class="btn btn-danger btn-sm remove_attribute"><i class="fa fa-close"></i></button>
                                            </div>
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer clearfix">
                            <div class="col-xs-12 option-with" style="margin-top: 15px">
                                <div align="center">
                                    <button class="btn btn-info mr-2 btn_submit">
                                        <i class="fa fa-save"></i> <span>Lưu</span>
                                    </button>
                                    <a href="{{route('admin::t_devices.attribute')}}" class="btn btn-default">
                                        <i class="fa fa-ban"></i> <span>Hủy</span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.col-xs-6 -->
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="modalConfirmModelDelete" class="modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red color-palette">
                        <button type="button" class="close close_model" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="modalConfirmModelDeleteLabel">Bạn có chắc chắn muốn xóa<i class="fa fa-question"></i>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Hành động này sẽ không thể quay lại. Bạn có chắc?</p>
                    </div>
                    <div class="modal-footer">
                        <a type="button" href="#" class="btn btn-danger" id="btnRemoveDevice">Xác nhận
                        </a>
                        <button type="button" class="btn btn-default close_model" id="btnModalCancelModelDelete">
                            Hủy bỏ
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>

    
    <script>
        $(document).ready(function(){

            $('.input_mutiple_edit').click(function(){
                var id_data_attribue_mutiple = $(this).attr('data-id')
                if($(this).is(':checked') == true)
                {
                    $('#value_request_mutiple_' + id_data_attribue_mutiple).val(0)
                }else{
                    $('#value_request_mutiple_' + id_data_attribue_mutiple).val(1)
                }
            })
            $('.input_required_edit').click(function(){
                var id_data_attribue_required = $(this).attr('data-id')
                if($(this).is(':checked') == true)
                {
                    $('#value_request_required_' + id_data_attribue_required).val(0)
                }else{
                    $('#value_request_required_' + id_data_attribue_required).val(1)
                }
            })
            var count = 0;
            $('.add_input').click(function(){
                let data_count = Number($(this).attr('data-count'));
                $('.append_input').append(`
                <div class="form-group row col-md-12">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Thuộc tính</label>
                    <div class="col-sm-5">
                        <input name="attribute[${data_count + 1}]" type="text" value="" class="form-control validate">
                    </div>
                    <div class="col-sm-2 form-check-inline">
                        <input class="form-check-input input_mutiple" data-id="${data_count + 1}" data-count="${data_count + 1}" type="checkbox">
                        <input type="hidden" name="is_mutiple[${data_count + 1}]" value="1" id="value_input_mutiple_${data_count + 1}">
                        <label class="form-check-label" for="">
                            Mutiple
                        </label>
                    </div>
                    <div class="col-sm-2 form-check-inline"> 
                        <input value="1" class="form-check-input input_required"  data-id="${data_count + 1}" data-count="${data_count + 1}" type="checkbox">
                        <input type="hidden" name="is_required[${data_count + 1}]" value="1" id="value_input_required_${data_count + 1}">
                        <label class="form-check-label" for="">
                            Bắt buộc 
                        </label>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-danger btn-sm btn_remove_attr_append"><i class="fa fa-close"></i></button>
                    </div>
                </div>
                `)
                $(this).attr('data-count', data_count + 1)
                $('#append_item').find('.btn_remove_attr_append').click(function(){
                    $(this).parent().parent().remove();
                })
                $('#append_item').find('.input_mutiple').click(function(){
                    var data_id_mutiple = $(this).attr('data-id')
                    if($(this).is(':checked') == true)
                    {
                        $('#append_item').find('#value_input_mutiple_' + data_id_mutiple).val(0)
                    }else{
                        $('#append_item').find('#value_input_mutiple_' + data_id_mutiple).val(1)
                    }
                })
                $('#append_item').find('.input_required').click(function(){
                    var data_id_required = $(this).attr('data-id')
                    if($(this).is(':checked') == true)
                    {
                        $('#append_item').find('#value_input_required_' + data_id_required).val(0)
                    }else{
                        $('#append_item').find('#value_input_required_' + data_id_required).val(1)
                    }
                })
            })

            
        })

        $("#formSubmit").submit(function (e) {

        //stop submitting the form to see the disabled button effect
        $.each($('.append_input').find('.validate'), function(key, value){

            if($(value).val() == ''){
                e.preventDefault();
                $('.error').find('.error-notification').remove()
                $('.error').append(`
                    <p class="text-danger error-notification">Bạn không được bỏ trồng trường nào!</p>
                `)
            }
        })

        //disable the submit button
        $("#btn_submit").attr("disabled", true);

        return true;

        });


        $('.remove_attribute').click(function(){
            $('#modalConfirmModelDelete').show();
            var id_xoa_thuoc_tinh = $(this).attr('data-id');
            $('#btnRemoveDevice').attr('href', '../remove-attr-device/' + id_xoa_thuoc_tinh);
        })

        $('.close_model').click(function(){
            $('#modalConfirmModelDelete').hide();
        })
        
    </script>
</section>
@endsection
