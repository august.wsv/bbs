    
@extends('layouts.admin.master')

<?php
    if($role == 'UPGRADE')
    {
        $title = 'Nâng cấp thiết bị nhân viên';
        $tyle_hiden = '';
    }else
    {
        $title = 'Đổi thiết bị nhân viên';
        $tyle_hiden = 'disabled';
    }
?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::configs') !!}
@endsection

{{-- Page Title --}}
@section('page-title', $title)

{{-- Page Subtitle --}}
@section('page-subtitle', $title)

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')

<section class="content">
    <div class="row" id="find_validate">
        <div class="box box-info">
            <div class="box-header with-border" style="margin-bottom: 20px">
                <h3 class="box-title">{{$title}}</h3>
                <div class="box-tools">
                    <a href="{{route('admin::t_devices.t_list_devices')}}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                        <i class="fa fa-search"></i> <span>Danh sách</span>
                    </a>
                </div>
                
            </div>
            <div class="box-body">
                <div class="tab-content clearfix">
                    <div class="col-md-12">
                        <form id="formSubmit" action="{{route('admin::t_devices.saveFormUpgrade', ['id'=> $device_upgrade->id, 'role' => $role])}}" method="POST">
                        @csrf
                        <input type="hidden" name="device_id" value="{{$device_upgrade->device->id}}">
                        <input type="hidden" name="import_device_id" value="{{$device_upgrade->device->import_device_id}}">
                        <input type="hidden" name="device_user_id" value="{{$device_upgrade->id}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row col-md-12">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Thiết bị</label>
                                    <div class="col-sm-10 row">
                                        {{$device_upgrade->device->name}}
                                        <input type="hidden" name="name" value="{{$device_upgrade->device->name}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row col-md-6">
                                    <label for="staticEmail" class="col-sm-4 col-form-label">Loại thiết bị</label>
                                    <div class="col-sm-8 row">
                                        {{$device_upgrade->device_name->name}}
                                        <input type="hidden" name="device_type_id" value="{{$device_upgrade->device_name->id}}">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="user_id" value="{{$device_upgrade->user_id}}">

                            @foreach($device_upgrade->device->specification as $key=>$value)
                                <div class="col-md-12">   
                                    @foreach($value as $key_v=>$value_v)
                                        @if(is_array($value_v))
                                        <div class="form-group row col-md-7 parent_attribute">
                                            <label for="staticEmail" class="col-sm-3 col-form-label">{{$key}}</label>
                                            <div class="col-sm-7 append_input">
                                                <input readonly name="specification[{{$key}}][{{$key_v}}][name]" type="text" value="{{$value_v['name']}}" class="form-control">
                                                <input {{$tyle_hiden}} name="specification[{{$key}}][{{$key_v}}][required]" value="{{$value_v['required']}}" type="hidden">
                                                <input {{$tyle_hiden}} name="specification[{{$key}}][{{$key_v}}][mutiple]" value="{{$value_v['mutiple']}}" type="hidden">
                                            </div>
                                            @if(array_key_last($value) == $key_v)
                                            
                                            @if($role == 'UPGRADE' && $value_v['mutiple'] == 0)
                                            <div class="col-sm-2">
                                                <button data-required="{{$value_v['required']}}" data-mutiple="{{$value_v['mutiple']}}" data-key-last="{{array_key_last($value)}}" data-name="specification[{{$key}}]" type="button" class="btn btn-primary btn-sm add_input"><i class="fa fa-plus"></i></button>
                                            </div>
                                            @endif
                                            @endif
                                        </div>
                                        @if($role == 'UPGRADE')
                                        <div class="form-group row col-md-5">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
                                            <div class="col-sm-10 @if(array_key_last($value) == $key_v) append_note_input @endif">
                                                <input readonly name="specification[{{$key}}][{{$key_v}}][note]" value="@if(isset($value_v['note'])){{$value_v['note']}}@endif" type="text" class="form-control">
                                            </div>
                                        </div>
                                        @endif
                                        @else
                                            @if($key_v == 'name')
                                            <div class="form-group row col-md-6">
                                                <label for="staticEmail" class="col-sm-4 col-form-label">{{$key}}</label>
                                                <div class="col-sm-8">
                                                    <input {{$tyle_hiden}} name="specification[{{$key}}][name]" type="text" value="{{$value_v}}" class="form-control">
                                                </div>
                                                @if($role == 'UPGRADE')
                                                    <div class="col-sm-4">
                                                        <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
                                                    </div>  
                                                @endif
                                            </div>
                                            @if($role == 'UPGRADE')
                                            <div class="form-group row col-md-6">
                                                <label for="staticEmail" class="col-sm-4 col-form-label">Ghi chú</label>
                                                <div class="col-sm-8">
                                                    <input readonly name="specification[{{$key}}][note]" value="" type="text" class="form-control">
                                                </div>
                                            </div>
                                            @endif
                                            @endif
                                        @endif
                                    @endforeach
                                    
                                </div>
                            @endforeach
                        </div>
                        @if($role != 'UPGRADE')
                        <div style="border-top:1px solid #ccc; padding-bottom: 12px">
                            <div style="margin-top: 20px" class="row">
                                <div class="col-md-12">
                                    <div class="error">

                                    </div>
                                    <div class="form-group row col-md-12">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Đổi thiết bị</label>
                                        <div class="col-sm-8 row append_input">
                                            <select data-key="" class="form-control select_device" name="id_device_new">
                                                <option selected disabled>Chọn thiết bị muốn đổi</option>
                                                @foreach($device_import as $item_option)
                                                    @if($device_upgrade->device_import_id != $item_option->id)
                                                    <option value="{{$item_option->id}}">{{$item_option->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="device_type_id" name="device_type_id" value="{{$device_upgrade->device_name->id}}">

                                <div class="col-md-12 attribute_device">
                                    
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="box-footer clearfix">
                            <div class="col-xs-12 option-with" style="margin-top: 15px">
                                <div align="center">
                                    <button class="btn btn-info mr-2">
                                        <i class="fa fa-save"></i> <span>Lưu</span>
                                    </button>
                                    <a href="{{route('admin::t_devices.t_list_devices')}}" class="btn btn-default">
                                        <i class="fa fa-ban"></i> <span>Hủy</span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.col-xs-6 -->
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="application/javascript" src="{{asset_ver('js/admin/t-devices/upgrade.js')}}"></script>

<script type="application/javascript">
    
</script>
@endsection
