    
@extends('layouts.admin.master')


{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::configs') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Thêm mới thiết bị')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Thêm mới thiết bị')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
    
<section class="content">
   
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Thêm mới thiết bị nhân viên
                </h3>
                <div class="box-tools">
                    <a href="{{route('admin::t_devices.t_import_device_index')}}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                        <i class="fa fa-search"></i> <span>Danh sách</span>
                    </a>
                </div>
            </div>
            <div class="box-body">
                <div class="tab-content clearfix">
                    <div class="col-md-12">
                        <form id="formSubmit" action="{{route('admin::t_devices.save_device_import')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                
                            </div>
                            <div class="col-md-12" id="append_item">
                                <p for="user" class="mb-15">Danh sách thiết bị : </p>
                                <div class="error">

                                </div>
                            </div>
                            <div class="col-md-12"  style="margin-bottom: 12px">
                                <button type="button" id="add_device" class="btn btn-primary">Thêm thiết bị <i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="box-footer clearfix">
                            <div class="col-xs-12 option-with" style="margin-top: 15px">
                                <div align="center">
                                    <button class="btn btn-info mr-2">
                                        <i class="fa fa-save"></i> <span>Lưu</span>
                                    </button>
                                    <a href="{{route('admin::t_devices.t_import_device_index')}}" class="btn btn-default">
                                        <i class="fa fa-ban"></i> <span>Hủy</span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.col-xs-6 -->
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="application/javascript" src="{{asset_ver('js/admin/t-devices/add-device-import.js')}}"></script>
@endsection
