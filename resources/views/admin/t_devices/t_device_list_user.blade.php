<table class="table">
    <thead>
    <tr>
        <th scope="col">STT</th>
        <th scope="col">Ảnh</th>
        <th scope="col">Tên nhân viên</th>
        <th scope="col">Ngày cấp</th>
    </tr>
    </thead>
    <tbody>
    @foreach($listUserUseDevice as $key => $user)
    <tr>
        <th scope="row">{{$key + 1}}</th>
        <td><img style="width: 20px; height: 20px" src="{{$user['avatar']}}" alt="Ảnh đại diện"></td>
        <td>{{$user['name']}}</td>
        <td>{{$user['recieve_date']}}</td>
    </tr>
    @endforeach`
    </tbody>
</table>