<table style="border:2px solid #000000">
    <thead>
        <tr style="border:2px solid #000000">
            <th style="border:4px solid #000000; font-weight:bold; text-align:center" rowspan="2">STT</th>
            <th style="border:2px solid #000000; font-weight:bold" rowspan="2">Mã nhân viên</th>
            <th style="border:2px solid #000000; font-weight:bold" rowspan="2">Tên nhân viên</th>
            <th style="border:2px solid #000000; font-weight:bold; text-align:center" colspan="2">Danh sách thiết bị</th>
            <th style="border:2px solid #000000; font-weight:bold; text-align:center" rowspan="2">Ngày cấp</th>
            <th style="border:2px solid #000000; font-weight:bold; text-align:center" rowspan="2">Tình trạng</th>
        </tr>
        <tr style="border:2px solid #000000">
            <th style="border:2px solid #000000; font-weight:bold; text-align:center">Loại thiết bị</th>
            <th style="border:2px solid #000000; font-weight:bold; text-align:center">Chi tiết</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $count = 1;
        ?>
        @foreach($users as $item)
            <?php
                $counts = count($item->device_user);
            ?>
            @if($counts != 0)
            <tr style="border:2px solid #000000">
                <td style="border:2px solid #000000; text-align:center" rowspan="@if($counts == 0) {{0}} @else {{$counts}} @endif">{{$count++}}</td>
                <td style="border:2px solid #000000" rowspan="@if($counts == 0) {{0}} @else {{$counts}} @endif">{{$item->staff_code}}</td>
                <td style="border:2px solid #000000; font-weight:bold" rowspan="@if($counts == 0) {{0}} @else {{$counts}} @endif">
                    <a style="text-decoration:none" href="{{route('admin::t_devices.t_detail_device', ['id' => $item->id])}}">{{$item->name}}</a>
                </td>
                @foreach($item->device_user as $key_device_first=>$device_item_first)
                    @if($key_device_first == 0)
                        <td style="border:2px solid #000000">{{$device_item_first->device->device_name->name}}</td>
                        <td style="border:2px solid #000000">
                            {{$device_item_first->device->name}}
                        </td>
                        <td style="border:2px solid #000000">{{$device_item_first->start_date}}</td>
                        <td style="border:2px solid #000000; font-weight:bold; text-align:center">
                            @if($device_item_first->status == GOOD_DEVICE)
                                Tốt
                            @elseif($device_item_first->status == BROKEN_DEVICE)
                                Hỏng
                            @endif
                        </td>
                    @endif
                @endforeach
            </tr>
            @foreach($item->device_user as $key_device_item=>$device_item)
            @if($key_device_item != 0)
                <tr style="border:2px solid #000000">
                    <td style="border:2px solid #000000">{{$device_item->device->device_name->name}}</td>
                    <td style="border:2px solid #000000">
                        {{$device_item->device->name}}
                    </td>
                    <td style="border:2px solid #000000">{{$device_item->start_date}}</td>
                    <td style="border:2px solid #000000; font-weight:bold; text-align:center">
                        @if($device_item->status == GOOD_DEVICE)
                            Tốt
                        @elseif($device_item->status == BROKEN_DEVICE)
                            Hỏng
                        @endif
                    </td>
                </tr>
            @endif
            @endforeach
            @else
                <tr style="border:2px solid #000000">
                    <td style="border:2px solid #000000">{{$count++}}</td>
                    <td style="border:2px solid #000000">{{$item->staff_code}}</td>
                    <td style="border:2px solid #000000; font-weight:bold">
                        <a href="{{route('admin::t_devices.t_detail_device', ['id' => $item->id])}}">{{$item->name}}</a>
                    </td>
                    <td style="border:2px solid #000000">Không có</td>
                    <td style="border:2px solid #000000">
                        Không có
                    </td>
                    <td style="border:2px solid #000000">Không có</td>
                    <td style="border:2px solid #000000; font-weight:bold; text-align:center">
                        Không có
                    </td>
                </tr>
            @endif
        @endforeach
    </tbody>
</table>