    
@extends('layouts.admin.master')

<?php

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::configs') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Thiết bị nhân viên')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Thiết bị nhân viên')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
    
<section class="content">
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Danh sách
                </h3>
                <div class="box-tools pull-right">
                    <form id="searchForm" class="form" role="form" method="GET" action="{{ route('admin::t_devices.t_list_devices') }}">
                        <div class="input-group input-group-sm margin-r-5 pull-left" style="width: 200px;">
                            <input type="text" name="search" class="form-control" value="" placeholder="Nhập tên NV hoặc tên thiết bị ...">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <a href="{{ route('admin::t_devices.t_add_devices', ['user_id' => 'null']) }}" class="btn btn-sm btn-primary pull-right toggle-create">
                            <i class="fa fa-plus"></i> <span>Thêm mới +</span>
                        </a>
                        <a href="{{ route('admin::t_devices.t_export_device') }}" class="btn btn-sm btn-primary pull-right toggle-create">
                            <i class="fa fa-plus"></i> <span>Xuất danh sách</span>
                        </a>
                    </form>
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="padding-5">

                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif

                <div class="table-responsive list-records">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center" scope="col" rowspan="2">#</th>
                                <th class="text-center" scope="col" rowspan="2">Mã NV</th>
                                <th class="text-center" scope="col" rowspan="2">Tên nhân viên</th>
                                <th class="text-center" scope="col" colspan="2">Danh sách thiết bị</th>
                                <th class="text-center" scope="col" rowspan="2">Ngày cấp</th>
                                <th class="text-center" scope="col" rowspan="2">Tình trạng</th>
                                <th class="text-center" scope="col" rowspan="2">Chức năng</th>
                            </tr>
                            <tr>
                                <th class="text-center" scope="col">Loại thiết bị</th>
                                <th class="text-center" scope="col">Chi tiết</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 1;
                            ?>
                            @foreach($devices as $item)
                                <?php
                                    $counts = count($item->device_user);

                                    $count_check_search = 0;
                                    if($item->device_user != null)
                                    {
                                        foreach($item->device_user as $item_check)
                                        {
                                            if($item_check->device->device_name != null)
                                            {
                                                $count_check_search++;
                                            }
                                        }
                                    }
                                ?>
                                @if($device == null)
                                    @if($counts != 0)
                                        <tr>
                                            <td rowspan="@if($counts == 0) {{0}} @else {{$counts}} @endif">{{$count++}}</td>
                                            <td rowspan="@if($counts == 0) {{0}} @else {{$counts}} @endif">{{$item->staff_code}}</td>
                                            <td style="width:150px" rowspan="@if($counts == 0) {{0}} @else {{$counts}} @endif">
                                                <a href="{{route('admin::t_devices.t_detail_device', ['id' => $item->id])}}">{{$item->name}}</a>
                                            </td>
                                            @foreach($item->device_user as $key_device_first=>$device_item_first)
                                                    @if($key_device_first == 0)
                                                        <td>
                                                        @if(isset($device_item_first->device->device_name))
                                                            <a href="{{route('admin::t_devices.t_list_devices', ['device' => $device_item_first->device->device_name->id, 'role' => 'FIND_DEVICE'])}}">
                                                                {{$device_item_first->device->device_name->name}}
                                                            </a>
                                                        @endif
                                                        </td>
                                                        <td>
                                                            <?php
                                                                $arr_list = $device_item_first->device->device_name->name;
                                                                foreach($device_item_first->device->specification as $key_attr=>$item_attribute)
                                                                {
                                                                    $array_item = $key_attr;
                                                                    $array_attr = "";
                                                                    foreach($item_attribute as $key=>$value)
                                                                    {
                                                                        if($key == 0){
                                                                            $note = '';
                                                                        }
                                                                        else
                                                                        {
                                                                            $note = ' - ';
                                                                        }
                                                                        $array_attr .= $note . $value['name'];
                                                                    }
                                                                    $array_item .= ' : ' . $array_attr;

                                                                    $arr_list .= ' | ' . $array_item;
                                                                }
                                                            ?>
                                                            {{$arr_list}}
                                                        </td>
                                                        <td>{{$device_item_first->start_date}}</td>
                                                        <td>
                                                            @if($device_item_first->status == GOOD_DEVICE)
                                                                Tốt
                                                            @elseif($device_item_first->status == BROKEN_DEVICE)
                                                                Hỏng
                                                            @endif
                                                        </td>
                                                        <td><a href="{{route('admin::t_devices.upgrade', ['id' => $device_item_first->id, 'role' => 'UPGRADE'])}}">Nâng cấp</a> | <a href="{{route('admin::t_devices.upgrade', ['id' => $device_item_first->id, 'role' => 'CHANGE'])}}">Đổi</a> | <a data-id="{{$device_item_first->id}}" class="thu-hoi-thiet-bi" href="#">Thu hồi</a></td>
                                                    @endif
                                            @endforeach
                                        </tr>
                                        @foreach($item->device_user as $key_device_item=>$device_item)
                                            @if($key_device_item != 0)
                                                <tr>
                                                    <td >
                                                        @if(isset($device_item->device->device_name))
                                                        <a href="{{route('admin::t_devices.t_list_devices', ['device' => $device_item->device->device_name->id, 'role' => 'FIND_DEVICE'])}}">
                                                            {{$device_item->device->device_name->name}}
                                                        </td>
                                                        @endif
                                                    <td style="width:300px">
                                                    <?php
                                                            $arr_list = $device_item->device->device_name->name;
                                                            foreach($device_item->device->specification as $key_attr=>$item_attribute)
                                                            {
                                                                $array_item = $key_attr;
                                                                $array_attr = "";
                                                                foreach($item_attribute as $key=>$value)
                                                                {
                                                                    if($key == 0){
                                                                        $note = '';
                                                                    }
                                                                    else
                                                                    {
                                                                        $note = ' - ';
                                                                    }
                                                                    $array_attr .= $note . $value['name'];
                                                                }
                                                                $array_item .= ' : ' . $array_attr;

                                                                $arr_list .= ' | ' . $array_item;
                                                            }
                                                        ?>
                                                        {!! $arr_list !!}
                                                    </td>
                                                    <td>{{$device_item->start_date}}</td>
                                                    <td>
                                                        @if($device_item->status == GOOD_DEVICE)
                                                            Tốt
                                                        @elseif($device_item->status == BROKEN_DEVICE)
                                                            Hỏng
                                                        @endif
                                                    </td>
                                                    
                                                    <td  style="width:200px"><a href="{{route('admin::t_devices.upgrade', ['id' => $device_item->id, 'role' => 'UPGRADE'])}}">Nâng cấp</a> | <a href="{{route('admin::t_devices.upgrade', ['id' => $device_item->id, 'role' => 'CHANGE'])}}">Đổi</a> | <a data-id="{{$device_item->id}}" class="thu-hoi-thiet-bi" href="#">Thu hồi</a></td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                    @if($role != 'FIND_DEVICE')
                                    <tr>
                                        <td>{{$count++}}</td>
                                        <td>{{$item->staff_code}}</td>
                                        <td>
                                            <a href="{{route('admin::t_devices.t_detail_device', ['id' => $item->id])}}">{{$item->name}}</a>
                                        </td>
                                        <td>Không có</td>
                                        <td>
                                            Không có
                                        </td>
                                        <td>Không có</td>
                                        <td>
                                            Không có
                                        </td>
                                        <td><a href="{{ route('admin::t_devices.t_add_devices', ['user_id' => $item->id]) }}">Thêm mới +</a></td>
                                    </tr>
                                    @endif
                                    @endif
                                @else
                                    @if($count_check_search > 0)
                                    @if($counts != 0)
                                        <tr>
                                            <td rowspan="@if($counts == 0) {{0}} @else {{$counts}} @endif">{{$count++}}</td>
                                            <td rowspan="@if($counts == 0) {{0}} @else {{$counts}} @endif">{{$item->staff_code}}</td>
                                            <td style="width:150px" rowspan="@if($counts == 0) {{0}} @else {{$counts}} @endif">
                                                <a href="{{route('admin::t_devices.t_detail_device', ['id' => $item->id])}}">{{$item->name}}</a>
                                            </td>
                                            @foreach($item->device_user as $key_device_first=>$device_item_first)
                                                @if($device_item_first->device->device_type_id == $device)
                                                    @if($key_device_first == 0)
                                                        <td>
                                                        @if(isset($device_item_first->device->device_name))
                                                            <a href="{{route('admin::t_devices.t_list_devices', ['device' => $device_item_first->device->device_name->id, 'role' => 'FIND_DEVICE'])}}">
                                                                {{$device_item_first->device->device_name->name}}
                                                            </a>
                                                        @endif
                                                        </td>
                                                        <td>
                                                            <?php
                                                                $arr_list = $device_item_first->device->device_name->name ?? '';
                                                                foreach($device_item_first->device->specification as $key_attr=>$item_attribute)
                                                                {
                                                                    $array_item = $key_attr;
                                                                    $array_attr = "";
                                                                    foreach($item_attribute as $key=>$value)
                                                                    {
                                                                        if($key == 0){
                                                                            $note = '';
                                                                        }
                                                                        else
                                                                        {
                                                                            $note = ' - ';
                                                                        }
                                                                        $array_attr .= $note . $value['name'];
                                                                    }
                                                                    $array_item .= ' : ' . $array_attr;

                                                                    $arr_list .= ' | ' . $array_item;
                                                                }
                                                            ?>
                                                            {{$arr_list}}
                                                        </td>
                                                        <td>{{$device_item_first->start_date}}</td>
                                                        <td>
                                                            @if($device_item_first->status == GOOD_DEVICE)
                                                                Tốt
                                                            @elseif($device_item_first->status == BROKEN_DEVICE)
                                                                Hỏng
                                                            @endif
                                                        </td>
                                                        <td><a href="{{route('admin::t_devices.upgrade', ['id' => $device_item_first->id, 'role' => 'UPGRADE'])}}">Nâng cấp</a> | <a href="{{route('admin::t_devices.upgrade', ['id' => $device_item_first->id, 'role' => 'CHANGE'])}}">Đổi</a> | <a data-id="{{$device_item_first->id}}" class="thu-hoi-thiet-bi" href="#">Thu hồi</a></td>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </tr>
                                        @foreach($item->device_user as $key_device_item=>$device_item)
                                                @if($key_device_item != 0)
                                                    <tr>
                                                    @if($device_item->device->device_type_id == $device)

                                                        <td >
                                                            @if(isset($device_item->device->device_name))
                                                            <a href="{{route('admin::t_devices.t_list_devices', ['device' => $device_item->device->device_name->id, 'role' => 'FIND_DEVICE'])}}">
                                                                {{$device_item->device->device_name->name}}
                                                            </td>
                                                            @endif
                                                        <td style="width:300px">
                                                        <?php
                                                                $arr_list = $device_item->device->device_name->name ?? '';
                                                                foreach($device_item->device->specification as $key_attr=>$item_attribute)
                                                                {
                                                                    $array_item = $key_attr;
                                                                    $array_attr = "";
                                                                    foreach($item_attribute as $key=>$value)
                                                                    {
                                                                        if($key == 0){
                                                                            $note = '';
                                                                        }
                                                                        else
                                                                        {
                                                                            $note = ' - ';
                                                                        }
                                                                        $array_attr .= $note . $value['name'];
                                                                    }
                                                                    $array_item .= ' : ' . $array_attr;

                                                                    $arr_list .= ' | ' . $array_item;
                                                                }
                                                            ?>
                                                            {!! $arr_list !!}
                                                        </td>
                                                        <td>{{$device_item->start_date}}</td>
                                                        <td>
                                                            @if($device_item->status == GOOD_DEVICE)
                                                                Tốt
                                                            @elseif($device_item->status == BROKEN_DEVICE)
                                                                Hỏng
                                                            @endif
                                                        </td>
                                                        
                                                        <td  style="width:200px"><a href="{{route('admin::t_devices.upgrade', ['id' => $device_item->id, 'role' => 'UPGRADE'])}}">Nâng cấp</a> | <a href="{{route('admin::t_devices.upgrade', ['id' => $device_item->id, 'role' => 'CHANGE'])}}">Đổi</a> | <a data-id="{{$device_item->id}}" class="thu-hoi-thiet-bi" href="#">Thu hồi</a></td>
                                                    </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        @if($role != 'FIND_DEVICE')
                                        <tr>
                                            <td>{{$count++}}</td>
                                            <td>{{$item->staff_code}}</td>
                                            <td>
                                                <a href="{{route('admin::t_devices.t_detail_device', ['id' => $item->id])}}">{{$item->name}}</a>
                                            </td>
                                            <td>Không có</td>
                                            <td>
                                                Không có
                                            </td>
                                            <td>Không có</td>
                                            <td>
                                                Không có
                                            </td>
                                            <td><a href="{{ route('admin::t_devices.t_add_devices', ['user_id' => $item->id]) }}">Thêm mới +</a></td>
                                        </tr>
                                    @endif
                                    @endif
                                    @endif
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if (count($devices) > 0 && $role != 'FIND_DEVICE')
                @include('common.paginate_device', ['records' => $devices])
            @endif
        </div>
    </div>

    <div id="modalConfirmModelDelete" class="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-red color-palette">
                    <button type="button" class="close close_model" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="modalConfirmModelDeleteLabel">Bạn có chắc chắn <i class="fa fa-question"></i>
                    </h4>
                </div>
                <div class="modal-body">
                    <p> Bạn có chắc chắn muốn thu hồi thiết bị này không?<i class="fa fa-question"></i></p>
                </div>
                <div class="modal-footer">
                    <a type="button" href="#" class="btn btn-danger" id="btnRemoveDevice">Xác nhận
                    </a>
                    <button type="button" class="btn btn-default close_model" id="btnModalCancelModelDelete">
                        Hủy bỏ
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</section>
<script>
    $(document).ready(function(){
        $('.thu-hoi-thiet-bi').click(function(){
            let id = $(this).attr('data-id')
            $('#modalConfirmModelDelete').show();
            $('#btnRemoveDevice').attr('data-id', id)
            $('#btnRemoveDevice').attr('href', '../t_devices/deletes/' + id)
        })
        $('.close_model').click(function(){
            $('#modalConfirmModelDelete').hide();
        })
    })
</script>
@endsection
