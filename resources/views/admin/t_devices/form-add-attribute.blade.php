    
@extends('layouts.admin.master')

<?php

?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin::configs') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Thêm mới loại thiết bị')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Thêm mới loại thiết bị')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Thêm mới thuộc tính thiết bị
                </h3>
                <div class="box-tools">
                    <a href="{{ route('admin::t_devices.attribute') }}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                        <i class="fa fa-search"></i> <span>Danh sách</span>
                    </a>
                </div>
            </div>
            <div class="box-body">
                <div class="tab-content clearfix">
                    <div class="col-md-12">
                        <form id="formSubmit" action="{{route('admin::t_devices.t_save_form_attribute')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-12" id="append_item">
                                <div class="border border">
                                    <div class="col-md-12" style="border: 1px solid #cccccc; padding-top: 12px; padding-bottom: 12px; margin-bottom: 12px">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <label for="staticEmail" class="col-md-9">Thiết bị :</label>
                                                    <div class="col-md-3">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="error col-md-12">

                                            </div>
                                        </div>
                                        <hr>    
                                        <div class="">
                                            <div class="col-md-12 row">
                                                <div class="form-group row col-md-12">
                                                    <label for="staticEmail" class="col-sm-2 col-form-label">Loại thiết bị</label>
                                                    <div class="col-sm-5">
                                                        <input name="name[0]" type="text" value="" class="form-control validate">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 row append_attribute append_input">
                                                <div class="form-group row col-md-12">
                                                    <label for="staticEmail" class="col-sm-2 col-form-label">Thuộc tính</label>
                                                    <div class="col-sm-5 ">
                                                        <input name="attribute[0][0]" type="text" value="" class="form-control validate">
                                                    </div>
                                                    <div class="col-sm-2 form-check-inline">
                                                        <input class="form-check-input" id="mutiple_checkbox_default" type="checkbox">
                                                        <input id="value_mutiple_checkbox_default" name="is_mutiple[0][0]" type="hidden" value="1">
                                                        <label class="form-check-label" for="">
                                                            Mutiple
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-2 form-check-inline">
                                                        <input class="form-check-input" id="required_checkbox_default" type="checkbox">
                                                        <input id="value_required_checkbox_default" name="is_required[0][0]" type="hidden" value="1">
                                                        <label class="form-check-label" for="">
                                                            Bắt buộc 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <button data-count="0" data-name="" type="button" class="btn btn-primary btn-sm add_input"><i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"  style="margin-bottom: 12px">
                                <button type="button" id="add_device" class="btn btn-primary">Thêm thiết bị <i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="box-footer clearfix">
                            <div class="col-xs-12 option-with" style="margin-top: 15px">
                                <div align="center">
                                    <button class="btn btn-info mr-2">
                                        <i class="fa fa-save btn_submit"></i> <span>Lưu</span>
                                    </button>
                                    <a href="{{route('admin::t_devices.attribute')}}" class="btn btn-default">
                                        <i class="fa fa-ban"></i> <span>Hủy</span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.col-xs-6 -->
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            
            $('.btn-remove-device').click(function(){
                $(this).parent().parent().remove();
            })
            var count = 1;
            $('#add_device').click(function(){
                $('#append_item').append(`
                    <div class="border border">
                        <div class="col-md-12" style="border: 1px solid #cccccc; padding-top: 12px; padding-bottom: 12px; margin-bottom: 12px">
                            <div>
                                <label for="staticEmail" class="">Thiết bị :</label>
                                <button data-count="" type="button" class="btn btn-danger btn-sm pull-right close_device_attr"><i class="fa fa-times"></i></button>
                            </div>
                            <hr>    
                            <div>
                                <div class="row col-md-12">
                                    <div class="form-group row col-md-12">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Loại thiết bị</label>
                                        <div class="col-sm-5">
                                            <input name="name[${count}]" type="text" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 row append_attribute append_input_ap">
                                    <div class="form-group row col-md-12 parent_ap_${count}">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Thuộc tính</label>
                                        <div class="col-sm-5">
                                            <input name="attribute[${count}][0]" type="text" value="" class="form-control">

                                        </div>
                                        <div class="col-sm-2 form-check-inline">
                                            <input class="form-check-input" data-id="${count}" id="mutiple_checkbox_default_${count}" type="checkbox">
                                            <input id="value_mutiple_checkbox_default_${count}" name="is_mutiple[${count}][0]" type="hidden" value="1">
                                            <label class="form-check-label" for="">
                                                Mutiple
                                            </label>
                                        </div>
                                        <div class="col-sm-2 form-check-inline">
                                            <input class="form-check-input" data-id="${count}" id="required_checkbox_default_${count}" type="checkbox">
                                            <input id="value_required_checkbox_default_${count}" name="is_required[${count}][0]" type="hidden" value="1" name="">
                                            <label class="form-check-label" for="">
                                                Bắt buộc 
                                            </label>
                                        </div>
                                        <div class="col-sm-1">
                                            <button data-count-name="${count}" data-count="0" type="button" class="btn btn-primary btn-sm add_input_append"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `)

                $('#append_item').find('#mutiple_checkbox_default_' + count).click(function(){
                    var id_mutiple = $(this).attr('data-id')
                    if($(this).is(':checked')){
                        $('#append_item').find('#value_mutiple_checkbox_default_' + id_mutiple).val(0)
                    }else{
                        $('#append_item').find('#value_mutiple_checkbox_default_' + id_mutiple).val(1)
                    }
                })
                $('#append_item').find('#required_checkbox_default_' + count).click(function(){
                    var id_required = $(this).attr('data-id')
                    if($(this).is(':checked')){
                        $('#append_item').find('#value_required_checkbox_default_' + id_required).val(0)
                    }else{
                        $('#append_item').find('#value_required_checkbox_default_' + id_required).val(1)
                    }
                })


                $('#append_item').find('.add_input_append').click(function(){
                    let data_count_name = Number($(this).attr('data-count-name'))
                    let data_count_input_append = Number($(this).attr('data-count'))
                    $(this).parent().parent().parent().append(`
                    <div class="form-group row col-md-12">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Thuộc tính</label>
                        <div class="col-sm-5 ">
                            <input name="attribute[${data_count_name}][${data_count_input_append + 1}]" type="text" value="" class="form-control validate">
                        </div>
                        <div class="col-sm-2 form-check-inline">
                            <input class="form-check-input" id="mutiple_checkbox_default_${data_count_name}_${data_count_input_append + 1}" data-id="${data_count_name}_${data_count_input_append + 1}" type="checkbox">
                            <input id="value_mutiple_checkbox_default_${data_count_name}_${data_count_input_append + 1}" name="is_mutiple[${data_count_name}][${data_count_input_append + 1}]" type="hidden" value="1">
                            <label class="form-check-label" for="">
                                Mutiple
                            </label>
                        </div>
                        <div class="col-sm-2 form-check-inline">
                            <input class="form-check-input" id="required_checkbox_default_${data_count_name}_${data_count_input_append + 1}" data-id="${data_count_name}_${data_count_input_append + 1}" type="checkbox">
                            <input id="value_required_checkbox_default_${data_count_name}_${data_count_input_append + 1}" name="is_required[${data_count_name}][${data_count_input_append + 1}]" type="hidden" value="1">
                            <label class="form-check-label" for="">
                                Bắt buộc 
                            </label>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-danger btn-sm btn_remove_attribute"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                    `)
                    
                    $('#append_item').find('#mutiple_checkbox_default_' + data_count_name + '_' + Number(data_count_input_append + 1)).click(function(){
                        var data_check_id_mutiple = $(this).attr('data-id') 
                        if($(this).is(':checked')){
                            $('#append_item').find('#value_mutiple_checkbox_default_' + data_check_id_mutiple).val(0)
                        }else{
                            $('#append_item').find('#value_mutiple_checkbox_default_' + data_check_id_mutiple).val(1)
                        }
                    })
                    $('#append_item').find('#required_checkbox_default_' + data_count_name + '_' + Number(data_count_input_append + 1)).click(function(){
                        var data_check_id_required = $(this).attr('data-id') 
                        if($(this).is(':checked')){
                            $('#append_item').find('#value_required_checkbox_default_' + data_check_id_required).val(0)
                        }else{
                            $('#append_item').find('#value_required_checkbox_default_' + data_check_id_required).val(1)
                        }
                    })

                    $('#append_item').find('.btn_remove_attribute').click(function(){
                        $(this).parent().parent().remove()
                    })

                    $(this).attr('data-count', data_count_input_append + 1)
                })

                count ++;

                $('#append_item').find('.close_device_attr').click(function(){
                    $(this).parent().parent().remove()
                })
                
            })

            $('#mutiple_checkbox_default').click(function(){
                if($(this).is(':checked')){
                    $('#value_mutiple_checkbox_default').val(0)
                }else{
                    $('#value_mutiple_checkbox_default').val(1)
                }
            })
            $('#required_checkbox_default').click(function(){
                if($(this).is(':checked')){
                    $('#value_required_checkbox_default').val(0)
                }else{
                    $('#value_required_checkbox_default').val(1)
                }
            })

            $('.add_input').click(function(){
                let data_count = Number($(this).attr('data-count'));
                $('.append_input').append(`
                <div class="form-group row col-md-12">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Thuộc tính</label>
                    <div class="col-sm-5 ">
                        <input name="attribute[0][${data_count + 1}]" type="text" value="" class="form-control validate">
                    </div>
                    <div class="col-sm-2 form-check-inline">
                        <input class="form-check-input" id="mutiple_checkbox_default_${data_count}" type="checkbox">
                        <input id="value_mutiple_checkbox_default_${data_count}" name="is_mutiple[0][${data_count + 1}]" type="hidden" value="1">
                        <label class="form-check-label" for="">
                            Mutiple
                        </label>
                    </div>
                    <div class="col-sm-2 form-check-inline">
                        <input class="form-check-input" id="required_checkbox_default_${data_count}" type="checkbox">
                        <input id="value_required_checkbox_default_${data_count}" name="is_required[0][${data_count + 1}]" type="hidden" value="1">
                        <label class="form-check-label" for="">
                            Bắt buộc 
                        </label>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-danger btn-sm btn_remove_attribute"><i class="fa fa-close"></i></button>
                    </div>
                </div>
                `)
                $('.append_input').find('#mutiple_checkbox_default_' + data_count).click(function(){
                    if($(this).is(':checked')){
                        $('.append_input').find('#value_mutiple_checkbox_default_' + data_count).val(0)
                    }else{
                        $('.append_input').find('#value_mutiple_checkbox_default_' + data_count).val(1)
                    }
                })
                $('.append_input').find('#required_checkbox_default_' + data_count).click(function(){
                    if($(this).is(':checked')){
                        $('.append_input').find('#value_required_checkbox_default_' + data_count).val(0)
                    }else{
                        $('.append_input').find('#value_required_checkbox_default_' + data_count).val(1)
                    }
                })
                $('.append_input').find('.btn_remove_attribute').click(function(){
                    $(this).parent().parent().remove()
                })
                $(this).attr('data-count', data_count + 1)
            })

            $("#formSubmit").submit(function (e) {

                //stop submitting the form to see the disabled button effect
                $.each($('#append_item').find('.validate'), function(key, value){

                    if($(value).val() == ''){
                        e.preventDefault();
                        $('.error').find('.error-notification').remove()
                        $('.error').append(`
                            <p class="text-danger error-notification">Bạn không được bỏ trồng trường nào!</p>
                        `)
                    }
                })

                //disable the submit button
                $("#btn_submit").attr("disabled", true);

                return true;

            });
        })
    </script>
</section>
@endsection
