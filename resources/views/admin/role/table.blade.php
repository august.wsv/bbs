<div class="table-responsive list-records">
    <table class="table table-hover table-bordered">
        <colgroup>
            <col style="width: 50px">
            <col>
            <col style="width: 200px">
            <col style="width: 120px">
            <col>
            <col style="width: 150px">
            <col style="width: 100px">
        </colgroup>
        <thead>
        <th>
            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o "></i>
            </button>
        </th>
        <th>Tên admin
            {!! __admin_sortable('name') !!}
        </th>
        <th>Tên admin
            {!! __admin_sortable('username') !!}
        </th>
        <th>Quyền
            {!! __admin_sortable('admin_role') !!}
        </th>
        <th>Mô tả
            {!! __admin_sortable('admin_role') !!}
        </th>
        <th>Ngày tạo
            {!! __admin_sortable('created_at') !!}
        </th>
        <th class="text-center">Chức năng</th>
        </thead>
        <tbody>
        {{--@dd($records)--}}
        @foreach ($records as $record)
            <?php
            $editLink = route($resourceRoutesAlias . '.edit', $record->id);
            $deleteLink = route($resourceRoutesAlias . '.destroy', $record->id);
            $formId = 'formDeleteModel_' . $record->id;
            ?>
            <tr>
                <td><input type="checkbox" name="ids[]" value="{{ $record->id }}" class="square-blue chkDelete"></td>
                <td class="table-text">
                    {{ $record->name }}
                </td>
                <td class="table-text">
                    {{ $record->username }}
                </td>
                <td class="text-text">{{ $record->role_name }}</td>
                <td class="text-text">{{ $record->role_description }}</td>
                <td class="text-text">{{ $record->created_at ? $record->created_at->format(DATE_FORMAT) : '' }}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{ $editLink }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete {{ $record->id === \App\Facades\AuthAdmin::id() ? 'disabled' : '' }}"
                           data-form-id="{{ $formId }}"><i class="fa fa-trash-o"></i></a>
                    </div>

                    <!-- Delete Record Form -->
                    <form id="{{ $formId }}" action="{{ $deleteLink }}" method="POST"
                          style="display: none;" class="hidden form-inline">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
