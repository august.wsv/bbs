<div class="col-md-6">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Tên admin *</label>
                <input type="text" class="form-control" name="name" value="{{ old('name', $record->name) }}" required
                       placeholder="Nhập tên admin">

                @if ($errors->has('name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group margin-b-5 margin-t-5{{ $errors->has('admin_role') ? ' has-error' : '' }}">
                <label for="group_id">Phân quyền *</label>
                {{ Form::select('admin_role', ['' => 'Chọn quyền'] + ROLE_NAME, $record->admin_role ?? '', ['class' => 'form-control']) }}

                @if ($errors->has('admin_role'))
                    <span class="help-block">
                        <strong>{{ $errors->first('admin_role') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group margin-b-5 margin-t-5{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="name">Tên đăng nhập *</label>
                <input type="text" class="form-control" name="username" value="{{ old('username', $record->username) }}"
                       required placeholder="Nhập tên đăng nhập">

                @if ($errors->has('username'))
                    <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group margin-b-5 margin-t-5{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="name">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password từ 8 đến 20 kí tự"
                       />

                @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>
    </div>
</div>

