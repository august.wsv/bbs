<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>BBS | Page Not Found</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<link href="{{asset('/metronic/css/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('/metronic/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="{{asset('/img/jvb-logo.png')}}" />
        <style>
            .go-home{
                padding: 8px;
                border: 2px solid #000000;
                color: #000000;
                margin-left: 7.85rem;
                font-size: 2.5rem;
                font-weight: 600;
            }
            .go-home:hover{
                color: #000000;
                text-decoration: none;
            }
        </style>
	</head>
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid  m-error-3" style="background-image: url(../../../metronic/img/bg3.jpg);">
				<div class="m-error_container">
					<span class="m-error_number">
						<h1>404</h1>
					</span>
					<p class="m-error_title m--font-light">
						How did you get here
					</p>
					<p class="m-error_subtitle">
						Sorry we can't seem to find the page you're looking for.
					</p>
					<p class="m-error_description">
						There may be amisspelling in the URL entered,
						<br> or the page you are looking for may no longer exist.
                    </p>
                    <a href="/" class="go-home">GO HOME</a>
				</div>
			</div>
		</div>
		<script src="{{asset('/metronic/css/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('/metronic/css/style.bundle.js')}}" type="text/javascript"></script>
	</body>
</html>
