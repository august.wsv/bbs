<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>BBS | Page Not Found</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<link href="{{asset('/metronic/css/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('/metronic/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="{{asset('/img/jvb-logo.png')}}" />
        <style>
            .go-home{
				margin: auto;
                padding: 8px;
                border: 2px solid #000000;
                color: #000000;
                font-size: 2.5rem;
                font-weight: 600;
            }
            .go-home:hover{
                color: #000000;
                text-decoration: none;
            }
        </style>
	</head>
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid  m-error-6" style="background-image: url(../../../metronic/img/bg6.jpg);">
				<div class="m-error_container">
					<div class="m-error_subtitle m--font-light">
						<h1>Oops...</h1>
					</div>
					<p class="m-error_description m--font-light">
						Looks like something went wrong.
						<br> We're working on it
                    </p>
                    <a href="/" class="go-home">GO HOME</a>
				</div>
			</div>
		</div>
		<script src="{{asset('/metronic/css/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('/metronic/css/style.bundle.js')}}" type="text/javascript"></script>
	</body>
</html>
