<div class="box-footer clearfix">
    <div class="row">
        <div class="col-md-5">
            <div class="row pagination-row">
                
            </div>

        </div>
        <div class="col-md-7">
            <!-- Pagination -->
            <div class="pull-right">
                <div class="no-margin text-center">
                    {!! $records->render() !!}
                </div>
            </div>
            <!-- / End Pagination -->
        </div>
    </div>


</div>
<!-- /.box-footer -->
