@extends('layouts.end_user')
@section('page-title','Nhắc việc')
@section('content')
<style type="text/css">
    label {
        display: inline-block;
        width: 200px;
    }
    label.error {
        display: inline-block;
        color:red;
        width: 300px;
    }
    label #start_date-error{
        margin-top: 20px;
    }
    .dropdown-content {
        width: 768.021px;
        position: absolute;
        top: 0px;
        left: 0px;
        opacity: 1;
        display: block;
        height: 430px;
    }
</style>
<div class="row">
    <div class="col-12 col-xxl-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <form action="" class="form-inline" role="form">
                    <div class="form-group">
                        <a href="{{route('reminder_status')}}" style="border-radius: 100px;border:2px solid #efb1d1;" class="btn btn-primary fa  fa-check" title="Nhắc việc hoàn thành"></a>
                    </div>
                </form>
            </div>
            @if(Session::has('level'))
            <div class="col-md-6">
                <div class="alert alert-{{Session::get('level')}}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>{{Session::get('message')}}</strong>
                </div>
            </div>
            @endif
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Tên công việc</th>
                        <th class="text-center">Ngày nhắc việc</th>
                        <th class="text-center">Thời gian bắt đầu - Thời gian kết thúc</th>
                        <th class="text-center">Lặp lại(phút )</th>
                        <th class="text-center">Trạng thái</th>
                        <th class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($reminders) > 0)
                    @foreach($reminders as $reminder)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td class="text-center" width="250px">{!!$reminder->name!!}</td>
                        <td class="text-center">{{$reminder->start_date}}</td>
                        <td class="text-center">{{$reminder->start_loop}} | {{$reminder->end_loop}}</td>
                        <td class="text-center">
                            @if($reminder->option_loop == 5)
                            {{"5.min"}}
                            @elseif($reminder->option_loop == 15)
                            {{"15.min"}}
                            @elseif($reminder->option_loop == 30)
                            {{"30.min"}}
                            @elseif($reminder->option_loop == 60)
                            {{"60.min"}}
                            @endif
                        </td>
                        <td class="text-center">
                            @if($reminder->status == ACTIVE_STATUS)
                            {{"Hoàn thành!"}}
                            @else
                            {{"Chưa hoàn thành!"}}
                            @endif
                        </td>
                        <td class="text-center">
                            @if($reminder->route_detail_reminder == 'meetings')
                            <a href="{{route('reminder_detail', $reminder->id)}}" title="Xem chi tiết người tham gia"><i class="fa fa-eye "></i></a>
                            {!! csrf_field() !!}
                            {{method_field('DELETE')}}
                            <a href="{{route('reminder_delete',$reminder->id)}}" onclick="return confirm('Bạn có muốn xoá nhắc việc không?')" title="Xóa nhắc việc"><i class="fa fa-trash"></i></a>
                            @else
                            <a href="{{route('reminder_detail', $reminder->id)}}" title="Xem chi tiết người tham gia"><i class="fa fa-eye "></i></a>
                            <a href="{{route('reminder_edit',$reminder->id)}}" title="Cập nhật nhắc việc"><i class="fa fa-edit "></i></a>
                            {!! csrf_field() !!}
                            {{method_field('DELETE')}}
                            <a href="{{route('reminder_delete',$reminder->id)}}" onclick="return confirm('Bạn có muốn xoá nhắc việc không?')" title="Xóa nhắc việc"><i class="fa fa-trash"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <h3 style="color: red;">Không có bản ghi nào trong cơ sở dữ liệu!</h3>
                    @endif
                </tbody>
            </table>
            <div class="text-left">
                {{$reminders->links()}}
            </div>
            <form action="{{route('reminder_store')}}" id="form_reminder" method="POST">
                @csrf
                <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header text-center">
                                <h4 class="modal-title w-100 font-weight-bold">TẠO NHẮC VIỆC</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <input type="text" name="name" class="form-control" value="{!!old('name')!!}" placeholder="Tên nhắc việc">
                                </div>
                                <div class="row mt-1">
                                    <div class="col-12 users_id">
                                        <select class="mdb-select md-form" multiple searchable="Người làm cùng ..." name="user_id[]">
                                            <option disabled>Thành viên tham gia</option>
                                            @foreach($receivers as $groupName => $users)
                                            <optgroup label="{{$groupName}}">
                                                @foreach($users as $user)
                                                <option value="{{$user['id']}}"
                                                data-icon="{{asset_image($user['avatar'])}}"
                                                >{{$user['name']}}</option>
                                                @endforeach
                                            </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    {{-- start_date --}}
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input placeholder="Giờ bắt đầu lặp" type="text" id="input_starttime" name="start_loop" value="{{ old('start_loop') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input placeholder="Giờ kết thúc lặp" type="text" id="input_endtime" name="end_loop" value="{{ old('end_loop') }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2" >
                                        <h4 class="text-left" style="font-size: 14px;font-weight: bold;margin-top: 7px;">Ngày lặp:</h4>
                                    </div>
                                    <div class="col-md-8 text-left">
                                        <div class="form-group">
                                            <div class="input-group date">
                                                <input type="date" class="form-control pull-right" autocomplete="off"
                                                name="start_date"
                                                value="{!!old('end_date')!!}" id="start_date">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <h4 style="font-size: 14px;font-weight: bold;margin-top: 7px;">Lặp lại:</h4>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="materialInline2" name="option_loop"
                                                    value="5" checked>
                                                    <label class="form-check-label" for="materialInline2">5.min</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="materialInline3" name="option_loop"
                                                    value="15">
                                                    <label class="form-check-label" for="materialInline3">15.min</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <!-- Material inline 4 -->
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="materialInline4" name="option_loop"
                                                    value="30">
                                                    <label class="form-check-label" for="materialInline4">30.min</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="materialInline5" name="option_loop"
                                                    value="60">
                                                    <label class="form-check-label" for="materialInline5">60.min</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <lable class="ml-0" style="font-size: 14px;font-weight: bold;">Nội dung: </lable>
                                        <textarea class="form-control" name="content" style="height: 150px;" placeholder="Nội dung nhắc việc..."></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-deep-orange">Lưu lại</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="text-right bottom-right">
                <a href="#" class="btn-floating btn-lg red waves-effect waves-light text-white mb-4" data-toggle="modal" title="Tạo nhắc việc" data-target="#modalRegisterForm"><i class="fas fa-pencil-alt"></i></a>
            </div>
        </div>
    </div>
</div>

@endsection
@push('extend-js')
<script src="js/jquery.validate.min.js"></script>
<script>
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });
</script>

<script>
    $('#input_starttime').pickatime({
        // Light or Dark theme
        darktheme: true,
    });
    $('#input_endtime').pickatime({
        darktheme: true,
    });
</script>

<script>
    $(function () {
        $("#form_reminder").validate({
            lang: 'ja',
            rules: {
                "name": {
                    required: true,
                    maxlength: 255,
                    minlength: 5,
                },
                "start_date": {
                    required: true,
                },
                "start_loop": {
                    required: true,
                },
                "end_loop": {
                    required: true,
                },
                "content": {
                    required: true,
                },
            },
            messages: {
                "name": {
                    required: '<?php echo __("Trường tên nhắc việc không được để trống!") ?>',
                    maxlength: '<?php echo __("Trường tên quá dài!") ?>',
                    minlength: '<?php echo __("Trường tên quá ngắn!") ?>',
                },
                "start_date": {
                    required: '<?php echo __("Vui lòng chọn ngày lặp!") ?>'
                },
                "start_loop": {
                    required: '<?php echo __("Vui lòng chọn thời gian bắt đầu lặp!") ?>'
                },
                "end_loop": {
                    required: '<?php echo __("Vui lòng chọn thời gian kết thúc lặp!") ?>'
                },
                "content": {
                    required: '<?php echo __("Vui lòng nhập nội dung nhắc việc!") ?>'
                },
            }
        });
    });
</script>
@endpush


