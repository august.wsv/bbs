@extends('layouts.end_user')
@section('page-title','Nhắc việc')
@section('content')

<div class="row">
    <div class="col-12">
     <div class="panel panel-primary">
        <div class="panel-heading">
        </div>
        <div class="panel-body">
           <form action="" class="form-inline"  role="form">
            <div class="form-group">

                <a href="{{route('reminder_index')}}" style="border-radius: 100px;border:2px solid #efb1d1;" class="btn btn-success fa  fa-list" title="Nhắc việc chưa hoàn thành!"></a>

            </div>
        </form>
    </div>
    @if(Session::has('level'))
    <div class="col-md-6">
        <div class="alert alert-{{Session::get('level')}}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{Session::get('message')}}</strong>
        </div>
    </div>
    @endif
    <table class="table table-hover">
        <thead>
            <tr>
                <th class="text-center">STT</th>
                <th class="text-center">Tên công việc</th>
                <th class="text-center">Ngày nhắc việc</th>
                <th class="text-center">Thời gian bắt đầu - Thời gian kết thúc</th>
                <th class="text-center">Lặp lại (phút )</th>
                <th class="text-center">Trạng thái</th>
                <th class="text-center"></th>
            </tr>
        </thead>
        <tbody>
            @if(count($successfulls) > 0)
            @foreach($successfulls as $successfull)
            <tr>
                <td class="text-center">{{$successfull->id}}</td>
                <td class="text-center">{!!$successfull->name!!}</td>
                <td class="text-center">{{$successfull->start_date}}</td>
                <td class="text-center">{{$successfull->start_loop}} | {{$successfull->end_loop}}</td>
                <td class="text-center">
                    @if($successfull->option_loop == 5)
                    {{"5.min"}}
                    @elseif($successfull->option_loop == 15)
                    {{"15.min"}}
                    @elseif($successfull->option_loop == 30)
                    {{"30.min"}}
                    @elseif($successfull->option_loop == 60)
                    {{"60.min"}}   
                    @endif
                </td>
                <td class="text-center">
                    @if($successfull->status == ACTIVE_STATUS)
                    {{"Hoàn thành!"}}
                    @else
                    {{"Chưa hoàn thành!"}}
                    @endif
                </td>
                <td class="text-center">
                    <a href="{{route('reminder_detail', $successfull->id)}}" title="Xem chi tiết người tham gia"><i class="fa fa-eye "></i></a>
                </td>
            </tr>
            @endforeach
            @else
            <h3 style="color: red; margin-top:60px;">Không có bản ghi nào ở trạng thái hoàn thành!</h3>
            @endif

        </tbody>
    </table>
    <div class="text-center">
        {{$successfulls->links()}}
    </div>
</div>
</div>
</div>

@endsection