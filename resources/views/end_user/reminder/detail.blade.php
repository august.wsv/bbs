@extends('layouts.end_user')
@section('page-title','Nhắc việc')

@section('content')

<div class="row">
    <div class="col-12 col-xxl-10">
        <div class="panel panel-primary">
            <div>
                <div class="card text-center">
                  <div class="card-header">
                    <h3 class="font-weight-bold">Chi tiết nhắc việc: {{$count}} thành viên.</h3>
                </div>
                <div class="card-body">
                    <h5 class="card-title">{!!$reminder->name!!}</h5>
                    <p class="card-text">{!!$reminder->content!!}</p>
                    <p class="card-text">Ngày nhắc việc: {{$reminder->start_date}}</p>
                    <p class="card-text">Bắt đầu lặp: {{$reminder->start_loop}} || Kết thúc lặp: {{$reminder->end_loop}}</b></p>
                    <!-- Section: Team v.1 -->
                    <section class="team-section text-center my-5">
                        <!-- Grid row -->
                        <div class="row">
                            @if(count($reminder_users) > 0)
                            @foreach($reminder_users as $item)
                            <!-- Grid column -->
                            <div class="col-lg-3 col-md-6 mb-lg-0 mb-5">
                                <div class="avatar mx-auto">
                                    <img src="{{asset('img/jvb-logo.png')}}" class="rounded-circle z-depth-1"
                                    alt="Sample avatar" width="50px">
                                </div>
                                <h5 class="font-weight-bold mt-4 mb-3">{!!$item->name!!}</h5>
                                <p class="text-uppercase blue-text"><strong>{{$item->staff_code}}</strong></p>
                                <p class="grey-text">{{$item->email}}</p>
                            </div>
                            <!-- Grid column -->
                            @endforeach
                            @else
                            <h3 style="color: red;">Không có bản ghi nào trong cơ sở dữ liệu!</h3>
                            @endif
                        </div>
                        <!-- Grid row -->
                    </section>
                    <!-- Section: Team v.1 -->
                </div>
                <div class="card-footer text-muted">
                    <a href="{{route('reminder_index')}}" class="btn btn-primary fa  fa-list"> Danh sách</a>    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
