@extends('layouts.end_user')
@section('page-title', 'Cập nhật nhắc việc')
@section('content')
<form action="{{route('reminder_update',$reminder->id)}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12 text-right">                        
                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
            </div>
            <label for="choose_week">Tên công việc:</label>
            <div class="md-form mt-1 mb-0">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" name="name" placeholder="Nhập vào tên công việc" value="{!!old('name',isset($reminder)? $reminder->name:null)!!}">
                    </div>

                    <div class="col-md-6">
                        <h4 class="text-center">Lựa chọn lặp nhắc việc:</h4>
                        <!-- Material inline 2 -->
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="materialInline2" name="option_loop"
                            value="5" checked>
                            <label class="form-check-label" for="materialInline2">5.min</label>
                        </div>

                        <!-- Material inline 3 -->
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="materialInline3" name="option_loop"
                            value="15">
                            <label class="form-check-label" for="materialInline3">15.min</label>
                        </div>

                        <!-- Material inline 4 -->
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="materialInline4" name="option_loop"
                            value="30">
                            <label class="form-check-label" for="materialInline4">30.min</label>
                        </div>

                        <!-- Material inline 5 -->
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" id="materialInline5" name="option_loop"
                            value="60">
                            <label class="form-check-label" for="materialInline5">60.min</label>
                        </div>
                    </div>
                </div>
            </div>
            {{-- thoi gian --}}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group margin-b-5 margin-t-5">
                        <label for="start_date">Ngày nhắc việc *</label>
                        <div class="input-group date">
                            <input type="date" class="form-control pull-right datepicker" autocomplete="off"
                            name="start_date"
                            value="{{$reminder->start_date}}" id="start_date">
                        </div>
                    </div>
                </div>
                {{-- start_date --}}
                <div class="col-md-4">
                    <div class="form-group margin-b-5 margin-t-5{{ $errors->has('start_loop') ? ' has-error' : '' }}">
                        <label for="start_loop">Giờ bắt đầu lặp *</label>
                        <div class="input-group date">
                            <input type="time" class="form-control pull-right" autocomplete="off"
                            name="start_loop"
                            value="{{$reminder->start_loop}}"
                            id="start_loop">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group margin-b-5 margin-t-5{{ $errors->has('end_loop') ? ' has-error' : '' }}">
                        <label for="end_loop">Giờ kết thúc lặp *</label>
                        <div class="input-group date">

                            <input type="time" class="form-control pull-right"
                            name="end_loop" autocomplete="off"
                            value="{{$reminder->end_loop}}"
                            id="end_loop">
                        </div>
                    </div>
                </div>   
            </div>
            <div class="col-md-6">
                <h4 class="text-left">Trạng thái công việc</h4>
                <!-- Material inline 1 -->
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input" id="status1" name="status"
                    value="1" >
                    <label class="form-check-label" for="status1">Hoàn thành!</label>
                </div>

                <!-- Material inline 2 -->
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input" id="status2" name="status"
                    value="0" checked>
                    <label class="form-check-label" for="status2">Chưa hoàn thành!</label>
                </div>
            </div>
            {{-- thoi gian --}}
            <label class="mt-3" for="content">Nội dung công việc *</label>
            <div class="md-form mt-1 mb-0">
                <i class="fas fa-pencil-alt prefix grey-text"></i>
                <textarea type="text" id="content" name="content"
                class="md-textarea form-control">{{old('content', $reminder->content)}}</textarea>
            </div>                        
            
        </div>
    </div>
</form>

@endsection

@push('extend-js')
<script src="{{asset_ver('js/tinymce/tinymce.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });
    tinymce.init({
        selector: 'textarea',
        paste_data_images: true,
        height: '350px',
        plugins: [
        "advlist autolink lists charmap preview hr anchor pagebreak",
        ],
    });
</script>

<script>
    $(function () {
        myDatePicker($("#work_day"));
        $("#start_loop, #end_loop").timepicker({
            format: 'LT',
            showMeridian: false
        });
    })
</script>
@endpush
