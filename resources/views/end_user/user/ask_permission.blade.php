@extends('layouts.end_user')
@section('page-title', __l('ask_permission'))
@section('breadcrumbs')
    {!! Breadcrumbs::render('ask_permission') !!}
@endsection
@section('content')
    <style>
        .md-form {
            margin-top: 0.1rem;
            margin-bottom: 1.6rem;
        }
    </style>
    @if (session()->has('create_permission_success'))
        @if (session()->get('day_off_success') != '')
            <script>
                swal({
                    title: "Thông báo!",
                    text: "Bạn đã gửi đơn thành công!",
                    icon: "success",
                    button: "Đóng",
                });
            </script>
        @else
            <script>
                swal({
                    title: "Thông báo!",
                    text: "Bạn đã sửa đơn thành công!",
                    icon: "success",
                    button: "Đóng",
                });
            </script>
        @endif
        @if (session()->get('create_permission_success') != '')
            <script>
                swal({
                    title: "Thông báo!",
                    text: "Bạn đã gửi đơn thành công!",
                    icon: "success",
                    button: "Đóng",
                });
            </script>
        @else
            <script>
                swal({
                    title: "Thông báo!",
                    text: "Bạn đã sửa đơn thành công!",
                    icon: "success",
                    button: "Đóng",
                });
            </script>
        @endif
    @endif
    @if (session()->has('permission_error'))
        <script>
            swal({
                title: "Thông báo!",
                text: "Đơn đã được phê duyệt!",
                icon: "error",
                button: "Đóng",
            });
        </script>
    @endif
    @if (session()->has('approver_success') || session()->has('reject_success'))
        <script>
            swal({
                title: "Thông báo!",
                text: "Bạn đã duyệt đơn thành công!",
                icon: "success",
                button: "Đóng",
            });
        </script>
    @endif
    @if (session()->has('multiApprover_successully'))
        <script>
            swal({
                title: "Thành công!",
                text: "",
                icon: "success",
                button: "Đóng",
            });
        </script>
    @endif
    @if ($errors->has('work_day'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('work_day') }}</strong>
        </span>
        <br>
    @endif
    @if ($errors->has('id'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('id') }}</strong>
        </span>
        <br>
    @endif
    @if ($errors->has('approve_type'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('approve_type') }}</strong>
        </span>
        <br>
    @endif
    @if ($errors->has('type'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('type') }}</strong>
        </span>
        <br>
    @endif
    @if ($errors->has('permission_type'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('permission_type') }}</strong>
        </span>
        <br>
    @endif
    @if ($errors->has('status'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('status') }}</strong>
        </span>
        <br>
    @endif
    @if ($errors->has('creator_id'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('creator_id') }}</strong>
        </span>
        <br>
    @endif
    @if ($errors->has('reason_reject'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('reason_reject') }}</strong>
        </span>
        <br>
    @endif
    @if ($errors->has('project_id'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('project_id') }}</strong>
        </span>
        <br>
    @endif
    @if ($errors->has('work_time_explanation_id'))
        <span class="help-block mb-5 color-red">
            <strong>{{ $errors->first('work_time_explanation_id') }}</strong>
        </span>
        <br>
    @endif

    @cannot('master')
        <div class="row">
            <div class="d-block d-sm-none col-xxl-7 col-12 text-xl-right" id="btn-group-ask">
                <button onclick="location.href='{{ route('day_off') }}?t=1'"
                    class="btn btn-success no-box-shadow waves-effect waves-light mr-0 mr-sm-3 btn-with" id="btn-off">
                    Xin nghỉ phép
                </button>
                <button type="button" class="btn btn-primary no-box-shadow approve-btn-ot waves-effect waves-light btn-with"
                    id="btn-late-ot">
                    Xin OT
                </button>
                <button type="button" class="approve-btn-early btn btn-warning no-box-shadow waves-light btn-with"
                    id="btn-early">
                    Xin về sớm
                </button>
                <button type="button" class="approve-btn-late btn btn-danger no-box-shadow waves-light btn-with"
                    id="btn-late">
                    Xin đi muộn
                </button>
            </div>
        </div>
    @endcan

    <div class="row mb-3 mt-3">
        <div class="col-md-4">
            @can('team-leader')
                <h2 class="mobile-font-17 mb-0">Danh sách xin phép</h2>
            @endcan
        </div>
    </div>

    @can('team-leader')
        @if ($managerApproveOther || $managerApproveOT)
            <!-- Nav tabs -->
            <ul class="nav nav-tabs md-tabs nav-justified primary-color m-0" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active tab-nav-link-ot btn-ask" data-toggle="tab" href="#panelAbsence" role="tab">Xin
                        nghỉ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn-ot-one" data-toggle="tab" href="#panelApprove" role="tab">Xin đi
                        muộn/sớm</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link tab-nav-link-ot btn-ask" data-toggle="tab" href="#panelOT" role="tab">Xin
                        OT</a>
                </li>
            </ul>
            <!-- Nav tabs -->

            <!-- Tab panels -->
            <div class="tab-content mt-2 p-0">

                <!-- Panel 1 -->
                <div class="tab-pane fade in show active" id="panelAbsence" role="tabpanel">
                    <div class="row mb-2">
                        <div class="col-12 col-sm-9 offset-md-4 col-md-6 col-lg-5 offset-lg-5 col-xl-auto ml-xl-auto">
                            <form action="{{route('get_day_off')}}" class="d-flex align-items-center justify-content-between" id="search-day-off-form">
                                <div class="mr-1 mr-md-3">
                                    <div class="d-md-flex justify-content-center">
                                        {{-- <div class="d-none d-md-block">
                                            <label class="text-w-400 pt-2" for="">Từ ngày</label>
                                        </div> --}}
                                        <div class="position-relative">
                                            <input type="text"
                                                   class="form-control z- search-date"
                                                   id="search_start_at" autocomplete="off" name="search_start_at">
                                            <i class="far fa-calendar-alt position-absolute calendar-search"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="mr-1 mr-md-3" style="font-weight: 400">Đến</div>
                                <div class="mr-1 mr-md-3">
                                    <div class="d-md-flex justify-content-center">
                                        {{-- <div class="d-none d-md-block">
                                            <label class="text-w-400 pt-2 label-from-days" for="inputZip">Tới ngày</label>
                                        </div> --}}
                                        <div class="position-relative">
                                            <input type="text"
                                                   class="form-control search-date"
                                                   id="search_end_at" autocomplete="off" name="search_end_at">
                                            <i class="far fa-calendar-alt position-absolute calendar-search"></i>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="d-md-flex justify-content-center">
                                        <div>
                                            <button class="form-control select-item border-0 btn-secondary">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 col-sm-3 col-md-2 col-xl-auto">{{ Form::select('status', SHOW_DAY_OFFF, $status ?? ALL_DAY_OFF, ['class' => 'browser-default custom-select search-day-off border-radius-1 option-select float-right']) }}</div>
                    </div>
                    <table id="tablePreview" class="table table-striped table-bordered pt-3 mt-3 dataTable no-footer table-head-sticky" style="width: 100%; --top: 10px;">
                        <!--Table head-->
                        <thead class="grey lighten-2">
                            <tr>
                                <th class="text-center d-none d-lg-table-cell stt" style="width: 10px">STT</th>
                                <th class="text-center" style="">Tên nhân viên</th>
                                <th class="text-center d-none d-xl-table-cell date-col">Group</th>
                                <th class="text-center date-col" style="min-width: 100px">Ngày nghỉ</th>
                                <th class="text-center reason-col">Lý do</th>
                                <th class="text-center d-none d-lg-table-cell">Dự án</th>
                                <th class="text-center d-none d-md-table-cell w-0">Phê duyệt</th>
                                <th class="text-center w-0">Xem thêm</th>
                            </tr>
                        </thead>
                        <!--Table head-->
                        <?php $increment = 1; ?>
                        <!--Table body-->
                        <tbody id="ajax-show">
                            @foreach ($dataDayOff as $keys => $record)
                                <tr id="rowApprove{{ $loop->index + 1 }}" data-id="{{ $record->id }}"
                                    data-number-off="{{ $record->absent }}" data-number-off-remain="">

                                    <td class="text-left d-none d-lg-table-cell">
                                        {{ $loop->index + 1 }}
                                    </td>

                                    <td class="text-left">
                                        {{ $record->user->name }}
                                    </td>

                                    <td class="text-center d-none d-xl-table-cell">
                                        {{ optional($record->user->group())->name }}
                                    </td>

                                    <td class="text-right">
                                        {{ (new \Carbon\Carbon($record->start_at))->format('d-m-Y') }}
                                    </td>

                                    <td class="text-left truncate">
                                        {{ $record->reason }}
                                    </td>

                                    <td class="text-left d-none d-lg-table-cell aTagGroup">
                                        @php $firstProject = true @endphp
                                        @foreach ($record->user->projects as $project)
                                            {!! !$firstProject ? ",&nbsp;": $firstProject = false !!}
                                            <a href="{{ route('project_detail', ['id' => $project->id]) }}">
                                                {{ $project->name }}
                                            </a>
                                        @endforeach
                                    </td>

                                    <td class="text-center p-0 d-none d-md-table-cell status-approve-icon"
                                        style="vertical-align: middle;">
                                        @if ($record->status == STATUS_DAY_OFF['abide'])
                                        <i data-toggle="tooltip" data-placement="right" title="Chờ phê duyệt" class="fas fa-meh-blank fa-2x text-warning text-center"></i>
                                        @elseif($record->status == STATUS_DAY_OFF['active'])
                                            <i data-toggle="tooltip" data-placement="right" title="Đã duyệt đơn"
                                                class="fas fa-grin-stars fa-2x text-success"></i>
                                        @else
                                            <i data-toggle="tooltip" data-placement="right" title="Không duyệt"
                                                class="fas fa-frown fa-2x text-danger"></i>
                                        @endif
                                    </td>

                                    <td class="text-center">
                                        <a href="{{route('day_off_approval') . '#' . $record->id}}" class="btn btn-link btn-px-0 btn-panelAbsence">Chi tiết >></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                        <!--Table body-->
                    </table>
                    <br>
                    <br>
                </div>
                <!-- Panel 1 -->

                <!-- Panel 2 -->
                <div class="tab-pane fade in btn-small-ot" id="panelApprove" role="tabpanel">
                    <div class="input-group">
                        <select id="GroupFilterOther" class="col-md-3 form-control browser-default GroupFilterOther">
                            <option value="" selected>Chọn Group</option>
                            @foreach ($groups as $row)
                                <option value="{{ $row->name }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <table class="contactTbl table table-striped table-bordered table-head-sticky" id="tableOther">
                        <colgroup>
                            <col class=" id-col">
                            <col class="day-col-manager">
                            <col class="name-col">
                            <col class="day-col-manager">
                            <col class="reason-col">
                            <col>
                            <col>
                        </colgroup>
                        <thead class="grey lighten-2">
                            <tr>
                                <th class=" sorting_disabled nosort text-center d-none d-md-table-cell">#</th>
                                <th class="text-center mb-with-32">Ngày</th>
                                <th class="table-with-42">Tên nhân viên</th>
                                <th>Hình thức</th>
                                <th class="d-none d-md-table-cell">Nội dung</th>
                                <th class="d-none d-md-table-cell">Nội dung phản hồi</th>
                                <th class="text-center" style="width: 10%;">Trạng Thái</th>
                            </tr>
                        </thead>
                        <?php $increment = 1; ?>
                        <tbody>
                            @foreach ($managerApproveOther as $item)
                                <tr class="{!! $item->groups !!}">
                                    <th class="text-center d-none d-md-table-cell" style="padding: 15px">{{ $increment++ }}
                                    </th>
                                    <th class="text-center" style="padding: 15px">{{ $item['work_day'] ?? '' }}</th>
                                    <th class="table-with-42">{{ $item->creator->name ?? '' }}</th>
                                    <td>
                                        @if ($item->type == 1)
                                            Đi muộn
                                            {{ empty($item->hours_permission) ? '' : '(' . $item->hours_permission . ')' }}
                                        @elseif($item->type == 2)
                                            Về sớm
                                            {{ empty($item->hours_permission) ? '' : '(' . $item->hours_permission . ')' }}
                                        @endif
                                    </td>
                                    <td class="d-none d-md-table-cell">{!! $item['note'] !!}</td>
                                    <td class="d-none d-md-table-cell" id="td-note_respond-{{ $item['id'] }}">
                                        {!! $item['reason_reject'] !!}</td>
                                    <td class="text-center td-approve" id="td-approve-{{ $item['id'] }}">
                                        @can('manager')
                                            @if ($item['status'] == array_search('Chưa duyệt', OT_STATUS))
                                                <button class="btn btn-info text-uppercase text-center approve"
                                                    id="ask-permission-{{ $item['id'] }}" data-permission="other"
                                                    data-id="{{ $item['id'] ? $item['id'] : '' }}"
                                                    data-itemType="@if ($item->type == 1) Duyệt đơn xin đi muộn @elseif($item->type == 2) Duyệt đơn xin về sớm @endif">
                                                    Duyệt
                                                </button>
                                            @elseif($item['status'] == array_search('Đã duyệt', OT_STATUS))
                                                <i class="fas fa-grin-stars fa-2x text-success"
                                                    title="{{ $item->workTimeApprover->name ?? '' }}"></i>
                                            @elseif($item['status'] == array_search('Từ chối', OT_STATUS))
                                                <i class="fas fa-frown fa-2x text-danger"
                                                    title="{{ $item->workTimeApprover->name ?? '' }}"></i>
                                            @endif
                                        @elsecan('team-leader')
                                            @if ($item['status'] == array_search('Đã duyệt', OT_STATUS))
                                                <i class="fas fa-grin-stars fa-2x text-success"
                                                    title="{{ $item->workTimeApprover->name ?? '' }}"></i>
                                            @elseif($item['status'] == array_search('Chưa duyệt', OT_STATUS))
                                                <i class="fas fa-meh-blank fa-2x text-warning" title="Chưa duyệt"></i>
                                            @elseif($item['status'] == array_search('Từ chối', OT_STATUS))
                                                <i class="fas fa-frown fa-2x text-danger"
                                                    title="{{ $item->workTimeApprover->name ?? '' }}"></i>
                                            @endif
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                </div>
                <!-- Panel 2 -->

                <!-- Panel 3 -->
                <div class="tab-pane fade in btn-small-ask" id="panelOT" role="tabpanel">
                    @can('team-leader')
                        <div id="zoneApproveButton">
                            <button type="button" class="btn btn-primary" id="btn-option-yes">ĐỒNG Ý</button>
                            <button type="button" class="btn btn-danger" id="btn-option-no">TỪ CHỐI</button>
                            <button type="button" class="btn btn-warning" id="btn-option-assgin">ỦY QUYỀN PHÊ DUYỆT
                            </button>
                        </div>
                    @endcan
                    <div class="input-group">
                        <select id="GroupFilterOt" class="col-md-3 form-control browser-default">
                            <option value="" selected>Chọn Group</option>
                            @foreach ($groups as $row)
                                <option value="{{ $row->name }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <form action="" method="post" id="form-option">
                        @csrf
                        <table class="contactTbl table table-striped table-bordered pt-3 mt-3" id="tableOt">
                            <colgroup>
                                <col class="id-col-manager">
                                <col class="day-col">
                                <col class="status-col">
                                <col class="status-col">
                                <col class="reason-col">
                                <col class="reason-col">
                                <col>
                            </colgroup>
                            <thead class="grey lighten-2">
                                <tr>
                                    @can('team-leader')
                                        <th class="sorting_disabled nosort text-center d-md-table-cell">
                                            <button type="button" class="m-0 p-0 btn btn-light btn-sm checkbox-toggle"><i
                                                    id="check-all" class="far fa-2x fa-square"></i></button>

                                        </th>
                                    @else
                                        <th class="sorting_disabled nosort text-center d-md-table-cell">#</th>
                                    @endcan
                                    <th class="text-center mb-with-32" style="padding: 15px">Thời gian</th>
                                    <th class="table-with-42">Tên nhân viên</th>
                                    <th class="d-none d-md-table-cell">Dự án</th>
                                    <th class="d-none d-md-table-cell">Nội dung</th>
                                    <th class="d-none d-md-table-cell">Nội dung phản hồi</th>
                                    <th class="text-center" style="width: 10%;">Trạng Thái</th>
                                </tr>
                            </thead>
                            <?php $increment = 1; ?>
                            <tbody>
                                @foreach ($managerApproveOT as $item)
                                    <!-- ## -->
                                    <tr class="{!! $item->groups !!}">
                                        @can('team-leader')
                                            <td class="list-records">
                                                <div class="form-check @if (
                                                    $item['status'] != DEFAULT_VALUE ||
                                                        (\Illuminate\Support\Facades\Auth::user()->isTeamLeader() &&
                                                            $item['assign_id'] !== \Illuminate\Support\Facades\Auth::id())) hidden @endif">
                                                    <input type="checkbox" class="form-check-input"
                                                        id="chkMult-{{ $item['id'] }}" name="ids[]"
                                                        value="{{ $item['id'] }}">
                                                    <label class="form-check-label" for="chkMult-{{ $item['id'] }}"></label>
                                                </div>
                                            </td>
                                        @else
                                            <th class="text-center d-none d-md-table-cell" style="padding: 15px">
                                                {{ $increment++ }}
                                            </th>
                                        @endcan

                                        <th class="text-left">{{ $item['work_day'] }} <br>
                                            {{ $item->description_time }}</th>
                                        <th class="table-with-42">{{ $item->creator->name ?? '' }}</th>
                                        <td class="d-none d-md-table-cell">
                                            {{ $item->project_name ?? ($item->project->name ?? '') }}
                                        </td>
                                        <td class="d-none d-md-table-cell">{!! $item['reason'] !!}</td>
                                        <td class="d-none d-md-table-cell" id="td-note_respond-ot-{{ $item['id'] }}">
                                            {!! $item['note_respond'] !!}</td>
                                        <td class="text-center td-approve" id="td-approve-ot-{{ $item['id'] }}">
                                            @can('manager')
                                                @if ($item['status'] == array_search('Chưa duyệt', OT_STATUS))
                                                    <p @if ($item['assign_id'] != 0 && $item->authorizer) title="Đã được ủy quyền cho {{ $item->authorizer->name }} xét duyệt"
                                                        class="btn btn-warning text-uppercase text-center approve"
                                                        @else
                                                        class="btn btn-info text-uppercase text-center approve" @endif
                                                        data-permission="ot" id="ot-{{ $item['id'] }}"
                                                        data-id="{{ $item['id'] ? $item['id'] : '' }}"
                                                        data-itemtype="{{ $item->ot_type ? ($item->ot_type == array_search('Dự án', OT_TYPE) ? 'OT Dự án' : 'OT cá nhân') : '' }}">
                                                        Duyệt
                                                    </p>
                                                @elseif($item['status'] == array_search('Đã duyệt', OT_STATUS))
                                                    <i class="fas fa-grin-stars fa-2x text-success"
                                                        title="{{ $item->approver->name ?? '' }}"></i>
                                                @elseif($item['status'] == array_search('Từ chối', OT_STATUS))
                                                    <i class="fas fa-frown fa-2x text-danger"
                                                        title="{{ $item->approver->name ?? '' }}"></i>
                                                @else
                                                @endif
                                            @elsecan('team-leader')
                                                @if (
                                                    $item['status'] == array_search('Chưa duyệt', OT_STATUS) &&
                                                        $item['assign_id'] == \Illuminate\Support\Facades\Auth::id())
                                                    <p class="btn btn-info text-uppercase text-center approve"
                                                        data-permission="ot" id="ot-{{ $item['id'] }}"
                                                        data-id="{{ $item['id'] ? $item['id'] : '' }}"
                                                        data-itemtype="{{ $item->ot_type ? ($item->ot_type == array_search('Dự án', OT_TYPE) ? 'OT Dự án' : 'OT cá nhân') : '' }}">
                                                        Duyệt
                                                    </p>
                                                @elseif($item['status'] == array_search('Đã duyệt', OT_STATUS))
                                                    <i class="fas fa-grin-stars fa-2x text-success"
                                                        title="{{ $item->approver->name ?? '' }}"></i>
                                                @elseif($item['status'] == array_search('Chưa duyệt', OT_STATUS))
                                                    <i class="fas fa-meh-blank fa-2x text-warning" title="Chưa duyệt"></i>
                                                @elseif($item['status'] == array_search('Từ chối', OT_STATUS))
                                                    <i class="fas fa-frown fa-2x text-danger"
                                                        title="{{ $item->approver->name ?? '' }}"></i>
                                                @endif
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </form>

                    <br>

                </div>
                <!-- Panel 3 -->
            </div>
            <!-- Tab panels -->
        @endif
    @endcan
    @cannot('master')
        <div class="row m-t-20 mb-3">
            <div class="col-xl-5 col-12">
                <h2 class="mb-2 mt-2">Xin phép cá nhân</h2>
            </div>
            <div class="d-none d-sm-block col-xxl-7 col-12 text-xl-right" id="btn-group-ask">
                <button onclick="location.href='{{ route('day_off') }}?t=1'"
                    class="btn btn-success no-box-shadow waves-effect waves-light mr-0 mr-sm-3 btn-with" id="btn-off">
                    Xin nghỉ phép
                </button>
                <button type="button" class="btn btn-primary no-box-shadow approve-btn-ot waves-effect waves-light btn-with"
                    id="btn-late-ot">
                    Xin OT
                </button>
                <button type="button" class="approve-btn-early btn btn-warning no-box-shadow waves-light btn-with"
                    id="btn-early">
                    Xin về sớm
                </button>
                <button type="button" class="approve-btn-late btn btn-danger no-box-shadow waves-light btn-with"
                    id="btn-late">
                    Xin đi muộn
                </button>
            </div>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs md-tabs nav-justified primary-color m-0" role="tablist">
            <li class="nav-item">
                <a class="nav-link active btn-ot-one" data-toggle="tab" href="#panel555" role="tab">Xin đi muộn/sớm</a>
            </li>
            <li class="nav-item">
                <a class="nav-link tab-nav-link-ot btn-ask" data-toggle="tab" href="#panel666" role="tab">Xin OT</a>
            </li>
        </ul>
        <!-- Nav tabs -->

        <!-- Tab panels -->
        <div class="tab-content mt-2 p-0">
            <!-- Panel 1 -->
            <div class="tab-pane fade in show active btn-small-ot" id="panel555" role="tabpanel">
                <table class="contactTbl table table-striped table-bordered">
                    <colgroup>
                        <col class="id-col">
                        <col class="day-col-ask">
                        <col class="status-col">
                        <col class="status-col">
                        <col>
                        <col>
                    </colgroup>
                    <thead class="grey lighten-2">
                        <tr>
                            <th class="text-center d-none d-md-table-cell">#</th>
                            <th class="text-center table-with-42">Ngày</th>
                            <th>Hình thức</th>
                            <th class="d-none d-md-table-cell">Nội dung</th>
                            <th class="d-none d-md-table-cell">Nội dung phản hồi</th>
                            <th class="text-center">Trạng Thái</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($askPermission as $increment => $item)
                            <tr>
                                <th class="text-center d-none d-md-table-cell">{{ $increment + 1 }}</th>
                                <th class="text-center" style="padding: 15px;width: 10%;">{{ $item['work_day'] ?? '' }}</th>
                                <td>
                                    @if ($item['type'] == array_search('Bình thường', WORK_TIME_TYPE))
                                        Bình thường
                                    @elseif($item['type'] == array_search('Đi muộn', WORK_TIME_TYPE))
                                        Đi muộn {{ empty($item->hours_permission) ? '' : '(' . $item->hours_permission . ')' }}
                                    @elseif($item['type'] == array_search('Về sớm', WORK_TIME_TYPE))
                                        Về sớm {{ empty($item->hours_permission) ? '' : '(' . $item->hours_permission . ')' }}
                                    @elseif($item['type'] == array_search('Overtime', WORK_TIME_TYPE))
                                        @if ($item['ot_type'] == array_search('Dự án', OT_TYPE))
                                            OT dự án
                                        @elseif($item['ot_type'] == array_search('OT lý do cá nhân', OT_TYPE))
                                            OT cá nhân
                                        @endif
                                    @endif
                                </td>
                                <td class="d-none d-md-table-cell">{!! $item['note'] ?? '' !!}</td>
                                <td class="d-none d-md-table-cell">{!! $item['reason_reject'] ?? '' !!}</td>
                                <td class="text-center td-approve">
                                    @if ($item['status'] == array_search('Đã duyệt', OT_STATUS))
                                        <i class="fas fa-grin-stars fa-2x text-success"
                                            title="{{ $item->workTimeApprover->name ?? '' }}"></i>
                                    @elseif($item['status'] == array_search('Chưa duyệt', OT_STATUS))
                                        <i class="fas fa-meh-blank fa-2x text-warning" title="Chưa duyệt"></i>
                                    @elseif($item['status'] == array_search('Từ chối', OT_STATUS))
                                        <i class="fas fa-frown fa-2x text-danger"
                                            title="{{ $item->workTimeApprover->name ?? '' }}"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- Panel 1 -->

            <!-- Panel 2 -->
            <div class="tab-pane fade btn-small-ask" id="panel666" role="tabpanel">

                <table class="contactTbl table table-striped table-bordered table-head-sticky">
                    <colgroup>
                        <col class="id-col">
                        <col class="day-col">
                        <col class="status-col">
                        <col class="status-col">
                        <col class="reason-col">
                        <col class="reason-col">
                        <col>
                    </colgroup>
                    <thead class="grey lighten-2">
                        <tr>
                            <th class="text-center d-none d-md-table-cell">#</th>
                            <th class="text-center">Ngày</th>
                            <th>Hình thức</th>
                            <th>Thời gian</th>
                            <th class="d-none d-md-table-cell">Nội dung</th>
                            <th class="d-none d-md-table-cell">Nội dung từ chối</th>
                            <th class="text-center">Trạng Thái</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($otTimes as $increment => $item)
                            <tr>
                                <th class="text-center d-none d-md-table-cell">{{ $increment + 1 }}</th>
                                <th class="text-center mb-with-32" style="padding: 15px">{{ $item['work_day'] }}</th>
                                <td>
                                    @if ($item->ot_type == array_search('Dự án', OT_TYPE))
                                        OT Dự án
                                    @else
                                        OT cá nhân
                                    @endif
                                </td>
                                <td class="mb-with-32">{{ $item->description_time }}</td>
                                <td class="d-none d-md-table-cell">{!! $item['reason'] !!}</td>
                                <td class=" d-none d-md-table-cell">{!! $item['note_respond'] !!}</td>
                                <td class="text-center td-approve">
                                    @if ($item['status'] == array_search('Đã duyệt', OT_STATUS))
                                        <i class="fas fa-grin-stars fa-2x text-success"
                                            title="{{ $item->approver->name ?? '' }}"></i>
                                    @elseif($item['status'] == array_search('Chưa duyệt', OT_STATUS))
                                        <i class="fas fa-meh-blank fa-2x text-warning" title="Chưa duyệt"></i>
                                    @elseif($item['status'] == array_search('Từ chối', OT_STATUS))
                                        <i class="fas fa-frown fa-2x text-danger"
                                            title="{{ $item->approver->name ?? '' }}"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <!-- Panel 2 -->

        </div>
        <!-- Tab panels -->
    @endcan
    <div class="modal fade reject" id="modal-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-center permission-modal-set-center mx-auto" role="document">
            <div class="modal-content modal-ipad-pro" id="bg-img"
                style="background-image: url({{ asset_ver('img/font/xin_nghi.png') }})">
                <div class="modal-header text-center border-bottom-0 p-3">
                    <h4 class='mg-center mb-2 modal-title w-100 font-weight-bold pt-2 mg-left-10 title-permission'>Nội
                        dung đơn</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="btn-close-icon" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="permission-detail">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-4">
                                <h5 class="bold">Người xin phép:</h5>
                                <p class="creator_id"></p>
                                <div class="content-hidden">
                                    <h5 class="bold">Tên dự án:</h5>
                                    <p class="project_id"></p>
                                </div>
                            </div>

                            <div class="col-4">
                                <h5 class="bold">Ngày tạo đơn:</h5>
                                <p class="work_day"></p>
                            </div>

                            <div class="col-4">
                                <h5 class="bold">Thời gian:</h5>
                                <p class="option_time"></p>
                            </div>
                        </div>

                        <div class="row content-hidden">
                            <div class="col-4">
                                <h5 class="bold">Giờ bắt đầu:</h5>
                                <p class="start_at"></p>
                            </div>
                            <div class="col-4">
                                <h5 class="bold">Giờ kết thúc:</h5>
                                <p class="end_at"></p>
                            </div>
                            <div class="col-4">
                                <h5 class="bold">Số phút OT</h5>
                                <p class="minute"></p>
                            </div>
                            <br>
                        </div>
                        <!-- ## -->
                        <div class="row content-hidden">
                            <div class="col-4">
                                <h5 class="bold">Ngày làm việc:</h5>
                                <p class="work_day"></p>
                            </div>
                            <div class="col-4">
                                <h5 class="bold">Thời gian:</h5>
                                <p class="period"></p>
                            </div>
                            <div class="col-4">
                                <h5 class="bold">Thời gian tính:</h5>
                                <p class="caculator_time"></p>
                            </div>
                            <br>
                        </div>
                        <h5 class="bold">Nội dung:</h5>
                        <p class="reason"></p>
                    </div>

                </div>
                <div class="d-flex justify-content-center text-area-reason" id="div-reason"></div>
                <input type="hidden" class="id" name="id" id="ajax_user_id">
                <input type="hidden" class="approve-type" name="approve_type" id="ajax_approve_type">
                <input type="hidden" class="permission-type" name="permission_type" id="ajax_permission_type">
                <div style="font-size: 18px; font-weight: 400;" class="container-fluid">Ý kiến phê duyệt</div>
                <textarea class="form-control permission-reason" name="reason_approve" id="ajax_reason_approve" cols="48"
                    rows="6" placeholder="Nhập ý kiến..."></textarea>
                <div class="pt-3 pb-4 d-flex justify-content-center border-top-0 rounded mb-0">
                    <button class="btn-send btn-approve mr-2" id="btn-approve">DUYỆT ĐƠN</button>
                    <button class="btn-send btn-reject ml-2" id="btn-reject">KHÔNG DUYỆT</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade myModal" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-center permission-modal-set-center modal-small" role="document">
            <div class="modal-content modal-ipad-pro" id="bg-img"
                style="background-image: url({{ asset_ver('img/font/xin_nghi.png') }})">
                <div class="modal-header text-center border-bottom-0 p-3">
                    <h4
                        class='mg-center mb-2 modal-title w-100 font-weight-bold pt-2 header-permission-late title-permission'>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="btn-close-icon" aria-hidden="true">×</span>
                    </button>
                </div>
                @include('elements.ask_permission_image')
                <form action="{{ route('ask_permission.create') }}" method="get" id="permission_late">
                    <div class="container-fluid">
                        <div class="d-flex justify-content-center text-area-reason" id="div-reason"></div>
                        <div class="">
                            <div class="row col-12 option-permission"></div>
                            <input name='permission_late' type='hidden'>
                            <input name='permission_type' type='hidden' value="1">
                            <label class=" text-w-400" for="inputCity">Chọn ngày *</label>
                            <input style="width: 43%;" type="text"
                                class="form-control select-item {{ $errors->has('work_day') ? ' has-error' : '' }}"
                                id="work-day-late" autocomplete="off" name="work_day" value="{{ old('work_day') }}"
                                readonly="readonly">
                        </div>
                        <div class="my-2">
                            <label class="text-w-400" for="inputCity">Thời gian *</label>&nbsp
                            <span class="sum_option_time"></span>
                            <div>
                                @foreach (OPTION_TIME as $key => $value)
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input checkbox-late" type="checkbox"
                                            id="Checkbox{{ $key }}" name="option_time[]"
                                            value="{{ $value }}" data-err="checkbox-late">
                                        <label class="form-check-label label-option-time"
                                            for="Checkbox{{ $key }}">{{ TIME_ASK_PERMISSION[$key] }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div style="color:red">
                                <div id="chk_error_late"></div>
                                <div class='checkbox-err ml-1'></div>
                            </div>
                        </div>
                        <div class="reason-late">
                            <label class=" text-w-400" for="inputCity">Lý do *</label>
                            @foreach (REASON_LATE as $key => $value)
                                <div class="form-check reason-late">
                                    <input class="form-check-input" id="option-reason{{ $key }}" type="radio"
                                        name="option_reason" value="{{ $value }}">
                                    <label class="form-check-label"
                                        for="option-reason{{ $key }}">{{ $value }}</label>
                                </div>
                            @endforeach
                        </div>
                        <textarea class="form-control permission-reason-late" name="note" cols="48" rows="6"
                            placeholder="Nhập lý do ..." readonly></textarea>
                        <div class="pt-3 pb-4 d-flex justify-content-center border-top-0 rounded mb-0">
                            <button class="btn btn-primary btn-send btn-permission-late btn-block">GỬI ĐƠN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-early" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-center permission-modal-set-center modal-small" role="document">
            <div class="modal-content modal-ipad-pro" id="bg-img"
                style="background-image: url({{ asset_ver('img/font/xin_nghi.png') }})">
                <div class="modal-header text-center border-bottom-0 p-3">
                    <h4
                        class='mg-center mb-2 modal-title w-100 font-weight-bold pt-2 header-permission-early title-permission'>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="btn-close-icon" aria-hidden="true">&times;</span>
                    </button>
                </div>
                @include('elements.ask_permission_image')
                <form action="{{ route('ask_permission.create') }}" method="get" id="permission_early">
                    <div class="container-fluid">
                        <div class="d-flex justify-content-center text-area-reason" id="div-reason"></div>
                        <div class="">
                            <div class="option-permission"></div>
                            <input name='permission_early' type='hidden'>
                            <input name='permission_type' type='hidden' value="2">
                            <label class=" text-w-400" for="inputCity">Chọn ngày *</label>
                            <input style="width: 43%;" type="text"
                                class="form-control select-item {{ $errors->has('work_day') ? ' has-error' : '' }}"
                                id="work-day-early" autocomplete="off" name="work_day" value="{{ old('work_day') }}"
                                readonly="readonly">
                        </div>
                        <div class="my-2">
                            <label class="text-w-400">Thời gian</label>
                            <span class="sum_option_time"></span>
                            <div>
                                @foreach (OPTION_TIME as $key => $value)
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input checkbox-early" type="checkbox"
                                            id="Checkbox{{ $key + 4 }}" name="option_time[]"
                                            value="{{ $value }}" data-err="checkbox-early">
                                        <label class="form-check-label label-option-time"
                                            for="Checkbox{{ $key + 4 }}">{{ TIME_ASK_PERMISSION[$key] }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div style="color:red">
                                <div id="chk_error_early"></div>
                                <div class='checkbox-err ml-1'></div>
                            </div>
                        </div>
                        <textarea class="form-control" name="note" cols="48" rows="6" placeholder="Nhập lý do ..."></textarea>
                        <div class="pt-3 pb-4 d-flex justify-content-center border-top-0 rounded mb-0">
                            <button class="btn btn-primary btn-send btn-permission-early btn-block">GỬI ĐƠN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade myModal" id="modal-form-ot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-center permission-modal-set-center modal-small" role="document">
            <div class="modal-content modal-ipad-pro" id="bg-img"
                style="background-image: url({{ asset_ver('img/font/xin_nghi.png') }})">
                <div class="modal-header text-center border-bottom-0 p-3">
                    <h4
                        class='mg-center mb-2 modal-title w-100 font-weight-bold pt-2 header-permission-late title-permission'>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="btn-close-icon" aria-hidden="true">&times;</span>
                    </button>
                </div>
                @include('elements.ask_permission_image')
                <form action="{{ route('ask_permission.create') }}" method="get" id="ot_id">
                    <div class="d-flex justify-content-center text-area-reason" id="div-reason"></div>
                    <div class="container-fluid my-2">
                        <div class="row">
                            <input type="hidden" name="ot_id" class="ot_id">
                            <input type="hidden" name="permission_status" class="permission_status">
                            <div class="col-6 text-center mt-3">
                                <input style="position: relative;opacity: 1;pointer-events: inherit"
                                    class="other-ot ml-0 ml-md-5" type="radio" name="ot_type" id="project-ot" checked
                                    value="1">
                                <label for="project-ot">OT dự án</label>
                            </div>
                            <div class="col-6 text-center mt-3">
                                <input style="position: relative;opacity: 1;pointer-events: inherit" class="other-ot"
                                    type="radio" name="ot_type" id="other-ot" value="2">
                                <label for="other-ot" class="mr-0 mr-md-5">Lý do cá nhân</label>
                            </div>
                        </div>

                        <select class="browser-default form-control  my-2 permission_project_id" name="project_id"
                            id="project_id">
                            <option value="0">Chọn dự án</option>
                            @foreach ($projects as $project)
                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                            @endforeach

                        </select>
                        <div class="row">
                            <div class="col-6 my-2">
                                <label for="inputCity" class="my-2">Chọn ngày *</label>
                            </div>
                            <div class="col-6 my-2">
                                <input type="hidden" name="permission_ot">
                                <input type="hidden" value="4" name="permission_type">
                                <input type="text"
                                    class="form-control select-item {{ $errors->has('work_day') ? ' has-error' : '' }}"
                                    id="work_day_ot" autocomplete="off" name="work_day"
                                    value="{{ old('work_day', date('Y-m-d')) }}" readonly="readonly"
                                    style="width: 96.5%;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 my-2">
                                <label for="start_at">T/g bắt đầu *</label>
                                <div class="input-group date">
                                    <input type="time" class="form-control pull-right" autocomplete="off"
                                        name="start_at" value="" id="start_at">
                                </div>
                            </div>
                            <div class="col-6 my-2">
                                <label for="end_at">T/g kết thúc *</label>
                                <div class="input-group date">
                                    <input type="time" class="form-control pull-right" name="end_at"
                                        autocomplete="off" value="" id="end_at">
                                </div>
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-6">
                                <label for="option-break-time">Chọn thời gian nghỉ</label>
                                <div class="input-group date">
                                    <select class="browser-default form-control option-break-time" name="break_time">
                                        @foreach (OPTION_BREAK_TIME as $key => $value)
                                            <option value="{{ $value }}">{{ $key }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <label>Tính toán thời gian</label>
                                <div class="input-group date">
                                    <div class="sum-time-ot" style="padding-top:10px"></div>
                                </div>
                            </div>
                        </div>
                        <textarea class="mt-2 form-control permission-reason-ot" name="note" id="permission-reason-ot" cols="48"
                            rows="4" placeholder="Nhập lý do ..."></textarea>

                        <div class="pt-3 pb-4 d-flex justify-content-center border-top-0 rounded mb-0">
                            <button class="btn btn-primary btn-send btn-permission-ot btn-block">GỬI ĐƠN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade reject" id="modal-assign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-center permission-modal-set-center" role="document">
            <div class="modal-content modal-ipad-pro" id="bg-img"
                style="background-image: url({{ asset_ver('img/font/xin_nghi.png') }})">
                <div class="modal-header text-center border-bottom-0 p-3">
                    <h4 class='mg-center mb-2 modal-title w-100 font-weight-bold pt-2 mg-left-10 title-permission'>
                        {{ __l('assgin_approver_ot') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="btn-close-icon" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('multi_approver', ['check' => 'assgin']) }}" method="post" id="form-assgin">
                    @csrf
                    <div id="append-value-assgin">

                    </div>
                    <div class="permission-detail">
                        <div class="container-fluid">
                            <div>
                                <h5 class="bold">Chỉ định người phê duyệt(*):</h5>
                                <select id="mdb-select" class="mdb-select md-form" searchable="Tìm người nhận ..."
                                    name="to_id">
                                    <option value="">Chọn nhân viên</option>
                                    @foreach ($assigns as $user)
                                        <option value="{{ $user['id'] }}"
                                            data-icon="{{ asset_image($user['avatar']) }}" class="rounded-circle">
                                            {{ $user['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <h5 class="bold">Nội dung ủy quyền:</h5>
                                <textarea class="mt-2 form-control" name="assgin_comment" cols="48" rows="4"
                                    placeholder="Nhập lý do ..."></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="pt-3 pb-4 d-flex justify-content-center border-top-0 rounded mb-0">
                        <button class="btn-send btn-approve mr-2" id="btn-assgin-ot-time">ĐỒNG Ý</button>
                        <button class="btn-send btn-reject ml-2" id="btn-reject-assgin">HỦY BỎ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-option" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <!-- Change class .modal-sm to change the size of the modal -->
        <div class="modal-dialog modal-md" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title w-100" id="modal-option-title">Modal title</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="body">
                    <input type="hidden" name="ids">
                </div>
                <div class="modal-footer">
                    <div id="append-buttom-submit">
                    </div>
                    <div>
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal_notification" id="popup_notification" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="top: 25%;">
            <div class="modal-content">
                <div id="icon_messenger_success">
                    <div class="swal-icon swal-icon--success">
                        <span class="swal-icon--success__line swal-icon--success__line--long"></span>
                        <span class="swal-icon--success__line swal-icon--success__line--tip"></span>
                        <div class="swal-icon--success__ring"></div>
                        <div class="swal-icon--success__hide-corners"></div>
                    </div>
                </div>
                <div id="icon_messenger_error" class="hidden">
                    <div class="swal-icon swal-icon--error">
                        <div class="swal-icon--error__x-mark">
                            <span class="swal-icon--error__line swal-icon--error__line--left"></span>
                            <span class="swal-icon--error__line swal-icon--error__line--right"></span>
                        </div>
                    </div>
                </div>
                <div class="swal-title" style="">Thông báo!</div>
                <div class="swal-text popup_message" style="text-align: center"></div>
                <div class="swal-footer">
                    <div class="swal-button-container">

                        <button class="swal-button swal-button--confirm" data-dismiss="modal">Đóng</button>

                        <div class="swal-button__loader">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-open custom-modal approval-form" id="modal-form-absence" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-center approval-ipad-pro" role="document">
            <div class="modal-content modal-center-display bg-img-day-off" id="bg-img"
                style="background-image: url({{ asset_ver('img/font/xin_nghi.png') }})">
                <div class="modal-header text-center border-bottom-0 pb-0">
                    <h4 class="modal-title w-100 font-weight-bold pt-2">NỘI DUNG ĐƠN</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="btn-close-icon" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3 mt-0 p-0">
                    <div class="mb-3">
                        <label class="ml-3 text-d-bold" for="exampleFormControlTextarea5">Tên nhân
                            viên</label>
                        <div class="ml-3 modal-form-absence-employee-name" id="user-day-off"></div>

                    </div>

                    <div class="mb-3">
                        <label class="ml-3 text-d-bold" for="exampleFormControlTextarea5">Các dự án</label>
                        <div class="ml-3 modal-form-absence-employee-name" id="modal-form-absence-project"></div>
                    </div>

                    <div class="mb-3">
                        <label class="ml-3 text-d-bold" for="exampleFormControlTextarea5">Group</label>
                        <div class="ml-3 modal-form-absence-employee-name" id="modal-form-absence-group"></div>
                    </div>

                    <div class="mb-2">
                        <label class="ml-3 text-d-bold" for="exampleFormControlTextarea5">Số ngày nghỉ phép tồn :
                        </label>
                        <strong class="" id="number_off_remain"></strong>

                    </div>

                    <div class="mb-2 ml-3 ">
                        <!-- Default input -->
                        <label class="text-d-bold" for="exampleForm2">Thời gian được tính:</label>
                        <strong class="" id="number_off"></strong>
                        <span id="approver_num" class="text-danger"></span>
                    </div>

                    <div class="mb-2">
                        <!-- Default input -->
                        <label class="ml-3 text-d-bold" for="exampleForm2">Ngày nghỉ:</label>
                        <div class="ml-3" id="strat_end"></div>
                    </div>

                    <div class="mb-3">
                        <!-- Default input -->
                        <label class="ml-3 text-d-bold" for="exampleForm2">Lý do:</label>
                        <div class="ml-3" id="title"></div>
                    </div>

                    <div class="mb-2">
                        <label class="ml-3 text-d-bold" for="exampleFormControlTextarea5">Chi tiết lý
                            do:</label>
                        <div class="ml-3" id="reason" style="word-wrap: break-word"></div>
                    </div>

                    <div class="mb-2">
                        <div class="row">
                            <div class="form-group col-6 m-0">
                                <label class="ml-3 text-d-bold" for="inputCity">Người duyệt</label>
                                <div class="ml-3" id="approver_name"></div>
                            </div>
                            <!-- Default input -->
                            <div class="form-group col-6 m-0 p-0" id="remove-app-date">
                                <label class="ml-3 text-d-bold" for="inputZip">Ngày duyệt</label>
                                <div class="ml-3" id="approver_date"></div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 ml-3" id="remove-app-comment">
                        <label class="text-d-bold" for="exampleFormControlTextarea5">Ý kiến người
                            duyệt</label>
                        <div class="" id="app-comment"></div>
                    </div>

                    <div class=" mb-1 pb-2 d-flex justify-content-center border-top-0 rounded mb-0" id="btn-submit-form">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('extend-css')
    <link href="{{ asset_ver('bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset_ver('bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset_ver('css/user/ask-permissions.css') }}" rel="stylesheet">
@endpush
@push('extend-js')
<script>
    const user_approval = "{{ auth()->id() }}";
    const urlGetDayOff = "{{ route('get_day_off') }}/";
    const urlApproveDayOff = "{{ route('day_off_approval') }}";
    const statusIcons = {
        0: `<i data-toggle="tooltip" data-placement="right" title="Chờ phê duyệt" class="fas fa-meh-blank fa-2x text-warning text-center"></i>`,
        1: `<i data-toggle="tooltip" data-placement="right" title="Đã duyệt đơn" class="fas fa-grin-stars fa-2x text-success"></i>`,
        2: `<i data-toggle="tooltip" data-placement="right" title="Không duyệt" class="fas fa-frown fa-2x text-danger"></i>`,
    }
    const title = {
        1: "Lý do cá nhân",
        2: "Nghỉ đám cưới",
        3: "Nghỉ đám hiếu",
        4: "Nghỉ thai sản",
    }
</script>


    <script src="{{ asset_ver('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset_ver('bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset_ver('bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset_ver('js/tinymce/tinymce.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js" integrity="sha512-WFN04846sdKMIP5LKNphMaWzU7YpMyCU245etK3g/2ARYbPK9Ub18eG+ljU96qKRCWh+quCY7yefSmlkQw1ANQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset_ver('adminlte/plugins/iCheck/icheck.js') }}"></script>
    <script src="{{asset_ver('js/user/absence.js')}}"></script>
    <script type="text/javascript">
        $(function() {
            localStorage.clear();
            myDataTable($('.contactTbl'));

            var hash = window.location.hash;
            if (hash) {
                var check = hash.split('-');
                if (check[0] == '#ot') {
                    $('.btn-ot-one , .btn-small-ot').removeClass('show active');
                    $('.btn-ask , .btn-small-ask').addClass('show active');
                }
                var otThis = $(hash);
                if (otThis.attr('id') === 'btn-reject') {
                    $('.approve-type').attr('value', 2)
                } else if (otThis.attr('id') === 'btn-approve') {
                    $('.approve-type').attr('value', 1)
                }
                var id = otThis.data("id"),
                    permissionType = otThis.data("permission"),
                    itemtype = $(this).data("itemtype");
                $('.modal-title').text(itemtype)
                // debugger
                $('.id').attr('value', id);
                getData(id, permissionType)
            }
            $(document).on('click', '#notification', function() {
                location.reload();
            })

            var date = new Date(),
                currentDate = date.getDate(),
                currentMonth = date.getMonth() + 1,
                currentYear = date.getFullYear(),
                currentFullTime = currentYear + '-' + currentMonth + '-' + currentDate;
            $('#work-day-late,#work-day-early,#work_day_ot').datepicker({
                format: 'yyyy-mm-dd'
            });
            $('.approve-btn-late').on('click', function() {
                $(".permission-reason").empty();
                $('#modal-form').modal('show');
                $(".permission-reason-late").append("<input name='type' type='text' value='1'>");
                $(".title-permission").text("Xin đi muộn");
                // $('#work_day').datepicker("setDate", currentFullTime);
                $('#work-day-late').datepicker("setDate", date);
            });

            $('.approve-btn-early').on('click', function() {
                $('#modal-early').modal('show');
                $(".permission-reason-late").empty();
                $(".permission-reason").empty();
                $(".permission-reason").append("<input name='type' type='text' value='2'>");
                $(".title-permission").text("Xin về sớm");
                $('#work-day-early').datepicker("setDate", date);
            });
            $('.approve-btn-ot').on('click', function() {
                $('#modal-form-ot').modal('show');
                $(".permission-reason").append("<input name='type' type='text' value='4'>");
                $(".title-permission").text("Xin OT");
                $('#work_day_ot').datepicker("setDate", (date));
            });
            @if ($autoShowModal)
                $('.approve-btn-late').trigger('click');;
                $("input[name='option_reason']").first().prop('checked', true);
                $(".permission-reason-late").prop("readonly", "remove").val($("input[name='option_reason']:checked")
                    .val());
            @endif
            // duyet click
            $(document).on('click', '.approve', function() {
                var otThis = $(this);
                if (otThis.attr('id') === 'btn-reject') {
                    $('.approve-type').attr('value', 2)
                } else if (otThis.attr('id') === 'btn-approve') {
                    $('.approve-type').attr('value', 1)
                }
                var id = $(this).data("id"),
                    permissionType = $(this).data("permission"),
                    itemtype = $(this).data("itemtype");
                $('.modal-title').text(itemtype)
                // debugger
                $('.id').attr('value', id);
                $('#ajax_reason_approve').val('');
                getData(id, permissionType)
            });

            $('#work-day-late').on('change', function() {
                var data = $(this).val(),
                    type = 1,
                    workDayLate = $(this);
                $.ajax({
                    url: '{{ route('ask_permission.modal') }}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {
                        'data': data,
                        'type': type,
                    },
                    success: function(respond) {
                        if (respond === 0) {
                            var status = 0;
                        } else {
                            var status = respond.status;
                        }
                        workDayLate.append(
                            "<input name='permission_status' type='hidden' value='" +
                            status + "'>");
                        var note = respond[0].note ? respond[0].note : '',
                            otType = respond[0].ot_type;
                        $('.permission-reason-late').text(note);
                        if (otType) {
                            if (otType === 1) {
                                $('#project-ot').prop('checked', true);
                                $('#other-ot').prop('checked', false);
                            } else if (otType === 2) {
                                $('#other-ot').prop('checked', true);
                                $('#project-ot').prop('checked', false);
                            }
                        }

                        if (respond[0].status === 1) {
                            $('.header-permission-late').text('Đơn đã được duyệt');
                            $('.permission-reason-late,.btn-permission-late').prop('disabled',
                                true);
                        } else {
                            $('.header-permission-late').text('Xin đi muộn');
                            $('.permission-reason-late,.btn-permission-late').prop('disabled',
                                false);
                        }
                    }
                });
            });

            $('#work-day-early').on('change', function() {
                var data = $(this).val(),
                    type = 2,
                    workDayLate = $(this);
                $.ajax({
                    url: '{{ route('ask_permission.modal') }}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {
                        'data': data,
                        'type': type,
                    },
                    success: function(respond) {
                        workDayLate.append(
                            "<input name='permission_status' type='hidden' value='" +
                            respond[0].status + "'>");
                        var note = respond[0].note ? respond[0].note : '',
                            otType = respond[0].ot_type;
                        $('.permission-reason-early').text(note);
                        if (otType) {
                            if (otType === 1) {
                                $('#project-ot').prop('checked', true)
                                $('#other-ot').prop('checked', false)
                            } else if (otType === 2) {
                                $('#other-ot').prop('checked', true)
                                $('#project-ot').prop('checked', false)
                            }
                        }

                        if (respond[0].status === 1) {
                            $('.header-permission-early').text('Đơn đã được duyệt');
                            $('.permission-reason-early,.btn-permission-early').prop('disabled',
                                true);
                        } else {
                            $('.title-permission').text('Xin về sớm');
                            $('.permission-reason-early,.btn-permission-early').prop('disabled',
                                false);
                        }
                    }
                });
            });
            $(document).on('change', '#start_at , #project_id ,#end_at , #permission-reason-ot', function() {
                if (!localStorage.getItem('checkOt') || localStorage.getItem('checkOt') != 1) {
                    localStorage.setItem("project_id", $('#project_id').val());
                    localStorage.setItem("start_at", $('#start_at').val());
                    localStorage.setItem("end_at", $('#end_at').val());
                    localStorage.setItem("note", $('#permission-reason-ot').val());
                }
            })

            function calculateTimeOT(timeOT) {
                if (timeOT <= 0) {
                    $('.sum-time-ot').html('Thời gian không hợp lệ').css("color", "red");
                    $('.btn-permission-ot').prop('disabled', true);
                } else {
                    var hours = Math.floor(timeOT / 60);
                    minutes = (Math.floor((timeOT % 60) / 15) * 15) / 60;
                    $('.sum-time-ot').html(hours + minutes + "h").css("color", "");
                    $('.btn-permission-ot').removeAttr("disabled", true);
                }
            }

            $('#work_day_ot').on('change', function() {
                let data = $(this).val(),
                    type = 4,
                    workDayOT = $(this);

                $.ajax({
                    url: '{{ route('ask_permission.modal') }}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {
                        'data': data,
                        'type': type,
                    },
                    success: function(respond) {
                        /* if (respond[1]) {
                             respond[1].forEach(function (element) {
                                 $('.project_id').append('<option value="' + element.id + '">' + element.name + '</option>');
                             });
                         } else {
                             $('.permission-reason-ot').empty();
                         }*/

                        if (respond[0]) {
                            localStorage.setItem('checkOt', 1)
                            $('.project_id').val(respond[0].project_id);
                            $('.permission_status').attr('value', respond[0].status);
                            $('#start_at').val(respond[0].start_at);
                            $('#end_at').val(respond[0].end_at);
                            $('.ot_id').attr('value', respond[0].id);
                            $(".option-break-time").val(respond[0].break_time);
                            calculateTimeOT(respond[0].minute);

                            workDayOT.append("<input name='' type='hidden' value='" + respond[0]
                                .status + "'>");
                            var note = respond[0].reason ? respond[0].reason : '',
                                otType = respond[0].ot_type;
                            $('.permission-reason-ot').val(note);
                            if (otType) {
                                if (otType === 1) {
                                    $('#project-ot').prop('checked', true)
                                    $('#other-ot').prop('checked', false)
                                } else if (otType === 2) {
                                    $('#other-ot').prop('checked', true)
                                    $('#other-ot').prop('checked', true)
                                    $('#project_id').prop('disabled', 'disabled');
                                    $('#project-ot').prop('checked', false)
                                }
                            }
                            // if (respond[0].ot_type == 2) {
                            //     $('.project_id ').prop('disabled', true);
                            // } else {
                            //     $('.project_id ').prop('disabled', false);
                            // }

                        } else {
                            $('#start_at').val(localStorage.getItem('start_at'));
                            $('#end_at').val(localStorage.getItem('end_at'));
                            $('#project_id').val(localStorage.getItem('project_id'));
                            $('#permission-reason-ot').val(localStorage.getItem('note'));
                            localStorage.setItem('checkOt', 2)


                        }


                        if (respond[0].status === 1) {
                            $('.header-permission-ot').text('Đơn đã được duyệt');
                            $('.permission-reason-ot,.btn-permission-ot,#start_at,#end_at')
                                .prop('disabled', true);
                        } else {
                            $('.title-permission').text('Xin OT');
                            $('.permission-reason-ot,.btn-permission-ot,#start_at,#end_at')
                                .prop('disabled', false);
                        }
                    }
                });
                $('.project_id option[value!="0"]').remove();
            });
            $('.approve-type').attr('value', '');

            $('#btn-approve, #btn-reject').on('click', function() {
                var selectorID = $(this).attr('id');
                var approveType, tdID, iconID, icon, tdNoteRespond;
                var permissionType = $('#ajax_permission_type').val();
                var reasonApprove = $('#ajax_reason_approve').val();
                var id = $('#ajax_user_id').val();
                if (selectorID === 'btn-approve') {
                    $('.approve-type').attr('value', 1);
                    approveType = 1;
                    icon = '<i class="fas fa-grin-stars fa-2x text-success"></i>';
                } else if (selectorID === 'btn-reject') {
                    $('.approve-type').attr('value', 2);
                    approveType = 2;
                    icon = '<i class="fas fa-frown fa-2x text-danger"></i>'
                }
                if (permissionType === 'ot') {
                    tdID = '#td-approve-ot-' + id;
                    tdNoteRespond = '#td-note_respond-ot-' + id;
                    iconID = '#ot-' + id;
                } else {
                    tdID = '#td-approve-' + id;
                    tdNoteRespond = '#td-note_respond-' + id;
                    iconID = '#ask-permission-' + id;
                }
                $.ajax({
                    url: '{{ route('ask_permission.approvePermission') }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        'id': id,
                        'approve_type': approveType,
                        'permission_type': permissionType,
                        'reason_approve': reasonApprove,
                    },
                    success: function(respond) {
                        $('#modal-reject').modal('hide');
                        $('.popup_message').text(respond.message);
                        setTimeout(function() {
                            $('#popup_notification').modal('show');
                        }, 1000);
                        $(tdNoteRespond).text(reasonApprove);
                        if (respond.status === 'success') {
                            $(iconID).remove();
                            $(tdID).append(icon);
                            $('#icon_messenger_success').removeClass('hidden');
                            $('#icon_messenger_error').addClass('hidden');
                        } else {
                            $('#icon_messenger_success').addClass('hidden');
                            $('#icon_messenger_error').removeClass('hidden');
                        }

                    }
                })

            });

            $('#project-ot').click(function() {
                $('#project_id').prop('disabled', false)
            });

            $('#other-ot').click(function() {
                $('#project_id').prop('disabled', true)

            });

            $("#work_day_ot, .option-break-time, #end_at, #start_at").change(function() {
                var endAt = $("#end_at").val();
                var startAt = $("#start_at").val();
                var sttime = new Date($("#work_day_ot").val() + ' ' + startAt);
                var endtime = new Date($("#work_day_ot").val() + ' ' + endAt);
                var diff = endtime.getTime() - sttime.getTime();
                var breakTime = ($(".option-break-time option:selected").val());
                if (sttime > endtime && endAt.length > 0 && startAt.length > 0) {
                    var minutes = Math.floor(diff / 60000) - Number(breakTime) + 24 * 60;
                    calculateTimeOT(minutes);
                } else if (endAt.length > 0 && startAt.length > 0) {
                    var minutes = (Math.floor(diff / 60000)) - Number(breakTime);
                    calculateTimeOT(minutes);
                }
            });
            $("#work-day-late").change(function() {
                var workDayLate = new Date(new Date($("#work-day-late").val()).toDateString());
                var currentDate = new Date(currentFullTime);
                if (workDayLate > currentDate) {
                    $("input[name='option_reason']").last().prop('checked', true);
                    $(".permission-reason-late").val("").removeAttr("readonly").attr('placeholder',
                        'Nhập lý do ...');
                    $(".reason-late").hide();
                } else {
                    $("input[name='option_reason']").first().prop('checked', true);
                    $(".permission-reason-late").prop("readonly", "remove").val($(
                        "input[name='option_reason']:checked").val());
                    $(".reason-late").show();
                }
            })
            $("input[name='option_reason']").change(function() {
                if ($(this).val() != 'Lý do khác') {
                    $(".permission-reason-late").prop("readonly", "remove").val($(this).val());
                } else {
                    $(".permission-reason-late").focus().val("").removeAttr("readonly").attr('placeholder',
                        'Nhập lý do ...');
                }
            });
            $('.checkbox-late, .checkbox-early').change(function() {
                var selectedValue = 0;
                var classCheckbox = $(this).data("err");
                $("." + classCheckbox).each(function(idx, el) {
                    if ($(el).is(':checked')) {
                        selectedValue += Number($(el).val());
                    }
                });
                $('.' + classCheckbox).filter(':checked').length > 0 ? $(".sum_option_time").html('(' +
                    convertHourToMinute(selectedValue) + ')') : $(".sum_option_time").html("")
                if (selectedValue > 2) {
                    $(".checkbox-err").html('Vui lòng chọn thời gian không quá 2h')
                    classCheckbox == 'checkbox-late' ? $('.btn-permission-late').prop('disabled', true) : $(
                        '.btn-permission-early').prop('disabled', true);
                } else {
                    $(".checkbox-err").html("")
                    classCheckbox == 'checkbox-late' ? $('.btn-permission-late').prop('disabled', false) :
                        $('.btn-permission-early').prop('disabled', false);
                }
            });

            function convertHourToMinute(hour) {
                var minute = hour * 60;
                if (minute >= 60) {
                    hour = Math.trunc(minute / 60);
                    minute = minute % 60;
                    return minute == 0 ? hour + 'h' : hour + 'h' + minute + 'p';
                }
                return minute + 'p';
            }
            $('#modal-early, #modal-form').on('hidden.bs.modal', function(e) {
                $('.checkbox-late, .checkbox-early').prop("checked", false);
                $(".checkbox-err").html("");
                $(".sum_option_time").html("");
            });

            $('#permission_early').validate({
                ignore: '[readonly]',
                rules: {
                    work_day: {
                        required: true,
                    },
                    note: {
                        required: true,
                    },
                    'option_time[]': {
                        required: true,
                    },
                },
                messages: {
                    work_day: {
                        required: "Vui lòng chọn ng",
                    },
                    note: {
                        required: "Vui lòng chọn lý do",
                    },
                    'option_time[]': {
                        required: "Vui lòng chọn thời gian",
                    },
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "option_time[]") {
                        error.insertAfter("#chk_error_early");
                        return;
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $('#permission_late').validate({
                ignore: '[readonly]',
                rules: {
                    work_day: {
                        required: true,
                    },
                    note: {
                        required: true,
                    },
                    'option_time[]': {
                        required: true,
                    },
                },
                messages: {
                    work_day: {
                        required: "Vui lòng chọn ng",
                    },
                    note: {
                        required: "Vui lòng chọn lý do",
                    },
                    'option_time[]': {
                        required: "Vui lòng chọn thời gian",
                    },
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "option_time[]") {
                        error.insertAfter("#chk_error_late");
                        return;
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            $('#ot_id').validate({
                ignore: '[readonly]',
                rules: {
                    work_day: {
                        required: true,
                    },
                    note: {
                        required: true,
                    },
                    start_at: {
                        required: true,
                    },
                    end_at: {
                        required: true,
                    },
                    project_id: {
                        required: true,
                        min: 1
                    },
                    ot_type: {
                        required: true,
                    },
                },
                messages: {
                    work_day: {
                        required: "Vui lòng chọn ngày",
                    },
                    note: {
                        required: "Vui lòng nhập lý do",
                    },
                    start_at: {
                        required: "Vui lòng chọn thời gian bắt đầu",
                    },
                    end_at: {
                        required: "Vui lòng chọn thời gian kết thúc",
                    },
                    project_id: {
                        min: "Vui lòng chọn dự án",
                    },
                    ot_type: {
                        required: "Vui lòng chọn hình thức ot",
                    },

                }
            });

            $('#form-assgin').validate({
                ignore: '[readonly]',
                rules: {
                    to_id: {
                        required: true,
                    },
                    assgin_comment: {
                        maxlength: 255
                    }
                },
                messages: {
                    to_id: {
                        required: "Vui lòng chọn người bất kỳ",
                    },
                    assgin_comment: {
                        maxlength: "Bạn đã nhập quá ký tự cho phép"
                    }
                }
            });

        });

        function getData(id, permissionType) {
            $.ajax({
                url: '{{ route('ask_permission.approveDetail') }}',
                type: 'GET',
                dataType: 'JSON',
                data: {
                    'id': id,
                    'permission-type': permissionType,
                },
                success: function(respond) {
                    // $('.id').attr('value', respond.id);
                    if (respond.status == 0) {
                        $('#modal-reject').modal('show');

                    }
                    if (permissionType === 'ot') {
                        $('.creator_id').text(respond.creator_id);
                        $('.project_id').text(respond.project_name);
                        $('.start_at').text(respond.start_at);
                        $('.end_at').text(respond.end_at);
                        $('.minute').text(respond.minute);
                        $('.reason').text(respond.reason);
                        $('.period').text(respond.start_at.slice(0, 5) + ' - ' + respond.end_at.slice(0, 5))
                        $('.caculator_time').text(respond.caculator_time)
                        $('.work_day').text(respond.work_day)
                    } else if (permissionType === 'other') {
                        $('.creator_id').text(respond.user_id);
                        $('#modal-reject .work_day').text(respond.work_day);
                        $('.reason').text(respond.note);
                        $('#modal-reject .option_time').text(respond.option_time)
                    }
                }
            });
            if (permissionType === 'ot') {
                $('.content-hidden').prop('hidden', false)
                $('.permission-type').attr('value', 'ot')
            } else {
                $('.content-hidden').prop('hidden', true)
                $('.permission-type').attr('value', 'other')
                // $('.permission-detail').hide()
                $('.creator_id,.project_id,.start_at,.end_at,.minute,.reason').empty()
            }
        }
    </script>
    <script>
        function message(message, check = false, url = null) {
            $('#form-option-submit').remove();
            if ($(".list-records input[type='checkbox']").is(':checked')) {
                if (check) {
                    $('#modal-assign').modal('show');
                } else {
                    $('#form-option').removeAttr('href');
                    $('#form-option').attr('action', url);
                    $('#modal-option-title').text(message)
                    $('#append-buttom-submit').append(
                        '<button type="button" id="form-option-submit" class="btn btn-primary btn-sm">Đồng Ý</button>')
                    $('#modal-option').modal('show');
                }
            } else {
                $('#modal-option-title').text('Vui lòng chọn đơn bạn muốn phê duyệt!')
                $('#modal-option').modal('show');
            }
        }

        $(document).ready(function() {
            $('#btn-option-yes').on('click', function() {
                let url = '{{ route('multi_approver') }}' + '/yes';
                message('Bạn có chắc muốn duyệt những đơn này?', false, url)
            });
            $('#btn-option-no').on('click', function() {
                let url = '{{ route('multi_approver') }}' + '/no';
                message('Bạn có chắc muốn hủy những đơn này?', false, url)
            });
            $('#btn-option-assgin').on('click', function() {
                let html = '';
                $(".list-records input[type='checkbox']:checked").each(function() {
                    html += '<input type="hidden" name="ids[]" id="assgin-id" value="' + $(this)
                        .val() + '">'
                });
                $('#append-value-assgin').append(html);
                message(' ', true, null)
            });
            $(document).on('click', '#form-option-submit', function() {
                $('#form-option').submit();
            });

            $('#mdb-select').materialSelect();
            $(".checkbox-toggle").click(function() {
                var clicks = $(this).data('clicks');
                if (clicks) {
                    //Uncheck all checkboxes
                    // $("input[type='checkbox']").removeAttr('checked')

                    $(".list-records input[type='checkbox']").iCheck("uncheck");
                    $("#check-all").removeClass("fa-check-square").addClass('fa-square');
                } else {
                    //Check all checkboxes
                    $(".list-records input[type='checkbox']").iCheck("check");
                    $("#check-all").removeClass("fa-square").addClass('fa-check-square');
                }
                $(this).data("clicks", !clicks);

            });
            $("#tableOther_filter.dataTables_filter").prepend($("#GroupFilterOther")).addClass(
                'row justify-content-end');
            $("div#tableOther_filter.dataTables_filter label").addClass("text-right").css("margin-right", "15px");
            $("#tableOt_filter.dataTables_filter").prepend($("#GroupFilterOt")).addClass('row justify-content-end');
            $("div#tableOt_filter.dataTables_filter label").addClass("text-right").css("margin-right", "15px");
            // $$("label#tableOt_filter.dataTables_filter label")
            $('#GroupFilterOther, #GroupFilterOt').on('change', function() {
                var idOption = $(this).attr('id');
                var idTable = idOption == 'GroupFilterOther' ? 'tableOther' : 'tableOt';
                $.fn.dataTable.ext.search.push(
                    function(settings, data, dataIndex) {
                        if (settings.nTable.id !== idTable) {
                            return true;
                        }
                        var selectedItem = $(`#${idOption}`).val()
                        if (selectedItem !== "") {
                            var api = new $.fn.dataTable.Api(settings);
                            return api.row(dataIndex).nodes().to$().hasClass(selectedItem);
                        } else {
                            return true;
                        }
                    }
                );
                var tableSelected = $(`#${idTable}`).DataTable();
                tableSelected.draw();
            });
        });
    </script>
@endpush
