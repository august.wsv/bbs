<link href="{{asset_ver('css/org_chart.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ asset_ver('mdb/js/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var dataGroup = @json($dataGroup);
        var TotalGroup = Object.keys(dataGroup).length;
        $('.level-2-wrapper').css('grid-template-columns', 'repeat(' + TotalGroup + ', 2fr)')
        function setContrast(rgb) {
            rgb = rgb.substring(4, rgb.length-1).replace(/ /g, '').split(',');
            return (Number(rgb[0]) * 299 + Number(rgb[1]) * 587 + Number(rgb[2]) * 114) / 1000 > 125 ? 'black' : 'white';
        }
        $(".rectangle").each(function() {
           var color = $(this).css("background-color");
           $(this).css("color", setContrast(color));
        });
    });
</script>

<div class="container">
    <h1 class="rectangle level-1">JVB</h1>
    @foreach($directors as $key => $director)
        <div class="level-1-{{ $key }} rectangle">
            <img class="avatar" src="{{ $director->avatar }}" alt="{{ $director->name }}" onerror="this.src='{{ URL_IMAGE_NO_AVATAR }}'">
            {{$director->name}}
        </div>
    @endforeach
    <ol class="level-2-wrapper">
        @foreach($dataGroup as $name_groups => $groups)
            <?php $teamOfGroup = count((array)$groups) ?>
            <li style="width:{{ 220*$teamOfGroup + ($teamOfGroup - 1)*20 }}px">
                <h2 class="level-2 rectangle">{{$name_groups}}</h2>
                <ol class="level-3-wrapper" style=" grid-template-columns: repeat({{ $teamOfGroup }}, 1fr); @if($teamOfGroup == 1) position: unset; @endif ">
                <div class="path" style="width: {{ ($teamOfGroup - 1)*240 }}px"></div>
                    @foreach($groups as $name_teams => $teams)
                        <li>                    
                            <h3 class="level-3 rectangle" style="background-color:{{ $teams['color'] }} ">
                                <?php $logoUrl =  $teams['image'] ? lfm_thumbnail($teams['image']) : JVB_LOGO_URL; ?>
                                <img class="avatar" alt="{{ $name_teams }}" src="{{ $logoUrl }}" onerror="this.src='{{ JVB_LOGO_URL }}'">
                                <span>{{ $name_teams }}</span>
                            </h3>
                            <ol class="level-4-wrapper">
                                <li>
                                    <?php $color = isset(ORG_TREE_LEADER_COLORS[($teams['leader']['jobtitle_leader'])]) ? ORG_TREE_LEADER_COLORS[($teams['leader']['jobtitle_leader'])] : '';?>
                                    <h4 class="level-4 rectangle" style="background-color:{{$color}} ">       
                                        <img class="avatar" src="{{ $teams['leader']['avatar_leader'] }}" alt="{{ $teams['leader']['name_leader'] }}" onerror="this.src='{{ URL_IMAGE_NO_AVATAR }}'">
                                        <span>{{ $teams['leader']['name_leader'] }}</span>
                                    </h4>
                                </li>
                                @foreach($teams['member'] as $index => $person)
                                    <li>
                                        <?php $color = $person['jobtitle_member'] == 0 ? ORG_TREE_POSITION_COLORS[($person['position_member'])] : ORG_TREE_LEADER_COLORS[($person['jobtitle_member'])];?>
                                        <h4 class="level-4 rectangle" style="background-color:{{ $color }} ">       
                                            <img class="avatar" src="{{ $person['avatar'] }}" alt="{{ $person['name'] }}" onerror="this.src='{{ URL_IMAGE_NO_AVATAR }}'">
                                            <span>{{ $person['name'] }}</span>
                                        </h4>
                                    </li>
                                @endforeach
                            </ol>
                        </li>
                    @endforeach
                </ol>
            </li>
        @endforeach
    </ol>
</div>
