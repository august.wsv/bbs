@php
    use App\Models\Project;
    use App\Models\Group;
    $groups = Group::select('id', 'name')->get();
    $categories = Project::select('category')
        ->whereNotNull('category')
        ->distinct('category')
        ->get()
        ->pluck('category');
@endphp
@extends('layouts.end_user')
@section('page-title', __l('Project'))

@section('breadcrumbs')
    {!! Breadcrumbs::render('project') !!}
@endsection
@section('content')

    @can('team-leader')
        <div class=" fixed-action-btn">
            <a href="#" onclick="location.href='{{route('create_project')}}'"
               class="btn-floating btn-lg red waves-effect waves-light text-white"
               title="Tạo dự án">
                <i class="fas fa-plus"></i>
            </a>
        </div>
    @endcan
   <form class="mb-0">
        <div class="md-form active-cyan-2 mb-3">
            <div class="custom-css">
                <select name="group_id" id="" class="custom-select">
                    <option value="">Group quản lý</option>
                    @foreach ($groups as $group)
                    <option @if (request('group_id')==$group->id) selected @endif value="{{ $group->id }}">
                        {{ $group->name }}
                    </option>
                    @endforeach
                </select>

                <select name="category" id="" class="custom-select">
                    <option value="">Lĩnh vực</option>
                    @foreach ($categories as $category)
                    <option @if (request('category')==$category) selected @endif value="{{ $category }}">
                        {{ $category }}
                    </option>
                    @endforeach
                </select>

                <select name="status" id="" class="custom-select">
                    <option value="">Trạng thái</option>
                    @foreach (STATUS_PROJECT as $key => $status)
                    <option @if (request('status') !='' && request('status')==$key) selected @endif value="{{ $key }}">
                        {{ $status }}
                    </option>
                    @endforeach
                </select>

                <input id="search_input" name="search" value="{{request('search', $search)}}" class="form-control mb-0"
                    type="text" placeholder="{{__l('Search')}}" aria-label="Search" style="font-size: 15px">
                

                <div class="input-group-prepend">
                    <button id="btn_search_submit" class="btn btn-primary btn-sm" style="margin-top: 0px !important;"
                        id="inputGroup-sizing-default">Tìm kiếm</button>
                </div>
            </div>
            <input type="hidden" name="page_size" value="{{$perPage}}">
        </div>
    </form>
    @if($projects->isNotEmpty())
        <p class="mb-0">{{__l('total_record', ['number' => $projects->total()])}}</p>
        <table id="contactTbl" class="table table-striped">
            <colgroup>
                <col class="d-none d-sm-table-cell" style="width: 50px">
                <col style="">
                <col class="d-none d-sm-table-cell" style="">
                <col class="d-none d-sm-table-cell" style="width: 180px">
                <col class="d-none d-sm-table-cell" style="width: 10%">
                <col style="width: 180px">
                <col class="d-none d-sm-table-cell" style="width: 10%">
                <col class="d-none d-sm-table-cell" style="width: 60px">
            </colgroup>
            <thead>
            <tr>
                <th class="d-none d-sm-table-cell">#</th>
                <th>Tên</th>
                <th class="d-none d-sm-table-cell">Khách hàng</th>
                <th>Người tạo dự án</th>
                <th class="d-none d-sm-table-cell text-center">Bắt đầu</th>
                <th class="d-none d-sm-table-cell text-center">Kết thúc</th>
                <th class="d-none d-sm-table-cell">Trạng thái</th>
                <th class="d-none d-sm-table-cell"></th>
            </tr>
            </thead>
            <tbody>

            @foreach($projects as $id => $row)
                <tr>
                    <th class="d-none d-sm-table-cell">{{ $id + 1 }}</th>
                    <td onclick="location.href='{{route('project_detail', ['id' => $row->id])}}'">{{$row->name}}</td>
                    <td class="d-none d-sm-table-cell">{{$row->customer}}</td>
                    <td>{{$row->leader->name ?? 'Đang cập nhật'}}</td>
                    <td class="text-center d-none d-sm-table-cell">
                        {{$row->start_date}}
                    </td>
                    <td class="text-center d-none d-sm-table-cell">
                        {{$row->end_date}}
                    </td>
                    <td class="d-none d-sm-table-cell">{{STATUS_PROJECT[$row->status]}}</td>
                    <td class="text-center d-none d-sm-table-cell">
                        <a href="{{route('project_detail', ['id' => $row->id])}}"
                           class="btn btn-primary btn-sm">Chi tiết
                        </a>
                        @can('create_task', $row)
                        <a href="{{route('project_tasks', ['id' => $row->id])}}"
                           class="btn btn-warning btn-sm">Quản lý task
                        </a>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        @if ($projects->lastPage() > 1)
            @include('common.paginate_eu', ['records' => $projects])
        @endif
    @else
        <h2>{{__l('list_empty', ['name'=>'dự án'])}}</h2>
    @endif
@endsection

@push('extend-css')
    <link rel="stylesheet" href="{{asset_ver('css/project/index.css')}}">
@endpush