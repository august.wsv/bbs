@extends('layouts.end_user')
@section('page-title', $project->name)

@section('breadcrumbs')
    {!! Breadcrumbs::render('project_detail', $project) !!}
@endsection
@php
    $sprintStatus = array_map(function ($item) {
        return __l("sprint_status_$item");
    }, array_flip(SPRINT_STATUS));
    
    $storeErrors = $errors->store;
    $updateErrors = $errors->update;
    $bagErrors = [];
    foreach ($errors->getBags() as $bagName => $bagItem) {
        $bagErrors[$bagName] = $bagItem->toArray();
    }
@endphp
@section('content')
    <div class="mt-4">
        <div class="text-uppercase">
            <i class="fas fa-circle" style="{{ COLOR_STATUS_PROJECT[$project->status] }}">
                {{ STATUS_PROJECT[$project->status] }}</i>
        </div>
        @can('edit', $project)
            <a href="{{ route('project_edit', ['id' => $project->id]) }}" class="d-none d-lg-block float-right btn btn-warning"
                style="position: relative; top: -40px;">Chỉnh sửa</a>
        @endcan
    </div>
    <div class="row mt-4">
        <div class="col-md-4 text-center">
            <img alt="Dự án" src="{{ $project->image_url }}" class="img-fluid z-depth-1 mb-3">
        </div>
        <div class="col-md-8">

            <table class="table table-bordered table-striped">
                <tr>
                    <td width="30%" class="font-weight-bold">Khách hàng</td>
                    <td>{{ $project->customer }}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Loại dự án</td>
                    <td>{{ PROJECT_TYPE[$project->project_type] }}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Quy mô</td>
                    <td>{{ $project->scale }} man/month</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Thời gian</td>
                    <td>{{ $project->amount_of_time }} months</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Ngày bắt đầu</td>
                    <td>{{ $project->start_date }}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Ngày kết thúc</td>
                    <td>{{ $project->end_date }}</td>
                </tr>
            </table>
            @if ($project->projectMembers->count() > 0)
                <div class="card">
                    <div class="card-body">
                        <div id="table" class="table-editable">
                            <span>
                                <h5 class="text-center">Danh sách thành viên trong dự án</h5>
                            </span>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center center-td" rowspan="2">Tên</th>
                                        <th class="text-center center-td" rowspan="2">Vai trò</th>
                                        <th class="text-center" colspan="2">Công số</th>
                                        <th class="text-center " colspan="2">Thời gian</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">Hợp đồng</th>
                                        <th class="text-center">Thực tế</th>
                                        <th class="text-center">Bắt đầu</th>
                                        <th class="text-center">Kết thúc</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($project->projectMembers as $projectMember)
                                        <tr>
                                            <td>{{ $projectMember->user->name ?? '' }}</td>
                                            <td>{{ array_key_exists($projectMember->mission, MISSION_PROJECT) ? MISSION_PROJECT[$projectMember->mission] : '' }}
                                            </td>
                                            <td class="text-right">{{ $projectMember->contract }}</td>
                                            <td class="text-right">{{ $projectMember->reality }}</td>
                                            <td class="text-right">{{ $projectMember->time_start }}</td>
                                            <td class="text-right">{{ $projectMember->time_end }}</td>
                                        </tr>
                                    @endforeach
                                    <!-- This is our clonable table line -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <h3 class="my-4">Chưa có thông tin về thành viên của dự án</h3>
            @endif
            <div class=" border border-light rounded my-3 p-4">
                <div class=>
                    <h5 class="text-info">Kỹ thuật</h5>
                    <p> {!! nl2br(e($project->technical)) !!}</p>
                </div>
                <div class="mt-4">
                    <h5 class="text-info">Công cụ sử dụng</h5>
                    <span id="font-top-subtitle"> {!! nl2br(e($project->tools)) !!} </span>
                </div>
                <div class="mt-4">
                    <h5 class="text-info">Mô tả</h5>
                    <p>{!! $project->description !!}</p>
                </div>
            </div>
        </div>

    </div>

    @can('crudSprint', $project)
        <div class="row mt-4" style="margin-bottom: 500px">
            <div class="col-12">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <h4 class="text-center"><span style="opacity: 0.8">Sprints</span></h4>

                                    @if ($project->projectSprints->count() > 0)
                                        <div class="btn btn-info btn-sm float-right mr-0" data-toggle="modal"
                                            data-target="#modal-store-sprint">
                                            Thêm sprint
                                        </div>
                                        <table class="table table-bordered" id="table-sprints">
                                            <colgroup>
                                                <col style="width: 30px;">
                                                <col>
                                                <col>
                                                <col style="width: 8rem">
                                                <col style="width: 100px;">
                                                <col style="width: 5rem;">
                                            </colgroup>

                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tên</th>
                                                    <th>Mô tả</th>
                                                    <th class="p-0 position-relative">
                                                        <div class="d-flex flex-wrap">
                                                            <div class="py-3"
                                                                style="border-bottom: 1px solid #ccc; width: 100%;">
                                                                Thời gian
                                                            </div>
                                                            <div class="flex-1 py-3">Bắt đầu</div>

                                                            <div class="separate" style="--height: 50%;"></div>

                                                            <div class="flex-1 py-3">Kết thúc</div>
                                                        </div>
                                                    </th>
                                                    <th class="p-0 position-relative">
                                                        Công số
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach ($sprints as $key => $sprint)
                                                    <tr class="fw-normal {{ $sprint->status < 1 ? '' : ($sprint->status == 1 ? 'active-row' : 'ended-row') }}"
                                                        data-sprint-id="{{ $sprint->id }}"
                                                        data-project-id="{{ $project->id }}"
                                                        data-sprint-status="{{ $sprint->status }}">
                                                        <td class="sprint-id">{{ $key + 1 }}</td>
                                                        <td class="truncate fw-semibold sprint-name" style="width: 5rem">
                                                            {{ html_entity_decode($sprint->name) }}</td>
                                                        <td class="sprint-description">{!! $sprint->description !!}</td>
                                                        <td class="p-0 position-relative">
                                                            <div class="h-100-js multi-col">
                                                                <div class="py-3 multi-col-item">
                                                                    <input type="text" readonly
                                                                        value="{{ Carbon::parse($sprint->start_date)->format('Y/m/d') }}"
                                                                        class="form-control form-control-sm sprint-time-start w-100px">
                                                                </div>

                                                                <div class="separate"></div>

                                                                <div class="py-3 multi-col-item">
                                                                    <input type="text" readonly
                                                                        value="{{ Carbon::parse($sprint->end_date)->format('Y/m/d') }}"
                                                                        class="form-control form-control-sm sprint-time-end w-100px">
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="text-center sprint-effort">{{$sprint->effort}}</td>
                                                        <td>
                                                            <div class="d-flex h-100-js align-items-center" style="gap: 20px;">
                                                                <div class="btn-custom btn-edit sprint waves-effect"
                                                                    data-sprint-id="{{ $sprint->id }}">Sửa</div>
                                                                <div class="btn-custom btn-delete sprint waves-effect"
                                                                    data-url="{{ route('project_sprint.delete', $sprint->id) }}">
                                                                    Xóa</div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="btn btn-primary d-block mx-auto" data-toggle="modal"
                                            data-target="#modal-store-sprint">Hãy tạo sprint đầu tiên cho dự án</div>
                                    @endif

                                    {{$sprints->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-store-sprint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-center approval-ipad-pro" role="document">
                <div class="modal-content modal-center-display bg-img-day-off" id="bg-img"
                    style="background-image: url({{ asset_ver('img/font/xin_nghi.png') }})">
                    <div class="modal-header text-center border-bottom-0 pb-0">
                        <h4 class="modal-title w-100 font-weight-bold pt-2">Thêm sprint</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="btn-close-icon" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="modal-body mx-3 mt-0 py-0" id="form-add-sprint" method="POST"
                        action="{{ route('project_sprint.store') }}">
                        @csrf
                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                        <div class="mb-3 group-input">
                            <label class="text-w-400" for="input-print-name">
                                <span>Tên sprint</span> <span style="color: red">*</span>
                            </label>
                            <input type="text" id="input-print-name" class="form-control" autocomplete="off"
                                name="name" value="{{ old('name') }}">
                            <div class="error"></div>
                        </div>

                        <div class="mb-3 group-input">
                            <label class="text-w-400" for="input-print-desc">
                                <span>Mô tả</span>
                                <span style="color: red">*</span>
                            </label>
                            <input name="description" class="form-control sprint-editor" id="input-print-desc"
                                value="{{ old('description') }}">
                            <div class="error"></div>
                        </div>

                        <div class="mb-3 d-flex flex-wrap" style="gap: 0 5rem">
                            <div class="group-input">
                                <label class="text-w-400" for="input-print-name">
                                    <span>Từ ngày</span> <span style="color: red">*</span>
                                </label>
                                <div class="position-relative">
                                    <input type="text" class="form-control input-date" id="input-start-date"
                                        autocomplete="off" name="start_date" readonly value="{{ old('start_date') }}">
                                    <i class="far fa-calendar-alt position-absolute calendar-search"></i>
                                </div>
                                <div class="error"></div>
                            </div>

                            <div class="group-input">
                                <label class="text-w-400" for="input-print-name">
                                    <span>Đến ngày</span> <span style="color: red">*</span>
                                </label>
                                <div class="position-relative">
                                    <input type="text" class="form-control input-date" id="input-end-date"
                                        autocomplete="off" name="end_date" readonly value="{{ old('start_date') }}">
                                    <i class="far fa-calendar-alt position-absolute calendar-search"></i>
                                </div>
                                <div class="error"></div>
                            </div>
                        </div>

                        <div class="mb-3 group-input">
                            <label class="text-w-400" for="input-print-effort">
                                <span>Công số</span> <span style="color: red">*</span>
                            </label>
                            <input type="text" id="input-print-effort" class="form-control border-none" autocomplete="off"
                                name="effort" value="{{ old('effort') }}">
                            <div class="error"></div>
                        </div>

                        <div class="mb-3 group-input">
                            <label class="text-w-400" for="input-print-status">
                                <span>Trạng thái</span>
                                <span style="color: red">*</span>
                            </label>

                            {{ Form::select('status', $sprintStatus, old('status', SPRINT_STATUS['new']), [
                                'class' => 'browser-default custom-select search-day-off border-radius-1 option-select',
                            ]) }}
                        </div>

                        <div class="mb-3">
                            <button class="btn btn-primary d-block mx-auto">Thêm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-delete-sprint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <input type="hidden" value="32" name="id_close" id="id-close">
            <div class="modal-dialog" role="document">
                <form class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="ml-4 modal-title text-center" id="exampleModalLabel" style="color:!important">Bạn có chắc
                            muốn xóa?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-secondary w-25 waves-effect waves-light">ĐỒNG Ý</button>
                        <span class="btn btn-primary w-25 waves-effect waves-light" data-dismiss="modal">HỦY</span>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal fade" id="modal-update-sprint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-center approval-ipad-pro" role="document">
                <div class="modal-content modal-center-display bg-img-day-off"
                    style="background-image: url({{ asset_ver('img/font/xin_nghi.png') }})">
                    <div class="modal-header text-center border-bottom-0 pb-0">
                        <h4 class="modal-title w-100 font-weight-bold pt-2"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="btn-close-icon" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="modal-body mx-3 mt-0 py-0" id="form-edit-sprint" method="POST"
                        action="{{ route('project_sprint.update') }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                        <input type="hidden" name="sprint_id" value="{{ old('sprint_id') }}">
                        <div class="mb-3 group-input">
                            <label class="text-w-400" for="form-edit-input-print-name">
                                <span>Tên sprint</span> <span style="color: red">*</span>
                            </label>
                            <input type="text" id="form-edit-input-print-name" class="form-control"
                                autocomplete="off" name="name" value="{{ old('name') }}">
                            <div class="error"></div>
                        </div>

                        <div class="mb-3 group-input">
                            <label class="text-w-400" for="form-edit-input-print-desc">
                                <span>Mô tả</span>
                                <span style="color: red">*</span>
                            </label>
                            <input name="description" class="form-control sprint-editor" id="form-edit-input-print-desc"
                                value="{{ old('description') }}">
                            <div class="error"></div>
                        </div>

                        <div class="mb-3 d-flex flex-wrap" style="gap: 0 5rem">
                            <div class="group-input">
                                <label class="text-w-400" for="form-edit-input-print-name">
                                    <span>Từ ngày</span> <span style="color: red">*</span>
                                </label>
                                <div class="position-relative">
                                    <input type="text" class="form-control input-date" id="form-edit-input-start-date"
                                        autocomplete="off" name="start_date" readonly value="{{ old('start_date') }}">
                                    <i class="far fa-calendar-alt position-absolute calendar-search cursor-pointer"></i>
                                </div>
                                <div class="error"></div>
                            </div>

                            <div class="group-input">
                                <label class="text-w-400" for="form-edit-input-print-name">
                                    <span>Đến ngày</span> <span style="color: red">*</span>
                                </label>
                                <div class="position-relative">
                                    <input type="text" class="form-control input-date" id="form-edit-input-end-date"
                                        autocomplete="off" name="end_date" readonly value="{{ old('start_date') }}">
                                    <i class="far fa-calendar-alt position-absolute calendar-search cursor-pointer"></i>
                                </div>
                                <div class="error"></div>
                            </div>
                        </div>

                        <div class="mb-3 group-input">
                            <label class="text-w-400" for="form-edit-input-print-effort">
                                <span>Công số</span> <span style="color: red">*</span>
                            </label>
                            <input type="text" id="form-edit-input-print-effort" class="form-control"
                                autocomplete="off" name="effort" value="{{ old('effort') }}">
                            <div class="error"></div>
                        </div>


                        <div class="mb-3 group-input">
                            <label class="text-w-400" for="form-edit-input-print-status">
                                <span>Trạng thái</span>
                                <span style="color: red">*</span>
                            </label>

                            {{ Form::select('status', $sprintStatus, old('status', SPRINT_STATUS['new']), [
                                'class' => 'browser-default custom-select search-day-off border-radius-1 option-select',
                            ]) }}
                        </div>

                        <div class="mb-3">
                            <button class="btn btn-primary d-block mx-auto">Sửa</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@endsection

@push('extend-css')
    <link rel="stylesheet" href="{{ asset_ver('css/user/project-detail.css') }}">
    <link href="{{ asset('bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@endpush


@push('extend-js')
    <script>
        const bagErrors = JSON.parse('{{ json_encode($bagErrors) }}'.replaceAll('&quot;', '"'));
        const oldSprintId = {{ old('sprint_id', 'null') }};
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset_ver('js/user/project-detail.js') }}"></script>
@endpush
