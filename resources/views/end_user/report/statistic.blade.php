@extends('layouts.end_user')
@section('page-title', __l('Statistic'))

@section('content')
<iframe id="project_iframe" src="{{ config('app.bbs_mobile_url') . '/user-statistical/board-user/' }}" title="{{ __l('Statistic') }}" style="width: 100%; border: none">
</iframe>
@endsection

@push('extend-css')
<style>
    #project_iframe {
        min-height: 80vh;
        height: calc(100vh - 80px);
    }
</style>
@endpush

@push('extend-js')
<script>
    $(function() {
        $('#project_iframe').on('load', function() {
            if (this.contentDocument) {
                this.style.height = this.contentDocument.body.scrollHeight + 'px';
            }
        });
    })
</script>
@endpush