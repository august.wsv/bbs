@extends('layouts.end_user_test_vue')
@section('page-title', __l('Event'))

@section('breadcrumbs')
    @if(empty($search))
        {!! Breadcrumbs::render('event') !!}
    @else
        {!! Breadcrumbs::render('event_search', $search) !!}
    @endif
@endsection
@section('content')
    <form>
        <div class="md-form active-cyan-2 mb-2">
            @include('layouts.partials.frontend.search-input', ['class' => 'tien','search' => $search, 'text' => __l('Search')])
            <input type="hidden" name="page_size" value="{{$perPage}}">
        </div>
    </form>
    @if($events->isNotEmpty())
        <div class="row mb-3">
            <div class="col-sm-6"></div>
            <div class="col-sm-6 text-right">
                <a href="{{route('event_calendar')}}" class="btn btn-primary waves-effect" id="inputGroup-sizing-default">
                    <i class="fas fa-calendar icon-calendar-event"></i> Xem lịch
                </a>
            </div>
        </div>
        <event-list-view />

        @if ($events->lastPage() > 1)
            @include('common.paginate_eu', ['records' => $events])
        @endif
    @else
        <h2>{{__l('list_empty', ['name'=>'sự kiện'])}}</h2>
    @endif

@endsection
