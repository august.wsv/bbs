@extends('layouts.end_user')
@section('breadcrumbs')
    {!! Breadcrumbs::render('device') !!}
@endsection
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="{{asset_ver('css/t_device/style.css')}}">
    
    <div class="col-md-12">
        <form id="formSubmit" action="{{route('t_devices.t_save_form_device')}}" method="POST">
        @csrf
        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
        <div class="row">
            <div class="col-md-12" id="append_item">
                <p for="user" class="mb-15">Khai báo thiết bị : </p>
                <div class="error">

                </div>
                <?php
                    $count = 0;
                ?>
                <!-- @foreach($devices as $key_item=>$item)
                <div class="border_{{$count}}">
                    <div class="col-md-12" style="border: 1px solid #cccccc; padding-top: 12px; padding-bottom: 12px; margin-bottom: 12px">
                        <div>
                            <label for="staticEmail" class="">Thiết bị :<a href="#"></a></label>
                            <button data-count="{{$count}}" type="button" class="btn btn-danger btn-sm pull-right btn-remove-device"><i class="fa fa-times"></i></button>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group row col-md-6">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Tìm thiết bị</label>
                                
                                <div class="col-sm-8">
                                <select style="display:block!important" data-key="{{$key_item}}" class="form-control slect_device_import user_device" name="device_select[{{$key_item}}]">
                                
                                    <option value="MANUALLY">Tự thêm</option>

                                    @foreach($option_devices as $option)
                                        <option value="{{$option->id}}" @if($item->id == $option->id) selected @endif>{{$option->name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group row col-md-6">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Loại thiết bị</label>
                                <div class="col-sm-8">
                                    <select style="display:block!important" class="form-control slect_type_{{$key_item}}" value="" name="device_type_id[{{$count}}]">
                                        @foreach($type_devices as $type_device)
                                            <option @if($item->device_type_id == $type_device->id) selected="selected" @endif value="{{$type_device->id}}">{{$type_device->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                            </div>
                        </div>
                        <div class="attribute_device_{{$key_item}}">
                            @foreach($item->specification as $key=>$value)
                                <div class="row">   
                                    @foreach($value as $key_v=>$value_v)
                                        @if(is_array($value_v))
                                        <div class="form-group row col-md-6 parent_attribute">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">{{$key}}</label>
                                            <div class="col-sm-8 append_input">
                                                <input readonly name="specification[{{$count}}][{{$key}}][{{$key_v}}][name]" type="text" value="{{$value_v['name']}}" class="form-control">
                                                <input type="hidden" name="specification[{{$count}}][{{$key}}][{{$key_v}}][mutiple]" value="{{$value_v['mutiple']}}">
                                                <input type="hidden" name="specification[{{$count}}][{{$key}}][{{$key_v}}][required]" value="{{$value_v['required']}}"> 
                                            </div>
                                            @if(array_key_last($value) == $key_v)
                                            <div class="col-sm-2">
                                                <button disabled data-key-last="{{array_key_last($value)}}" data-name="specification[{{$count}}][{{$key}}]" type="button" class="btn btn-primary btn-sm add_input"><i class="fa fa-plus"></i></button>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="form-group row col-md-6">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
                                            <div class="col-sm-8">
                                                <input readonly name="specification[{{$count}}][{{$key}}][{{$key_v}}][note]" value="@if(isset($value_v['note'])){{$value_v['note']}}@endif" type="text" class="form-control">
                                            </div>
                                        </div>
                                        @else
                                            @if($key_v == 'name')
                                            <div class="form-group row col-md-6">
                                                <label for="staticEmail" class="col-sm-2 col-form-label">{{$key}}</label>
                                                <div class="col-sm-8">
                                                    <input readonly name="specification[{{$count}}][{{$key}}][name]" type="text" value="{{$value_v}}" class="form-control">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button disabled type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="form-group row col-md-6">
                                                <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
                                                <div class="col-sm-8">
                                                    <input readonly name="specification[{{$count}}][{{$key}}][note]" value="" type="text" class="form-control">
                                                </div>
                                            </div>

                                            @endif
                                        @endif
                                    @endforeach
                                    
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <?php
                    $count++;
                ?>
                @endforeach -->
                
            </div>
            <div class="col-md-12"  style="margin-bottom: 12px">
                <button type="button" id="add_device" class="btn btn-primary">Thêm thiết bị <i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="box-footer clearfix">
            <div class="col-xs-12 option-with" style="margin-top: 15px">
                <div align="center">
                    <button class="btn btn-info mr-2" id="btn_submit_device" disabled>
                        <i class="fa fa-save"></i> <span>Lưu</span>
                    </button>
                </div>
            </div>
            <!-- /.col-xs-6 -->
        </div>
        </form>
    </div>

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script type="application/javascript" src="{{asset('js/admin/t-devices/add-user.js')}}"></script>

@endsection
