@extends('layouts.end_user_test_vue')
@section('page-title', $post->name)

@section('breadcrumbs')
    {!! Breadcrumbs::render('post_detail', $post) !!}
@endsection
@section('content')
    <post-detail-view />
@endsection