@extends('layouts.end_user')
@section('page-title', $post->name)

@section('breadcrumbs')
    {!! Breadcrumbs::render('post_detail', $post) !!}
@endsection
@section('content')
    <div class="card mb-3">
        <div class="card-body">
            <div class="d-flex justify-content-center">
                <img width="500px" src="{{ asset($post->image_url) }}" alt="">
            </div>
            <p class="card-text">{!! $post->content !!}</p>
            <p class="card-title">{{ implode(',', $post->tag_arrs) }}</p>
            <p>
                <b>{{ $post->author_name }}</b>, {{ $post->created_at->format(DATE_FORMAT) }}
            </p>
        </div>
    </div>
@endsection
