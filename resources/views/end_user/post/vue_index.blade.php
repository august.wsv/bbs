@extends('layouts.end_user_test_vue')
@section('page-title', __l('Post'))

@section('breadcrumbs')
    @if(empty($search))
        {!! Breadcrumbs::render('post') !!}
    @else
        {!! Breadcrumbs::render('post_search', $search) !!}

    @endif
@endsection
@section('content')
    <!-- Search form -->
    <form class="mb-4">
        <div class="md-form active-cyan-2 mb-3">
            <input name="search" value="{{old('search', $search)}}" class="form-control" type="text"
                   placeholder="{{__l('Search')}}" aria-label="Search">
            <input type="hidden" name="page_size" value="{{$perPage}}">
        </div>
    </form>
    @if($posts->isNotEmpty())
        <post-list-view />
    @else
        <h2>{{__l('list_empty', ['name'=>'thông báo'])}}</h2>
    @endif
@endsection