@extends('layouts.end_user')
@section('breadcrumbs')
    {!! Breadcrumbs::render('meetings') !!}
@endsection
@push('extend-css')
    <style>
        .filter-option-inner-inner {
            line-height: 32px !important;
        }
    </style>
@endpush
@section('content')
    <div class="row my-3">
        <div class="col-6 mt-2 d-none d-sm-block" id="select-room">
            @foreach($meetingRooms as $key => $room)
                <input type="checkbox" class="form-check-input meeting_room_btn" id="room_{{ $key }}" name="room_name" value="{{$room->id}}" checked>
                <label class="form-check-label pl-4 mr-3" for="room_{{ $key }}"
                       style="color: {{$room->color}}" data-toggle="tooltip"
                       data-html="true" title="{!! nl2br($room->detail) !!}">
                    Room {{$room->name}}
                </label>
            @endforeach
        </div>
        <div class="col-6 mt-2" id="filterForm">
            <div class="form-check pl-0">
                <input type="checkbox" class="form-check-input" id="is_join" name="is_join"
                       value="1">
                <label class="form-check-label" for="is_join"> Có tham gia
                </label>
            </div>
            <input id="txtSearch" name="search" class="form-control" type="text"
                   placeholder="{{__l('Search')}}" aria-label="Search">
        </div>
        <div class="col-12 d-sm-none">
            <div class="row">
                @foreach($meetingRooms as $room)
                    <div class="col-6">
                        <input type="checkbox" class="form-check-input" id="room_{{ $key }}">
                        <label class="form-check-label pl-4 mr-3 meeting_room_btn" for="room_{{ $key }}"
                               style="color: {{$room->color}}" data-id="{{$room->id}}" data-toggle="tooltip"
                               data-html="true" title="{!! nl2br($room->detail) !!}">
                            Room {{$room->name}}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- search -->


    <!-- end:search -->

    <div id="calendar-meeting">
    </div>

    <!-- add booking -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center w-100" id="myModalLabel">Đặt lịch họp</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="addMeeting" method="post">
                        @csrf
                        <input type="hidden" name="meeting_id" id="meeting_id" value="">
                        <input type="hidden" name="booking_id" id="booking_id" value="">
                        <input type="hidden" name="days_repeat" id="days_repeat" value="">
                        <div class="row mb-2">
                            <div class="meeting_room_id col-6">
                                <label class="ml-0">Chọn phòng họp *</label>
                                <select class="form-control browser-default" name="meeting_room_id"
                                        id="meeting_room_id">
                                    <option value="">Chọn phòng họp</option>
                                    @foreach($meetingRooms as $meeting)
                                        <option value="{{$meeting->id}}">{{$meeting->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-6 start_time">
                                        <label class="ml-0">
                                            <span class="d-block d-sm-none">Từ *</span>
                                            <span class="d-none d-sm-block">Thời gian bắt đầu *</span>
                                        </label>
                                        <div class="  timepicker">
                                            <input class="form-control" name="start" id="start_time" data-format="hh:mm"
                                                   data-provide="timepicker">
                                        </div>
                                    </div>
                                    <div class="col-6 mb-1 end_time">

                                        <label class="ml-0">
                                            <span class="d-block d-sm-none">Đến *</span>
                                            <span class="d-none d-sm-block">Thời gian kết thúc *</span></label>
                                        <div class=" bootstrap-timepicker timepicker">
                                            <input class="form-control  timepicker " name="end" id="end_time"
                                                   data-format="hh:mm" data-provide="timepicker">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title">
                            <lable class="ml-0 mt-3">Tiêu đề *</lable>
                            <input type="text" class="form-control mt-2 mb-3" name="title" id="title"
                                   placeholder="Tiêu đề cuộc họp...">
                        </div>
                        <div class="row mt-1">
                            <div class="col-12 users_id">
                                <label class="ml-0">Chọn người tham gia *</label>
                                <select class="selectpicker form-control" multiple data-live-search="true"
                                        name="participants[]" id="participants" data-none-selected-text
                                        title="Chọn người tham gia">
                                    @foreach($groups as $group => $users)
                                        <optgroup label="{{$group}}">
                                            @foreach($users as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="content">
                            <lable class="ml-0">Nội dung *</lable>
                            <textarea class="form-control mt-2" name="content" id="content"
                                      placeholder="Nội dung cuộc họp ..."></textarea>
                        </div>
                        </br>
                        <div id="#repeat" class="mb-2">
                            <lable class="mr-2">Lặp lại:</lable>
                            <input type="radio" name="repeat_type" id="non_repeat" value="0" checked
                                   style="display: none;">
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" id="materialInline1"
                                       name="repeat_type"
                                       value="1">
                                <label class="form-check-label" for="materialInline1">Tuần</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" id="materialInline2"
                                       name="repeat_type"
                                       value="2">
                                <label class="form-check-label" for="materialInline2">Tháng</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" id="materialInline3"
                                       name="repeat_type"
                                       value="3">
                                <label class="form-check-label" for="materialInline3">Năm</label>
                            </div>
                        </div>
                        <div class="mt-0">
                            <div class="form-check pl-0">
                                <input type="checkbox" class="form-check-input" id="is_notify" name="is_notify" checked
                                       value="1">
                                <label class="form-check-label" for="is_notify"> Gửi thông báo cho các thành
                                    viên</label>
                            </div>
                        </div>
                        <p class="text-danger notice-error mt-3 mb-0" style="display: none">* Nội dung trong khung đỏ là
                            bắt
                            buộc</p>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect waves-light" data-dismiss="modal">
                        Đóng
                    </button>
                    <button class="btn btn-primary" id="booking">Lưu</button>
                </div>
            </div>
        </div>
    </div>

    <!-- show modal -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="meetingFrame"
            >
                <div class="modal-header text-center">
                    <h4 class="font-weight-normal " id="show-title">Lịch họp</h4>
                    <span class="btn-share">
                    <i class="fa fa-share deep-orange-text" title="Share" onclick="copyImgScreenShot()"
                       data-clipboard-text="It worked!"></i>
                        </span>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id_booking" value="">
                    <input type="hidden" name="start_date" id="start_date" value="">
                    <h6 class="font-weight-normal">Nội dung:</h6>
                    <div id="show-content"></div>
                    <h6 class="font-weight-normal">Thành phần tham gia:</h6>
                    <div id="show-object"></div>
                    <hr/>
                    <div class="row mb-2">
                        <div class="col-6">
                            <h6 class="font-weight-normal">Phòng họp:</h6>
                            <strong id="show-meeting"></strong>
                        </div>
                        <div class="col-6">
                            <h6 class="font-weight-normal">Thời gian:</h6>
                            <strong id="time"></strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <h6 class="font-weight-normal">Người tạo:</h6>
                            <p id="show-creator"></p>
                        </div>
                        <div class="col-6">
                            <h6 class="font-weight-normal">Ngày tạo:</h6>
                            <p id="show-date-create"></p>
                        </div>

                    </div>
                </div>
                <div class="modal-footer" style="display: none">
                    <button class="btn btn-primary" id="edit">Thay đổi</button>
                    <button class="btn btn-danger" id="deleteMessage">Hủy buổi họp</button>
                </div>
            </div>
        </div>
    </div>
    <!-- delete modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">

            <div class="modal-content"
            >

                <div class="modal-body">
                    <h6>Bạn có chắc chắn muốn hủy buổi họp này không?</h6>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" id="cancel">Từ bỏ</button>
                    <button class="btn btn-danger" id="delete">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- success modal -->
    <div class="modal fade" id="deleteSuccessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">

            <div class="modal-content"
            >

                <div class="modal-body">
                    <h6 id="message" class="text-center"></h6>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="ok">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="alert_title">Cảnh báo
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6 id="alert_message" class="text-center"></h6>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" id="ok">OK</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('extend-css')
    <link href="{{asset_ver('css/bootstrap-glyphicons.css')}}" rel="stylesheet"/>
    {{-- <link href="{{asset_ver('bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet"/> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">
    <link href="{{asset_ver('bootstrap-datetimepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet"/>
    <link href="{{ asset_ver('fullcalendar/fullcalendar.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset_ver('css/meeting.css')}}?v=1">
@endpush
@push('extend-js')
    <script type="text/javascript" src="{{asset_ver('mdb/js/bootstrap.js')}}"></script>
    {{-- <script src="{{asset_ver('bootstrap-select/js/bootstrap-select.js')}}" type="text/javascript"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/js/i18n/defaults-*.min.js"></script>
    <script src="{{asset_ver('bootstrap-datetimepicker/js/bootstrap-timepicker.min.js')}}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset_ver('js/moment.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset_ver('fullcalendar/fullcalendar.min.js') }}"></script>
    <script type="">
        $(document).ready(function () {
            $('.mdb-select').materialSelect();
            $('[data-toggle="tooltip"]').tooltip();
            $('.selectpicker').selectpicker({
                size: 10
            });

            $('.selectpicker').on('shown.bs.select', () => {
                $(document.body).css('overflow-y', 'hidden')
            });

            $('.selectpicker').on('hidden.bs.select', () => {
                $(document.body).css('overflow-y', 'auto')
            });
            
            $('#start_time').timepicker({
                // 12 or 24 hour
                showInputs: false,
                showMeridian: false
            });

            $('#end_time').timepicker({
                // 12 or 24 hour
                showInputs: false,
                showMeridian: false
            });
        });
    </script>
    <script type="text/javascript" src="{{asset('js/meeting.js')}}?v=2020"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script>
      var copyImgScreenShot = function () {
        $(".btn-share").hide();
        setTimeout(function () {
          $(".btn-share").show();
        }, 1000);
        if (navigator.clipboard) {
          html2canvas(document.querySelector("#meetingFrame"), {
            onrendered: function (canvas) {
              canvas.toBlob(function (blob) {
                const item = new ClipboardItem({"image/png": blob});
                navigator.clipboard.write([item]);
              });
            }
          });

        }
      }
    </script>
@endpush


