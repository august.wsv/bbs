@extends('layouts.end_user')
@section('breadcrumbs')
    {!! Breadcrumbs::render('labor_calendar') !!}
@endsection
@push('extend-css')
    <style>
        .indigo {
            opacity: 0.5;
        }

        .card {
            overflow-x: auto;
        }
        .td-custom {
            height: 15px; width: 125px
        }
        .warning {
            font-size: 13px;
            font-style: italic;
        }
        .custom-info {
            padding: 10px 20px !important;
        }
        @media only screen and (min-width: 1366px){
            #end_user_calendar {
                width: 1000px;
            }
        }
        @media only screen and (max-width: 768px) {
            .card-header {
                width: 480px;
            }
        }
    </style>
@endpush
@section('content')
    <?php
            $date = $timeSearch->toDateString();
            $startDay_1 = \Carbon\Carbon::parse($date)->firstOfMonth();
            $startDay_2 = \Carbon\Carbon::parse($date)->firstOfMonth();
            $endDay_1 = \Carbon\Carbon::parse($date)->endOfMonth();
            $endDay_2 = \Carbon\Carbon::parse($date)->endOfMonth();
    ?>

    <div class="card">
        <form id="labor-form" action="{{route('labor_calendar_index')}}" method="GET">
            <h5 class="card-header h5" style="position: relative" >Lịch trực nhật tháng
                {{$timeSearch->format(MONTH_YEAR_FORMAT)}}
                <div style="display: inline-block; position: absolute; right: 15px;top: 12px">
                    <select style="display: block !important;" onchange="findToMonth()" name="month">
                        @foreach(get_months() as $key => $monthName)
                            @if(!empty($key))
                            <option value="{{$key}}" {{$month == $key ? 'selected' : ''}}>Tháng {{$monthName}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </h5>
        </form>
        @if(!empty($listLaborCalendars))
        <div class="card-body" style="margin: auto">

            <table id="end_user_calendar" class="table table-bordered text-center width-mobile" style="border-collapse: collapse;  height: 363px;">
                <thead>
                <tr class="secondary-color text-white bolder">
                    <td height="20"><strong><span>Chủ nhật</span></strong></td>
                    <td><strong><span>Thứ 2</span></strong></td>
                    <td><strong><span>Thứ 3</span></strong></td>
                    <td><strong><span>Thứ 4</span></strong></td>
                    <td><strong><span>Thứ 5</span></strong></td>
                    <td><strong><span>Thứ 6</span></strong></td>
                    <td><strong><span>Thứ 7</span></strong></td>
                </tr>
                </thead>
                <tbody>
                @while(true)
                <tr class="indigo lighten-2 text-white" style="height: 15.0pt;">
                    @for($j = 0; $j <= 6; $j++)
                            @if($startDay_1->dayOfWeek == $j && $startDay_1 <= $endDay_1)
                                <td class="xl65"><strong>{{$startDay_1->format(DATE_MONTH_REPORT)}}</strong></td>
                                <?php $startDay_1->addDays(1) ?>
                            @else
                                <td class="td-custom">&nbsp;</td>
                            @endif
                    @endfor
                </tr>
                <tr style="height: 12.75pt;">
                    @for($j = 0; $j <= 6; $j++)
                        @if($startDay_2->dayOfWeek == $j && $startDay_2 <= $endDay_2)
                            @if($startDay_2->dayOfWeek !== DAY_OF_WEEK['sunday'] && $startDay_2->dayOfWeek !== DAY_OF_WEEK['saturday'])
                                <td style="border-top: none;" class="td-custom">
                                    @foreach($listLaborCalendars as $laborCalendar)
                                        @if(array_key_exists($startDay_2->format(DATE_MONTH_REPORT),$laborCalendar))
                                            <p check-self-color="{{$laborCalendar['checkSelf']}}">{!! $laborCalendar[$startDay_2->format(DATE_MONTH_REPORT)] !!}</p>
                                            @if(!empty($laborCalendar['note']))
                                            <div class="warning"><i class="fas fa-exclamation-triangle" style="color: orange"></i> {!! $laborCalendar['note'] !!}</div>
                                            @endif
                                        @endif
                                    @endforeach
                                </td>
                                @else
                                <td class="td-custom">&nbsp;</td>
                            @endif
                            <?php $startDay_2->addDays(1) ?>
                        @else
                            <td class="td-custom">&nbsp;</td>
                        @endif
                    @endfor
                </tr>
                @if($startDay_1 > $endDay_1)
                    @break
                @endif
                @endwhile
                </tbody>
            </table>
            <p>&nbsp;</p>
            <h3>Danh sách công việc thực hiện:</h3>
            {!!$workContent!!}
        </div>
        @else
        <h5 class="custom-info">Chưa có lịch trực nhật trong tháng này</h5>
        @endif
    </div>
@endsection
@push('footer-scripts')
    <script >
        function findToMonth()
        {
            $('#labor-form').submit();
        }

        $('p[check-self-color]').each(function (index, value) {
                if($(this).attr('check-self-color') == true)
                {
                    $(this).closest('td').css('background-color','yellow');
                }
        });
    </script>
@endpush

