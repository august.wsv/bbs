@extends('layouts.end_user')
@section('breadcrumbs')
    {!! Breadcrumbs::render('fine_statistic') !!}
@endsection
@push('extend-css')
    <style>
        .content-right {
            text-align: right;
        }

        .customize-stt {
            width: 20px;
        }

        .content-center {
            text-align: center;
        }

        .header-top-background {
            background-color: red !important;
        }

        .header-total-background {
            background-color: orange !important;
        }

        .header-daily-background {
            background-color: black !important;
        }

        @media only screen and (max-width: 768px){
            .customize-mobile {
                overflow: auto;
                padding-right: 5px;
                width: 100%;
            }
            .hidden-column {
                display: none !important;
            }
            .customize-padding-left-auto {
                padding-left: 0px !important;
            }
        }
    </style>
@endpush
@section('content')

    <section class="">
        @if(!$listTypePunishes->isEmpty())
            <div class="row">
                <div class="col-xl-8">
                    <div class="col-12 customize-mobile" >
                        <div id="piechart"></div>
                    </div>
                    <div class="col-12 customize-mobile" style="padding-left: 0px">
                        <h3>Danh sách vi phạm trong tháng</h3>
                        <div class="col-xl-12" style="padding-right: 0px; padding-left: 0px">
                            <form action="{{route('fine_statistic_index')}}" method="get">
                                <div class="md-form active-cyan-2 mb-3">
                                    <div class="input-group">
                                        <input id="search_input" name="search" value="{{$keySearch}}" class="form-control" type="text"
                                               placeholder="Tìm kiếm theo tên người vi phạm" aria-label="Search"
                                               style="font-size: 15px">

                                        <div class="input-group-prepend">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light"
                                                    style="color:#ffffff!important;background:!important;">Tìm kiếm
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <table class="table table-bordered table-hover">
                            <thead class="grey white-text header-daily-background">
                            <tr class="content-center">
                                <th style="width: 50px" class="hidden-column">#</th>
                                <th>Ngày vi phạm</th>
                                <th class="d-sm-table-cell hidden-column">Mã nhân viên</th>
                                <th class="d-sm-table-cell">Tên nhân viên</th>
                                <th class="d-sm-table-cell">Tên vi phạm</th>
                                <th class="d-sm-table-cell">Tiền phạt (VND)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$listPunishesOnMonth->isEmpty())
                                @foreach($listPunishesOnMonth as $key => $punishInfo)
                                    <tr>
                                        <td style="text-align: center" class="hidden-column">{{$key+1}}</td>
                                        <td class="content-center">{{\Carbon\Carbon::parse($punishInfo->infringe_date)->format(DATE_TIME_FORMAT_VI)}}</td>
                                        <td class="content-center hidden-column">{{$punishInfo->code_staff}}</td>
                                        <td>{{$punishInfo->name_staff}}</td>
                                        <td>{{$punishInfo->rule_id ? \App\Models\Rules::find($punishInfo->rule_id)->name : 'Đi muộn' }}</td>
                                        <td class="content-right">{{number_format($punishInfo->total_money)}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">Danh sách trống</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="col-xl-4 customize-padding-left-auto" style="padding-right: 0px">
                    <div class="col-12" style="padding-top: 28px">
                        <table class="table table-bordered">
                            <thead class="grey white-text header-top-background">
                            <tr class="content-center">
                                <th class="customize-stt">#</th>
                                <th scope="col">Top vi phạm</th>
                                <th scope="col">Tiền phạt (VND)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$listTopPunishes->isEmpty())
                                @foreach($listTopPunishes as $key => $topPunish)
                                    <tr>
                                        <td class="content-center">
                                            @if($key + 1 == 1)
                                                <i class="fas fa-medal" style="color: yellow; text-shadow: 1px 2px 3px black"></i>
                                            @elseif($key + 1 == 2)
                                                <i class="fas fa-medal" style="color: silver; text-shadow: 1px 2px 3px black"></i>
                                            @elseif($key + 1 == 3)
                                                <i class="fas fa-medal" style="color: orange; text-shadow: 1px 2px 3px black"></i>
                                            @else
                                                {{$key+1}}
                                            @endif
                                        </td>
                                        <td scope="row">{{$topPunish->name}}</td>
                                        <td class="content-right">{{number_format($topPunish->total_money)}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">Danh sách trống</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-12">
                        <table class="table table-bordered">
                            <thead class="grey white-text header-total-background">
                            <tr class="content-center">
                                <th class="customize-stt">#</th>
                                <th scope="col">Theo vi phạm</th>
                                <th scope="col">Tổng tiền (VND)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$listTypePunishes->isEmpty())
                                @foreach($listTypePunishes as $key => $punishType)
                                    <tr>
                                        <td class="content-center">{{$key+1}}</td>
                                        <td scope="row">{{$punishType->rule_id ?   \App\Models\Rules::find($punishType->rule_id)->name : 'Đi muộn'}}</td>
                                        <td class="content-right">{{number_format($punishType->total_money)}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">Danh sách trống</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                        @include('common.paginate_eu', ['records' => $listPunishesOnMonth])
                </div>
            </div>
        @else
            <h3>Danh sách trống</h3>
        @endif
    </section>
@endsection
@push('footer-scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      // Load google charts
      @if(!$listTypePunishes->isEmpty())
      google.charts.load('current', {'packages': ['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      // Draw the chart and set the chart values
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
                @foreach($listTypePunishes as $typePunish)
          ['{{$typePunish->rule_id ? \App\Models\Rules::find($typePunish->rule_id)->name : 'Đi muộn'}}', {{$typePunish->total_money}}],
            @endforeach

        ]);

        // Optional; add a title and set the width and height of the chart
        var options = {'title': 'Thống kê tiền phạt trong tháng {{$monthYear}}', 'width': 800, 'height': 400};

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
      }
        @endif
    </script>
@endpush
