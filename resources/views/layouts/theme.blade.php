<script>
  var color_select;
  $(function () {
    setTimeout(() => {
      $('body').removeClass('preloading');
      $('.load').fadeOut('fast');
    }, 300)
    setTimeout(() => {
      $('#snowfall').fadeOut()
    }, 5000)

    function getCookie(cname) {
      var name = cname + '=';
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return '';
    }

    var bgColor = getCookie('colorBackground');
    console.log(bgColor)
    $('#change_background').change(function () {
      color_select = $(this).val();
      lightOrDark(color_select)
      document.cookie = 'colorBackground=' + color_select;
      $('#main-nav, .active, .btn-primary').attr('style', 'background:' + color_select + '!important');
    });

    if (bgColor != null) {
      $('#change_background').val(bgColor)
    }
    $('#main-nav, .menu-active, .btn-primary').attr('style', 'background:' + bgColor + '!important');

    var color_text;
    var colorText = getCookie('colorText');
    $('#change_title').change(function () {
      color_text = $(this).val()
      document.cookie = 'colorText=' + color_select;
      $('h1, h2, h3, h4, h5, h6').attr('style', 'color:' + colorText + '!important');
    })
    if (colorText != null) {
      document.cookie = 'colorText=' + colorText;
      $('#change_title').val(colorText)
    }

    $('h1, h2, h3, h4, h5, h6').attr('style', 'color:' + colorText + '!important');
    $('.content-card-day-off').attr('style', 'color:#ffffff!important');
    lightOrDark(bgColor)

    function lightOrDark(color) {
      if (color != null) {
        let bg = color;
        var r, g, b, hsp;
        if (color.match(/^rgb/)) {
          color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
          r = color[1];
          g = color[2];
          b = color[3];
        } else {
          color = +("0x" + color.slice(1).replace(color.length < 5 && /./g, '$&$&'));
          r = color >> 16;
          g = color >> 8 & 255;
          b = color & 255;
        }
        hsp = Math.sqrt(
          0.299 * (r * r) +
          0.587 * (g * g) +
          0.114 * (b * b)
        );
        if (hsp > 127.5) {
          $('#menu_headert li a, .menu-active>i, #notification_headert, #user_name_headert, #text_homet strong').attr('style', 'color:#000000!important');
          $('#menu_headert>.active a').attr('style', 'color:#ffffff!important;background: rgba(0,0,0,0.7)!important;');
          $('.menu-active, .btn-primary').attr('style', 'color:#000000!important;background:' + bg + '!important;');
        } else {
          $('#menu_headert li a, #text_homet strong, .menu-active>i, #user_name_headert, #notification_headert').attr('style', 'color:#ffffff!important');
          $('#menu_headert>.active a').attr('style', 'color:#000000!important;background:rgba(253,253,253,0.9)!important');
          $('.menu-active, .btn-primary').attr('style', 'color:#ffffff!important;background:' + bg + '!important;');
        }
      }
    }
  })
</script>
