<?php
$admin = \App\Facades\AuthAdmin::user();
?>

<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>

    @if($admin->can('admin'))
        <li class="{{ \App\Utils::checkRouteAdmin(['dashboard', 'admin']) ? 'active': '' }}">
            <a href="/admin">
                <i class="fa fa-dashboard"></i> <span>Trang quản trị</span>
            </a>
        </li>
        @if($admin->can('super-admin'))
            <li class="{{ \App\Utils::checkRouteAdmin(['role']) ? 'active': '' }}">
                <a href="{{ route('admin::role.index') }}">
                    <i class="fa fa-user"></i> <span>Quản trị viên</span>
                </a>
            </li>
        @endif
        <li class="{{ \App\Utils::checkRouteAdmin(['configs']) ? 'active': '' }}">
            <a href="{{ route('admin::configs.index') }}">
                <i class="fa fa-cog"></i> <span>Thiết lập hệ thống</span>
            </a>
        </li>
        <li class="treeview {{ \App\Utils::checkRouteAdmin(['users',
        'user_trash']) ? 'active': '' }}">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Nhân viên</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ \App\Utils::checkRouteAdmin(['users']) ? 'active': '' }}">
                    <a href="{{ route('admin::users.index') }}">
                        <i class="fa fa-circle"></i> <span>Danh sách nhân viên</span>
                    </a>
                </li>
                <li class="{{ \App\Utils::checkRouteAdmin(['user_trash']) ? 'active': '' }}">
                    <a href="{{ route('admin::user_trash.index')}}">
                        <i class="fa fa-circle"></i> <span>Nhân viên nghỉ việc</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="{{ \App\Utils::checkRouteAdmin(['group']) ? 'active': '' }}">
            <a href="{{ route('admin::group.index') }}">
                <i class="fa fa-object-group"></i><span>Group</span>
            </a>
        </li>
        <li class="{{ \App\Utils::checkRouteAdmin(['teams']) ? 'active': '' }}">
            <a href="{{ route('admin::teams.index') }}">
                <i class="fa fa-users"></i> <span>Nhóm</span>
            </a>
        </li>

        <li class="{{ \App\Utils::checkRouteAdmin(['blocks']) ? 'active': '' }}">
            <a href="{{ route('admin::blocks.index') }}">
                <i class="fa fa-th"></i></i><span>Khu vực</span>
            </a>
        </li>

        <li class="{{ \App\Utils::checkRouteAdmin(['projects']) ? 'active': '' }}">
            <a href="{{ route('admin::projects.index') }}">
                <i class="fa fa-anchor"></i> <span>Dự án</span>
            </a>
        </li>
        <li class="{{ \App\Utils::checkRouteAdmin(['meeting_rooms']) ? 'active': '' }}">
            <a href="{{ route('admin::meeting_rooms.index') }}">
                <i class="fa fa-bank"></i> <span>Phòng họp</span>
            </a>
        </li>
    @endif
    @if($admin->can('content'))
        <li class="{{ \App\Utils::checkRouteAdmin(['events']) ? 'active': '' }}">
            <a href="{{ route('admin::events.index') }}">
                <i class="fa fa-calendar"></i> <span>Sự kiện</span>
            </a>
        </li>
        <li class="{{ \App\Utils::checkRouteAdmin(['posts']) ? 'active': '' }}">
            <a href="{{ route('admin::posts.index') }}">
                <i class="fa fa-share-alt"></i> <span>Thông báo</span>
            </a>
        </li>
    @endif
    @if($admin->can('admin'))
        <li class="treeview {{ \App\Utils::checkRouteAdmin([
            'regulations',
            'rules',
            ]) ? 'active': '' }}">
            <a href="#">
                <i class="fa fa-podcast"></i>
                <span>Quiz</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ \App\Utils::checkRouteAdmin(['quizzes']) ? 'active': '' }}">
                    <a href="{{ route('admin::quizzes.index') }}">
                        <i class="fa fa-circle"></i> <span>Danh sách quiz</span>
                    </a>
                </li>
                <li class="{{ \App\Utils::checkRoute(['admin::quizzes.statistic']) ? 'active': '' }}">
                    <a href="{{ route('admin::quizzes.statistic') }}">
                        <i class="fa fa-circle"></i> <span>Thống kê kết quả</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="{{ \App\Utils::checkRouteAdmin(['suggestions']) ? 'active': '' }}">
            <a href="{{ route('admin::suggestions.index') }}">
                <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Đề xuất - Góp ý</span>
            </a>
        </li>
        <li class="treeview {{ \App\Utils::checkRouteAdmin([
            'regulations',
            'rules',
            ]) ? 'active': '' }}">
            <a href="#">
                <i class="fa fa-podcast"></i>
                <span>Nội quy/Quy định</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ \App\Utils::checkRouteAdmin(['regulations']) ? 'active': '' }}">
                    <a href="{{ route('admin::regulations.index') }}">
                        <i class="fa fa-circle"></i> <span>Danh sách</span>
                    </a>
                </li>
                <li class="{{ \App\Utils::checkRouteAdmin(['rules']) ? 'active': '' }}">
                    <a href="{{ route('admin::rules.index') }}">
                        <i class="fa fa-circle"></i> <span>Quy định tiền phạt</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="treeview {{ \App\Utils::checkRouteAdmin([
            'day_offs',
            'work_times',
            'over_times',
            'approve_permission',
            'work_time_register',
            'work_time_statistic',
            ]) ? 'active': '' }}">
            <a href="#">
                <i class="fa fa-calendar"></i>
                <span>Thời gian làm việc</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ \App\Utils::checkRouteAdmin(['day_offs']) ? 'active': '' }}"><a
                            href="{{ route('admin::day_offs.index') }}"><i class="fa fa-circle"></i> Nghỉ phép</a></li>
                <li class="{{ \App\Utils::checkRouteAdmin(['work_times']) ? 'active': '' }}"><a
                            href="{{ route('admin::work_times.index') }}"><i class="fa fa-circle"></i> Thời gian làm
                        việc</a>
                </li>
                <li class="{{ \App\Utils::checkRouteAdmin(['over_times']) ? 'active': '' }}"><a
                            href="{{ route('admin::over_times.index') }}"><i class="fa fa-circle"></i> Overtime</a>
                <li class="{{ \App\Utils::checkRouteAdmin(['approve_permission']) ? 'active': '' }}"><a
                            href="{{ route('admin::approve_permission.index') }}"><i class="fa fa-circle"></i> Xin phép</a>
                <li class="{{ \App\Utils::checkRouteAdmin(['work_time_register']) ? 'active': '' }}">
                    <a href="{{ route('admin::work_time_register.index') }}"><i class="fa fa-circle"></i>
                        Đăng ký lịch làm việc</a>
                </li>
                <li class="{{ \App\Utils::checkRouteAdmin(['work_time_statistic']) ? 'active': '' }}"><a
                            href="{{ route('admin::work_time_statistic.index') }}"><i class="fa fa-circle"></i> Thống kê</a>
                </li>
            </ul>
        </li>
    @endif
    @if($admin->can('itHelpDesk'))
        <li class="treeview {{ \App\Utils::checkRoute([
            'admin::devices.index',
            'admin::devices.create',
            'admin::devices.edit',
            'admin::deviceusers.index',
            'admin::deviceusers.create',
            'admin::deviceusers.edit',
            'admin::t_devices.attribute',
            'admin::t_devices.t_list_devices',
            'admin::t_devices.t_import_device_index',
            'admin::provided_device.index'
            ]) ? 'active': '' }}">
            <a href="#">
                <i class="fa fa-desktop"></i>
                <span>Quản lý thiết bị</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
            <!-- <li><a href="{{ route('admin::devices.index') }}"><i class="fa fa-circle"></i> Quản lý thiết bị</a></li> -->
            <!-- <li><a href="{{ route('admin::deviceusers.index') }}"><i class="fa fa-circle"></i> Cấp thiết bị</a></li> -->
                <li><a href="{{ route('admin::t_devices.attribute') }}"><i class="fa fa-circle"></i> Quản lý loại thiết
                        bị</a></li>
                <li><a href="{{ route('admin::t_devices.t_list_devices') }}"><i class="fa fa-circle"></i> Quản lý thiết
                        bị</a></li>
                <li><a href="{{route('admin::t_devices.t_import_device_index')}}"><i class="fa fa-circle"></i> Quản lý
                        kho thiết bị</a></li>
                <li><a href="{{ route('admin::provided_device.index') }}"><i class="fa fa-circle"></i> Yêu cầu cấp thiết
                        bị
                        <?php
                        $totalRequestDevice = \App\Models\ProvidedDevice::where('status', DEVICE_STATUS_WAITING)->count();
                        ?>

                        @if($totalRequestDevice > 0)
                            <span class="pull-right-container">
                            <small class="label pull-right bg-orange">{{$totalRequestDevice}}</small>
                        </span>
                        @endif
                    </a>
                </li>
            </ul>
        </li>
    @endif
    @if($admin->can('admin'))
        <li class="{{ \App\Utils::checkRouteAdmin(['punishes']) ? 'active': '' }}">
            <a href="{{ route('admin::punishes.index') }}">
                <i class="fa fa-money"></i> <span>Danh sách vi phạm</span>
            </a>
        </li>
        <li class="{{ \App\Utils::checkRouteAdmin(['statistic_punished']) ? 'active': '' }}">
            <a href="{{ route('admin::statistic_punished.index') }}">
                <i class="fa fa-pie-chart"></i> <span>Thống kê tiền phạt</span>
            </a>
        </li>
        <li class="{{ \App\Utils::checkRoute(['admin::labor_calendar.index', 'admin::labor_calendar.create', 'admin::labor_calendar.edit']) ? 'active': '' }}">
            <a href="{{ route('admin::labor_calendar.index') }}">
                <i class="fa fa-trash"></i> <span>Lịch trực nhật tháng</span>
            </a>
        </li>
        <li class="{{ \App\Utils::checkRoute(['admin::passport.index', 'admin::passport.create', 'admin::passport.edit']) ? 'active': '' }}">
            <a href="{{ route('admin::passport.index') }}">
                <i class="fa fa-trash"></i> <span>Quản lí client passport</span>
            </a>
        </li>
    @endif
    <i class="fa fa-envelope-o" aria-hidden="true"></i>
</ul>
