<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="noindex, nofollow, noarchive">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="shortcut icon" href="{{asset(JVB_FAVICON_PATH)}}">
<link rel="icon" href="{{asset(JVB_FAVICON_PATH)}}" type="image/x-icon" id="favicon">
<link rel="preload" href="/img/favicons/favicon-unread.ico" as="image" id="favicon-unread">

@hasSection('meta-image') @yield('meta-image') @endif
