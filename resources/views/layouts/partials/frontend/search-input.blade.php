<div class="input-group">
    <input id="search_input" name="search" value="{{request('search', $search)}}" class="form-control" type="text"
           placeholder="{{$text}}" aria-label="Search" style="font-size: 15px">

    <div class="input-group-prepend">
        <button id="btn_search_submit" class="btn btn-primary" id="inputGroup-sizing-default">Tìm kiếm</button>
    </div>
</div>
