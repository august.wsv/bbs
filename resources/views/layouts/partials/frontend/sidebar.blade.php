<?php
$team = \Auth::user()->team();

$logoUrl = ($team && $team->banner) ? lfm_thumbnail($team->banner) : JVB_LOGO_URL;
$name = $team->name ?? $config->name;
?>

<!-- Sidebar -->
<div class="sidebar fixed sidebar-fixed position-fixed" id="slide-out" style="transform: translateX(-100%);">

    <div class="text-center mb-xxl-4">
        <a href="#" class="logo-wrapper waves-effect">
            <img src="{{$logoUrl}}" onerror="this.src='{{JVB_LOGO_URL}}'" class="img-fluid" alt="">
        </a>

        <p><strong class="text-uppercase text-primary">
                {{$name}}
            </strong></p>
    </div>
    <div class="list-group list-group-flush" style="margin: 0 -15px">
        <a href="/"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['default']) ? 'active menu-active': '' }}">
            <i class="fas fa-home mr-3" style="color:#007BFF"></i> {{__l('Dashboard')}}
        </a>
        <a href="{{ route('post') }}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['post']) ? 'active menu-active': '' }}">
            <i class="fas fa-flag-checkered mr-3" style="color:#f33e20"></i> {{__l('Post')}}
        </a>
        <a href="{{route('event')}}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['event', 'event_detail']) ? 'active menu-active': '' }}">
            <i class="far fa-grin-stars mr-3" style="color:#F783AC"></i> {{__l('Event')}}
        </a>
        <a href="{{route('meetings')}}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['meetings']) ? 'active menu-active': '' }}">
            <i class="far fa-calendar-alt mr-3" style="color:#BE4BDB"></i> {{__l('Meeting')}}
        </a>
        <a href="{{route('project')}}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['project', 'project_detail']) ? 'active menu-active': '' }}">
            <i class="fas fa-search-dollar mr-3" style="color:#007BFF"></i> {{__l('Project')}}
        </a>
        <a href="{{route('contact')}}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['contact']) ? 'active menu-active': '' }}">
            <i class="far fa-address-book mr-3" style="color:#BE4BDB"></i> {{__l('contact')}}
        </a>
        <a href="{{route('regulation')}}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['regulation', 'regulation_detail']) ? 'active menu-active': '' }}">
           <i class="fas fa-skull-crossbones mr-3" style="color:#F783AC"></i> {{__l('regulation')}}
        </a>
        <a href="{{route('share_experience')}}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['share_experience']) ? 'active menu-active': '' }}">
            <i class="far fa-edit mr-3" style="color:#82C91E"></i> {{ __l('work_experience') }}
        </a>
        <a href="{{route('list_share_document')}}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['list_share_document']) ? 'active menu-active': '' }}">
           <i class="far fa-share-square mr-3" style="color:#FD7E14"></i> {{ __l('list_share_document') }}
        </a>
        <a href="{{ route('t_device.checkRoute') }}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['t_device.index', 't_devices.t_list_device']) ? 'active menu-active': '' }}">
            <i class="fas fa-laptop-code mr-3" style="color:#BE4BDB"></i> {{ __l('device') }}
        </a>
        @can('team-leader')
            <a href="{{route('list_suggestions')}}"
               class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['list_suggestions']) ? 'active menu-active': '' }}">
                <i class="fas fa-lightbulb mr-3" style="color:#007BFF"></i> {{ __l('list_suggestions') }}
            </a>
        @endcan
        <!-- <a href="{{ route('device_index') }}"
           class="list-group-item list-group-item-action waves-effect {{ \App\Utils::checkRoute(['device_index']) ? 'active': '' }}">
            <i class="fas fa-desktop mr-3"></i>Đề xuất thiết bị
        </a> -->
    </div>
</div>
<!-- Sidebar -->
