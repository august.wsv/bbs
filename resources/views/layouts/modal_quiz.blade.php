<!-- Modal -->
<div class="modal custom fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" data-backdrop="static" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-lg quiz" role="document">
        <div class="modal-content">
            <div class="container w-95 quiz">
                <div class="modal-header quiz">
                </div>
                <form action="{{ route('answer.store') }}" method="get">
                    <div class="modal-body quiz">
                        <div class="data_info">
                            <div class="form_container">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-center">
                        <button class="btn btn-primary btn-send btn-block w-30 btn-send-answer"> GỬI CÂU TRẢ LỜI</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var userID = {{ auth()->id() }};
    $(window).on('load', function() {
        $.ajax({
            url: '{{ route('get_quiz_must_do') }}',
            type: "GET",
            dataType: "json",
            data: {
                        'user_id': userID,
            },
            success: function (respond) {
                var quizId = respond.quiz_must_do[0];
                if (!(jQuery.isEmptyObject(respond.quiz_must_do))){
                    let titleModal = '';
                    titleModal += '<h2 class="modal-title mg-center h2" id="exampleModalLongTitle">' + respond.detail_quiz[0].name + '</h2>';
                    $('.modal-header').html(titleModal);
                    $('#exampleModalLong').modal('show');
                    $.ajax({
                        url: '{{ route('getquiz') }}',
                        type: "GET",
                        dataType: "json",
                        data: {
                                    'quiz_id': respond.quiz_must_do,
                        },
                        success: function (respond) {
                            let html = '';
                            var i = 1;
                            var x = 1;
                            var quizId= Object.values(respond.quiz_id)[0];
                            html += '<input type="hidden" value="'+ quizId +'" name="quiz_id">';
                            $.each(respond.question_answer, function (key, value) {
                                html += '<div class="input_wrap mt-20">';
                                html += '<div class="question_sample text-w-400"><span class="font-weight-bold">' + 'Câu ' + i +':</span>&nbsp' + value.question +'</div>';
                                html += '<div style="margin-top:10px">'

                                if(value.type == 1){
                                    html += '<div class="form-group" style="margin-top:20px">';
                                    html += '<textarea class="form-control" name="questions['+ value.id +']" rows="2" cols="48" style="margin-top: -15px; min-width: 100%;" placeholder="Câu trả lời của bạn..."></textarea>';
                                    html += '</div>';
                                }else{
                                    $.each(value.answers, function (key, value) {
                                        html += '<div class="answer_sample">';
                                        html += '<div class="form-check">';
                                        html += '<input class="form-check-input" type="radio" name="questions['+ value.question_id +']" id="exampleRadios'+ x +'" value="'+ value.answer +'">';
                                        html += '<label class="form-check-label text-w-400" for="exampleRadios'+ x +'" style="height:35px">' + value.answer +'</label>';
                                        html += '</div>';
                                        html += '</div>';
                                        x++;
                                    });
                                }
                                html += '</div>';
                                html += '</div>';
                                i++;
                            });
                            $('.form_container').html(html);
                        },
                    });
                }else{
                    $('#exampleModalLong').modal('hide');
                }

            },
        });
    });
    $( document ).ready(function() {
        $('.btn-send-answer').click(function(){
            var questionNotAnswer = 0;
            $(".err-quiz").remove();
            $('.input_wrap').each(function() {
                if (!($(this).find('input:radio').is(':checked')) && $(this).find('.answer_sample').length != 0 ) {
                    $(this).children(":last").children(":last").append( "<div class='err-quiz' style='color:red'>Vui lòng chọn đáp án</div>" );
                    event.preventDefault();
                    questionNotAnswer++;
                }
                if($(this).find('textarea').val() == "" && $(this).find('.answer_sample').length == 0 ){
                    $(this).children(":last").children(":last").append( "<div class='err-quiz' style='color:red'>Vui lòng điền đáp án</div>" );      
                    event.preventDefault();
                    questionNotAnswer++;
                }
            });
            if (questionNotAnswer == 0){
                $(".btn-send-answer").unbind('click').click(); 
            }
        })
             
    });

</script>