/**
* {{$Module}}Service class
* Author: jvb
* Date: {{date('Y/m/d H:i')}}
*/

namespace App\Services;

use App\Models\{{$Module}};
use App\Services\Contracts\I{{$Module}}Service;

class {{$Module}}Service extends AbstractService implements I{{$Module}}Service
{

}
