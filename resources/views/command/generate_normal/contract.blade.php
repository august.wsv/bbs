namespace App\Repositories\Contracts;

/**
* {{$Module}}Repository contract.
* Author: jvb
* Date: {{date('Y/m/d H:i')}}
*/
interface I{{$Module}}Repository extends IBaseRepository {

}
