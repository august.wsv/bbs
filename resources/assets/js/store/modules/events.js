import { API_ENDPOINT } from '../../constants'

export const eventsModule = {
  state: () => ({
    eventList: [],
    eventDetail: {}
  }),
  mutations: {
    setEventList(state, newList) {
      state.eventList = newList
    },
    setEventDetail(state, data) {
      state.eventDetail = data
    }
  },
  actions: {
    async getEventList({ commit }) {
      try {
        const res = await window.axios.get(API_ENDPOINT.EVENTS)
        commit('setEventList', res.data.data.events)
      } catch (e) {
        console.error(e)
      }
    },
    async getEventDetail({ commit }, eventID) {
      try {
        const res = await window.axios.get(`${API_ENDPOINT.EVENTS}/${eventID}`)
        commit('setEventDetail', res.data.data.event)
      } catch (e) {
        console.error(e)
      }
    }
  }
}