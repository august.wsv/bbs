import { API_ENDPOINT } from '../../constants'

export const postsModule = {
  state: () => ({
    postList: [],
    postDetail: {}
  }),
  mutations: {
    setPostList(state, newList) {
      state.postList = newList
    },
    setPostDetail(state, data) {
      state.postDetail = data
    }
  },
  actions: {
    async getPostList({ commit }) {
      try {
        const res = await window.axios.get(API_ENDPOINT.POSTS)
        commit('setPostList', res.data.data.posts)
      } catch (e) {
        console.error(e)
      }
    },
    async getPostDetail({ commit }, postID) {
      try {
        const res = await window.axios.get(`${API_ENDPOINT.POSTS}/${postID}`)
        commit('setPostDetail', res.data.data.post)
      } catch (e) {
        console.error(e)
      }
    }
  }
}