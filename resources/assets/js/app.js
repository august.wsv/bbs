import Vue from 'vue'
import Vuex from 'vuex'

import * as _ from './bootstrap'
import { postsModule, eventsModule } from './store'
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

// window.Vue = require('vue');

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        posts: postsModule,
        events: eventsModule,
    }
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('post-list-view', () => import('./views/PostListView.vue'));
Vue.component('post-detail-view', () => import('./views/PostDetailView.vue'));
Vue.component('event-list-view', () => import('./views/EventListView.vue'));
Vue.component('event-detail-view', () => import('./views/EventDetailView.vue'));

const app = new Vue({
    el: '#main',
    store
});
