export const BASE_URL = 'http://localhost:8001/api/v1'
export const API_ENDPOINT = {
  AUTH: {
    LOGIN: '/login',
    USER: '/user',
  },
  POSTS: '/posts',
  EVENTS: '/events',
}