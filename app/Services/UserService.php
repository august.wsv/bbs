<?php

/**
 * UserService class
 * Author: jvb
 * Date: 2018/07/16 10:34
 */

namespace App\Services;

use App\Events\DayOffNoticeEvent;
use App\Events\UserRegistered;
use App\Helpers\CollectionHelper;
use App\Helpers\DateTimeHelper;
use App\Http\Requests\AskPermissionRequest;
use App\Http\Requests\CreateDayOffRequest;
use App\Models\DayOff;
use App\Models\Group;
use App\Models\Project;
use App\Models\ProjectMember;
use App\Models\ProjectTask;
use App\Models\TaskLogWork;
use App\Models\Team;
use App\Models\User;
use App\Models\UserTeam;
use App\Repositories\Contracts\IOverTimeRepository;
use App\Repositories\Contracts\IUserRepository;
use App\Repositories\Contracts\IUserTeamRepository;
use App\Repositories\Contracts\IWorkTimesExplanationRepository;
use App\Services\Contracts\IUserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserService extends AbstractService implements IUserService
{
    protected $notificationService;
    protected $workTimesExplanationRepository;
    protected $overTimeRepository;

    /**
     * UserService constructor.
     *
     * @param \App\Models\User $model
     * @param \App\Repositories\Contracts\IUserRepository $repository
     * @param IUserTeamRepository $userTeamRepository
     * @param IWorkTimesExplanationRepository $workTimesExplanationRepository
     * @param IOverTimeRepository $overTimeRepository
     */
    public function __construct(
        User $model,
        IUserRepository $repository,
        IUserTeamRepository $userTeamRepository,
        IWorkTimesExplanationRepository $workTimesExplanationRepository,
        IOverTimeRepository $overTimeRepository
    ) {
        $this->model = $model;
        $this->repository = $repository;
        $this->userTeamRepository = $userTeamRepository;
        $this->workTimesExplanationRepository = $workTimesExplanationRepository;
        $this->overTimeRepository = $overTimeRepository;
        $this->notificationService = app()->make(NotificationService::class);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function register(Request $request)
    {
        $data = $request->all('email', 'password', 'name', 'invite_code');

        $data['activate_code_time'] = Carbon::now();
        $data['activate_code'] = strtoupper(str_random(4));

        $user = $this->repository->save($data);

        event(new UserRegistered($user));

        return $user;
    }

    /**
     * @param string $idCode
     *
     * @return int
     */
    public function getUserIdByIdCode(string $idCode)
    {
        $user = $this->model->where('id_code', $idCode)->first();

        return $user->id ?? 0;
    }

    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     *
     * @param bool    $isGetAll
     *
     * @return collection
     */
    public function getContact(Request $request, &$perPage, &$search, $isGetAll = true)
    {
        $userModels = User::where('status', ACTIVE_STATUS)
            ->where(function ($q) use ($request) {
                $search = $request->search;
                $q->search($search)->active();

                if ($search) {
                    $q->searchByGroup($search);
                }
            })->orderBy('jobtitle_id', 'desc')->orderBy('staff_code');

        if ($isGetAll) {
            return $userModels->get();
        } else {
            $perPage = $request->get('page_size', DEFAULT_PAGE_SIZE);

            return $userModels->paginate($perPage);
        }
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getUserManager()
    {
        $user = Auth::user();
        if ($user->jobtitle_id == MASTER_ROLE) {
            return collect();
        }
        if ($user->jobtitle_id == MANAGER_ROLE) {
            return $this->model->select('name', 'id', 'jobtitle_id')->where('jobtitle_id', MASTER_ROLE)->get()->groupBy('jobtitle_id');
        }
        if ($user->jobtitle_id == TEAMLEADER_ROLE) {
            $team = Team::where('leader_id', Auth::id())->first();
        } else {
            $team = UserTeam::where('user_id', Auth::id())->first()->team ?? null;
        }

        if ($team) {
            $id = $team->group->manager_id;
            return $this->model->select('name', 'id', 'jobtitle_id')->where('id', $id)->get()->groupBy('jobtitle_id');
        } else {
            return $this->model->select('name', 'id', 'jobtitle_id')->whereIn('jobtitle_id', [MASTER_ROLE, MANAGER_ROLE])->get()->groupBy('jobtitle_id');
        }
    }

    public function detail($id)
    {
        return $this->repository->findOneBy([
            'id' => $id,
        ]);
    }

    public function getUsersAssginOt()
    {
        $leaderUsers = $this->getUserModel([['jobtitle_id', '>=', TEAMLEADER_ROLE]]);
        return $leaderUsers->toArray();
    }

    public function getUserByGroup()
    {
        $userLogin = Auth::user();
        $nameTeam = $userLogin->team()->name ?? 'team';
        /*$masters = [
            'Ban giám đốc' => $this->getUserModel(['jobtitle_id' => MASTER_ROLE])->where('contract_type', STAFF_CONTRACT_TYPES)->toArray()
        ];*/
        $managerUsers = $this->getUserModel(['jobtitle_id' => MANAGER_ROLE], false);
        $managers = [
            'Manager' => $managerUsers->get()->toArray()
        ];
        $leaderUsers = $this->getUserModel(['jobtitle_id' => TEAMLEADER_ROLE], false);
        $leader = [
            'Team Leader' => $leaderUsers->get()->toArray()
        ];
        //other
        $users = $this->getUserInGroup();

        //internship
        $internship = $this->getUserModel(['contract_type' => CONTRACT_TYPES['internship']], false)->orderBy('name')->get()->toArray();
        $inters = [];
        if (count($internship) > 0)
            $inters = [
                'Thực tập sinh' => $internship
            ];

        if ($userLogin->isMaster() || $nameTeam == TEAM_HCNS) {
            return $managers + $leader + $users + $inters;
        } elseif ($userLogin->isManager()) {
            return $leader + $users + $inters;
        } elseif ($userLogin->isTeamLeader()) {
            $usersTeamId = $userLogin->userTeam($userLogin->team()->id ?? null);
            if ($usersTeamId) {
                $usersTeam = $this->getUserModel([], false)->whereIn('id', $usersTeamId)->where('contract_type', '!=', CONTRACT_TYPES['internship'])->whereNotIn('jobtitle_id', [MASTER_ROLE, TEAMLEADER_ROLE, MANAGER_ROLE])->orderBy('name')->get()->toArray();
                $usersTeam = [
                    $nameTeam => $usersTeam
                ];
                return $usersTeam + $inters;
            }
        }
        return array();
    }

    private function getUserInGroup()
    {
        $groups = Group::all();
        $users = [];
        $userLogin = Auth::user();
        $userIdInGroup = [];

        foreach ($groups as $group) {
            foreach ($group->teams as $team) {
                $others = $team->selectActiveMember();
                $userIdInGroup = array_merge($others->pluck('id')->toArray(), $userIdInGroup);

                if (count($others) > 0) {
                    $groupName = $group->name . ' - ' . $team->name;
                    $users[$groupName] = $others->toArray();
                }
            }
        }
        $results = [];
        $isGroupManager = $userLogin->isGroupManager();
        if ($isGroupManager) {
            $group = $userLogin->groupManage;
            $results1 = [];
            $results2 = [];
            foreach ($users as $name => $userList) {
                if (starts_with($name, $group->name)) {
                    $results1[$name] = $userList;
                } else {
                    $results2[$name] = $userList;
                }
                $results = $results1 + $results2;
            }
        } else {
            $results = $users;
        }
        $results['Others'] = $this->getUsersNotGroup($userIdInGroup)->toArray();
        return $results;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getUsersNotGroup($user_id)
    {
        return  User::select('id', 'staff_code', 'name', 'avatar')
            ->where('id', '!=', Auth::id())
            ->where('status', ACTIVE_STATUS)
            ->whereNotIn('id', $user_id)
            ->whereNotIn('contract_type', [2, 3])
            ->where('jobtitle_id', STAFF_ROLE)
            ->get();
    }

    private function getUserModel($conditions = [], $isGet = true)
    {
        $model = User::select('id', 'staff_code', 'name', 'avatar')
            ->where('id', '!=', Auth::id())
            ->where('status', ACTIVE_STATUS)->where($conditions);

        if ($isGet) {
            return $model->get();
        } else {
            return $model;
        }
    }

    public function getDayOff($array, $month, $year)
    {
        $new_list_day_off_in_month = [];
        foreach ($array as $list) {
            $get_year_date_off_from = get_year($list['date_off_from']);
            $get_year_date_off_to = get_year($list['date_off_to']);
            if ($list['is_repeat'] == REPEAT_EVERY_YEAR || ($list['is_repeat'] == NO_REPEAT_EVERY_YEAR) && ($get_year_date_off_from == $year || $get_year_date_off_to == $year)) {
                $new_list_day_off_in_month[] = $list;
            }
        }

        $all_date_off_in_this_month = [];
        foreach ($new_list_day_off_in_month as $key => $item) {
            $startDate = new Carbon($item['date_off_from']);
            $endDate = new Carbon($item['date_off_to']);
            while ($startDate->lte($endDate)) {
                $all_date_off_in_this_month[] = [$startDate->toDateString(), $item['date_name'], $item['is_repeat']];
                $startDate->addDay();
            }
        }

        for ($i = 0; $i < sizeof($all_date_off_in_this_month); $i++) {
            $get_year_day_off = get_year($all_date_off_in_this_month[$i][0]);
            if ($month != $get_year_day_off && $all_date_off_in_this_month[$i][2] == 1) {
                $exp = explode('-', $all_date_off_in_this_month[$i][0]);
                $all_date_off_in_this_month[$i][0] = $year . '-' . trim($exp[1] . '-' . trim($exp[2]));
            }
        }
        return $all_date_off_in_this_month;
    }


    // customUserTree
    public function getUserTree()
    {
        $results = [];
        $jobtitles = [];
        $results['Tất cả'] = [
            "ALL" => "Chọn tất cả",
        ];
        foreach (JOB_TITLES_MEETING as $value => $name) {
            $jobtitles['J-' . $value] = $name;
        }
        $results['Chức danh'] = $jobtitles;
        $positions = [];
        foreach (POSITIONS_MEETING as $value => $name) {
            $positions['P-' . $value] = $name;
        }
        $results['Chức vụ'] = $positions;
        $teams = Team::select(DB::raw("CONCAT('T-', teams.id) as id"), DB::raw("CONCAT(groups.name, ' - ', teams.name) as name"))
            ->join('groups', 'groups.id', 'teams.group_id')
            ->orderBy('groups.name')
            ->pluck('name', 'id')->toArray();

        $results['Teams'] = $teams;
        $users = User::select('id', DB::raw('CONCAT(staff_code, " - ", name) as name'))->active()->orderBy('jobtitle_id', 'desc')->orderBy('staff_code')->pluck('name', 'id')->toArray();
        $results['Danh sách nhân viên'] = $users;
        return $results;
    }

    // get UserTreecustom
    public function getParticipantIds($meeting)
    {
        $users = User::select('id', 'name', 'jobtitle_id', 'position_id')->active()->get();
        $userTeams = UserTeam::select('id', 'user_id', 'team_id')->get();
        $teams = Team::select('id', 'leader_id', 'name')->get();
        $projectMembers = ProjectMember::select('id', 'user_id');

        $userIds = [];
        $participantIds = $meeting;
        foreach ($participantIds as $participantId) {
            if (starts_with($participantId, 'J-')) {
                $jobTitleId = str_replace('J-', '', $participantId);
                $selectUsers = $users->where('jobtitle_id', $jobTitleId)->pluck('id')->toArray();
                $userIds = array_merge($userIds, $selectUsers);
            } elseif (starts_with($participantId, 'P-')) {
                $positionId = str_replace('P-', '', $participantId);
                $selectUsers = $users->where('position_id', $positionId)->pluck('id')->toArray();
                $userIds = array_merge($userIds, $selectUsers);
            } elseif (starts_with($participantId, 'T-')) {
                $teamId = str_replace('T-', '', $participantId);
                $selectUsers = $userTeams->where('team_id', $teamId)->pluck('user_id')->toArray();
                $leaderId = $teams->firstWhere('id', $teamId)->leader_id ?? '';
                $userIds = array_merge($userIds, $selectUsers);
                $userIds[] = $leaderId;
            } elseif (starts_with($participantId, 'ALL')) {
                $selectUsers = $users->pluck('id')->toArray();
                $userIds = array_merge($userIds, $selectUsers);
            } else {
                $userIds[] = (int)$participantId;
            }
        }
        return array_values(array_unique($userIds));
    }

    public function askPermissionCreate(AskPermissionRequest $request)
    {
        $permission_time = DateTimeHelper::hoursToHoursMinutes($request->option_time);
        if (array_search('Overtime', WORK_TIME_TYPE) == $request['permission_type']) {
            $minute = DateTimeHelper::calculateMinutesOt($request['start_at'], $request['end_at'], $request['work_day'], $request['break_time']);
            if ($request->has('project_id')) {
                $project = Project::find($request['project_id'])->name;
            } else {
                $project = null;
            }
            $overTime = $this->overTimeRepository->getModel()->where('creator_id', Auth::id())->where('work_day', $request['work_day'])->first();
            if ($overTime == null && ($request['permission_status'] == null || $request['permission_status'] == 0)) {
                $overTime = $this->overTimeRepository->getModel()->create([
                    'creator_id' => Auth::id(),
                    'work_day' => $request['work_day'],
                    'reason' => $request['note'],
                    'ot_type' => $request['ot_type'] ?? 1, //OT dự án
                    'minute' => $minute,
                    'start_at' => $request['start_at'],
                    'end_at' => $request['end_at'],
                    'project_id' => $request['project_id'],
                    'project_name' => $project,
                    'break_time' => $request['break_time']
                ]);
                $this->notificationService->sendAskPermission($overTime, WORK_TIME_TYPE[4]);
                return true;
            } else if (
                $request['permission_status'] == array_search(UNAPPROVED, OT_STATUS)
                || $request['permission_status'] == array_search(AWAIT, OT_STATUS)
            ) {
                $request['ot_id'] = $overTime->id ?? $request['ot_id'];
                $this->overTimeRepository->getModel()->where('id', $request['ot_id'])->update([
                    'reason' => $request['note'],
                    'ot_type' => $request['ot_type'],
                    'status' => WORK_TIME_OT_STATUS['Chưa duyệt'],
                    'start_at' => $request['start_at'],
                    'end_at' => $request['end_at'],
                    'minute' => $minute,
                    'project_name' => $project,
                    'project_id' => $request['project_id'],
                    'break_time' => $request['break_time']
                ]);
                return true;
            } else if ($request['permission_status'] == array_search('Đã duyệt', OT_STATUS)) {
                return false;
            }
        } elseif ($request['permission_type'] == array_search('Đi muộn', WORK_TIME_TYPE) || $request['permission_type'] == array_search('Về sớm', WORK_TIME_TYPE)) {
            $workTimeExplanation = $this->workTimesExplanationRepository->getWorkTimeExplanation(Auth::id(), $request['work_day'])
                ->whereIn('status', WORK_TIME_OT_STATUS)
                ->where('type', $request['permission_type'])->first();
            if (!empty($request['option_reason']) && $request['option_reason'] != REASON_LATE[2]) {
                $reason = $request['option_reason'];
            } else {
                $reason = $request['note'];
            }
            if ($workTimeExplanation) {
                $workTimeExplanation->update(['ot_type' => $request['ot_type'], 'note' => $reason, 'work_day' => $request['work_day'], 'option_time' => $permission_time]);
                return true;
            } else {
                $workTimeExplanation = $this->workTimesExplanationRepository->getModel()->create([
                    $workTimeExplanation = 'user_id' => Auth::id(),
                    'work_day' => $request['work_day'],
                    'type' => $request['permission_type'],
                    'ot_type' => $request['ot_type'],
                    'note' => $reason,
                    'option_time' => $permission_time,
                ]);
                if ($workTimeExplanation->type == 1) {
                    $this->notificationService->sendAskPermission($workTimeExplanation, WORK_TIME_TYPE[1]);
                } else {
                    $this->notificationService->sendAskPermission($workTimeExplanation, WORK_TIME_TYPE[2]);
                }
            }
            return true;
        }
    }

    /**
     * @return array
     */
    public function getOtAndAskPermission()
    {
        $askPermission = $this->workTimesExplanationRepository->permissionGetExplanation()
            ->where('user_id', Auth::id())->askPermissionLastSixMonth()->get();
        $otTimes = $this->overTimeRepository->getModel()
            ->where('creator_id', Auth::id())->askPermissionLastSixMonth()
            ->orderBy('work_day', 'desc')->get();
        return [
            'ask_permission' => $askPermission,
            'over_time' => $otTimes
        ];
    }

    /**
     * @param Request $request
     * @return array|false
     */
    public function detailOTWorkTimesExplanation(Request $request)
    {
        $datas = [];
        if ($request['type'] == array_search('Overtime', WORK_TIME_TYPE)) {
            $datas = $this->overTimeRepository->getModel()->where('work_day', $request['data'])
                ->where('creator_id', Auth::id())/*->where('status', '!=', array_search('Đã duyệt', OT_STATUS))*/
                ->first();
        } else if ($request['type'] == array_search('Đi muộn', WORK_TIME_TYPE) || $request['type'] == array_search('Về sớm', WORK_TIME_TYPE)) {
            $datas = $this->workTimesExplanationRepository->getModel()->where('work_day', $request['data'])
                ->where('status', '!=', array_search('Từ chối', OT_STATUS))
                ->where('type', $request['type'])->where('user_id', Auth::id())->first();
        }
        return $datas;
    }

    /**
     * @param CreateDayOffRequest $request
     * @return bool
     */
    public function dayOffSave(CreateDayOffRequest $request)
    {
        if ($request->id_hid) {
            $dayOff = DayOff::FindOrFail($request->id_hid);
        } else {
            $dayOff = new DayOff();
        }
        $dayOff->fill($request->all());
        $timeStart = $request->start == DEFAULT_VALUE ? CHECK_TIME_DAY_OFF_START_DATE : CHECK_TIME_DAY_OFF_HALT_DATE;
        $timeEnd = $request->end == DEFAULT_VALUE ? CHECK_TIME_DAY_OFF_HALT_DATE : CHECK_TIME_DAY_OFF_END_DATE;
        $dayOff->start_at = $request->start_at . SPACE . $timeStart;
        $dayOff->end_at = $request->end_at . SPACE . $timeEnd;
        $dayOff->title = DAY_OFF_TITLE_DEFAULT;
        $dayOff->user_id = Auth::id();
        $dayOff->save();

        if (!$request->id_hid) {
            event(new DayOffNoticeEvent($dayOff, Auth::user(), NOTIFICATION_DAY_OFF['create']));
        }
        return true;
    }

    public function getUserList(Request $request, &$perPage, &$search)
    {
        $userList = $this->getContact($request, $perPage, $search, false);
        $userListData = $userList->map(function ($user) {
            $teamName = isset($user->teams[0]) ? $user->teams[0]->name : '';
            return [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'team_name' => $teamName,
                'group_name' => $user->group_name,
                'job_name' => JOB_TITLES[$user->jobtitle_id] ?? '',
                'position_name' => POSITIONS[$user->position_id],
            ];
        });

        $meta = [
            'panigation' => [
                'last_page' => $userList->lastPage(),
                'next_page_url' => $userList->nextPageUrl(),
                'prev_page_url' => $userList->previousPageUrl(),
                'to' => $userList->lastItem(),
                'total' => $userList->total(),
            ]
        ];

        return [
            'meta' => $meta,
            'users' => $userListData,
        ];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function projectStatistic($data)
    {
        $data['sort_column'] = isset($data['sort_column']) ? $data['sort_column'] : 'id';
        $data['sort_type'] = isset($data['sort_type']) ? $data['sort_type'] : 'ASC';
        $data['search'] = isset($data['search']) ? $data['search'] : '';
        $current = Carbon::now()->format(DATE_FORMAT);
        $startDaySearch = Carbon::now()->subDays($data['day_number']?? PROJECT_STATISTIC_SUB_DAY)->format(DATE_FORMAT);
        list($projectsIds, $taskIds) = $this->projectTask($startDaySearch, $current);
        $totalTimeUserDay = $this->totalTimeUserDay($startDaySearch, $current);
        $users = User::select('name', 'avatar', 'jobtitle_id', 'id')
            ->search($data['search'])->active()
            ->where('jobtitle_id', '<', MANAGER_ROLE)
            ->searchTeamGroup($data['group_id'] ?? null, null)
            ->with(['projects' => function ($rel) use($projectsIds){
                return $rel->with('projectTasks')->whereIn('projects.id', $projectsIds);
            }])->get();
        $dayList = get_date_list($startDaySearch, $current);
        foreach ($users as &$user) {
            list($projects, $totalTask, $totalBug) = $this->customProjectUser($user, $taskIds);
            $user->group_name = $user->team()->group->name ?? '';
            unset($user->projects);
            $user->projects = $projects;
            list($performance, $totalTime) = $this->customTimeLogDay($dayList, $totalTimeUserDay, $user->id);
            $user->performance = $performance;
            $user->total_task = $totalTask;
            $user->total_bug = $totalBug;
            $user->total_time = $totalTime;;
            $user->evaluate = 0;
            $user->volatility = 0;
        }
        $users = CollectionHelper::sortBy($users,$data['sort_column'],$data['sort_type']);
        return CollectionHelper::paginate($users,$data['page_size'] ?? DEFAULT_PAGE_SIZE);
    }

    /**
     * @param $dayList
     * @param $totalTimeUserDay
     * @param $userId
     * @param $isProjectLog
     * @return array
     */
    private function customTimeLogDay($dayList, $totalTimeUserDay, $userId, $isProjectLog = false){
        $performance = [];
        $totalTime = 0;
        if (isset($totalTimeUserDay[$userId])){
            $timeDay = $totalTimeUserDay[$userId]->groupBy('day');
            foreach ($dayList as $day) {
                $logDay = $timeDay[$day] ?? [];
                $totalTimeDay = 0;
                if ($logDay){
                    $totalTimeDay = $logDay->sum('total_time');
                }
                if ($isProjectLog){
                    $performance[] = [
                        'project_log' => $logDay,
                        'total_time' => $totalTimeDay
                    ];
                } else {
                    $performance[] = [
                        'total_time' => $totalTimeDay
                    ];
                }
                $totalTime += $totalTimeDay;
            }
        }
        return [$performance, $totalTime];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function userStatisticPerformance($data)
    {
        $user = $this->repository->findOne($data['user_id']);
        $current = Carbon::now()->format(DATE_FORMAT);
        $startDaySearch = Carbon::now()->subDays($data['day_number']?? PROJECT_STATISTIC_SUB_DAY)->format(DATE_FORMAT);
        $totalTimeUserDay = $this->totalTimeUserDay($startDaySearch, $current, $data['user_id']);
        if (!$user || $totalTimeUserDay->isEmpty()) return [];
        $dayList = get_date_list($startDaySearch, $current);
        list($projectIds) = $this->projectTask($startDaySearch, $current, $data['user_id']);
        $projects = Project::select('id as project_id', 'name as project_name')->whereIn('id', $projectIds)->get()->toArray();
        list($performance) = $this->customTimeLogDay($dayList, $totalTimeUserDay, $data['user_id'], true);
        return [
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'team' => $user->team()->name ?? ''
            ],
            'charts' => $performance,
            'projects' => $projects
        ];
    }

    /**
     * @param $user
     * @param $taskIds
     * @return array
     */
    private function customProjectUser($user, $taskIds) {
        $projects = [];
        $totalTasks = TOTAL_DEFAULT;
        $totalBugs = TOTAL_DEFAULT;
        if ($user->projects->isNotEmpty()){
            foreach ($user->projects as $project) {
                $memberProject =  $project->projectMembers ? $project->projectMembers->where('user_id', $user->id)->first() : [];
                $totalTask = TOTAL_DEFAULT;
                $totalBug = TOTAL_DEFAULT;
                if ($project->projectTasks){
                    $projectTasks = $project->projectTasks->whereIn('id', $taskIds)->where('user_id', $user->id);
                    $totalTask = $projectTasks->where('type', TASK_TYPE['task'])->count();
                    $totalBug = $projectTasks->where('type', TASK_TYPE['bug'])->count();
                }
                $projects[] = [
                    'project_id' => $project->id,
                    'project_name' => $project->name,
                    'role_name' => $memberProject && array_key_exists($memberProject->mission, MISSION_PROJECT) ? MISSION_PROJECT[$memberProject->mission] : ''
                ];
                $totalTasks += $totalTask;
                $totalBugs += $totalBug;
            }
        }
        return [$projects, $totalTasks, $totalBugs];
    }

    /**
     * @param $startDaySearch
     * @param $current
     * @param $userId
     * @return mixed
     */
    public function totalTimeUserDay($startDaySearch, $current, $userId = null){
        return TaskLogWork::select('project_tasks.user_id', 'project_tasks.project_id', 'projects.name as project_name',
            DB::raw('DATE_FORMAT(task_logwork.time_start, "%Y-%m-%d") as day, SUM(task_logwork.time) as total_time
            ')
        )
            ->join('project_tasks', 'task_logwork.task_id','project_tasks.id')
            ->join('projects', 'projects.id','project_tasks.project_id')
            ->whereDate('task_logwork.time_start', '<=' , $current)
            ->whereDate('task_logwork.time_start', '>=' , $startDaySearch)
            ->when($userId, function ($query) use ($userId){
                return $query->where('project_tasks.user_id', $userId);
            })
            ->whereNull('projects.deleted_at')
            ->groupBy('project_tasks.user_id', 'day', 'project_tasks.project_id')
            ->get()->groupBy('user_id');
    }

    /**
     * @param $startDaySearch
     * @param $current
     * @param $userId
     * @return array
     */
    public function projectTask($startDaySearch, $current, $userId = null)
    {
        $projectTask = ProjectTask::
            where(function($q) use ($startDaySearch, $current){
                return  $q->whereDate('created_at', '>=', $startDaySearch)
                    ->orWhereNull('deadline')->orWhereDate('deadline', '>=', $startDaySearch)
                    ->orWhereHas('logWork', function ($q) use ($current, $startDaySearch){
                        return $q->whereDate('task_logwork.time_start', '<=' , $current)
                            ->whereDate('task_logwork.time_start', '>=' , $startDaySearch);
                    });
            })
            ->when($userId, function ($query) use ($userId){
                return $query->where('project_tasks.user_id', $userId);
            })
            ->pluck('project_id', 'id')->toArray();
        $taskIds = array_keys($projectTask);
        $projectsIds = array_unique(array_values($projectTask));
        return [$projectsIds, $taskIds];
    }

    /**
     * @param $managerId
     * @return mixed
     */
    public function allMembersOfManager($managerId)
    {
        $groups = Group::where('manager_id', $managerId)->get();
        $allUserIds = collect([])->push($this->model::find($managerId));
        foreach ($groups as $group) {
            $allUserIds = $allUserIds->merge($this->model::searchTeamGroup($group->id, null)->get());
        }
        return $allUserIds;
    }
}
