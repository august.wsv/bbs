<?php 

namespace App\Services;

use App\Models\QrCode;
use App\Repositories\Contracts\IQrCodeRepository;
use App\Services\Contracts\IQrCodeService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class QrCodeService extends AbstractService implements IQrCodeService
{
    public function __construct(QrCode $model, IQrCodeRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function refresh($request)
    {
        try {
            DB::beginTransaction();
            if ($request->qrcode) {
                $this->repository->multiDelete(['qr_code' => $request->qrcode]);
            }
            $data = [];
            $data['qr_code'] = codeQr();
            $recods = $this->repository->save($data);
            DB::commit();
            return $recods->qr_code;
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();
            return false;
        }
    }
}