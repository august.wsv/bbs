<?php

/**
 * WorkTimeService class
 * Author: jvb
 * Date: 2019/01/22 10:50
 */

namespace App\Services;

use App\Models\AdditionalDate;
use App\Models\CalendarOff;
use App\Models\Config;
use App\Models\User;
use App\Models\WorkTime;
use App\Models\QrCode;
use App\Notifications\SentMSTeam;
use App\Repositories\Contracts\IWorkTimeRepository;
use App\Services\Contracts\IDayOffService;
use App\Services\Contracts\IWorkTimeRegisterService;
use App\Services\Contracts\IWorkTimeService;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

/**
 * @property Config config
 */
class WorkTimeService extends AbstractService implements IWorkTimeService
{
    const LATE_UNIT = 1000;
    const LATE_SECOND_SUFFIX = ':00'; //00 to 59

    private $calendarOffs;
    /**
     * @var PunishesService
     */
    private $punishesService;

    protected $worktimeRegisterService;

    protected $dayOffService;

    /**
     * WorkTimeService constructor.
     *
     * @param \App\Models\WorkTime                            $model
     * @param \App\Repositories\Contracts\IWorkTimeRepository $repository
     * @param PunishesService                                 $punishesService
     * @param \App\Services\Contracts\IWorkTimeRegisterService $worktimeRegisterService
     * @param \App\Services\Contracts\IDayOffService $dayOffService
     */
    public function __construct(
        WorkTime $model,
        IWorkTimeRepository $repository,
        PunishesService $punishesService,
        IWorkTimeRegisterService $worktimeRegisterService,
        IDayOffService $dayOffService
    ) {
        $this->model = $model;
        $this->repository = $repository;
        try {
            $this->config = Config::first();
            $this->calendarOffs = CalendarOff::all();
            $this->additionalDates = AdditionalDate::all();
        } catch (\Exception $exception) {
        }
        $this->users = User::select('id', 'name', 'staff_code', 'contract_type', 'is_remote', 'jobtitle_id')
            ->with('workTimeRegisters')
            ->get();

        $this->punishesService = $punishesService;
        $this->worktimeRegisterService = $worktimeRegisterService;
        $this->dayOffService = $dayOffService;
    }

    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search)
    {
        $model = $this->getSearchModel($request);
        return $model->search($search)->paginate($perPage);
    }

    /**
     * @param Request $request
     * @param         $search
     *
     * @return mixed
     */
    public function export(Request $request)
    {
        $search = $request->search;
        $model = $this->getSearchModel($request, true);
        return $model->search($search)->get();
    }

    public function deletesTimeKeeper($startDate, $endDate, $isAllUser, $userIds)
    {

        $model = WorkTime::whereDate('work_day', '>=', $startDate)
            ->whereDate('work_day', '<=', $endDate)
            ->whereNull('is_enter_hand')
            ->where('checkin_type', WORK_TIME_CHECKIN_TYPE_TIMEKEEPER);

        if (!$isAllUser) {
            $model = $model->whereIn('user_id', $userIds);
        }

        $model->delete();
        $enterHands = WorkTime::whereDate('work_day', '>=', $startDate)
            ->whereDate('work_day', '<=', $endDate)
            ->where(function ($query) {
                $query->where('is_enter_hand', ENTER_HAND)
                    ->orWhereHas('user', function ($q) {
                        $q->where('is_remote_checkin', WORK_TIME_CHECKIN_TYPE_ONLINE)
                            ->orWhere('is_remote_checkin_vpn', WORK_TIME_CHECKIN_TYPE_ONLINE);
                    });
            })
            ->select('user_id', 'work_day', 'start_at', 'end_at')->get();
        return $enterHands;
    }

    public function importWorkTime($userCode, $staffCode, $work_day, $startAt, $endAt)
    {
        $userId = $userCode[$staffCode];
        $user = $this->users->firstWhere('id', $userId);

        if ($user) {
            $workTime = $this->getWorkTime($user, $work_day, $startAt, $endAt);

            if ($workTime) {
                $workTime['user_id'] = $userId;
                $workTime['work_day'] = $work_day->format(DATE_FORMAT_SLASH);
            }
            return $workTime;
        }
    }

    /**
     * @param $user
     * @param $date
     * @param $startAt
     * @param $endAt
     *
     * @return array
     * @throws \Exception
     */
    public function getWorkTime($user, $date, $startAt, $endAt)
    {
        if ($endAt == null && $startAt != null && $startAt > HAFT_AFTERNOON) {
            [$startAt, $endAt] = [$endAt, $startAt];
        }

        if (!$this->config || !$this->config->time_afternoon_go_late_at) throw new \Exception(__l('system_no_config_time'));
        $addData = false;
        $type = 0;
        $cost = 0;
        $checkIsAdditionalDate = $this->additionalDates->firstWhere('date_add', $date->format(DATE_FORMAT));
        //
        $check = !$checkIsAdditionalDate && $this->calendarOffs->where('date_off_from', '>=', $date->format(DATE_FORMAT))->where('date_off_to', '<=', $date->format(DATE_FORMAT))->first();
        $notes = [];

        if ($check) {
            $type = WorkTime::TYPES['calendar_off'];
            return [
                'start_at' => null,
                'end_at' => null,
                'note' => WorkTime::WORK_TIME_CALENDAR_DISPLAY[$type] ?? '',
                'type' => $type,
                'cost' => WORKTIME_COST_OFF,
            ];
        } else if (($this->config->work_days && in_array($date->format('N'), $this->config->work_days)) || !$this->config->work_days || $checkIsAdditionalDate) {
            //checkin in week
            if (!$user->is_remote && in_array($user->contract_type, [CONTRACT_TYPES['staff'], CONTRACT_TYPES['probation']])) {
                $addData = true;
                //check day is off
                if ($startAt == null && $endAt == null) {
                    $type = WorkTime::TYPES['off'];
                    $notes[] = __('worktimes.no_info');
                } else {
                    list($cost, $type, $notes) = $this->getCostWorkTime(null, $startAt, $endAt);
                }
            } else {
                //check dayoff partime or internship
                $workTimeRegister = $user->workTimeRegisters->firstWhere('day', (int)$date->format('N') + 1);

                if ($workTimeRegister) {
                    $addData = true;
                    //no register
                    if ($startAt == null && $endAt == null) {
                        $type = WorkTime::TYPES['off'];
                        $notes[] = __('worktimes.no_info');
                    } else {
                        if ($workTimeRegister->start_at <= SWITCH_TIME && $workTimeRegister->end_at <= SWITCH_TIME) {
                            list($cost, $type, $notes) = $this->getCostWorkTime($workTimeRegister->start_at, $startAt, $endAt, -1);
                        } else if ($workTimeRegister->start_at >= SWITCH_TIME) { //afternoon only
                            list($cost, $type, $notes) = $this->getCostWorkTime($workTimeRegister->start_at, $startAt, $endAt, 1);
                        } else {
                            list($cost, $type, $notes) = $this->getCostWorkTime($workTimeRegister->start_at, $startAt, $endAt);
                        }
                    }
                } else {
                    // external
                    $addData = true;
                }
            }
        } else {
            if ($startAt != null || $endAt != null) {
                $addData = true;
                $notes[] = __('worktimes.ot_weekend');

                $type += WorkTime::TYPES['ot'];
            }
        }
        if ($addData)
            return [
                'start_at' => $startAt,
                'end_at' => $endAt,
                'note' => implode(', ', $notes),
                'type' => $type,
                'cost' => $cost,
                'checkin_type' => WORK_TIME_CHECKIN_TYPE['timekeeper'],
            ];
    }

    /**
     * @param       $fromDate
     * @param       $toDate
     * @param array $userIds
     *
     * @return mixed
     */
    public function calculateLateTime($fromDate, $toDate, $userIds = [])
    {
        $date = date_create($fromDate);
        $month = $date->format('m');

        $this->punishesService->calculateLateTime($date->format('Y'), $month, $userIds);
    }

    protected function getSearchModel(Request $request, $forExport = false)
    {
        $model = $this->model;
        $year = $request->get('year');

        if ($year) {
            $model = $model->whereYear('work_day', $year);
        }
        $month = $request->get('month');
        if ($month) {
            $model = $model->whereMonth('work_day', $month);
        }
        $work_day = $request->get('work_day');
        if ($work_day) {
            $model = $model->whereDate('work_day', $work_day);
        }
        $type = $request->get('type');
        if ($type != null) {
            if ($type == WorkTime::TYPES['lately']) {
                //lately || lately + early || lately + OT
                $model = $model->whereIn('type', [1, 3, 5]);
            } else if ($type == WorkTime::TYPES['ot']) {
                //OT || lately + OT
                $model = $model->whereIn('type', [4, 5]);
            } else if ($type == WorkTime::TYPES['forget_timekeep']) {
                $model = $model->where(function ($q) {
                    $q->whereNull('start_at')
                        ->orWhereNull('end_at');
                });
            } else {
                $model = $model->where('type', $type);
            }
        }
        $userId = $request->get('user_id');
        if ($userId) {
            $model = $model->where('user_id', $userId);
        }

        if ($forExport) {
            $model->orderBy('user_id')->orderBy('work_day');
        } else if ($request->has('sort')) {
            $model->orderBy($request->get('sort'), $request->get('is_desc') ? 'asc' : 'desc');
        } else {
            $model->orderBy('work_day', 'desc')->orderBy('user_id');
        }

        return $model;
    }

    /**
     * @param     $startAt
     * @param     $endAt
     * @param int $typeCheck : 0: full, -1: morning; 1: afternoon
     *
     * @return array
     */
    private function getCostWorkTime($registerAt, $startAt, $endAt, $typeCheck = 0)
    {
        $type = 0;

        $notes = [];
        $hart_hours = HAFT_HOUR . ':00';
        $hart_afternoon = HAFT_AFTERNOON . ':00';
        $hart_morning = HAFT_MORNING . ':00';
        if ($registerAt) {
            $registerAt = date_create($registerAt)->modify('+1 minutes')->format('H:i:s');
        }
        if ($startAt) {
            $startAt .= ':00';
            //check đi muộn quá nửa buổi chiều -> nghỉ ngày
            if ($typeCheck >= 0 && $startAt >= $hart_hours) {
                $timeLateAt = $registerAt > $hart_hours ? $registerAt : $this->config->time_afternoon_go_late_at;

                if ($startAt > $hart_afternoon) {
                    $type = WorkTime::TYPES['off'];
                    $notes[] = __('worktimes.off');
                    $notes[] = __('worktimes.late_over_haft_afternoon');
                } //check đi muộn buổi chiều
                else if ($startAt >= $timeLateAt) {
                    $type += WorkTime::TYPES['lately'];
                    $notes[] = __('worktimes.lately_afternoon');
                } //Chấm công buổi chiều, nghỉ sáng
                elseif ($typeCheck == 0)
                    $notes[] = __('worktimes.off_morning');
            }
            if ($typeCheck <= 0 && $startAt < $hart_hours) {
                $timeLateAt = $registerAt ?? $this->config->time_morning_go_late_at;

                //check đi muộn quá nửa buổi sáng -> nghỉ sáng
                if ($startAt > $hart_morning) {
                    $notes[] = __('worktimes.late_over_haft_morning');
                } else if ($startAt >= $timeLateAt) {
                    $type += WorkTime::TYPES['lately'];
                    $notes[] = __('worktimes.lately_morning');
                }
            }
        }
        if ($endAt && $type != WorkTime::TYPES['off']) {
            $endAt .= ':00';

            if ($typeCheck >= 0) {
                if ($startAt > $hart_hours && $endAt < $hart_afternoon) {
                    $notes[] = __('worktimes.early_over_haft_afternoon');
                    if ($typeCheck > 0) {
                        $type = WorkTime::TYPES['off'];
                    }
                } else
                    if ($endAt >= $this->config->morning_end_work_at && $endAt < $this->config->afternoon_end_work_at) {

                    $type += WorkTime::TYPES['early'];
                    $notes[] = __('worktimes.early_afternoon');
                }
            }
            if ($typeCheck <= 0) {
                if ($endAt < $this->config->morning_end_work_at) {
                    $type += WorkTime::TYPES['early'];
                    $notes[] = __('worktimes.early_morning');
                } else if ($endAt < $hart_morning) {
                    $notes[] = __('worktimes.early_over_haft_morning');
                    if ($typeCheck < 0) {
                        $type = WorkTime::TYPES['off'];
                    }
                }
            }

            if ($this->config->time_ot_early_at && $endAt >= $this->config->time_ot_early_at) {
                $type += WorkTime::TYPES['ot'];
                $notes[] = __('worktimes.ot');
            }
        }
        //cost
        $cost = 0;
        $costStartAt = $startAt ?? $this->config->morning_start_work_at;
        $costEndAt = $endAt ?? $this->config->afternoon_end_work_at;

        //        if ($typeCheck == 0) {
        if ($costStartAt <= $hart_morning && $costEndAt > $hart_afternoon) {
            $cost = 1;
        } else if ( //morning
            ($costStartAt <= $hart_morning && $costEndAt > $hart_morning && $costEndAt < $hart_afternoon)
            //afternoon
            || ($costStartAt > $hart_morning && $costStartAt < $hart_afternoon && $costEndAt >= $hart_afternoon)
        ) {
            $cost = 0.5;
        }
        if ($cost == 0) {
            $notes[] = __('worktimes.off');
            $type = WorkTime::TYPES['off'];
        }
        //        } else if ($typeCheck > 0 && $costStartAt < HAFT_AFTERNOON && $costEndAt >= HAFT_AFTERNOON) {
        //            $cost = 0.5;
        //        } else if ($typeCheck < 0 && $costStartAt <= HAFT_MORNING && $costEndAt > HAFT_MORNING) {
        //            $cost = 0.5;
        //        }
        //
        //        if ($typeCheck < 0 && $costStartAt <= HAFT_MORNING && $costEndAt > HAFT_MORNING) {
        //            $cost = 0.5;
        //        }
        //        if ($typeCheck > 0 && $costStartAt < HAFT_AFTERNOON && $costEndAt >= HAFT_AFTERNOON) {
        //            $cost += 0.5;
        //        }

        return [$cost, $type, $notes];
    }

    /**
     * @return array|bool|mixed
     */
    public function checkStaffTimekeeper()
    {
        $work_day_current = Carbon::now()->format(DATE_FORMAT);
        $contents = '';
        $work_time_day_current = $this->model->where('work_day', $work_day_current)->whereNull('start_at')->get();
        if ($work_time_day_current->isNotEmpty()) {
            $list_user = [];
            foreach ($work_time_day_current as $user_no_checkin) {
                // Lấy thời gian đăng kí làm việc
                $work_time_registers = $this->worktimeRegisterService->getWorktimeRegisterWithUserId($user_no_checkin->user->id, $work_day_current);
                // lấy đơn nghỉ của user
                $dayOff = $this->dayOffService->getDayOffNow($user_no_checkin->user->id);
                // Kiểm tra có lịch đăng ký làm việc, thời gian đăng ký lịch với thời gian hiện tại
                $check_work_time_register = isset($work_time_registers) ? $this->checkWorkTimeRegister($work_time_registers->start_at, $work_time_registers->end_at) : true;
                // Kiểm tra có đơn đăng ký nghỉ, và thời gian đăng ký nghỉ với thời gian hiện tại
                $checkDayOff = isset($dayOff) ? $this->checkTimeDayOff($dayOff->start_time, $dayOff->end_time, $dayOff->start_date, $dayOff->end_date, $work_day_current) : true;
                // kiểm tra đối với TH ban giám đốc, nhân viên remonte, nhân viên còn làm việc, nhân viên parttime, nhân viên xin nghỉ
                if (
                    $user_no_checkin->user->jobtitle_id != MASTER_ROLE
                    && $user_no_checkin->user->is_remote == IS_NOT_REMOTE
                    && $user_no_checkin->user->end_date == null
                    && $check_work_time_register && $checkDayOff
                ) {
                    $list_user[] = $user_no_checkin->user->staff_code . ' ' . $user_no_checkin->user->name;
                }
            }
            if (!empty($list_user)) {
                $contents = __l('staff_absent') . '<br/>- ' . implode(',<br/>- ', $list_user);
                return ['status' => STATUS_SEND['send'], 'contents' => $contents];
            } else {
                return ['status' => STATUS_SEND['not_send'], 'contents' => $contents];
            }
        } else {
            return ['status' => STATUS_SEND['not_send'], 'contents' => $contents];
        }
    }

    /**\admin\meeting_rooms
     * @param $start_at
     * @param $end_at
     * @return bool
     */
    public function checkWorkTimeRegister($start_at, $end_at)
    {
        $time_now = Carbon::now()->format(TIME_FORMAT);
        if ($start_at == OFF_TIME) {
            return false;
        } else {
            if ($start_at == WORK_TIME['start_at'] && $end_at == WORK_TIME['end_at']) {
                return true;
            } else {
                if ($end_at == END_WORK_TIME_MORNING) {
                    return $time_now <= $end_at;
                } else {
                    return $time_now >= $start_at && $time_now <= $end_at;
                }
            }
        }
    }

    /**
     * @param $start_time
     * @param $end_time
     * @param $start_date
     * @param $end_date
     * @param $day_check
     * @return bool
     */
    public function checkTimeDayOff($start_time, $end_time, $start_date, $end_date, $day_check)
    {
        $time_now = Carbon::now()->format(TIME_FORMAT);
        $hart_afternoon = Carbon::parse(HAFT_AFTERNOON)->format(TIME_FORMAT);
        if ($start_date == $end_date) {
            if ($start_time == WORK_TIME['start_at'] && $end_time == WORK_TIME['end_at']) {
                return false;
            } else {
                if ($end_time == CHECK_TIME_DAY_OFF_HALT_DATE) {
                    if ($time_now < CHECK_TIME_DAY_OFF_HALT_DATE) {
                        return false;
                    } else {
                        return $time_now >= CHECK_TIME_DAY_OFF_HALT_DATE && $time_now <= $hart_afternoon;
                    }
                } else {
                    if ($start_time == CHECK_TIME_DAY_OFF_HALT_DATE) {
                        return $time_now < CHECK_TIME_DAY_OFF_HALT_DATE;
                    } else {
                        return false;
                    }
                }
            }
        } else {
            if ($day_check >= $start_date && $day_check < $end_date) {
                if ($start_time == CHECK_TIME_DAY_OFF_HALT_DATE) {
                    return $time_now < CHECK_TIME_DAY_OFF_HALT_DATE;
                } else {
                    return false;
                }
            } else {
                if ($end_time == CHECK_TIME_DAY_OFF_HALT_DATE) {
                    if ($time_now < CHECK_TIME_DAY_OFF_HALT_DATE) {
                        return false;
                    } else {
                        return $time_now >= CHECK_TIME_DAY_OFF_HALT_DATE && $time_now <= $hart_afternoon;
                    }
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * @param Request $request
     * @param $userId
     * @return array
     * @throws \Exception
     */
    public function workTimeUser(Request $request, $userId)
    {
        $calendarData = [];
        $list_work_times_calendar = WorkTime::where('user_id', $userId)
            ->whereYear('work_day', $request->year)
            ->whereMonth('work_day', $request->month);

        if ($list_work_times_calendar) {
            foreach ($list_work_times_calendar->get()->toArray() as $item) {
                $startDay = $item['start_at'] ? new DateTime($item['start_at']) : '';
                $endDay = $item['end_at'] ? new DateTime($item['end_at']) : '';
                $dataStartDay = $item['start_at'];
                $dataEndDay = $item['end_at'];


                if ($dataStartDay && $dataStartDay != '00:00:00' || $dataEndDay && $dataEndDay != '00:00:00') {
                    if ($dataStartDay && $dataEndDay) {
                        $dataStartDay = $startDay->format('H:i');
                        $dataEndDay = $endDay->format('H:i');
                    } elseif ($dataEndDay == null || $dataEndDay == '00:00:00') {
                        $dataEndDay = '**:**';
                        $dataStartDay = $startDay->format('H:i');
                    } elseif ($dataStartDay == null || $dataStartDay == '00:00:00') {
                        $dataStartDay = '**:**';
                        $dataEndDay = $endDay->format('H:i');
                    }
                } elseif ($dataStartDay == '00:00:00' || $dataEndDay == '00:00:00' || $dataStartDay == null || $dataStartDay == '' || $dataEndDay == null || $dataEndDay == '') {
                    $dataStartDay = '**:**';
                    $dataEndDay = '**:**';
                } else {
                    $dataStartDay = '';
                    $dataEndDay = '';
                }

                $calendarData[] = [
                    'work_day' => $item['work_day'],
                    'start_at' => $dataStartDay,
                    'end_at' => $dataEndDay,
                    'type' => $item['type'],
                    'note' => $item['note'],
                    'id' => $item['id'],
                ];
            }
        }
        return $calendarData;
    }

    /**
     * @param mixed $request
     * @param mixed $qrCode
     * 
     * @return [type]
     */
    public function doCheckinOrCheckoutByQRCode($request)
    {
        $this->doCheckinOrCheckout($request);
        return __l('checkinout_success');
    }

    /**
     * @param mixed $request
     * 
     * @return [type]
     */
    public function doCheckinOrCheckout($request)
    {
        $model = $this->model;
        $user = User::find($request->user_id);
        $datetimeCheckinOrCheckout = $request->time;

        if (!empty($user)) {
            $dateCheckinOrCheckout = Carbon::parse($datetimeCheckinOrCheckout)->format(DATE_FORMAT);
            $timeCheckinOrCheckout = Carbon::parse($datetimeCheckinOrCheckout)->format(TIME_FORMAT);
            $convertToTime = Carbon::createFromTimeString($timeCheckinOrCheckout);
            $at10Hour = Carbon::createFromTime(10, 0, 0);
            $at15Hour = Carbon::createFromTime(15, 0, 0);
            $userWorkTime = $model->where([
                'user_id' => $user->id,
                'work_day' => $dateCheckinOrCheckout
            ])->first();

            if (empty($userWorkTime)) {

                $model->user_id = $user->id;
                $model->work_day = $dateCheckinOrCheckout;
                $model->checkin_type = WORK_TIME_CHECKIN_TYPE['checkin_online'];

                if ($convertToTime->lte($at15Hour)) {
                    $model->start_at = $timeCheckinOrCheckout;
                } else {
                    $model->end_at = $timeCheckinOrCheckout;
                }
                $message = __l('checkin_success');

                $model->save();
            } else {
                if ($convertToTime->gte($at10Hour) && $userWorkTime->start_at && $convertToTime->gte(Carbon::parse($userWorkTime->start_at))) {
                    $userWorkTime->end_at = $timeCheckinOrCheckout;
                    $userWorkTime->save();
                }
                $message = __l('checkout_success');
            }
        } else {
            $message = __l('user_not_found');
        }
        return $message;
    }
}
