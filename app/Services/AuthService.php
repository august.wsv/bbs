<?php

/**
 * AuthService class
 * Author: jvb
 * Date: 2023/05/30 10:14
 */

namespace App\Services;

use App\Services\Contracts\IAuthService;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService extends AbstractService implements IAuthService
{
    public function login($request)
    {
        if (!$token = JWTAuth::attempt($request->only(['email', 'password']))) {
            return false;
        }
        
        JWTAuth::setToken($token);
        $userDetail = $request->user();
        
        if ($request->fcm_token && $userDetail->fcm_token != $request->fcm_token) {
            $userDetail->fcm_token = $request->fcm_token;
            $userDetail->save();
        }

        return $token;
    }
}
