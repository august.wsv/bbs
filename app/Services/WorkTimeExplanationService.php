<?php

/**
 * WorkTimeExplanationService class
 * Author: jvb
 * Date: 2021/04/16 10:50
 */

namespace App\Services;

use App\Models\WorkTimesExplanation;
use App\Models\User;
use App\Services\Contracts\IWorkTimeExplanationService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @property Config config
 */
class WorkTimeExplanationService extends AbstractService implements IWorkTimeExplanationService
{
    /**
     * WorkTimeExplanationService constructor.
     *
     * @param \App\Models\WorkTimesExplanation                $model
     */
    public function __construct(
        WorkTimesExplanation $model
    ) {
        $this->model = $model;
    }

    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search)
    {
        $model = $this->getSearchModel($request);
        return $model->search($search)->paginate($perPage);
    }

    protected function getSearchModel(Request $request, $forExport = false)
    {
        $model = $this->model;
        $year = $request->get('year');
        if ($year) {
            $model = $model->whereYear('work_day', $year);
        }
        $month = $request->get('month');
        if ($month) {
            $model = $model->whereMonth('work_day', $month);
        }
        $type = $request->get('type');
        if ($type != null) {
            $model = $model->where('type', $type);
        }
        $work_day = $request->get('work_day');
        if ($work_day) {
            $model = $model->whereDate('work_day', $work_day);
        }
        $userId = $request->get('user_id');
        if ($userId) {
            $model = $model->where('user_id', $userId);
        }
        $groupName = $request->get('group_name');
        if ($groupName) {
            $model = $model->where(function ($q) use ($groupName) {
                $users = User::searchByGroup($groupName);
                $userIds = $users->pluck('id')->toArray();

                if (!empty($userIds)) {
                    $q->whereIn('user_id', $userIds);
                }
            });
        }

        return $model;
    }

    public function getTodayInfo($userIds)
    {
        $data = WorkTimesExplanation::select(
            'id',
            'user_id',
            'type',
            'note',
            'status',
            'created_at')
            ->whereRaw('DATE(work_day) = CURDATE()')
            ->whereIn('user_id', $userIds)
            ->with('user:id,name,avatar')
            ->get();

        return $data;
    }
}
