<?php

/**
 * ProjectPrint class
 * Author: Long
 * Date: 2023/06/06 17:40
 */

namespace App\Services;

use App\Models\Project;
use App\Repositories\Contracts\IProjectRepository;
use App\Repositories\Contracts\IProjectSprintRepository;
use App\Services\Contracts\IProjectSprintService;
use Illuminate\Http\Response;

class ProjectSprintService implements IProjectSprintService
{
    public $repository;
    public $projectRepo;
    private static $project;
    private $fields = [
        'name',
        'description',
        'project_id',
        'status',
        'start_date',
        'end_date',
        'effort',
    ];

    public function __construct(IProjectSprintRepository $repository, IProjectRepository $projectRepo)
    {
        $this->repository = $repository;
        $this->projectRepo = $projectRepo;
    }

    public function get($request)
    {
        return $this->repository->findBy(['project_id' => $request->project_id],['*'],true);
    }

    public function getProjectById($id)
    {
        if (!(self::$project instanceof Project) || self::$project->id != $id) {
            self::$project = $this->projectRepo->findOne($id);
        }

        return self::$project;
    }

    public function store($request)
    {
        $project = $this->getProjectById($request->project_id);
        if (!$project) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => __('messages.project_not_found'),
            ];
        }

        if ($request->status == SPRINT_STATUS['active']) {
            $project->projectSprints()
                ->where('status', SPRINT_STATUS['active'])
                ->update(['status' => SPRINT_STATUS['ended']]);
        }
        $request->status = $request->status?? 0;
        return $this->repository->save($request->only($this->fields));
    }

    public function update($request, $sprintId = null)
    {
        $project = $this->getProjectById($request->project_id);

        if (!$project) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => __('messages.project_not_found')
            ];
        }

        $projectSprint = $this->repository
            ->findOne($sprintId ?? $request->sprint_id);

        if (!$projectSprint) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => __('messages.sprint_not_found'),
            ];
        }

        if ($request->status == SPRINT_STATUS['active']) {
            $project->projectSprints()
                ->where('status', SPRINT_STATUS['active'])
                ->update(['status' => SPRINT_STATUS['ended']]);
        }
        $projectSprint->fill($request->only($this->fields));
        $projectSprint->save();
        return $projectSprint;
    }

    public function delete($id)
    {
        $projectSprint = $this->repository->findOne($id);

        if (!$projectSprint) {
            return [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => __('messages.sprint_not_found')
            ];
        }

        $projectSprint->delete();
        return true;
    }
}
