<?php

namespace App\Services\Contracts;

/**
 * IProvidedDeviceService contract
 * Author: jvb
 * Date: 2019/06/17 14:30
 */
interface IProvidedDeviceService extends IBaseService
{
}
