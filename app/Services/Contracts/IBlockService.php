<?php

namespace App\Services\Contracts;


/**
 * IWorkTimeService contract
 * Author: jvb
 * Date: 2019/01/22 10:50
 */
interface IBlockService extends IBaseService
{
    /**
     * getCurrentBlockUserIdList
     *
     * @param  mixed $record
     * @return void
     */
    public function getCurrentBlockUserIdList($record);

    /**
     * getListUserInTeam
     *
     * @return void
     */
    public function getListUserInTeam();

    /**
     * saveUserBlock
     *
     * @param  mixed $record
     * @return void
     */
    public function saveUserBlock($record);
}
