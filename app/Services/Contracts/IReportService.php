<?php

namespace App\Services\Contracts;

use App\Http\Requests\Api\ReportStatisticsRequest;
use App\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * IReportService contract
 * Author: jvb
 * Date: 2019/01/21 03:42
 */
interface IReportService extends IBaseService
{
    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     * @param int     $teamId
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search, &$teamId);

    /**
     * @return mixed
     */
    public function getReportReceiver();

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function create(Request $request);

    /**
     * @param int $id
     *
     * @return Report
     */
    public function detail($id);

    /**
     * @param int $id
     *
     * @return Report
     */
    public function detailApi($id);

    /**
     * @return Report
     */
    public function newReportFromTemplate();

    /**
     * @param int $type
     *
     * @return mixed
     */
    public function getReportTitle($type);

    /**
     * @return Report
     */
    public function getDraftReport();

    /**
     * @return Report
     */
    public function getFromDraftOrTemplate();

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function replyReport($reportId, Request $request);

    /**
     * @param ReportStatisticsRequest $request
     * @return mixed
     */
    public function statisticsReport(ReportStatisticsRequest $request);
}
