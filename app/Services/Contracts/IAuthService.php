<?php

namespace App\Services\Contracts;

/**
 * IAuthService contract
 * Author: jvb
 * Date: 2023/05/30 10:15
 */
interface IAuthService extends IBaseService
{    
    /**
     * login
     *
     * @param  \Illuminate\Http\Request $request
     * @return string|false
     */
    public function login($request);
}
