<?php

namespace App\Services\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * IOverTimeService contract
 * Author: jvb
 * Date: 2019/01/22 10:50
 */
interface IOverTimeService extends IBaseService
{
    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search);

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function export(Request $request);
}
