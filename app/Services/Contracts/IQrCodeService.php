<?php

namespace App\Services\Contracts;

/**
 * IQrCodeService contract
 * Author: jvb
 * Date: 2023/05/17 08:51
 */
interface IQrCodeService extends IBaseService
{
    public function refresh($request);
}
