<?php

/**
 * ProjectPrint class
 * Author: Long
 * Date: 2023/06/06 17:40
 */

namespace App\Services\Contracts;

interface IProjectSprintService {
    public function getProjectById($id);
    
    /**
     * store a sprint
     *
     * @param  \Illuminate\Http\Request $request
     * @return bool|array
     */
    public function store($request);

    /**
     * update a sprint
     *
     * @param  \Illuminate\Http\Request $request
     * @param int|null $sprintId
     * @return bool|array
     */
    public function update($request, $sprintId = null);

    /**
     * delete a sprint
     *
     * @param  int $id
     * @return bool|array
     */
    public function delete($id);
    
    /**
     * Get sprints
     *
     * @param  \Illuminate\Http\Request $request
     * @param  bool $isGetAll
     * @return \Illuminate\Database\Eloquent\Collection|static[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function get($request);
}
