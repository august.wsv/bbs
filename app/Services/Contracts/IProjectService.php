<?php

namespace App\Services\Contracts;

use App\Http\Requests\Api\ProjectTaskProgressRequest;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\Api\TaskLogTimeRequest;
use Illuminate\Support\Collection;

/**
 * IProjectService contract
 * Author: jvb
 * Date: 2019/01/31 05:00
 */
interface IProjectService extends IBaseService
{
    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search);

    /**
     * @param User $user
     * @param int $status
     *
     * @return collection
     */
    public function joinedList(User $user, $status = null);

    /**
     * @param User $user
     * @param $project
     * @param $data
     * @return mixed
     */
    public function createTask(User $user, $project, $data);

    /**
     * @param User $user
     * @param $project
     * @param $taskId
     * @param $data
     * @return mixed
     */
    public function updateTask(User $user, $project, $taskId, $data);

    /**
     * @param User $user
     * @param $projectId
     * @param $taskId
     * @return mixed
     */
    public function deleteTask(User $user, $projectId, $taskId);

    /**
     * @param int $id
     *
     * @return Project
     */
    public function detail($id);

    /**
     * @param int $id
     *
     * @return Project
     */
    public function overview($dataRequest, User $user, $id);

    /**
     * @param TaskLogTimeRequest $request
     * @param $taskId
     * @return mixed
     */
    public function logWork(TaskLogTimeRequest $request, $taskId);

    /**
     * @param $id
     * @return mixed
     */
    public function taskDetail($id);

    /**
     * @param $dataRequest
     * @param  $id
     * @param User $user
     * @return mixed
     */
    public function getTaskProgress($dataRequest, $id, User $user);
}
