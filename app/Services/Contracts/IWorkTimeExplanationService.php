<?php

namespace App\Services\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * IWorkTimeExplanationService contract
 * Author: jvb
 * Date: 2021/04/16 10:50
 */
interface IWorkTimeExplanationService extends IBaseService
{
    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search);

    public function getTodayInfo($userIds);
}
