<?php

namespace App\Services\Contracts;

use App\Http\Requests\AskPermissionRequest;
use App\Http\Requests\CreateDayOffRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * IUserService contract
 * Author: jvb
 * Date: 2018/07/16 10:34
 */
interface IUserService extends IBaseService
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function register(Request $request);

    /**
     * @param string $idCode
     *
     * @return int
     */
    public function getUserIdByIdCode(string $idCode);

    /**
     * @param Request $request
     * @param integer $perPage
     * @param string $search
     * @param bool $isGetAll
     *
     * @return collection
     */
    public function getContact(Request $request, &$perPage, &$search, $isGetAll = true);

    /**
     *
     * @return collection
     */
    public function getUserManager();

    /**
     *
     * @return collection
     */
    public function detail($id);

    /**
     *
     * @return collection
     */
    public function getUserByGroup();

    /**
     *
     * @return collection
     */
    public function getUsersAssginOt();

    public function getDayOff($array, $month, $year);

    /**
     *
     * @return collection
     */
    public function getParticipantIds($participants);

    /**
     * @param AskPermissionRequest $request
     * @return mixed
     */
    public function askPermissionCreate(AskPermissionRequest $request);

    /**
     * @return array
     */
    public function getOtAndAskPermission();

    /**
     * @param Request $request
     * @return mixed
     */
    public function detailOTWorkTimesExplanation(Request $request);

    /**
     * @param CreateDayOffRequest $request
     * @return mixed
     */
    public function dayOffSave(CreateDayOffRequest $request);

    /**
     * @param $data
     * @return mixed
     */
    public function projectStatistic($data);

    /**
     * @param $data
     * @return mixed
     */
    public function userStatisticPerformance($data);

    /**
     * @param $managerId
     * @return mixed
     */
    public function allMembersOfManager($managerId);

}
