<?php

/**
 * PunishesService class
 * Author: jvb
 * Date: 2019/04/22 08:21
 */

namespace App\Services;

use App\Models\Config;
use App\Models\Punishes;
use App\Models\Rules;
use App\Models\User;
use App\Models\WorkTime;
use App\Models\WorkTimesExplanation;
use App\Repositories\Contracts\IPunishesRepository;
use App\Services\Contracts\IPunishesService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * @property Config                                            config
 * @property \Illuminate\Database\Eloquent\Collection|static[] rules
 * @property NotificationService                               notificationService
 */
class PunishesService extends AbstractService implements IPunishesService
{
    public function __construct(IPunishesRepository $repository)
    {
        try {
            $this->config = Config::first();
            $this->rules = Rules::all();
        } catch (\Exception $exception) {
        }
        $this->notificationService = app()->make(NotificationService::class);

        $this->repository = $repository;
    }

    // Đi muộn không xin phép
    const LATE_WITHOUT_ASKING = 50;

    const THOUSAND_UNIT = 1000;

    public function detail($id)
    {
        return $this->repository->findOneBy([
            'id' => $id,
        ]);
    }

    /**
     * @param Request $request
     * @param         $userId
     * @param integer $perPage
     * @param string  $search
     *
     * @return collection
     */
    public function search(Request $request, $userId, &$perPage, &$search)
    {
        $criterias = $request->only('page', 'page_size', 'search');
        $criterias['user_id'] = $userId;
        $perPage = $criterias['page_size'] ?? DEFAULT_PAGE_SIZE;
        $search = $criterias['search'] ?? '';

        return $this->repository->findBy($criterias);
    }

    public function calculateLateTime($year, $month, $userIds = [])
    {
        [$firstDate, $endDate] = getStartAndEndDateOfMonth($month, $year);

        //read config file
        $path = storage_path('app/' . $this->config->late_time_rule_json ?? LATE_MONEY_CONFIG); // ie: /var/www/laravel/app/storage/json/filename.json

        $configs = collect(json_decode(file_get_contents($path), true)['configs']);

        $freeCount = $configs->where('free', true)->count();
        $aioConfigs = $configs->where('aio', '!=', 0);
        $eachConfigs = $configs->where('free', false)->where('aio', 0);

        //get late list
        $model = WorkTime::whereIn('type', [
            WorkTime::TYPES['lately'],
            WorkTime::TYPES['lately_ot'],
            WorkTime::TYPES['lately_early']
        ])
            ->where('cost', '>', 0)
            ->whereDate('work_day', '>=', $firstDate)
            ->whereDate('work_day', '<=', $endDate);

        if (!empty($userIds)) $model->whereIn('user_id', $userIds);

        $lateList = $model->orderBy('work_day')
            ->get()->groupBy('user_id');

        //clear old data
        $punish = Punishes::where('rule_id', LATE_RULE_ID)
            ->whereDate('infringe_date', '>=', $firstDate)
            ->whereDate('infringe_date', '<=', $endDate);
        if (!empty($userIds))
            $punish->whereIn('user_id', $userIds);

        $askingModel = WorkTimesExplanation::whereIn('type', PUNISH_WITHOUT_ASKING)
            ->whereBetween('work_day', [$firstDate, $endDate]);
        if (!empty($userIds))
            $askingModel = $askingModel->whereIn('user_id', $userIds);

        $askings = $askingModel->get();

        $managerIds = User::select('id')->manager()->active()->pluck('id')->toArray();

        DB::beginTransaction();
        $punish->forceDelete();
        $addPunishes = [];
        //caculate

        foreach ($lateList as $user_id => $workTimes) {
            if (in_array($user_id, $managerIds)) continue;
            $lateCount = $workTimes->count();
            if ($lateCount <= $freeCount) continue;

            //start caculate
            if ($lateCount > $aioConfigs->min('name')) {
                //Vé tháng
                $aio = $aioConfigs->sortByDesc('name')->firstWhere('name', '<=', $lateCount);
                if ($aio) {
                    $date = $workTimes->max('work_day');
                    $addPunishes[] = [
                        'rule_id' => LATE_RULE_ID,
                        'user_id' => $user_id,
                        'infringe_date' => $date,
                        'total_money' => $aio['aio'] * self::THOUSAND_UNIT,
                        'detail' => __l('punish_late_money_aio', [
                            'number' => $lateCount,
                            'month' => $month,
                        ])
                    ];
                }
            } else {
                foreach ($workTimes as $idx => $workTime) {
                    if ($idx < $freeCount) continue;
                    $number = $idx + 1;
                    $config = $eachConfigs->firstWhere('name', $number);
                    if ($config) {
                        $startAt = substr($workTime->start_at, 0, 5);
                        $times = collect($config['times']);
                        $time = $times->where('start', '<=', $startAt)
                            ->where('end', '>=', $startAt)
                            ->first();
                        if ($time) {
                            $addPunishes[] = [
                                'rule_id' => LATE_RULE_ID,
                                'user_id' => $user_id,
                                'infringe_date' => $workTime->work_day,
                                'total_money' => $time['total'] * self::THOUSAND_UNIT,
                                'detail' => __l('punish_late_money', [
                                    'number' => $number,
                                    'check_in' => date('h:i', strtotime($workTime->start_at)),
                                    'month' => $month,
                                ])
                            ];
                        }
                    }
                }
            }

            foreach ($workTimes as $idx => $workTime) {
                if ($idx < $freeCount) continue;

                // Đi muộn chưa quá 15p, không cần xin phép
                if (
                    $workTime->start_at < '09:00:00' || ($workTime->start_at > SWITCH_TIME && $workTime->start_at < '14:00:00')
                ) {
                    continue;
                }
                // Kiểm tra việc user có xin phép hay không?
                if ($askings->where('user_id', $user_id)->where('work_day', $workTime->work_day)->count() > 0) {
                    continue;
                }

                // Phạt đi muộn không xin phép
                $addPunishes[] = [
                    'rule_id' => LATE_RULE_ID,
                    'user_id' => $user_id,
                    'infringe_date' => $workTime->work_day,
                    'total_money' => self::LATE_WITHOUT_ASKING * self::THOUSAND_UNIT,
                    'detail' => __l('punish_late_money_without_asking')
                ];
            }
        }
        Punishes::insertAll($addPunishes);
        DB::commit();
    }

    /**
     * @param      $day
     * @param      $week
     * @param      $users
     * @param bool $mustConfirm
     */
    public function noWeeklyReport($day, $week, $users, $mustConfirm = 1)
    {
        //delete old data
        $weeklyReportRule = $this->rules->firstWhere('id', WEEKLY_REPORT_RULE_ID);
        Punishes::where([
            ['rule_id', '=', WEEKLY_REPORT_RULE_ID],
            ['week_num', '=', $week],
        ])->forceDelete();
        if ($weeklyReportRule) {
            $penalize = $weeklyReportRule->penalize;

            $punishes = [];
            foreach ($users as $user) {
                $punishes[] = [
                    'rule_id' => WEEKLY_REPORT_RULE_ID,
                    'week_num' => $week,
                    'user_id' => $user->id,
                    'infringe_date' => $day,
                    'total_money' => $penalize,
                    'detail' => __l('weekly_report_punish', [
                        'day' => $day,
                        'week' => $week,
                    ]),
                    'is_confirmed' => $mustConfirm == 1 ? PUNISH_NOT_CONFIRMED : PUNISH_CONFIRMED
                ];
            }

            if (!$mustConfirm) {
                // only sending notification if no need to confirm
                $this->notificationService->dontSentWeeklyReport($users, $week);
            }
            Punishes::insertAll($punishes);
        }
    }

    public function getListPunishesOnMonth($startBeforeMonth, $endBeforeMonth, $perPage, $keySearch)
    {
        return DB::table('punishes')
            ->join('users', 'punishes.user_id', 'users.id')
            ->select('users.staff_code as code_staff', 'users.name as name_staff', 'punishes.rule_id as rule_id', 'punishes.total_money as total_money', 'punishes.infringe_date as infringe_date')
            ->where('infringe_date', '>=', $startBeforeMonth)
            ->where('infringe_date', '<=', $endBeforeMonth)
            ->where('users.name', 'like', '%' . $keySearch . '%')
            ->whereNull('punishes.deleted_at')
            ->orderBy('infringe_date', 'desc')
            ->paginate($perPage)->withPath(route('fine_statistic_index', ['page_size' => $perPage, 'search' => $keySearch]));
    }

    public function getListTopPunishes($startBeforeMonth, $endBeforeMonth, $limit = 5)
    {
        return
            DB::table('punishes')
            ->join('users', 'punishes.user_id', 'users.id')
            ->select(DB::raw("users.name as name,SUM(punishes.total_money) as total_money"))
            ->where('infringe_date', '>=', $startBeforeMonth)
            ->where('infringe_date', '<=', $endBeforeMonth)
            ->whereNull('punishes.deleted_at')
            //                ->where('rule_id','<>',0)
            ->groupBy('users.id')
            ->orderBy('total_money', 'desc')
            ->take($limit)
            ->get();
    }

    public function getListTypePunishes($startBeforeMonth, $endBeforeMonth)
    {
        return
            DB::table('punishes')
            ->select(DB::raw("punishes.rule_id as rule_id, SUM(punishes.total_money) as total_money"))
            ->where('infringe_date', '>=', $startBeforeMonth)
            ->where('infringe_date', '<=', $endBeforeMonth)
            ->whereNull('punishes.deleted_at')
            ->groupBy('punishes.rule_id')
            ->get();
    }

    /**
     * getUserList 
     *
     * @param  mixed $startDate
     * @param  mixed $endDate
     * @param  mixed $groupId
     * @return void
     */
    public function getUserList($startDate, $endDate, $groupId, $paginate = false)
    {
        $listPunishedUser = Punishes::select('punishes.user_id', 'users.staff_code', 'users.name', DB::raw('SUM(total_money) as total'))
        ->searchBeetweenDate($startDate, $endDate)
            ->statisticByGroup($groupId)
            ->join('users', 'punishes.user_id', '=', 'users.id')
            ->groupBy('user_id')
            ->orderBy('total', 'DESC');
        return $paginate ? $listPunishedUser->paginate($paginate) : $listPunishedUser->get();
    }

    /**
     * getTotalLate
     *
     * @param  mixed $startDate
     * @param  mixed $endDate
     * @param  mixed $groupId
     * @return void
     */
    public function getTotalLate($startDate, $endDate, $groupId)
    {
        return Punishes::select('total_money')->where('rule_id', LATE_RULE_ID)->statisticByGroup($groupId)
            ->searchBeetweenDate($startDate, $endDate)->sum('total_money');
    }

    /**
     * getPunishTypes
     *
     * @param  mixed $startDate
     * @param  mixed $endDate
     * @param  mixed $groupId
     * @param  mixed $rules
     * @return void
     */
    public function getPunishTypes($startDate, $endDate, $groupId, $rules)
    {
        if ($rules->isEmpty()) return [];
        
        $ruleIdList = $rules->pluck('id')->toArray();
        $punishTypes = Punishes::select('total_money', 'rule_id')->whereIn('rule_id', $ruleIdList)
            ->searchBeetweenDate($startDate, $endDate)
            ->statisticByGroup($groupId)->get()->groupBy('rule_id');

        $moneyList = [];
            
        foreach ($punishTypes as $key => $totalMoney) {
            $moneyList[$key - 1] = $totalMoney->sum('total_money');
        }
        for ($i = 0; $i < count($ruleIdList); $i++) {
            if (!isset($moneyList[$i])) {
                $moneyList[$i] = 0;
            }
        }
        ksort($moneyList);
        return $moneyList;
    }
}
