<?php

/**
 * WorkTimeService class
 * Author: jvb
 * Date: 2019/01/22 10:50
 */

namespace App\Services;

use App\Models\Block;
use App\Models\Team;
use App\Models\User;
use App\Models\UserBlock;
use App\Repositories\Contracts\IBlockRepository;
use App\Services\Contracts\IBlockService;

/**
 * @property Config config
 */
class BlockService extends AbstractService implements IBlockService
{

    /**
     * __construct
     *
     * @return void
     */
    public function __construct(
        Block $model,
        IBlockRepository $repository
    ) {
        $this->model = $model;
        $this->repository = $repository;
    }

    /**
     * getCurrentBlockUserIdList
     *
     * @param  mixed $record
     * @return void
     */
    public function getCurrentBlockUserIdList($record)
    {
        $listUserIdOfTeam = [];
        foreach ($record->members as $user) {
            $listUserIdOfTeam[] = 'user_' . $user->id;
        }
        return $listUserIdOfTeam;
    }

    /**
     * getListUserInTeam
     *
     * @return void
     */
    public function getListUserInTeam()
    {
        $teams = Team::all();

        $data = [];
        foreach ($teams as $team) {
            $listUsers = [];
            $listMembers = $team->members;
            foreach ($listMembers as $member) {
                $listUsers[] = [
                    'id' => 'user_' . $member->id,
                    'title' => $member->staff_code . ': ' . $member->name .  $member->getCurrentBlockName(),
                ];
            }
            $listUsers[] = [
                'id' => 'user_' . $team->leader->id,
                'title' => $team->leader->staff_code . ': ' .
                    $team->leader->name . $team->leader->getCurrentBlockName(),
            ];
            $listUsers = array_unique($listUsers, SORT_REGULAR);
            $data[] = [
                'id' => $team->id,
                'title' => 'Team: ' . $team->name,
                'subs' => $listUsers,
            ];
        }
        return json_encode($data);
    }

    /**
     * saveUserBlock
     *
     * @param  mixed $record
     * @return void
     */
    public function saveUserBlock($request)
    {
        $record = $this->repository->findOne($request->id);
        if (!$record) {
            abort(404);
        }
        $listIdOfMember = explode(', ', $request->member_ids[0]);
        $listStaffCode = [];
        foreach ($listIdOfMember as $dataOfMember) {
            $checkTeam = strpos($dataOfMember, 'Team');
            if ($checkTeam) {
                continue;
            }
            $staffCode = '';
            $staffCode = explode(':', $dataOfMember)[0] ?? '';
            $listStaffCode[] = $staffCode;
        }
        $listIdMemerber = User::select('id')->whereIn('staff_code', $listStaffCode)->get()->pluck('id')->toArray();
        $idOfBlock = $record->id;
        UserBlock::whereIn('user_id', $listIdMemerber)->orWhere('block_id', $idOfBlock)->delete();
        $listMemberInBlock = [];
        foreach ($listIdMemerber as $idMember) {
            $listMemberInBlock[] = [
                'block_id' => $idOfBlock,
                'user_id' => $idMember,
            ];
        }

        UserBlock::insertAll($listMemberInBlock);
    }
}
