<?php
/**
 * GroupService class
 * Author: jvb
 * Date: 2019/05/16 14:31
 */

namespace App\Services;

use App\Helpers\DateTimeHelper;
use App\Models\Group;
use App\Models\LaborCalendar;
use App\Models\LaborCalendarDetail;
use App\Models\User;
use App\Services\Contracts\ILaborCalendarService;
use App\Models\CalendarOff;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use PhpParser\Node\Stmt\While_;

class LaborCalendarService extends AbstractService implements ILaborCalendarService
{
    public function saveLaborCalendar($month,$year,$listUserID,$content)
    {
            $dayOff = CalendarOff::select('date_off_from', 'date_off_to')->get()->toArray();
            $listDayOff = [];
            foreach ($dayOff as $pairDay) {
                $this->getDayOff($listDayOff, $pairDay['date_off_from'], $pairDay['date_off_to']);
            }
            $this->checkExistLaborMonth($month,$year);
            $day = Carbon::parse($month.'/1/'.$year);
            $dayOfMonth = $day->daysInMonth;
            $arrayMales = [];
            $arrayFemales = [];

            foreach ($listUserID as $id)
            {
                $user = User::withTrashed()->find($id);
                if($user->sex == SEX['male'])
                {
                    $arrayMales [] = $id;
                }else{
                    $arrayFemales [] = $id;
                }
            }

            if(empty($arrayMales))
            {
                $this->onlyGender($day, $dayOfMonth, $arrayFemales, $content, $listDayOff);
            }elseif(empty($arrayFemales))
            {
                $this->onlyGender($day, $dayOfMonth, $arrayMales, $content, $listDayOff);
            }else{
                for ($i = 1; $i <= $dayOfMonth ; $i++)
                {
                    if (in_array($day->toDateString(), $listDayOff)) {
                        $day->addDay(1);
                        continue;
                    }
                    //only apply for two user in day
                    if($day->dayOfWeek != DAY_OF_WEEK['sunday'] && $day->dayOfWeek != DAY_OF_WEEK['saturday'])
                    {
                        if(empty($arrayMales)){
                            $this->onlyGender($day, $dayOfMonth, $arrayFemales, $content, $listDayOff);
                            break;
                        }elseif(empty($arrayFemales)){
                            $this->onlyGender($day, $dayOfMonth, $arrayMales, $content, $listDayOff);
                            break;
                        }else{
                            $laborID = LaborCalendar::create([
                                'creator_id' => Auth::guard('admin')->user()->id,
                                'content' => $content,
                                'status' => LABOR_STATUS['default'],
                                'labor_date' => $day->toDateTimeString()
                            ])->id;

                            $randomIndexMales = rand(0,count($arrayMales) -1);
                            LaborCalendarDetail::create([
                                'user_id' => $arrayMales[$randomIndexMales],
                                'labor_calendar_id' => $laborID,
                            ]);
                            unset($arrayMales[$randomIndexMales]);
                            $arrayMales = array_values($arrayMales);

                            $randomIndexFemales = rand(0,count($arrayFemales) -1);
                            LaborCalendarDetail::create([
                                'user_id' => $arrayFemales[$randomIndexFemales],
                                'labor_calendar_id' => $laborID,
                            ]);
                            unset($arrayFemales[$randomIndexFemales]);
                            $arrayFemales = array_values($arrayFemales);
                        }
                    }
                    $day->addDay(1);
                }
            }
        return;
    }

    public function onlyGender($day, $dayOfMonth, $arrayID, $content, $listDayOff)
    {
        for ($i = 1 ; $i <= $dayOfMonth ; $i++)
        {
            if (in_array($day->toDateString(), $listDayOff)) {
                $day->addDay(1);
                continue;
            }
            if($day->dayOfWeek != DAY_OF_WEEK['sunday'] && $day->dayOfWeek != DAY_OF_WEEK['saturday'])
            {
                if(!empty($arrayID))
                {
                    $laborID = LaborCalendar::create([
                        'creator_id' => Auth::guard('admin')->user()->id,
                        'content' => $content,
                        'status' => LABOR_STATUS['default'],
                        'labor_date' => $day->toDateTimeString()
                    ])->id;

                    for($j = 0 ; $j < NUMBER_OF_USER_LABOR_IN_DAY; $j++)
                    {
                        $indexRandom = rand(0,count($arrayID)-1);
                        LaborCalendarDetail::create(
                            [
                                'user_id' => $arrayID[$indexRandom],
                                'labor_calendar_id' => $laborID
                            ]
                        );
                        unset($arrayID[$indexRandom]);
                        $arrayID = array_values($arrayID);
                    }
                }else{
                    break;
                }
            }
            $day->addDays(1);
        }
    }

    public function getLaborCalendar($searchMonth,$searchYear)
    {
        $laborCalendars = [];
        $startDayOfMonth = Carbon::parse($searchMonth.'/1/'.$searchYear)->firstOfMonth()->toDateString();
        $endDayOfMonth = Carbon::parse($searchMonth.'/1/'.$searchYear)->endOfMonth()->toDateString();
        $listLaborCalendar = LaborCalendar::where('labor_date','>=',$startDayOfMonth)
            ->where('labor_date','<=',$endDayOfMonth)
            ->get();

        foreach ($listLaborCalendar as $laborCalendar)
        {
            $status = '';
            foreach ($laborCalendar->laborCalendarDetail as $detail)
            {
                $user = User::withTrashed()->find($detail->user_id);
                $arrayLaborCalendar = [];
                $arrayLaborCalendar['title'] = $user->name;
                $arrayLaborCalendar['gender'] = $user->sex;
                $arrayLaborCalendar['start_date'] = $laborCalendar->labor_date;
                $arrayLaborCalendar['status'] = $laborCalendar->status;
                $arrayLaborCalendar['group_id'] = $laborCalendar->id;
                $arrayLaborCalendar['url'] = route('admin::labor_calendar.edit',$laborCalendar->id);
                $arrayLaborCalendar['check_type'] = 'user';
                $laborCalendars [] = $arrayLaborCalendar;
                $status = $laborCalendar->status;
            }

            $button = [];
            $button['title'] = 'Trạng thái';
            $button['start_date'] = $laborCalendar->labor_date;
            $button['status'] = $status;
            $button['group_id'] = $laborCalendar->id;
            $button['check_type'] = 'button';
//            $button['url'] = route('admin::labor_calendar.edit',$LaborCalendar->id);
            $laborCalendars [] = $button;
        }

        return $laborCalendars;
    }

    public function getInfoLaborCalendar($laborCalendarID)
    {
        return LaborCalendar::find($laborCalendarID);
    }

    public function checkExistLaborMonth ($month,$year)
    {
        $startDay = Carbon::parse($month.'/1/'.$year)->firstOfMonth()->toDateString();
        $endDay = Carbon::parse($month.'/1/'.$year)->lastOfMonth()->toDateString();

        $oldLaborList = LaborCalendar::where('labor_date','>=',$startDay)->where('labor_date','<=',$endDay)->get();
        if(!$oldLaborList->isEmpty())
        {
            foreach ($oldLaborList as $labor)
            {
                foreach ($labor->laborCalendarDetail as $laborDetail)
                {
                    $laborDetail->delete();
                }
                $labor->delete();
            }
        }

    }

    public function randomLaborCalendar($month, $year, $content) {
        $firstDayOfMonth = Carbon::parse($year.'/'.$month.'/1');
        $firstDayOfBeforeMonthObject = $firstDayOfMonth->subMonths(1);
        $firstDayOfBeforeMonth =  $firstDayOfBeforeMonthObject->startOfMonth()->toDateString();
        $finishDayOfBeforeMonth =  $firstDayOfBeforeMonthObject->endOfMonth()->toDateString();
        $listUserInBeforeMonth = DB::table('users')->select('users.*')
            ->join('labor_calendar_details', 'labor_calendar_details.user_id', 'users.id')
            ->join('labor_calendars', 'labor_calendars.id', 'labor_calendar_details.labor_calendar_id')
            ->where('labor_calendars.labor_date', '>=', $firstDayOfBeforeMonth)
            ->where('labor_calendars.labor_date', '<=', $finishDayOfBeforeMonth)
            ->where('users.status', '=', ACTIVE_STATUS)
            ->pluck('users.id')->toArray();
        $allUserID = User::
             where('contract_type', '<>', USER_ROLE['intership'])
            ->whereNull('users.end_date')
            ->where('users.jobtitle_id', '<>', MASTER_ROLE)
            ->where('users.status', '=', ACTIVE_STATUS)
            ->where('users.is_remote', '<>', IS_REMOTE)
            ->get()
            ->pluck('id')
            ->toArray();
        $listUserIDRemain = array_diff($allUserID, $listUserInBeforeMonth);
        $countListUserIDRemain = count($listUserIDRemain);
        $countUserChose = DateTimeHelper::countUserChose($month, $year);
        $countAllUser = count($allUserID);
        $listIDRandom = [];
        if($countAllUser <= $countUserChose) {
            $listIDRandom = $allUserID;
            $addUserCount = $countUserChose - $countAllUser;
            for ($i = 0; $i < $addUserCount; $i++)
            {
                $listIDRandom [] = $allUserID[rand(0, count($allUserID) - 1)];
            }
        } else {
            if ($countListUserIDRemain < $countUserChose) {
                $addUserCount = $countUserChose - $countListUserIDRemain;
                $this->randomIdUSer($listIDRandom,  $listUserInBeforeMonth, $addUserCount);
                $this->randomIdUSer($listIDRandom, $listUserIDRemain, $countListUserIDRemain);
            } else {
                $this->randomIdUSer($listIDRandom , $listUserIDRemain, $countUserChose);
            }
        }
        $this->saveLaborCalendar($month, $year, $listIDRandom, $content);
    }

    public function randomIdUSer(&$listIDRandom, $listUSerID, $countIDNeed) {
        $listUSerID = array_values($listUSerID);
        for ($i = 0; $i < $countIDNeed; $i++) {
            $id = $listUSerID[rand(0, count($listUSerID) - 1)];
            while (in_array($id, $listIDRandom)) {
                $id = $listUSerID[rand(0, count($listUSerID) - 1)];
            }
            $listIDRandom [] = $id;
        }
    }

    public function getListLaborCalendarAll($searchYear) {
        $firstDayOfYear = $searchYear.'/01/01';
        $finishDayOfYear = $searchYear.'/12/31';
        $allUser = User::where('status', ACTIVE_STATUS)->get();
        $listUser = [];
        $listLaborInYear = DB::table('labor_calendars')
                    ->join('labor_calendar_details', 'labor_calendars.id', 'labor_calendar_details.labor_calendar_id')
                    ->where('labor_date', '>=', $firstDayOfYear)
                    ->where('labor_date', '<=', $finishDayOfYear)
                    ->get()
                    ->groupBy('user_id');

        foreach ($allUser as $user) {
            $idUser = $user->id;
            $nameUser = $user->name;
            $info = [
                'id' => $idUser,
                'name' => $nameUser,
                'month_1' => false,
                'month_2' => false,
                'month_3' => false,
                'month_4' => false,
                'month_5' => false,
                'month_6' => false,
                'month_7' => false,
                'month_8' => false,
                'month_9' => false,
                'month_10' => false,
                'month_11' => false,
                'month_12' => false,
            ];
            $listUser[$idUser] = $info;
        }
        foreach ($listLaborInYear as $idUSer => $infoUserInYear) {
            foreach ($infoUserInYear as $infoUserInMonth) {
                $month = Carbon::parse($infoUserInMonth->labor_date)->month;
                if (array_key_exists($idUSer, $listUser)) {
                    $listUser[$infoUserInMonth->user_id]['month_'.$month] = true;
                }
            }
        }
        return $listUser;
    }

    public function getListLaborUserInMonth($searchMonth, $searchYear) {
        $firstDayOfMonthSearch = $searchYear.'-'.$searchMonth.'-'.'01';
        $finishDayOfMonthSearch = Carbon::parse($firstDayOfMonthSearch)->endOfMonth()->toDateString();

        return DB::table('users')
            ->select('users.id as id','users.name as name')
            ->join('labor_calendar_details', 'users.id', 'labor_calendar_details.user_id')
            ->join('labor_calendars', 'labor_calendars.id', 'labor_calendar_details.labor_calendar_id')
            ->where('labor_calendars.labor_date', '>=', $firstDayOfMonthSearch)
            ->where('labor_calendars.labor_date', '<=', $finishDayOfMonthSearch)
            ->where('users.status', ACTIVE_STATUS)
            ->get();
    }

    public function getListLaborUserInMonthUnChose($searchMonth, $searchYear) {
       $listUserChose = $this->getListLaborUserInMonth($searchMonth, $searchYear);
       $listUserID = [];
       foreach ($listUserChose as $user) {
           $listUserID [] = $user->id;
       }
        return User::select('id','name')->whereNotIn('id', $listUserID)->where('users.status', ACTIVE_STATUS)->get();
    }

    public function getDayOff(&$listDayOff, $dayOffFrom, $dayOffTo){
        $dayFrom = Carbon::parse($dayOffFrom);
        $dayTo = Carbon::parse($dayOffTo);
        While (true) {
            $listDayOff [] =  $dayFrom->toDateString();
            $dayFrom->addDays(1);
            if ($dayFrom > $dayTo) {
                break;
            }
        }
    }

}
