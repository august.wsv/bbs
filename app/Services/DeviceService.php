<?php
/**
* DeviceService class
* Author: jvb
* Date: 2019/03/11 06:46
*/

namespace App\Services;

use App\Exports\TDeviceExport;
use App\Models\Device;
use App\Models\TDevice;
use App\Models\TDeviceType;
use App\Models\TDeviceTypeAttribute;
use App\Models\TDeviceUpgradeHistory;
use App\Models\TImportDevice;
use App\Models\TUserDevice;
use App\Models\User;
use App\Services\Contracts\IDeviceService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use stdClass;
use Illuminate\Support\Facades\Auth;

class DeviceService extends AbstractService implements IDeviceService
{

    public function getDevicesByType($type)
    {
        return Device::where('types_device_id',  $type)
                    ->where('final', '>', 0)
                    ->select('id', 'name', 'types_device_id')->get();
    }

    public function getDevicesService($request, $device, $role)
    {   
        if($request->search) {
            $users = User::searchDevices($request->search)->paginate(20, ['id', 'name', 'staff_code']);
            return $users;
        }
        if($role != 'FIND_DEVICE')
        {
            if($request->per_page)
            {
                $users = User::with('device_user.device.device_name')->limit($request->per_page)->get()->count();
            }else
            {
                $users = User::with('device_user.device.device_name')->paginate(20, ['id', 'name', 'staff_code']);
            }
        }else{
            $users = User::with(['device_user.device.device_name'=> function ($query) use ($device) {
                $query->where('id', $device);
            }])->get();
        }
        return $users;
    }

    public function getOptionDevices()
    {
        $devices = TImportDevice::orderBy('id', 'desc')->limit(2)->get();
        return $devices;
    }




    public function saveFormDeviceService($request)
    {
        $dataInsertSpecs = $this->handleSpecificationDevice($request);
        foreach ($request->device_type_id as $key => $deviceTypeId) {
            $deviceImport = TImportDevice::find($request->device_select[$key]);
            $deviceImport->quantity = $deviceImport->quantity - 1;
            $deviceImport->save();

            $devices = new TDevice();
            $devices->device_type_id = $request->device_type_id[$key];
            $devices->name = $deviceImport->name;
            $devices->specification = $dataInsertSpecs[$key]['specification'] ?? [];
            $devices->import_device_id = $deviceImport->id;
            $devices->save();

            $deviceUser = new TUserDevice();
            $deviceUser->device_import_id = $deviceImport->id;
            $deviceUser->specification = $dataInsertSpecs[$key]['specification'] ?? [];
            $deviceUser->user_id = $request->user_id;
            $deviceUser->device_id = $devices->id;
            $deviceUser->start_date = $request->date_import[$key] ?? date("Y-m-d");
            $deviceUser->save();
        }
        return $devices;
    }

    public function updateDeviceService($id, $role)
    {
        $device_user = TUserDevice::find($id);
        $device = TDevice::find($device_user->device_id);
        $device_user['device'] = $device;
        $device_user['device_name'] = TDeviceType::where('id', $device->device_type_id)->select('id','name')->first();
        $device_user['device_type'] = TDeviceTypeAttribute::where('device_type_id', $device->device_type_id)->get();
        return $device_user;
    }

    public function saveFormUpgradeService($request, $id, $role)
    {
        if($role == 'UPGRADE' || $role = 'CHANGE')
        {
            $old_device_import = TImportDevice::find($request->import_device_id);
            $old_device_import->quantity = $old_device_import->quantity + 1;
            $old_device_import->save();
            $device_import = TImportDevice::find($request->id_device_new);
            if (!$device_import) {
                return false;
            }
            $device_import->quantity = $device_import->quantity - 1;
            $device_import->save();

            $device = new TDevice();
            $device->device_type_id = $request->device_type_id;
            $device->import_device_id = $device_import->id;
            $arr_list = "";
            foreach($request->specification as $key=>$value)
            {
                $array_item = $key;
                $array_attr = "";
                foreach($value as $k=>$v)
                {

                    if($k == 0){
                        $note = '';
                    }
                    else
                    {
                        $note = ' - ';
                    }
                    if(isset($v['name'])){
                        $array_attr .= $note . $v['name'];
                    }
                }
                $array_item .= ' : ' . $array_attr;
                $arr_list .= ' | ' . $array_item;
                $options[$key] = $value;
            }
            $device->name = $device_import->name;
            $device->specification = $options;
            $device->save();

            if($role == 'UPGRADE')
            {
                $upgrade_check = TDeviceUpgradeHistory::where('device_last_id', $request->device_id)->first();
                $t_device_upgrade_histories = new TDeviceUpgradeHistory();
                $t_device_upgrade_histories->device_first_id = $request->device_id;
                $t_device_upgrade_histories->device_last_id = $device->id;
                $t_device_upgrade_histories->user_id = $request->user_id;
                if($upgrade_check != null)
                {
                    $t_device_upgrade_histories->level_upgrade = $upgrade_check->level_upgrade + 1;
                }else{
                    $t_device_upgrade_histories->level_upgrade = 1;
                }
                $t_device_upgrade_histories->save();
            }
            $device_user = TUserDevice::find($request->device_user_id);
            $device_user->device_id = $device->id;
            $device_user->save();
            if($device){
                return $device;
            }else{
                return false;
            }
        }
    }

    public function getDeviceDetailByUserId($id)
    {
        $user = User::find($id);
        $user['user_devices'] = TUserDevice::where('user_id', $id)->get();
        foreach($user->user_devices as $user_device)
        {
            $user_device['devices'] = [];
            $devices = TDevice::find($user_device->device_id);
            $devices['device_name'] = TDeviceType::where('id', $devices->device_type_id)->first();
            $user_device['devices'] = $devices;

        }
        return $user;
    }


    public function getDevicesServiceById($id)
    {
        $device = Device::where('id', $id)->with('device_detail')->first();
        return $device;
    }

    public function getDeviceOptions()
    {
        $devices = TDeviceType::all();
        return $devices;
    }

    public function allDeviceImportByService()
    {
        $device_type = TDeviceType::all();
        foreach($device_type as $item)
        {
            $item['device_type'] = TImportDevice::where('device_type_id', $item->id)->where('quantity', '>' , 0)->get();
        }
        return $device_type;
    }

    public function getAttributeByTypeId($id)
    {
        $device_attributes = TDeviceTypeAttribute::where('device_type_id', $id)->get();
        return $device_attributes;
    }

    public function getListDeviceAttribute()
    {
        $devices = TDeviceType::all();
        foreach($devices as $item)
        {
            $item['attribute'] = TDeviceTypeAttribute::where('device_type_id', $item->id)->get();
        }
        return $devices;
    }

    public function saveFormAttributeService($request)
    {
        foreach($request->name as $key=>$name)
        {
            $device = new TDeviceType();
            $device->name = $name;
            $device->save();
            foreach($request->attribute[$key] as $key_attr=>$attribute_item)
            {
                $attribute = new TDeviceTypeAttribute();
                $attribute->device_type_id = $device->id;
                $attribute->name = $attribute_item;
                $attribute->field_name = $attribute_item;
                $attribute->is_required = $request->is_required[$key][$key_attr];
                $attribute->is_mutiple = $request->is_mutiple[$key][$key_attr];
                $attribute->note = '';
                $attribute->save();
            }
        }
        return $device;
    }

    public function getListImportDevice($request, $device)
    {
        $query =  DB::table('t_import_devices')
            ->select('t_import_devices.id as id', 't_import_devices.device_type_id as device_type_id',
                't_import_devices.name as device_import_name', 't_import_devices.sum_total as sum_total',
                't_device_types.name as device_name', 't_import_devices.quantity as quantity',
                't_import_devices.date_import as date_import', 't_import_devices.guarantee as guarantee',
                't_import_devices.quantity as quantity', 't_import_devices.code as code')
            ->join('t_device_types', 't_import_devices.device_type_id', 't_device_types.id');

        if($request->search) {
            $searchString = $request->search;
            $query->where('t_device_types.name', 'like', '%'.$searchString.'%')
                ->orWhere('t_import_devices.name', 'LIKE', '%'.$searchString.'%')
                ->orWhere('t_import_devices.code', 'LIKE', '%'.$searchString.'%');
        }
        if($device !== null){
            $query->where('t_import_devices.device_type_id', $device);
        }
        return $query->paginate(20);
    }

    public function getDetailDeviceImport($id)
    {
        $device = TImportDevice::find($id);
        return $device;
    }

    public function thanhLyThietBi($id, $request)
    {
        $device = TImportDevice::find($id);
        if($device->quantity >= (int)$request->quantity)
        {
            $device->quantity = $device->quantity - (int)$request->quantity;
            $device->sum_total = $device->sum_total - (int)$request->quantity;
            $device->save();
            return $device;
        }
        else{
            return false;
        }

    }

    public function removeImportDeviceService($id)
    {
        $device_import = TImportDevice::find($id);
        $devices = TDevice::where('import_device_id', $id)->get();
        foreach($devices as $device)
        {
            $user_device = TUserDevice::where('device_id', $device->id)->first();
            if($user_device ){
                $user_device->delete();
            }
            $device_remove = TDevice::find($device->id);
            $device_remove->delete();
        }
        $device_import->delete();
        return true;
    }

    public function saveAddAttrInSerevice($request, $id)
    {
        $device = TDeviceType::find($id);
        $device->name = $request->name_device;
        $device->save();
        if($request->name_attribute)
        {
            foreach($request->name_attribute as $key_attribute=>$value_attribute)
            {
                $attribute_edit = TDeviceTypeAttribute::find($request->id_attribute[$key_attribute]);
                $attribute_edit->device_type_id = $id;
                $attribute_edit->name = $value_attribute;
                $attribute_edit->field_name = $value_attribute;
                $attribute_edit->is_required = $request->is_required_edit[$key_attribute];
                $attribute_edit->is_mutiple = $request->is_mutiple_edit[$key_attribute];
                $attribute_edit->save();
            }
        }


        if($request->attribute)
        {
            foreach($request->attribute as $key=>$value)
            {
                $attribute_add =  new TDeviceTypeAttribute();
                $attribute_add->device_type_id = $id;
                $attribute_add->name = $value;
                $attribute_add->field_name = $value;
                $attribute_add->is_required = $request->is_required[$key];
                $attribute_add->is_mutiple = $request->is_mutiple[$key];
                $attribute_add->save();
            }
        }
        return true;
    }

    public function saveDeviceImportInService($request)
    {
        $dataInsertSpecs = $this->handleSpecificationDevice($request);
        $dataInsert = [];

        foreach ($request->device_type_id as $key => $deviceTypeId) {
            $dataInsert[] = [
                'specification' => json_encode($dataInsertSpecs[$key]['specification'] ?? []),
                'name' => $dataInsertSpecs[$key]['name'] ?? $request->code[$key],
                'device_type_id' => $deviceTypeId,
                'quantity' => $request->sum_total[$key],
                'sum_total' => $request->sum_total[$key] ?? 0,
                'code' => $request->code[$key],
                'guarantee' => $request->guarantee[$key],
                'date_import' => $request->date_import[$key],
            ];
        }
        TImportDevice::insert($dataInsert);

        return true;
    }

    private function handleSpecificationDevice($request)
    {
        $specifications = is_array($request->specification) ? $request->specification : [];
        $result = [];

        foreach ($specifications as $speciKey => $specification) {
            $arrList = $request->code[$speciKey] ?? '';

            foreach ($specification as $key => $value) {
                $arrayAttr = array_reduce(array_keys($value), function($attr, $key) use ($value) {
                    return $attr . ($key == 0 ? '' : ' - ') . $value[$key]['name'];
                });
                
                $arrList .= " | $key :  $arrayAttr";

                $options[$key] = $value;
            }

            if ($arrList == '') {
                $arrList = $request->code[$speciKey];
            }
            
            $result[$speciKey]['specification'] = $options;
            $result[$speciKey]['name'] = $arrList;
        }

        return $result;
    }

    public function editNameImportDeviceByService($request)
    {
        $device_import = TImportDevice::find($request->id);
        $device_import->name = $request->name;
        $device_import->save();
        $devices = TDevice::where('import_device_id', $request->id)->get();
        foreach($devices as $item)
        {
            $device_edit = TDevice::find($item->id);
            $device_edit->name = $request->name;
            $device_edit->save();
        }
        return true;
    }

    public function getListAlmostExpireDevice()
    {
        return DB::table('t_import_devices')
            ->select('t_import_devices.name as info_device', 't_import_devices.date_import as date_import', 't_import_devices.guarantee as guarantee', 'users.name as name', 't_user_devices.start_date as date_recieve')
            ->leftJoin('t_user_devices', 't_import_devices.id', 't_user_devices.device_import_id')
            ->leftJoin('users', 't_user_devices.user_id', 'users.id')
            ->orderBy('t_import_devices.date_import', 'Desc')
            ->get();
    }

    public function listUserUseDevice($importDeviceID)
    {
        $listUserDeviceID = TUserDevice::where('device_import_id', $importDeviceID)->pluck('user_id')->toArray();
        $listUser = DB::table('users')->whereNull('deleted_at')->whereIn('id', $listUserDeviceID)->get();
        $arrayUser = [];
        foreach ($listUser as $user) {
            $userDevice = TUserDevice::where('user_id', $user->id)->where('device_import_id', $importDeviceID)->first();
            $arrayUser [] = ['name' => $user->name, 'avatar' => $user->avatar, 'recieve_date' => $userDevice->start_date];
        }
        return $arrayUser;
    }

}
