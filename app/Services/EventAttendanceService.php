<?php
/**
 * EventAttendanceListService class
 * Author: jvb
 * Date: 2019/03/11 09:35
 */

namespace App\Services;

use App\Models\EventAttendance;
use App\Models\User;
use App\Services\Contracts\IEventAttendanceService;
use Illuminate\Support\Facades\DB;

class EventAttendanceService extends AbstractService implements IEventAttendanceService
{
    /**
     * PostService constructor.
     *
     * @param \App\Models\EventAttendanceList $model
     */
    public function __construct(EventAttendance $model)
    {
        $this->model = $model;
    }

    public function getListUserJoinEvent($id)
    {
        return EventAttendance::select('event_attendance.*', 'users.name', 'users.staff_code')
            ->join('users', 'event_attendance.user_id', '=', 'users.id')
            ->where('event_attendance.event_id', $id)
            ->orderBy('event_attendance.id', 'desc')
            ->get();
    }

    public function getListUserJoinEventAdmin($id)
    {
        $listUserIDJoinEvent = EventAttendance::where('event_id', $id)->get();
        $listRegister = [];
        $listUnregister = [];
        $infoUserAttendance = [];
        $countUnJoin = 0;
        $countJoin = 0;
        $countUnRegister = 0;
        foreach ($listUserIDJoinEvent as $eventAttendance) {
            $infoUserAttendance [$eventAttendance->user_id] = ['status' => $eventAttendance->name_status,
                'content' => $eventAttendance->content,
                'created_at' => $eventAttendance->created_at];
        }
        $infoAllUser = User::active()->get();
        foreach ($infoAllUser as $user) {
            $infoUser = [];
            $infoUser['name'] = $user->name;
            $infoUser['team_name'] = !empty($user->team()) ? $user->team()->name : null;
            $infoUser['staff_code'] = $user->staff_code;
            $infoUser['position'] = $user->position;
            $idUser = $user->id;
            if (array_key_exists($idUser, $infoUserAttendance) ) {
                if ($infoUserAttendance[$idUser]['status'] == STATUS_JOIN_EVENT[0]) {
                    $countUnJoin++;
                } else {
                    $countJoin++;
                }
                $infoUser['status'] = $infoUserAttendance[$idUser]['status'];
                $infoUser['content'] = $infoUserAttendance[$idUser]['content'];
                $infoUser['created_at'] = $infoUserAttendance[$idUser]['created_at'];
                $listRegister[] = $infoUser;
            } else {
                $countUnRegister++;
                $infoUser['status'] = 'Chưa đăng kí';
                $infoUser['content'] = null;
                $infoUser['created_at'] = null;
                $listUnregister[] = $infoUser;
            }
        }
        return [
            'user_un_register' => $countUnRegister,
            'user_join' => $countJoin,
            'user_un_join' => $countUnJoin,
            'total' => $countUnRegister + $countJoin + $countUnJoin,
            'list_user' => array_merge($listRegister, $listUnregister)
        ];
    }
}
