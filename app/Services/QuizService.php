<?php 
/**
* QuizService class
* Author: Hoai
* 
*/

namespace App\Services;

use App\Models\Quiz;
use App\Models\Team;
use App\Models\User;
use App\Services\Contracts\IQuizService;
use App\Services\Contracts\IUserService;
use App\Repositories\Contracts\IQuizRepository;

class QuizService extends AbstractService implements IQuizService
{
    /**
     * QuizService constructor.
     *
     * @param \App\Models\Quiz                            $model
     * @param \App\Repositories\Contracts\IQuizRepository $repository
     */
    public function __construct(Quiz $model, IQuizRepository $repository, IUserService $userService)
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->userService = $userService;
    }
        
    /**
     * Get list quiz
     *
     * @return void
     */
    public function getListQuiz()
    {
        $quizzes = $this->repository->getListQuiz();
        foreach ($quizzes as $quiz){
            $participants = explode(',', $quiz->participants);
            $quiz->participants = count($this->userService->getParticipantIds($participants));
        }

        return $quizzes;
    }
    
    /**
     * Get list users
     *
     * @param  int $quizId
     * @return array
     */
    public function getListUsers($quizId)
    {
        $listUsers = $this->repository->getUser($quizId);
        $answerUsers = [];
        foreach ($listUsers as $value) {
            $participants = $value->participants;
            $answerUsers[] = [
                'user_id' => $value->user_id,
                'staff_code' => $value->staff_code,
                'name' => $value->name,
                'submited_at' => $value->submited_at,
            ]; 
        }
        $participants = $this->userService->getParticipantIds(explode(',', $participants));
        foreach ($answerUsers as $value) {
            $participants = array_diff($participants, [$value['user_id']]);
        }
        $users = User::select(
            'name',
            'staff_code',
            'id as user_id'
        )
        ->whereIn('id', $participants)
        ->orderBy('staff_code')->get()->toArray();
        $users = array_merge($users, $answerUsers);
        return $users;
    }
    
    /**
     * Count correct answer
     *
     * @param  int $quizId
     * @return AmountCorrectAnswers
     */
    public function countCorrectAnswer($quizId)
    {
        $answerUser = $this->repository->countCorrectAnswer($quizId);
        $amountCorrectAnswers=[];
        foreach($answerUser as $value){
            $amountCorrectAnswers[$value->user_id][] = $value->is_correct;
        }
        foreach($amountCorrectAnswers as $user_id => $value){
            $accepted = array_filter($value, function($item) {
                return $item == 1;
            });
            $amountCorrectAnswers[$user_id] = count($accepted);
        }
        return $amountCorrectAnswers;
    }
    
    /**
     * Get participants
     *
     * @param  array $participant
     * @return string
     */
    public function getParticipants($participant){
        $participantIds = explode(',',$participant);
        $userIds = [];
        foreach ($participantIds as $participantId){
            if (starts_with($participantId, 'J-')) {
                $jobTitle = str_replace('J-', '', $participantId);
                $selectUsers = JOB_TITLES_MEETING[$jobTitle];
                array_push($userIds, $selectUsers);

            } elseif (starts_with($participantId, 'P-')) {
                $position = str_replace('P-', '', $participantId);
                $selectUsers = POSITIONS_MEETING[$position];
                array_push($userIds, $selectUsers);

            } elseif (starts_with($participantId, 'T-')) {
                $teamId = str_replace('T-', '', $participantId);
                $selectUsers = Team::select('name')->where('id', $teamId)->pluck('name')->toArray();
                $userIds = array_merge($userIds, $selectUsers);

            } elseif (starts_with($participantId, 'ALL')) {
                array_push($userIds, 'Tất cả');
            }       
        }
        return implode(', ', $userIds);
    }
}