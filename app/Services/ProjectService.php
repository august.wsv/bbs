<?php

/**
 * ProjectService class
 * Author: jvb
 * Date: 2019/01/31 05:00
 */

namespace App\Services;

use App\Http\Requests\Api\ProjectTaskProgressRequest;
use App\Http\Requests\Api\TaskLogTimeRequest;
use App\Models\Project;
use App\Models\ProjectTask;
use App\Models\TaskLogWork;
use App\Models\User;
use App\Repositories\Contracts\IDayOffRepository;
use App\Repositories\Contracts\IProjectRepository;
use App\Services\Contracts\IProjectService;
use App\Services\Contracts\IWorkTimeExplanationService;
use App\Services\Contracts\IUserService;
use Carbon\Carbon;
use ErrorException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProjectService extends AbstractService implements IProjectService
{
    public $repository;
    private $userService;

    public function __construct(
        Project $model,
        IProjectRepository $repository,
        IWorkTimeExplanationService $workTimeExplanationService,
        IDayOffRepository $dayOffRepository,
        IUserService $userService
    ) {
        $this->model = $model;
        $this->repository = $repository;
        $this->workTimeExplanationService = $workTimeExplanationService;
        $this->dayOffRepository = $dayOffRepository;
        $this->userService = $userService;
    }

    public function detail($id)
    {
        return $this->repository->findOneBy([
            'id' => $id,
        ]);
    }

    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search)
    {
        $criterias = $request->only('page', 'page_size', 'search');
        $perPage = $criterias['page_size'] ?? DEFAULT_PAGE_SIZE;
        $search = $criterias['search'] ?? '';

        $staffCodes = explode(',', env('DEVELOP_MANAGER_STAFF_CODE'));
        $user = Auth::user();
        $userId = $user->id;
        if ($user->isMaster() || in_array($user->staff_code, $staffCodes)) {
            $request->merge(['userIds' => null]);
        } elseif ($user->isManager()) {
            $groupUsers = $this->userService->allMembersOfManager($userId);
            $request->merge(['userIds' => $groupUsers->pluck('id')->toArray()]);
        } else {
            $request->merge(['userIds' => [$userId]]);
        }

        return $this->repository->findBy($criterias, [
            'id',
            'name',
            'customer',
            'project_type',
            'leader_id',
            'start_date',
            'end_date',
            'status',
            'image_url'
        ], false, $request);
    }

    /**
     * @param User $user
     *
     * @return collection
     */
    public function joinedList(User $user, $status = null)
    {
        $model = $this->model->select(
            'id',
            'name',
            'customer',
            'project_type',
            'leader_id',
            'start_date',
            'end_date',
            'status',
            'image_url',
            'leader_id'
        )->inProgress()->with('currentMembers')->withCount(['projectTasks as total', 'projectTasks as total_closed' => function ($query) {
            return $query->where('status', TASK_STATUS['closed']);
        }])->when($status, function ($query) use ($status) {
            return $query->where('status', $status);
        });
        $projects = $this->filterOfUser($user, $model)->get();

        // Thêm dữ liệu thể hiện việc xin phép, ngày nghỉ của các dự án
        $projectMemberIds = array_unique($projects->reduce(function ($memberIds, $item) {
            return array_merge($memberIds, $this->getProjectMemberIds($item));
        }, []));

        // Lấy ra toàn bộ ngày nghỉ phép, xin phép của các thành viên trong dự án
        $userInfo = $this->getUserInfos($projectMemberIds);
        $userOffs = $userInfo['off'] ?? [];
        $userAsks = $userInfo['asking'] ?? [];

        foreach ($projects as &$project) {
            $memberIds = $project->currentMembers->pluck('user_id')->toArray();
            $project->userInfo = [
                'off' => $userOffs->whereIn('user_id', $memberIds),
                'asking' => $userAsks->whereIn('user_id', $memberIds),
            ];
            $userTodayOffs = $project->userInfo['off']->toArray() ?? [];
            $currentMembers = $project->currentMembers->toArray() ?? [];
            if (!empty($userTodayOffs) && !empty($currentMembers)) {
                $userTodayOffsIds = array_column($userTodayOffs, "user_id");
                $currentMembersIds = array_column($currentMembers, "user_id");
                $commonUserIds = array_intersect($userTodayOffsIds, $currentMembersIds);
                foreach ($currentMembers as $key => $currentMemberItem) {
                    if (in_array($currentMemberItem["user_id"], $commonUserIds)) {
                        $userOffById = $userOffs->where('user_id', $currentMemberItem["user_id"]);
                        if (!empty($userOffById)) {
                            unset($currentMembers[$key]);
                        }
                    }
                }
                $currentMembers = array_values($currentMembers);
                unset($project->currentMembers);
                $project->current_members = $currentMembers;
            }
        }
        return $projects;
    }

    public function overview($dataRequest, User $user, $id)
    {
        $model = $this->model->whereId($id);

        $project = $this->filterOfUser($user, $model)
            ->with('currentMembers.user:id,name,email,avatar', 'projectSprints')
            ->first();

        if (!$project) return null;

        $projectMemberIds = array_unique($this->getProjectMemberIds($project));
        $userInfo = $this->getUserInfos($projectMemberIds);

        $project->userInfo = $userInfo;
        if (isset($dataRequest['is_all']) && $dataRequest['is_all']) {
            $project->tasks = [
                'back_log' => $this->setProgress($dataRequest, $project->id, $user, TASK_BACK_LOG),
                'to_do' => $this->setProgress($dataRequest, $project->id, $user, TASK_TODO),
                'doing' => $this->setProgress($dataRequest, $project->id, $user, TASK_DOING),
                'preview' => $this->setProgress($dataRequest, $project->id, $user, TASK_PREVIEW),
                'done' => $this->setProgress($dataRequest, $project->id, $user, TASK_DONE),
            ];
        } else {
            $project->tasks = $this->getTasks($dataRequest, $project->id, $user)->get();
        }

        return $project;
    }

    private function setProgress($dataRequest, $projectId, User $user, $progress)
    {
        $dataRequest['progress'] = $progress;
        return $this->getTaskProgress($dataRequest, $projectId, $user);
    }

    private function getProjectMemberIds($project)
    {
        return $project->currentMembers->pluck('user_id')->toArray() + [$project->leader_id];
    }

    private function getTasks($dataRequest, $projectId, User $user)
    {
        $taskModel = ProjectTask::where('project_id', $projectId)->with('sprint:id,name');
        $isAll = isset($dataRequest['is_all']) && $dataRequest['is_all'];
        if (!$isAll) {
            $taskModel = $taskModel->when(($user->jobtitle_id == STAFF_ROLE), function ($query) use ($user) {
                return $query->where('user_id', $user->id);
            });
        }

        if (isset($dataRequest['sprint_id'])) {
            $taskModel = $taskModel->whereSprintId($dataRequest['sprint_id']);
        }

        return $taskModel;
    }

    /**
     * @param $dataRequest
     * @param $id
     * @param User $user
     * @return mixed
     */
    public function getTaskProgress($dataRequest, $id, User $user)
    {
        $dataRequest['is_all'] = true;
        $task = $this->getTasks($dataRequest, $id, $user);
        if (isset($dataRequest['progress'])) {
            $task->whereBetween('progress', TASK_PROCESS[$dataRequest['progress']]);
        }
        return $task->paginate($dataRequest['page_size'] ?? DEFAULT_PAGE_SIZE);
    }

    public function createTask(User $user, $project, $data)
    {
        return $project->projectTasks()->create(array_merge($data, [
            'creator_id' => $user->id,
            'updator_id' => $user->id,
            'task_code' => $project->projectTasks->max('task_code') + 1,
            'user_id' => $data['user_id'] ?? $user->id,
        ]))->refresh()->loadMissing('sprint:id,name');
    }

    public function updateTask(User $user, $project, $taskId, $data)
    {
        $task = ProjectTask::find($taskId);

        if (isset($data['task_code'])) {
            unset($data['task_code']);
        }

        $task->fill($data);
        $task->updator_id = $user->id;
        $task->save();

        return ProjectTask::with('sprint:id,name')->find($task->id);
    }

    public function deleteTask(User $user, $projectId, $taskId)
    {
        $project = $this->detail($projectId);

        if (!$project || !$user->can('edit_task', $project)) throw new ErrorException(Response::$statusTexts[Response::HTTP_FORBIDDEN], Response::HTTP_FORBIDDEN);
        $task = ProjectTask::find($taskId);

        if (!$task) throw new ErrorException(Response::$statusTexts[Response::HTTP_NOT_FOUND], Response::HTTP_NOT_FOUND);

        $task->delete();

        return $task;
    }

    private function getUserInfos($userIds = [])
    {
        if (empty($userIds)) return [];

        $dayOffs = $this->dayOffRepository->getTodayInfo($userIds);
        $workTimes = $this->workTimeExplanationService->getTodayInfo($userIds);

        return ['off' => $dayOffs, 'asking' => $workTimes];
    }

    private function filterOfUser(User $user, $model)
    {
        if ($user->isManager()) return $model;

        return $model->ofUser($user->id);
    }

    /**
     * @param TaskLogTimeRequest $request
     * @param $taskId
     * @return mixed
     */
    public function logWork(TaskLogTimeRequest $request, $taskId)
    {
        $request['task_id'] = $taskId;
        $request['time'] = change_log_time($request['time_text']);
        $request['creator_id'] = Auth::id();
        $request['time_start'] = $request['time_start'] ? Carbon::parse($request['time_start'])->format(DATE_TIME_FORMAT_SHORT) : Carbon::now()->format(DATE_TIME_FORMAT_SHORT);
        try {
            DB::beginTransaction();
            $dataCreated = TaskLogWork::create(Arr::only($request->all(), ['time', 'time_text', 'task_id', 'creator_id', 'description', 'time_start']));
            DB::commit();
            return $dataCreated;
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function taskDetail($id)
    {
        return ProjectTask::with('logWork')->find($id);
    }

    public function  overviewTaskLogWork($request)
    {
        $time = $request->time;
        $taskLogworkStatus = $request->status;
        $pageSize = $request->page_size;
        $search = $request->search;
        $projectId = $request->project_id;
        
        $endDate = Carbon::now()->format(DATE_TIME_FORMAT);
        $startDate = Carbon::parse($endDate)->subDays($time)->startOfDay()->format(DATE_TIME_FORMAT);

        $taskLogWorks = TaskLogWork::with(['projectTask' => function ($query) {
            $query->with('user', 'project');
        }]);
        if (isset($search)) {
            $taskLogWorks->whereHas('projectTask.user', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            });
        }
        if (isset($projectId)) {
            $taskLogWorks->whereHas('projectTask.project', function ($query) use ($projectId) {
                $query->where('id', $projectId);
            });
        }
        if (isset($taskLogworkStatus)) {
            $taskLogWorks->where('task_logwork.status', $taskLogworkStatus);
        }
        if (isset($time)) {
            $taskLogWorks->whereBetween('task_logwork.time_start', [$startDate, $endDate]);
        }
        $dashboard = [
            'count_log_work' => $taskLogWorks->count(),
            'total_log_work' => custom_total_log_work($taskLogWorks->sum('time'))
        ];
        $taskLogWorksGet = $taskLogWorks->get();
        $logWorkStatus = [
            'await' => $taskLogWorksGet->where('status', TASK_LOGWORK_STATUS['await'])->count(),
            'approved' => $taskLogWorksGet->where('status', TASK_LOGWORK_STATUS['approved'])->count(),
            'rejected' => $taskLogWorksGet->where('status', TASK_LOGWORK_STATUS['rejected'])->count()
        ];

        return [
            'dashboard' => $dashboard,
            'log_work_status' => $logWorkStatus,
            'project_total_log' => $this->projectTotalLog($taskLogWorks),
            'records' => $this->taskLogWorks($taskLogWorks, $pageSize)
        ];
    }

    private function projectTotalLog($taskLogWorks) {
        $data = $taskLogWorks->get()->map(function ($data) {
            return [
                'project_name' => $data->projectTask->project->name ?? null,
                'total_log' => $data->time
            ];
        });
        $result = [];
        foreach ($data as $item) {
            $name = $item['project_name'];
            $totallog = $item['total_log'];
            if (!isset($result[$name])) {
                $result[$name] = $totallog;
            } else {
                $result[$name] += $totallog;
            }
        }
        return array_map(function ($name, $totalLogWork) {
            return ['name' => $name, 'total_log_work' => custom_total_log_work($totalLogWork)];
        }, array_keys($result), $result);
    }

    private function taskLogWorks($taskLogWorks, $pageSize)
    {
        $users = User::get();
        $taskLogWorksPaginate = $taskLogWorks->paginate($pageSize ?? DEFAULT_PAGE_SIZE);
        $data = $taskLogWorksPaginate->map(function ($data) use ($users) {
            return [
                'id' => $data->id,
                'user_name' => $data->projectTask->user->name ?? null,
                'project_name' => $data->projectTask->project->name ?? null,
                'task_code' => $data->projectTask->task_code ?? null,
                'job_description' => $data->description,
                'start_date' => Carbon::parse($data->time_start)->format(DATE_FORMAT),
                'total_log' => custom_total_log_work($data->time),
                'status' => $data->status,
                'approver_name' => $users->find($data->approver_id)->name ?? null,
                'approve_at' => $data->approve_at ?? null,
            ];
        });
        $paginator = $taskLogWorksPaginate->appends(request()->query());
        $meta = [
            'panigation' => [
                'total' => $paginator->total(),
                'per_page'=> $paginator->perPage(),
                'current_page'=> $paginator->currentPage(),
                'total_pages'=> ceil($paginator->total()/$paginator->perPage()),
                'links'=> [
                    'next'=> $paginator->nextPageUrl()
                ]
            ]
        ];
        return [
            'meta' => $meta,
            'data' => $data
        ];
    }
}
