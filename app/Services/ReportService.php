<?php

/**
 * ReportService class
 * Author: jvb
 * Date: 2019/01/21 03:42
 */

namespace App\Services;

use App\Events\ReportCreatedNoticeEvent;
use App\Http\Requests\Api\ReportStatisticsRequest;
use App\Models\Config;
use App\Models\Group;
use App\Models\Report;
use App\Models\ReportReceiver;
use App\Models\ReportReply;
use App\Models\Team;
use App\Models\User;
use App\Models\UserTeam;
use App\Notifications\SentReport;
use App\Repositories\Contracts\IReportRepository;
use App\Repositories\Contracts\IUserRepository;
use App\Services\Contracts\IReportService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ReportService extends AbstractService implements IReportService
{
    protected $userRepository;
    /**
     * ReportService constructor.
     *
     * @param \App\Models\Report                            $model
     * @param \App\Repositories\Contracts\IReportRepository $repository
     * @param \App\Repositories\Contracts\IUserRepository $userRepository
     */
    public function __construct(
        Report $model,
        IReportRepository $repository,
        IUserRepository $userRepository
    )
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @param integer $perPage
     * @param string  $search
     * @param int     $teamId
     *
     * @return collection
     */
    public function search(Request $request, &$perPage, &$search, &$teamId)
    {
        if (!$request->has('year'))
            $request->merge(['year' => date('Y')]);
        if (!$request->has('month')) {
            // if it's first dates of month: show report of last month
            $request->merge(['month' => date('n') - (date('j') > date('j', strtotime("first friday of this month")) ? 0 : 1)]);
        }
        if (!$request->has('team_id')) {
            $user = Auth::user();
            $team = $user->team();
            if ($team) {
                $teamId = $team->id;

                if (!$request->has('type')) {
                    $request->merge(['type' => 2]);
                    $request->merge(['team_id' => $teamId]);
                }

                if ($request->type == 2 && !$request->has('team_id')) {
                    $request->merge(['team_id' => $teamId]);
                }
            } else {
                $teamId = 0;
            }
        } else {
            $teamId = $request->get('team_id');
        }

        $criterias = $request->only('page', 'page_size', 'search', 'type', 'date_from', 'date_to', 'year', 'month', 'team_id');
        $currentUser = Auth::user();

        $perPage = $criterias['page_size'] ?? REPORT_PAGE_SIZE;
        $search = $criterias['search'] ?? '';
        $model = $this->model
            ->select([
                'reports.id',
                'reports.user_id',
                'users.avatar',
                'reports.week_num',
                'reports.to_ids',
                'reports.title',
                'reports.status',
                'reports.content',
                'reports.report_type',
                'reports.report_date',
                'reports.color_tag',
                'reports.created_at',
                'reports.updated_at',
            ])
            ->leftJoin('users', 'users.id', 'reports.user_id')
            ->where(function ($q) use ($currentUser) {
                $q->where('reports.status', ACTIVE_STATUS)
                    ->orWhere(function ($p) use ($currentUser) {
                        $p->where('reports.status', REPORT_DRAFT)->where('reports.user_id', $currentUser->id);
                    });
            })
            ->search($search)
            ->with(['receivers' => function ($q) {
                $q->select('users.id', 'users.name', 'avatar')->orderBy('jobtitle_id', 'desc');
            }, 'reportReplies' => function ($q) {
                $q->orderBy('report_reply.id', 'desc');
            }])
            ->withCount('reportReplies')
            ->orderBy('reports.id', 'desc');

        if(!empty($request->input('is_api'))){
            $model = $model->addSelect([
                'users.name', 
                DB::raw('SUBSTRING(reports.title, 1, INSTR(reports.title, ":") - 1) as title')
            ]);
        }
        if (isset($criterias['date_from'])) {
            $model->where('reports.created_at', '>=', $criterias['date_from']);
        }
        if (isset($criterias['date_to'])) {
            $model->where('reports.created_at', '<=', $criterias['date_to']);
        }
        if (!empty($criterias['year'])) {
            $model->where('reports.year', $criterias['year']);
        }
        if (!empty($criterias['month'])) {
            $model->where('reports.month', $criterias['month']);
        }
        $model->where(function ($modelInner) use ($request, $criterias, $currentUser) {
            $type = $request->get('type');
            if (isset($type)) {
                if ($type == REPORT_SEARCH_TYPE['private']) {
                    $modelInner->where('reports.user_id', Auth::id());
                } elseif ($type == REPORT_SEARCH_TYPE['team']) {
                    if (isset($criterias['team_id'])) {
                        $modelInner->where('reports.team_id', $criterias['team_id']);
                    }
                }
            }
            if ($type != REPORT_SEARCH_TYPE['private']) {
                if ($currentUser->isMaster()) {
                } else if ($currentUser->isGroupManager()) {
                    $groupManage = Group::where('manager_id', $currentUser->id)->first();
                    if ($groupManage) {
                        $groupId = $groupManage->id;

                        $modelInner->where(function ($q) use ($groupId) {
                            $q->where('reports.is_private', REPORT_PUBLISH)->orWhere('reports.group_id', $groupId);
                        });
                    }
                } else {
                    $team = $currentUser->team();
                    if ($team) {
                        $modelInner->where(function ($q) use ($type, $team, $currentUser) {
                            $q->where('reports.is_private', REPORT_PUBLISH)
                                ->orWhere(function ($p) use ($team) {
                                    $p->where('reports.is_private', REPORT_PRIVATE)
                                        ->where('reports.team_id', $team->id);
                                });

                            if ($type == REPORT_SEARCH_TYPE['all']) {
                                $q->orWhere(function ($p) use ($currentUser) {
                                    $p->where('reports.user_id', $currentUser->id);
                                });
                            }
                        });
                    } else {
                        $modelInner->where('reports.is_private', REPORT_PUBLISH);
                    }
                }
            }
            if (!$currentUser->isMaster()) {
                if (!($type == REPORT_SEARCH_TYPE['team'] && isset($criterias['team_id']))) {
                    $modelInner->orWhereHas('reportReceivers', function ($q) use ($currentUser) {
                        $q->where('user_id', $currentUser->id);
                    });
                }
            }
        });
        return $model->paginate($perPage);
    }

    public function create(Request $request)
    {
        $data = $request->only('status', 'choose_week', 'to_ids', 'content', 'is_new', 'is_private');
        $listID = $data['to_ids'];
        if (count($listID) >= 30) {
            $listID = [];
            $teamID = UserTeam::where('user_id', Auth::id())->first()->team_id;
            $team = Team::find($teamID);
            $listID[] =  $team->leader_id;
            $managerID = Group::find($team->group_id)->manager_id;
            if (!in_array($managerID, $listID)) {
                $listID[] = Group::find($team->group_id)->manager_id;
            }
        }
        $choose_week = $data['choose_week'];
        $reportType = REPORT_TYPE_WEEKLY;
        $reportDate = null;
        if ($choose_week == 0 || $choose_week == 1) {
            get_week_info($choose_week, $week_number);
        } else {
            $reportDate = date_create_from_format('d/m', $data['choose_week']);

            $choose_week = 0;
            $reportType = REPORT_TYPE_DAILY;
            $week_number = get_week_number($choose_week);
        }

        $data['title'] = $this->getReportTitle($data['choose_week']);

        $data['week_num'] = $week_number;
        $data['year'] = date('Y');
        $data['user_id'] = Auth::id();
        $data['month'] = getMonthFormWeek($week_number);
        $data['report_date'] = $reportDate;

        $report = $this->getDraftReport();
        DB::beginTransaction();
        if ($report) {
            $report->fill($data);
            $report->save();
        } else {
            $report = Report::where([
                'user_id' => Auth::id(),
                'year' => date('Y'),
                'month' => $data['month'],
                'week_num' => $week_number,
                'report_type' => $reportType,
                'report_date' => $reportDate,
            ])->first();

            if (!$report) {
                $user = Auth::user();
                $team = $user->team();
                if ($team)
                    $data['color_tag'] = $team->color;

                $data['report_type'] = $reportType;
                $report = new Report($data);
                $report->save();
                $user->notify(new SentReport($report));
            } else {
                $report->fill($data);
                $report->save();
            }
        }

        //make receivers
        $receivers = User::whereIn('id', $listID)->get();
        ReportReceiver::where('report_id', $report->id)->delete();
        foreach ($receivers as $receiver) {
            $dataReceivers[] = [
                'report_id' => $report->id,
                'user_id' => $receiver->id,
            ];
            broadcast(new ReportCreatedNoticeEvent($report, $receiver))->toOthers();
        }
        ReportReceiver::insertAll($dataReceivers);

        DB::commit();

        return $report;
    }

    /**
     * @return array
     */
    public function getReportReceiver()
    {
        $user = Auth::user();
        $masters = [
            'Ban giám đốc' => $this->getUserModel(['jobtitle_id' => MASTER_ROLE])->toArray()
        ];
        $managerUsers = $this->getUserModel(['jobtitle_id' => MANAGER_ROLE], false);
        $managers = [
            'Manager' => $managerUsers->get()->toArray()
        ];

        if ($user->jobtitle_id >= MANAGER_ROLE) {
            return $masters;
        } else if ($user->jobtitle_id == TEAMLEADER_ROLE) {
            return $managers + $masters;
        } else {
            $team = $user->team();
            if ($team) {
                //team leader
                $userIds = [
                    $team->leader_id,
                    $team->group->manager_id ?? 0,
                ];
                $directs = [
                    'Quản lý trực tiếp' => $this->getUserModel([], false)->whereIn('id', $userIds)->get()->toArray()
                ];
                //manager
                $otherManagers = $managerUsers->whereNotIn('id', $userIds)->get();
                $managers = [
                    'Manager' => $otherManagers->toArray()
                ];
                //other
                $others = $this->getUserModel([], false)->where('jobtitle_id', '!=', MASTER_ROLE)->whereNotIn('id', $userIds + $otherManagers->pluck('id')->toArray())->orderBy('name')->get()->toArray();
                $users = [
                    'Khác' => $others
                ];
                return $directs + $managers + $masters + $users;
            } else {
                // manager + team leader
                $teamLeadUsers = $this->getUserModel(['jobtitle_id' => TEAMLEADER_ROLE]);
                $teamLeads = [
                    'Team Leader' => $teamLeadUsers->toArray()
                ];
                //other
                $others = $this->getUserModel([], false)->where('jobtitle_id', '=', STAFF_ROLE)->whereNotIn('id', [$user->id])->orderBy('name')->get()->toArray();
                $users = [
                    'Khác' => $others
                ];
                return $teamLeads + $managers + $masters + $users;
            }
        }
    }

    private function getUserModel($conditions = [], $isGet = true)
    {
        $model = User::select('id', 'staff_code', 'name', 'avatar')
            ->where('id', '!=', Auth::id())
            ->where('contract_type', STAFF_CONTRACT_TYPES)->where('status', ACTIVE_STATUS)->where($conditions);

        if ($isGet) {
            return $model->get();
        } else {
            return $model;
        }
    }

    /**
     * @param int $id
     *
     * @return Report
     */
    public function detail($id)
    {
        $currentUserId = Auth::id();
        $report = $this->model->where('reports.id', $id)
            ->where(function ($query) use ($currentUserId) {
                $query->where('status', ACTIVE_STATUS)
                    ->orWhere(function ($p) use ($currentUserId) {
                        $p->where('status', REPORT_DRAFT)->where('user_id', $currentUserId);
                    });
            })
            ->with('user:id,name,email,avatar')
            ->with('receivers:users.id,name,email,avatar')
            ->with('reportReplies.user:users.id,name,avatar')->first();
        return $report;
    }

    /**
     * detailApi
     *
     * @param int $id
     *
     * @return Report
     */
    public function detailApi($id)
    {
        $report = $this->detail($id);
        if (empty($report)) {
            return null;
        }
        $report->title = Str::before($report->title, ':');
        return $report;
    }

    /**
     * @return Report
     */
    public function newReportFromTemplate()
    {
        $report = new Report();
        $report->is_new = true;

        //Fill data from template
        $config = Config::firstOrNew(['id' => 1]);

        $report->title = $this->generateTitle($config->weekly_report_title);

        $report->content = $config->html_weekly_report_template;

        return $report;
    }

    /**
     * @param int $type : -1: daily report; 0:
     *
     * @return mixed
     */
    public function getReportTitle($type)
    {
        if ($type == 0 || $type == 1) {
            $config = Config::firstOrNew(['id' => 1]);

            $template = $config->weekly_report_title;

            return $this->generateTitle($template, $type);
        } else {
            return "Báo cáo ngày [" . $type . "]: " . Auth::user()->name;
        }
    }

    /**
     * @return Report
     */
    public function getFromDraftOrTemplate()
    {
        $report = $this->getDraftReport();

        if (!$report)
            $report = $this->newReportFromTemplate();

        return $report;
    }

    /**
     * @return Report
     */
    public function getDraftReport()
    {
        return Report::where([
            'user_id' => Auth::id(),
            'status' => REPORT_DRAFT
        ])->orderBy('id', 'desc')->first();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function replyReport($reportId, Request $request)
    {
        $data = $request->only('content', 'report_id');
        $data['user_id'] = Auth::id();
        $data['report_id'] = $reportId;

        $reportReply = new ReportReply($data);
        $reportReply->save();
        /** @var NotificationService $notificationService */
        $notificationService = app()->make(NotificationService::class);
        $notificationService->sentReportNotification($reportId, Auth::user(), $request->get('content'));

        return isset($reportReply);
    }

    /**
     * @param $template
     *
     * @return mixed
     */
    private function generateTitle($template, $type = 0)
    {
        [$firstDay, $lastDay] = get_first_last_day_in_week($type, $day);

        $result_week = 'Báo cáo tuần ' . get_week_info($type, $week_number);
        //1. ${staff_name}
        $result = str_replace('${staff_name}', Auth::user()->name, $template);

        //2. ${week_number}
        $result = str_replace('${week_number}', $week_number, $result);

        //3. ${d}
        $result = str_replace('${d}', date('d'), $result);

        //4. ${m}
        $result = str_replace('${m}', date('m'), $result);

        //5. ${Y}
        $result = str_replace('${Y}', date('Y'), $result);

        //6. ${first_day}
        $result = str_replace('${first_day}', $firstDay, $result);

        //7. ${last_day}
        $result = str_replace('${last_day}', $lastDay, $result);

        return $result;
    }

    /**
     * @param ReportStatisticsRequest $request
     * @return array
     */
    public function statisticsReport(ReportStatisticsRequest $request){
        $currentTime = Carbon::now();
        $year = $request->year ?? $currentTime->year;
        $month = $request->month ?? $currentTime->month;
        $groupId = $request->group_id ?? null;
        $teamId = $request->team_id ?? null;
        $userIds = $this->userRepository->getUserIdReport($groupId, $teamId);
        $report = $this->model->where('year', $year)
            ->where('month', $month)
            ->whereIn('user_id', array_unique($userIds))
            ->where('status', ACTIVE_STATUS)->get();
        $data = ['user_report' => count(array_unique($userIds)),
                'statistics' => []];
        [$firstDate, $lastDate] = getStartAndEndDateOfMonth($month, $year);
        //nhóm ngày theo theo các tuần của tháng
        $weeks =  get_week_date_list($firstDate, $lastDate);
        foreach ($weeks as $weekNum => $days) {
            $totalWeek = $report->where('week_num', $weekNum)->where('report_type', DAILY_REPORT)->count();
            $totalLate = $report->where('week_num', $weekNum)->where('report_type', DAILY_REPORT)
                ->where('created_at','>', array_pop($days))->count();
            $reportDay = [];
            foreach ($days as $day) {
                if (Carbon::parse($day)->month != $month || !check_day_of_week($day)) continue;
                $reportDay[] = [
                    'day' => $day,
                    'total_report_day' => $report->where('week_num', $weekNum)->where('report_type', WEEKLY_REPORT)->where('report_date', $day)->count(),
                    'total_report_day_late' => $report->where('week_num', $weekNum)->where('report_type', WEEKLY_REPORT)->where('report_date', $day)->reject(function ($value){
                        return strtotime($value->report_date) >= strtotime(Carbon::parse($value->created_at)->format(DATE_FORMAT));
                    })->count()
                ];
            }
            if (!$reportDay) continue;
            $data['statistics'][] = [
                'week_num' => $weekNum,
                'total_report' => $totalWeek,
                'total_report_day_late' => $totalLate,
                'days' => $reportDay
            ];
        }
        return $data;
    }
}
