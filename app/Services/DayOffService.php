<?php
/**
 * DayOffService class
 * Author: jvb
 * Date: 2019/01/22 10:50
 */

namespace App\Services;

use App\Events\DayOffNoticeEvent;
use App\Models\AdditionalDate;
use App\Models\CalendarOff;
use App\Models\DayOff;
use App\Models\RemainDayoff;
use App\Models\Team;
use App\Models\User;
use App\Models\UserTeam;
use App\Repositories\Contracts\IDayOffRepository;
use App\Services\Contracts\IDayOffService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DayOffService extends AbstractService implements IDayOffService
{
    public $repository;
    /**
     * DayOffService constructor.
     *
     * @param \App\Models\DayOff                            $model
     * @param \App\Repositories\Contracts\IDayOffRepository $repository
     */
    public function __construct(DayOff $model, IDayOffRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    /**
     * Query a list of day off
     *
     * @param Request $request
     * @param array   $moreConditions
     * @param array   $fields
     * @param string  $search
     * @param int     $perPage
     *
     * @return mixed
     */
    public function findList(Request $request, $moreConditions = [], $fields = ['*'], &$search = '', &$perPage = DEFAULT_PAGE_SIZE)
    {
        $criterias = $request->only('page', 'per_page', 'search', 'year', 'month', 'approve');
        $perPage = $criterias['per_page'] ?? DEFAULT_PAGE_SIZE;
        $search = $criterias['search'] ?? '';

        $isApprove = $criterias['approve'] ?? null;
        if ($isApprove == DayOff::APPROVED_STATUS || $isApprove == DayOff::NOTAPPROVED_STATUS) {
            $moreConditions['day_offs.status'] = $isApprove;
        }

        $model = $this->model
            ->select($fields)
            ->with('approval')
            ->where($moreConditions)
            ->search($search)
            ->orderBy('id');

        $year = $criterias['year'] ?? null;

        if ($year != null) {
            $model->whereYear('start_at', $year);
        }

        $month = $criterias['month'] ?? null;
        if ($month != null) {
            $model->whereMonth('start_at', $month);
        }

        return $model->paginate($perPage);
    }

    public function listApprovals($minJobTitle = 1)
    {
        $containerRecord = new User();

        $listApproval = $containerRecord->approverUsers($minJobTitle);
        return $listApproval->get();
    }

    /**
     * @param $userId
     *
     * @return array
     */
    public function getDayOffUser($request, $userId, $check = true, $isApi = false)
    {
        $model = $this->model->where('user_id', $userId);
        $remainDay = RemainDayoff::firstOrCreate(['user_id' => $userId ],['year'=>date('Y')]);
        $thisYear = (int)date('Y');
        $datas = $model->select('*', DB::raw('DATE_FORMAT(start_at, "%d/%m/%Y (%H:%i)") as start_date'),
            DB::raw('DATE_FORMAT(end_at, "%d/%m/%Y (%H:%i)") as end_date'),
            DB::raw('DATE_FORMAT(approver_at, "%d/%m/%Y (%H:%i)") as approver_date'))
            ->orderBy('id', 'DESC');
        if ($check) {
            $datas = $datas->whereDate('start_at', '>=', date('Y') - PRE_YEAR . '-m-d');
        } else {
            $datas = $datas->whereYear('start_at', $thisYear);
        }
        if ($request->month) {
            $datas = $datas->whereMonth('start_at', $request->month);
        }
        if ($request->year) {
            $datas = $datas->whereYear('start_at', $request->year);
        }
        $datas = $datas->paginate(PAGINATE_DAY_OFF);
        return $isApi ? $datas : [
            'data' => $datas,
            'total' => $remainDay->previous_year + $remainDay->current_year,
            'total_previous' => $remainDay->previous_year,
            'total_current' => $remainDay->current_year,
            'remain_current' => $model->whereYear('start_at', $thisYear)->where('status', DayOff::APPROVED_STATUS)->sum('number_off'),
            'remain_previous' => $model->whereYear('start_at', $thisYear - 1)->where('status', DayOff::APPROVED_STATUS)->sum('number_off')
        ];
    }

    public function updateStatusDayOff($recordID, $approvalID, $comment, $approve = DayOff::APPROVED_STATUS, $number_off = 0.5)
    {
        $approval = User::find($approvalID);
        if ($approval == null || $approval->id == null || $approval->jobtitle_id < \App\Models\Report::MIN_APPROVE_JOBTITLE) {
            return false;
        }
        $record = $this->model
            ->where('id', $recordID)
            ->where('status', DayOff::NOTAPPROVED_STATUS)->first();
        if ($record !== null && $record->id !== null) {
            $record->approver_id = $approval->id;
            $record->approve_comment = $comment;
            $record->status = DayOff::APPROVED_STATUS;
            $record->approver_at = Carbon::now();
            $record->number_off = $number_off;
            return $record->update() != null;
        } else {
            return false;
        }
    }

    public function getRecordOf($idRecord)
    {
        $recordFound = $this->model->with('user')->find($idRecord);
        return $recordFound;
    }

    public function create($idUser, $title, $reason, $start_at, $end_at, $approvalID)
    {
        $existed = DayOff::where('status', DayOff::APPROVED_STATUS)
            ->where("start_at", "<=", $start_at)
            ->where("end_at", ">=", $end_at)->first();

        if ($existed !== null && $existed->id !== null) {
            return [
                "status" => false,
                "record" => null,
                "message" => "Đã tồn tại"
            ];
        }

        $rec = new DayOff([
            'user_id' => $idUser,
            'title' => $title,
            "reason" => $reason,
            "start_at" => $start_at,
            "end_at" => $end_at,
            "status" => 0,
            "approver_id" => $approvalID
        ]);

        $result = $rec->save();
        return [
            "status" => $result,
            "record" => $result ? $rec : null,
            "message" => "Tạo đơn thất bại"
        ];
    }

    public function showList($status, array $relations = [])
    {
        $model = $this->getdata()->whereYear('day_offs.start_at', '=', date('Y'));
        if (!empty($relations)) {
            $model = $model->with($relations);
        }
        $data = clone $model->orderBy('id', 'DESC')->get();

        if ($status != null) {
            $dataDate = $model;
            if ($status < ALL_DAY_OFF) {
                $dataDate = $dataDate->where('day_offs.status', $status);
            }
        } else {
            $dataDate = $model->whereMonth('day_offs.start_at', '=', date('m'));
        }
        $dataDate = $dataDate->orderBy('id', 'DESC')->paginate(PAGINATE_DAY_OFF);
        return [
            'dataDate' => $dataDate,
            'data' => $this->getDayOffApprove(),
            'total' => $data->count(),
            'totalActive' => $data->where('status', STATUS_DAY_OFF['active'])->count(),
            'totalAbide' => $data->where('status', STATUS_DAY_OFF['abide'])->count(),
            'totalnoActive' => $data->where('status', STATUS_DAY_OFF['noActive'])->count(),

        ];
    }

    public function getDataDate($request, $status = ALL_DAY_OFF, array $relations = [])
    {
        $dates = [
            'search_start_at' => $request->search_start_at,
            'search_end_at' => $request->search_end_at
        ];
        
        foreach ($dates as $name => $date) {
            // Nếu đúng định dạng yyyy-mm-dd hoặc yyyy/mm/dd thì giá trị $date được giữ nguyên, nếu không thì gán giá trị của $date bằng null
            $dates[$name] = preg_match('/^\d{4}([\/\-]+\d{2}){2}$/', $date) ? $date : null;
        }

        extract($dates);

        $model = $this->model->searchByDates($search_start_at, $search_end_at);
        
        if (!empty($relations)) {
            $model = $model->with($relations);
        }

        $dataDate = $status < ALL_DAY_OFF ? $model->where('day_offs.status', $status) : $model; 

        return $dataDate->orderBy('start_at', 'desc')->paginate(PAGINATE_DAY_OFF);
    }

    public function getDayOff($request, $status = ALL_DAY_OFF)
    {
        return $this->getDataDate(
            $request,
            $status,
            [
                'user.projects' => function ($query) {
                    $query->inProgress();
                },
                'user',
                'user.teams',
                'user.teams.group'
            ]
        )->items();
    }

    public function getDayOffApprove(){
        return $this->getdata()->whereDate('start_at', '>=', date('Y') - PRE_YEAR . '-m-d')->orderBy('id', 'DESC')->paginate(PAGINATE_DAY_OFF);
    }

    public function getDataSearch($start, $end, $status, $search = null)
    {
        $data = $this->getdata();
        if ($start) {
            $data = $data->whereDate('start_at', '>=', date('Y-m-d', strtotime($start)));
        }
        if ($end) {
            $data = $data->whereDate('start_at', '<=', date('Y-m-d', strtotime($end)));
        }

        if ($search) {
            $data = $data->Where('users.name', 'like', '%' . $search . '%');

        }
        if ($status < ALL_DAY_OFF) {
            $data = $data->where('day_offs.status', $status);
        }
        $data = $data->paginate(PAGINATE_DAY_OFF);
        return [
            'data' => $data,
        ];
    }

    public function getOneData($id)
    {
        return $data = $this->getdata(false, $id)->first();
    }

    public function countDayOff($id, $check = false)
    {
        if ($check) {
            $data = [
                'countDayOffCurrenYear' => $this->sumDayOff($id, null, false),
                'countDayOffPreYear' => $this->sumDayOff($id, TOTAL_MONTH_IN_YEAR, true),
            ];

        } else {
            $data = $this->model::groupBy('user_id', 'check_free')
                ->select('user_id', 'check_free', DB::raw('sum(number_off) as total'))
                ->where('user_id', $id)
                ->where('title', '=', DAY_OFF_TITLE_DEFAULT)
                ->where('status', STATUS_DAY_OFF['active'])
                ->whereMonth('day_offs.start_at', '=', date('m'))
                ->whereYear('day_offs.start_at', '=', date('Y'))->first();
        }
        return $data;
    }

    /**
     * @param integer $status
     *
     * @return collection
     */
    public function searchStatus($start, $end, $status)
    {
        $data = DayOff::select('*', DB::raw('DATE_FORMAT(start_at, "%d/%m/%Y (%H:%i)") as start_date'),
            DB::raw('DATE_FORMAT(end_at, "%d/%m/%Y (%H:%i)") as end_date'),
            DB::raw('DATE_FORMAT(approver_at, "%d/%m/%Y (%H:%i)") as approver_date'))
            ->where('user_id', Auth::id());
        if ($start) {
            $data = $data->whereDate('start_at', '>=', date('Y-m-d', strtotime($start)));
        }
        if ($end) {
            $data = $data->whereDate('start_at', '<=', date('Y-m-d', strtotime($end)));
        }
        if ($status < ALL_DAY_OFF) {
            $data = $data->where('status', $status);
        }
        $data = $data->orderBy('id', 'DESC')
            ->paginate(PAGINATE_DAY_OFF);
        return $data;
    }

    /**
     *
     * @return array
     */

    public function countDayOffUserLogin()
    {
        $user = Auth::user();
        $total = $this->sumDayOff(null, null, false);
        $sumDayOffPreYear = RemainDayoff::where('user_id', $user->id)->where('year', (int)date('Y') - PRE_YEAR)->first();
        $sumDayOffCurrentYear = RemainDayoff::where('user_id', $user->id)->where('year', (int)date('Y'))->first();
        return $countDayyOff = [
            'total' => $total,
            'previous_year' => $sumDayOffPreYear->remain_pre_year ?? DAY_OFF_DEFAULT,
            'current_year' => $sumDayOffCurrentYear->remain ?? DAY_OFF_DEFAULT
        ];
    }

    public function searchUserLogin($request)
    {
        // TODO: Implement searchUserLogin() method.
    }


    public function statisticalDayOffExcel($request)
    {
        $result = [];
        $users = User::whereNull('end_date');
        if ($request['search']) {
            $users->where('name', 'like', '%' . $request['search'] . '%');
        }
        $users = $users->get();
        $dayOffMonth = $this->statisticalDayOff($request);
        foreach ($users as $keys => $user) {
            $vacationMode = $this->statisticalVacationModeDayOff($user->id);
            $dayOffPreYear = RemainDayoff::where('user_id', $user->id)->where('year', date('Y') - PRE_YEAR)->first();
            $dayOffYear = RemainDayoff::where('user_id', $user->id)->where('year', date('Y'))->first();
            $dayOffPreYearTotal = $dayOffPreYear->remain ?? DEFAULT_VALUE;
            $dayOffYearTotal = $dayOffYear->remain ?? DEFAULT_VALUE;
            $totalMonth = [];
            if (count($dayOffMonth)) {
                foreach ($dayOffMonth as $key => $value) {
                    if ($value->user_id == $user->id) {
                        for ($i = JANUARY; $i <= DECEMBER; $i++) {
                            if ($value->month == $i) {
                                $totalMonth[$i] = $value->total + $value->total_absfirstOrCreateent;
                            }
                        }
                    }
                }
            }
            $result[] = [
                'stt' => $keys + DAY_OFF_INCREMENT,
                'name' => $user->name,
                'staff_code' => $user->staff_code,
                'sex' => $user->sex == DEFAULT_VALUE ? 'Nam' : 'Nữ',
                'part_time' => $user->contract_type == CONTRACT_TYPES['parttime'] ? 'V' : '',
                'probation_at' => $user->probation_at ? Carbon::parse($user->probation_at)->format('d-m-Y') : '',
                'strat_date' => Carbon::parse($user->start_date)->format('d-m-Y'),
                'remain_day_off_current_year' => $dayOffYear->remain_increment ?? '0',
                'remain_day_off_pre_year' => !empty($dayOffPreYearTotal) && $dayOffPreYear->remain_pre_year > DEFAULT_VALUE ? $dayOffPreYear->remain_pre_year : '0',
                'day_off_month_Jan' => $totalMonth[1] ?? '0',
                'day_off_month_Feb' => $totalMonth[2] ?? '0',
                'day_off_month_Mar' => $totalMonth[3] ?? '0',
                'day_off_month_Apr' => $totalMonth[4] ?? '0',
                'day_off_month_May' => $totalMonth[5] ?? '0',
                'day_off_month_Jun' => $totalMonth[6] ?? '0',
                'day_off_month_Jul' => $totalMonth[7] ?? '0',
                'day_off_month_Aug' => $totalMonth[8] ?? '0',
                'day_off_month_Sep' => $totalMonth[9] ?? '0',
                'day_off_month_Oct' => $totalMonth[10] ?? '0',
                'day_off_month_Nov' => $totalMonth[11] ?? '0',
                'day_off_month_Dec' => $totalMonth[12] ?? '0',
                'day_off_total' => array_sum($totalMonth),
                'day_off_regulation' => $vacationMode->total ?? '0',
                'day_off_remain_total' => $dayOffPreYearTotal + $dayOffYearTotal == DEFAULT_VALUE ? '0' : $dayOffPreYearTotal + $dayOffYearTotal,
                'day_off_end_year_reset' => $dayOffPreYearTotal == DEFAULT_VALUE ? '0' : $dayOffPreYearTotal,
                'day_off_turn_next_year' => $dayOffYearTotal == DEFAULT_VALUE ? '0' : $dayOffYearTotal,
            ];

        }
        return $result;
    }

    public function calculateDayOff($request, $id)
    {

        //manager active day off
        $dayOff = DayOff::findOrFail($id);
        //check reamin day off Current Year and Pre Year -> update column ramian of table remain_day_offs
        $userDayOff = User::findOrFail($dayOff->user_id);
        $dayOff->approver_at = now();
        $dayOff->approve_comment = $request->approve_comment;
        $dayOff->status = STATUS_DAY_OFF['active'];

        $numOffApprove = $this->checkDateUsable($dayOff->start_at, $dayOff->end_at, null, null, true)[0];
        // user create day off = staff -> check remain day off table
        if ($dayOff->title != DAY_OFF_TITLE_DEFAULT && $userDayOff->contract_type == CONTRACT_TYPES['staff']) {
            $dayOff->number_off = $numOffApprove;

        } else if ($dayOff->title == DAY_OFF_TITLE_DEFAULT && $userDayOff->contract_type == CONTRACT_TYPES['staff'] && $userDayOff->end_date == null) {
            $dayOff->number_off = $numOffApprove;
            // create new if reamin day off curent year = null
            $remainDayOffCurrentYear = RemainDayoff::where('user_id', $dayOff->user_id)->where('year', date('Y'))->first() ?? RemainDayoff::create(['user_id' => $dayOff->user_id, 'year' => (int)date('Y'), 'remain' => DAY_OFF_DEFAULT, 'remain_increment' => DAY_OFF_DEFAULT,]);


            $remainDayOffPreYear = RemainDayoff::where('user_id', $dayOff->user_id)->where('year', (int)date('Y') - 1)->first();
            // day off exist pre year
            $dayOffCurrentYear = $remainDayOffCurrentYear->remain ?? 0;

            // check day off free user = female
            $dayOffFreeCurrentYear = $remainDayOffCurrentYear->day_off_free_female ?? 0;

            // day off exist Current year
            $dayOffPreYear = $remainDayOffPreYear ? $remainDayOffPreYear->remain_pre_year : DAY_OFF_DEFAULT;

            if ($dayOffFreeCurrentYear >= $numOffApprove) {
                $remainDayOffCurrentYear->day_off_free_female = $dayOffFreeCurrentYear - $numOffApprove;
                $remainDayOffCurrentYear->save();
            } else {
                if ($dayOffPreYear + $dayOffFreeCurrentYear >= $numOffApprove) {
                    $remainDayOffPreYear->remain_pre_year = $dayOffPreYear + $dayOffFreeCurrentYear - $numOffApprove;
                    $remainDayOffPreYear->save();

                } elseif ($dayOffCurrentYear + $dayOffPreYear + $dayOffFreeCurrentYear >= $numOffApprove) {
                    if ($remainDayOffPreYear) {
                        $remainDayOffPreYear->remain_pre_year = DAY_OFF_DEFAULT;
                        $remainDayOffPreYear->save();
                    }
                    $remainDayOffCurrentYear->remain = $dayOffCurrentYear + $dayOffPreYear + $dayOffFreeCurrentYear - $numOffApprove;
                    $remainDayOffCurrentYear->save();

                } else {
                    if ($remainDayOffPreYear) {
                        $remainDayOffPreYear->remain_pre_year = DAY_OFF_DEFAULT;
                        $remainDayOffPreYear->save();
                    }
                    $remainDayOffCurrentYear->remain = DAY_OFF_DEFAULT;
                    $dayOff->absent = $numOffApprove - ($dayOffCurrentYear + $dayOffPreYear + $dayOffFreeCurrentYear);
                    $dayOff->number_off = $numOffApprove - $dayOff->absent;
                    $remainDayOffCurrentYear->save();
                };

                $remainDayOffCurrentYear->day_off_free_female = DEFAULT_VALUE;
                $remainDayOffCurrentYear->save();
            }
        } else {
            // user create day off != staff -> insert column absent table day off = total number off
            $dayOff->absent = $numOffApprove;
        }
        $dayOff->save();
        event(new DayOffNoticeEvent($dayOff, Auth::user(), NOTIFICATION_DAY_OFF['active']));

    }


    public function checkDateUsable($startDate, $endDate, $startTime, $endTime, $flag = false)
    {
        if ($flag) {
            $start = Carbon::createFromFormat(DATE_TIME_FORMAT, $startDate)->format(DATE_FORMAT_DAY_OFF);
            $end = Carbon::createFromFormat(DATE_TIME_FORMAT, $endDate)->format(DATE_FORMAT_DAY_OFF);
            $stratTimeDayOff = Carbon::createFromFormat(DATE_TIME_FORMAT, $startDate)->format(TIME_FORMAT);
            $endTimeDayOff = Carbon::createFromFormat(DATE_TIME_FORMAT, $endDate)->format(TIME_FORMAT);
            $startDateDayOff = Carbon::createFromFormat(DATE_TIME_FORMAT, $startDate)->format(DATE_FORMAT_SLASH);
            $endDateDayOff = Carbon::createFromFormat(DATE_TIME_FORMAT, $endDate)->format(DATE_FORMAT_SLASH);
        } else {
            $checkStart = $startTime == DEFAULT_VALUE ? CHECK_TIME_DAY_OFF_USABLE_START[0] : CHECK_TIME_DAY_OFF_USABLE_START[1];
            $checkEnd = $endTime == DEFAULT_VALUE ? CHECK_TIME_DAY_OFF_USABLE_END[0] : CHECK_TIME_DAY_OFF_USABLE_END[1];
            $start = $startDate . ' ' . $checkStart;
            $end = $endDate . ' ' . $checkEnd;
        }
        $from = Carbon::createFromFormat(DATE_FORMAT_DAY_OFF, $start);
        $to = Carbon::createFromFormat(DATE_FORMAT_DAY_OFF, $end);
        if (strtotime($from) > strtotime($to)) {
            return false;
        }
        $numberDate = $to->diffInDays($from) + REMAIN_DAY_OFF_DEFAULT;
        if ($flag) {
            if ($stratTimeDayOff == CHECK_TIME_DAY_OFF_START_DATE) {
                $stratTimeDayOff = CHECK_TIME_DAY_OFF_USABLE_START[0];
            }
            if ($endTimeDayOff == CHECK_TIME_DAY_OFF_END_DATE) {
                $endTimeDayOff = TIME_END_DATE;
            }
            $e = Carbon::createFromFormat(DATE_FORMAT_DAY_OFF, $startDateDayOff . ' ' . $stratTimeDayOff);
            $f = Carbon::createFromFormat(DATE_FORMAT_DAY_OFF, $endDateDayOff . ' ' . $endTimeDayOff);
            $day = ($e->diffInHours($f)) / HOURS_OF_DAY;
        } else {
            if ($startTime == DEFAULT_VALUE && $endTime != DEFAULT_VALUE) {
                $day = ($to->diffInHours($from) + ONE_HOURS) / HOURS_OF_DAY;
            } elseif ($startTime == REMAIN_DAY_OFF_DEFAULT && $endTime == REMAIN_DAY_OFF_DEFAULT) {
                $day = ($to->diffInHours($from) + ONE_HOURS) / HOURS_OF_DAY;
            } else {
                $day = ($to->diffInHours($from) + INT_HALT_DATE) / HOURS_OF_DAY;
            }
        }
        $total = [];
        $totalCalendarOff = [];
        $checkAdditional = DEFAULT_VALUE;
        for ($i = DEFAULT_VALUE; $i < $numberDate; $i++) {
            $convertDay = Carbon::createFromFormat(DATE_FORMAT_DAY_OFF, $start)->addDay($i)->format('D');
            if ($convertDay == SUN || $convertDay == SAT) {
                array_push($total, $i);
            }
        }
        $addDate = AdditionalDate::whereYear('date_add', date('Y'))->get();
        foreach ($addDate as $value) {
            $date = Carbon::createFromFormat(DATE_FORMAT, $value->date_add);
            if (strtotime($from) <= strtotime($date) && strtotime($to) >= strtotime($date)) {
                $checkAdditional = $checkAdditional + REMAIN_DAY_OFF_DEFAULT;
            }
        }
        $check = false;
        $calender = CalendarOff::all();
        foreach ($calender as $keys => $value) {
            $numCalenderOffStart = Carbon::createFromFormat(DATE_FORMAT, $value->date_off_from)->format(DATE_FORMAT);
            $numCalenderOffEnd = Carbon::createFromFormat(DATE_FORMAT, $value->date_off_to)->format(DATE_FORMAT);
            for ($i = DEFAULT_VALUE; $i < $numberDate; $i++) {
                $convertDayCheckStart = Carbon::createFromFormat(DATE_FORMAT_DAY_OFF, $start)->addDay($i)->format(DATE_FORMAT);
                $convertDayCheckEnd = Carbon::createFromFormat(DATE_FORMAT_DAY_OFF, $end)->addDay($i)->format(DATE_FORMAT);
                if (strtotime($convertDayCheckStart) >= strtotime($numCalenderOffStart) && strtotime($convertDayCheckStart) <= strtotime($numCalenderOffEnd)) {
                    array_push($totalCalendarOff, $i);
                }
            }
            if (strtotime($to->format(DATE_FORMAT)) <= strtotime($numCalenderOffEnd) && strtotime($to->format(DATE_FORMAT)) >= strtotime($numCalenderOffStart)) {
                $check = true;
            }
        }
        $totalDayOff = ($day + $checkAdditional) - (count($total) + count($totalCalendarOff));
        return [$totalDayOff, $check];
    }

    /**
     * @param integer $month
     * @param Boolean $check
     *
     * @return collection
     */
    private function sumDayOff($user_id = null, $month = null, $check = false)
    {
        $user = $user_id ? User::findOrFail($user_id) : Auth::user();
        $total = 0;
        $monthSearch = $month ?? date('m');
        $yearSearch = $check ? (int)date('Y') - PRE_YEAR : date('Y');
        $data = $this->model::select('user_id', 'check_free', DB::raw('YEAR(start_at) year, MONTH(start_at) month'), DB::raw('sum(number_off) as total'), DB::raw('sum(absent) as total_absent'))
            ->groupBy('user_id', 'check_free', 'year', 'month')
            ->where('user_id', $user->id)
            ->where('status', STATUS_DAY_OFF['active'])
            ->whereMonth('start_at', '<=', $monthSearch)
            ->whereYear('day_offs.start_at', '=', $yearSearch)
            ->get();
        foreach ($data as $key => $value) {
            $total = $total + $value->total + $value->total_absent;
        }
        return $total;
    }

    /**
     * @param integer $id
     * @param Boolean $check
     *
     * @return collection
     */
    private function getdata($check = true, $id = null)
    {
        $data = DayOff::has('user')->select('day_offs.*', DB::raw('DATE_FORMAT(day_offs.start_at, "%d/%m/%Y (%H:%i)") as start_date'),
            DB::raw('DATE_FORMAT(day_offs.end_at, "%d/%m/%Y (%H:%i)") as end_date'),
            DB::raw('DATE_FORMAT(day_offs.approver_at, "%d/%m/%Y (%H:%i)") as approver_date'), 'users.name')
            ->join('users', 'users.id', '=', 'day_offs.user_id');
        if ($check) {
            $user = Auth::user();
            if ($user->isTeamLeader()) {
                $team = Team::where('leader_id', $user->id)->first();
                if ($team) {
                    $userIds = UserTeam::where('team_id', $team->id)->pluck('user_id')->toArray();
                    $data = $data->whereIn('day_offs.user_id', $userIds);
                } else {
                    $data = $data
                        ->where('day_offs.approver_id', Auth::id())
                        ->where('day_offs.user_id', '<>', Auth::id());
                }
            } else {
                $data = $data
                    ->where('day_offs.approver_id', Auth::id())
                    ->where('day_offs.user_id', '<>', Auth::id());
            }


            $data->orderBy('day_offs.id', DESC);
            return $data;
        } else {
            $data = $data->where('day_offs.id', $id);
            return $data;
        }
    }

    private function statisticalVacationModeDayOff($id)
    {

        return $this->model::select('day_offs.user_id',
            DB::raw('YEAR(start_at) year'),
            DB::raw('sum(number_off) as total'))
            ->join('users', 'users.id', '=', 'day_offs.user_id')
            ->groupBy('day_offs.user_id', 'year')
            ->where('day_offs.user_id', $id)
            ->where('day_offs.status', STATUS_DAY_OFF['active'])
            ->whereYear('day_offs.start_at', '=', date('Y'))
            ->whereNull('users.end_date')
            ->whereIn('day_offs.title', ARRAY_TITLE_DAYOFF_VACATION_MODE)
            ->first();

    }


    private function statisticalDayOff($request)
    {
        $datas = $this->model::select('day_offs.user_id', 'day_offs.check_free',
            DB::raw('YEAR(start_at) year, MONTH(start_at) month'),
            DB::raw('sum(number_off) as total'),
            DB::raw('sum(absent) as total_absfirstOrCreateent'))
            ->join('users', 'users.id', '=', 'day_offs.user_id')
            ->groupBy('day_offs.user_id', 'day_offs.check_free', 'year', 'month')
            ->where('day_offs.status', STATUS_DAY_OFF['active']);
        if ($request['month']) {
            $datas->whereMonth('day_offs.start_at', '=', $request['month']);
        } else {
            $datas->whereMonth('day_offs.start_at', '<=', date('m'));
        }
        if ($request['year']) {
            $datas->whereYear('day_offs.start_at', '=', $request['year']);
        } else {
            $datas->whereYear('day_offs.start_at', '<=', date('Y'));
        }
        $datas = $datas->whereNull('users.end_date')->get();
        return $datas;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getDayOffNow($user_id) {
        $day_now = Carbon::now()->format(DATE_FORMAT);
         return $this->model->select(DB::raw('DATE_FORMAT(day_offs.start_at, "%Y-%m-%d") as start_date'),
            DB::raw('DATE_FORMAT(day_offs.end_at, "%Y-%m-%d") as end_date'),
            DB::raw('DATE_FORMAT(day_offs.end_at, "%H:%i:%s") as end_time'),
            DB::raw('DATE_FORMAT(day_offs.start_at, "%H:%i:%s") as start_time'))
             ->where('status', STATUS_DAY_OFF['active'])
            ->where('user_id', $user_id)->whereDate('start_at','<=',$day_now)->whereDate('end_at', '>=',$day_now)->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findDayOff($id){
        return $this->model->findOrFail($id);
    }
}
