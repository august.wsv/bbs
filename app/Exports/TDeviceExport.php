<?php

namespace App\Exports;

use App\Models\TDevice;
use App\Models\TDeviceType;
use App\Models\TUserDevice;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Helpers\ExcelHelper;
use PHPExcel_Style_Fill;

class TDeviceExport implements WithHeadings, ShouldAutoSize, FromView
{

  
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        
        return [
            ['STT','Mã nhân viên','Tên nhân viên','Loại thiết bị','Chi tiết','Ngày cấp','Tình trạng'],
        ];
    }

    public function view(): View
    {

        $users = User::query()->with('device_user.device.device_name')->get();
        return view('admin.t_devices.t_table_export', [
            'users' => $users
        ]);
    }

}

