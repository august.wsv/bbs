<?php

namespace App\Exports;

use App\Models\CalendarOff;
use App\Models\DayOff;
use App\Models\RemainDayoff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromArray;

class WorkTimeGridExport extends WTGridExport implements FromArray
{
    protected $moreColumnNumber = 4;
    private $dayoffs;
    private $remainDayOffs;

    /**
     * WorkTimeGridExport constructor.
     *
     * @param $records
     */
    public function __construct($records, Request $request)
    {
        parent::__construct($records, $request);

        $this->dayoffs = DayOff::select(
            '*',
            DB::raw("DATE_FORMAT(start_at,'%Y-%m-%d') as start_date"),
            DB::raw("DATE_FORMAT(end_at,'%Y-%m-%d') as end_date")
        )->whereBetween('start_at', [$this->firstDate, $this->lastDate])
            ->orWhereBetween('end_at', [$this->firstDate, $this->lastDate])->get();

        $this->remainDayOffs = RemainDayoff::select('user_id', 'check_add_free', 'day_off_free_female')
            ->where('year', Carbon::now()->year)->get();

        $this->holidays = CalendarOff::all();

        $this->getHeadings();
        $this->getList();
    }

    protected function getHeadings(): void
    {
        $row1 = ['', '', 'Thứ'];
        foreach ($this->dateLists as $date) {
            $row1[] = get_day_name($date, true);
        }
        $row1[] = '';
        $row2 = ['#', 'Họ và tên', 'Mã nhân viên'];
        foreach ($this->dateLists as $date) {
            $row2[] = date_format(date_create($date), 'd');
        }
        $row2[] = 'Tổng';

        $headings[] = $row1;
        $headings[] = $row2;
        $this->headings = $headings;
    }

    protected function getList(): void
    {
        $results = [];
        $userIds = [];
        $rowNum = 1;

        foreach ($this->users as $user) {
            $workTimes = $this->records->where('user_id', $user->id);

            $userIds[] = $user->id;
            $userData = [
                $rowNum++,
                $user->name,
                $user->staff_code,
            ];

            foreach ($this->dateLists as $date) {
                if ($user->start_date > $date) {
                    $userData[] = '-';
                    continue;
                }

                $holiday = $this->holidays
                    ->where('date_off_from', '<=', $date)
                    ->where('date_off_to', '>=', $date)->first();
                if ($holiday) {
                    $userData[] = '-';
                    continue;
                }
                $workTime = $workTimes->firstWhere('work_day', $date);
                $dayOff = $this->dayoffs
                    ->where('user_id', $user->id)
                    ->where('start_date', '<=', $date)
                    ->where('end_date', '>=', $date)->first();
                if ($workTime && $workTime->cost) {
                    $userData[] = $this->userData($dayOff, $date, $holiday, $workTime->cost);
                } else {
                    $userData[] = $this->userData($dayOff, $date, $holiday);
                }
            }

            $userData[] = $workTimes->sum('cost');
            $results[] = $this->customDataWomen($userData, $user);
            $rowNum++;
        }

        if ($this->users->count() > 1) {
            $lastRow = ['', '', 'Tổng'];
            foreach ($this->dateLists as $date) {
                $lastRow[] = $this->records->whereIn('user_id', $userIds)
                    ->where('cost', '>', 0)
                    ->where('work_day', $date)->count();
            }
            $lastRow[] = '';
            $results[] = $lastRow;
        }
        $this->importList = $results;
    }

    public function customDataWomen($userData, $user)
    {
        $remainDayOff = $this->remainDayOffs->where('user_id', $user->id)->first();
        if ($remainDayOff && $user->sex == SEX['female'] && (int)$remainDayOff->check_add_free == REMAIN_DAY_OFF_DEFAULT) {
            $filter_sabbatical = array_filter($userData, function ($value) {
                return $value === ALLOWED || $value === HAFT_COST . ALLOWED;
            }, ARRAY_FILTER_USE_BOTH);
            if (!empty($filter_sabbatical)) {
                $keys = array_keys($filter_sabbatical);
                if ($filter_sabbatical[$keys[DEFAULT_VALUE]] === ALLOWED) {
                    $userData[$keys[DEFAULT_VALUE]] = WOMEN;
                } else if ($filter_sabbatical[$keys[DEFAULT_VALUE]] === HAFT_COST . ALLOWED) {
                    $userData[$keys[DEFAULT_VALUE]] = HAFT_COST . WOMEN;
                }
                if (count($keys) > ONE_VALUE && $filter_sabbatical[$keys[DEFAULT_VALUE]] !== ALLOWED) {
                    if ($filter_sabbatical[$keys[ONE_VALUE]] === ALLOWED) {
                        $userData[$keys[ONE_VALUE]] = HAFT_COST . WOMEN . '+' . HAFT_COST . ALLOWED;
                    } else if ($filter_sabbatical[$keys[ONE_VALUE]] === HAFT_COST . ALLOWED) {
                        $userData[$keys[ONE_VALUE]] = HAFT_COST . WOMEN;
                    }
                }
            }
        }
        return $userData;
    }

    public function userData($dayOff, $date, $holiday, $cost = '')
    {
        if ($holiday) {
            return '-';
        }
        //nếu là ngày nghỉ cuối tuần trả về ''
        if (!check_day_of_week(Carbon::parse($date))) {
            return '';
        }
        $checkDayOffApproval = $dayOff && $dayOff->status == DayOff::APPROVED_STATUS;
        if (!$checkDayOffApproval) {
            return $cost;
        }
        $startDate = $dayOff->start_date;
        $endDate = $dayOff->end_date;
        $cost = $cost == ONE_VALUE ? '' : $cost;
        if ($dayOff->title == DAY_OFF_TITLE_DEFAULT) {
            // thời gian nghỉ trong 1 ngày
            if ($startDate == $endDate) {
                if ($dayOff->absent > DEFAULT_VALUE) {
                    if ($dayOff->absent < 1 && $dayOff->number_off > 0 && $dayOff->number_off < 1) {
                        return  HAFT_COST . ALLOWED . '+' . HAFT_COST . NO_SALARY;
                    }
                    return $cost . NO_SALARY;
                }
                return $cost . ALLOWED;
                // thời gian nghỉ nhiều hơn 1 ngày và ngày kiển tra là ngày bắt đầu nghỉ
            } else if ($startDate == $date) {
                return $this->customUserData($cost, $dayOff->number_off, false);
                // thời gian nghỉ nhiều hơn 1 ngày và ngày kiểm tra nằm trong khoảng thời gian nghỉ
            } else if ($startDate < $date && $date < $endDate) {
                $addTime = Carbon::parse($dayOff->start_at)->format('H:i:s') == CHECK_TIME_DAY_OFF_START_DATE
                    ? 1
                    : 0.5;
                //tính khoảng thời gian từ ngày kiểm tra đến ngày bắt đầu nghỉ
                $diffDayOff = Carbon::parse($date)->diffInDays(Carbon::parse($date));
                $sub_holiday = $this->subHoliday($diffDayOff, $diffDayOff, $date);
                //thêm thời gian của ngày bắt đầu nghỉ
                $a = $sub_holiday + $addTime;
                $sub = $dayOff->number_off - $a;
                return $this->customUserData('', $sub, false);
            } else {
                return $this->customUserData($cost, $dayOff->absent);
            }
        }
        return $cost . REGIME;
    }

    public function subHoliday($subHoliday, $diffDayOff, $date)
    {
        //loại bỏ ngày lễ, ngày nghỉ cuối tuần
        for ($i = 1; $i < $diffDayOff; $i++) {
            $checkHoli = $this->holidays->where('date_off_from', '<=', Carbon::parse($date)->subDay($i))
                ->where('date_off_to', '>=', Carbon::parse($date)->subDay($i))->first();
            if ($checkHoli || !check_day_of_week(Carbon::parse($date)->subDay($i))) {
                $subHoliday -= 1;
            }
        }
        return $subHoliday;
    }

    public function customUserData($cost, $numberDay, $absent = true)
    {
        if ($cost == '') {
            if ($numberDay >= 1) {
                return $cost . $this->title($absent);
            } else if ($numberDay > 0 && $numberDay < 1) {
                return HAFT_COST . ALLOWED . '+' . HAFT_COST . NO_SALARY;
            } else {
                return $cost . $this->title(!$absent);
            }
        }
        if ($numberDay >= 0.5) {
            return $cost . $this->title($absent);
        } else {
            return $cost . $this->title(!$absent);
        }
    }

    public function title($absent)
    {
        return $absent ? NO_SALARY : ALLOWED;
    }

    /**
     * @return array
     */
    public function array(): array
    {
        return array_merge($this->headings, $this->importList);
    }

    protected function extendFormatSheet($sheet)
    {
        $values = [
            'alloweds' => [
                ALLOWED,
                HAFT_COST . ALLOWED,
                REGIME,
                WOMEN,
                HAFT_COST . WOMEN,
                HAFT_COST . WOMEN . '+' . HAFT_COST . ALLOWED,
            ],
            'haft' => [
                HAFT_COST,
                HAFT_COST . ALLOWED . '+' . HAFT_COST . NO_SALARY
            ],
            'no_salary' => [
                NO_SALARY,
                HAFT_COST . NO_SALARY
            ]
        ];

        $sheetData = array_merge($this->headings, $this->importList);
        for ($i = 3; $this->totalRow > $i; $i++) {
            $user = $this->users->firstWhere('staff_code', $sheetData[$i - 1][2]);
            if ($user) {
                if (
                    $user->jobtitle_id == MASTER_ROLE
                    || $user->is_remote
                ) {
                    continue;
                }

                foreach ($this->cols as $col) {
                    $cellValue = $sheet->getCell($col . $i)->getValue();
                    if (empty($cellValue) || in_array($cellValue, $values['haft'])) {
                        $sheet->getStyle($col . $i)->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB(\PHPExcel_Style_Color::COLOR_YELLOW);
                    } elseif (in_array($cellValue, $values['alloweds'])) {
                        $sheet->getStyle($col . $i)->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB(\PHPExcel_Style_Color::COLOR_GREEN);
                    } elseif (in_array($cellValue, $values['no_salary'])) {
                        $sheet->getStyle($col . $i)->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB(\PHPExcel_Style_Color::COLOR_RED);
                    }
                }
            }
        }
    }
}
