<?php
/**
 * DiaryListExport
 * @subpackage classExport
 * @author     LuongTuanHung
 */

namespace App\Exports;

use App\Services\Contracts\IEventAttendanceService;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;


class DowloadExcelEventExport implements FromArray, WithHeadings, ShouldAutoSize
{
    public $eventId;

    /**
     * DiaryListExport constructor.
     *
     * @param string $filename
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        $results = [];
        $footer = false;
        $lineEnd = count($this->data['list_user']) - 1;
        foreach ($this->data['list_user'] as $key => $userJoin) {
            if ($lineEnd == $key) {
                $footer = true;
            }
            $item = $this->makeRow($this->data, $userJoin, ++$key, $footer);
            $results[] = $item;
        }
        return $results;
    }

    /**
     * Function Heading
     *
     * @create_date: 2018/08/27
     * @author     : Tiennm
     * @return array
     */
    public function headings(): array
    {
        return [
            'STT',
            'Mã Nhân viên',
            'Tên thành viên ',
            'Team',
            'Chức danh',
            'Trạng thái',
            'Ý kiến cá nhân',
            'Ngày đăng kí'
        ];
    }

    /**
     * Function Make Row data
     *
     * @create_date: 2018/08/27
     * @author     : Tiennm
     * @return array
     */
    public function makeRow($data, $listUserJoinEventValue, $idx, $footer = false)
    {
        $row = [];
        $row [] =  [
            'stt' => $idx,
            'staff_code' => $listUserJoinEventValue['staff_code'],
            'name' => $listUserJoinEventValue['name'],
            'team_name' => $listUserJoinEventValue['team_name'],
            'position' => $listUserJoinEventValue['position'],
            'status' => $listUserJoinEventValue['status'],
            'content' => $listUserJoinEventValue['content'],
            'created_at' => $listUserJoinEventValue['created_at'],
        ];
        if ($footer) {
            $row [] = [];
            $row [] = ['', '', 'label' => 'Số nhân viên:', 'content' => $data['total']];
            $row [] = ['', '', 'label' => 'Số người chưa đăng kí:', 'content' => $data['user_un_register']];
            $row [] = ['', '', 'label' => 'Số người tham gia:', 'content' => $data['user_join']];
            $row [] = ['', '', 'label' => 'Số người Không tham gia:', 'content' => $data['user_un_join']];
        }
        return $row;
    }

}