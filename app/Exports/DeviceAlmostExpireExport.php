<?php


namespace App\Exports;


use App\Services\DeviceService;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class DeviceAlmostExpireExport implements FromArray, WithHeadings, ShouldAutoSize
{
    public function __construct($listDeviceAlmostExpire)
    {
        $this->listDeviceAlmostExpire = $listDeviceAlmostExpire;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'STT',
            'Mã thiết bị',
            'Tên thiết bị',
            'Ngày mua',
            'Hạn bảo hành',
            'Nhân viên',
            'Ngày cấp'
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function array(): array
    {
        $results = [];
        foreach ($this->listDeviceAlmostExpire as $key => $deviceAlmostExpire) {
            $item = $this->makeRow($key + 1, $deviceAlmostExpire);
            $results[] = $item;
        }
        return $results;
    }


    public function makeRow($stt, $deviceAlmostExpire)
    {
        $dateImport = Carbon::parse($deviceAlmostExpire->date_import);
        $dateExpireGuarantee = $dateImport->addMonth($deviceAlmostExpire->guarantee);
        $deviceName = explode(' | ', $deviceAlmostExpire->info_device);
        $nameDevice = '';
        foreach ($deviceName as $key => $elementName) {
            if ($key !== 0) {
                $nameDevice .= $elementName . ' | ';
            }
        }
        return [
            'stt' => $stt,
            'code_device' => $deviceName[0],
            'name_device' => substr($nameDevice, 0, -2),
            'date_import' => $deviceAlmostExpire->date_import,
            'date_expire_guarantee' => $dateExpireGuarantee->toDateString(),
            'user_name' =>  $deviceAlmostExpire->name ??  'Đang ở trong kho',
            'date_receive' => $deviceAlmostExpire->date_recieve
        ];
    }
}
