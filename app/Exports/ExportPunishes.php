<?php

namespace App\Exports;

use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ExportPunishes implements WithHeadings, Responsable, FromCollection, WithColumnFormatting
{
    use Exportable;
    private $data;

    /**
     * Optional Writer Type
     */
    private $writerType = Excel::XLSX;

    /**
     * Optional headers
     */
    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    /**
     * DiaryListExport constructor.
     *
     * @param string $filename
     */
    public function __construct($data)
    {
        $this->data = $data;
    }


    /**
     * function get data patient and diary export CSV
     *
     * @return array
     */
    public function Collection()
    {
        return collect($this->data);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return ['Mã nhân viên', 'Họ và tên', 'Tiền phạt'];
    }

    /**
     * Format columns
     *
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'O' => NumberFormat::FORMAT_TEXT,
            'R' => NumberFormat::FORMAT_TEXT,
            'S' => NumberFormat::FORMAT_TEXT,
            'U' => NumberFormat::FORMAT_TEXT,
        ];
    }
}
