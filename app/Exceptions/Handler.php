<?php

namespace App\Exceptions;

use App\Notifications\SentMSTeam;
use App\Traits\RESTActions;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    use RESTActions;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);

        if (!$this->shouldntReport($exception) && config('services.teams.error_webhook')) {
            Notification::send(
                'Notification',
                (new SentMSTeam(
                    config('services.teams.error_webhook'),
                    $exception->getTraceAsString(),
                    $exception->getMessage(),
                    'error'
                ))
            );
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $exception
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $requestUri = $request->getRequestUri();

        if (isset($requestUri) && str_contains($requestUri, '/api/')) {
            if ($exception instanceof NotFoundHttpException) {
                return $this->respondNotfound($exception->getMessage());
            }
            if ($exception instanceof MethodNotAllowedHttpException) {
                return $this->respondFail($exception->getMessage(), null, Response::HTTP_METHOD_NOT_ALLOWED);
            }
            if ($exception instanceof ValidationException) {
                return $this->respondFailValidation(trans('messages.validation_error'), $exception->errors());
            }
            if ($exception instanceof HttpException) {
                if ($exception->getStatusCode() == Response::HTTP_FORBIDDEN) {
                    return $this->respondFailForbidden($exception->getMessage());
                } else if ($exception->getStatusCode() == Response::HTTP_TOO_MANY_REQUESTS) {
                    return $this->respondFail(__('messages.too_many_attempts'));
                }

                return $this->respondFail($exception->getMessage());
            }
            if ($exception instanceof AuthenticationException || $exception instanceof UnauthorizedHttpException) {
                $message = config('app.env') != 'production' ? $exception->getMessage() : '';
                return $this->respondAuthFail($message);
            }

            if (config('app.env') != 'production') {
                if ($exception instanceof QueryException) {
                    return $this->respondFail('Error DB: ' . $exception->getMessage() . $exception->getTraceAsString());
                }
                if ($exception instanceof Exception) {
                    return $this->respondFail('Error: ' . $exception->getMessage() . $exception->getTraceAsString());
                }
            } else {
                if ($exception instanceof Exception) {
                    return $this->respondFail(__('messages.server_error'));
                }
            }
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param \Illuminate\Http\Request                 $request
     * @param \Illuminate\Auth\AuthenticationException $exception
     *
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        if (str_contains($request->path(), 'admin')) {
            return redirect()->guest('admin/login');
        }
        return redirect()->guest('login');
    }
}
