<?php

namespace App\Jobs;

use App\Traits\SendFcmNotificationTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendFcmNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use SendFcmNotificationTrait;

    protected $listTokens;
    protected $notification;

    /**
     * Create a new job instance.
     * @param $listTokens
     * @param $notification
     * @return void
     */
    public function __construct($listTokens, $notification)
    {
        $this->listTokens = $listTokens;
        $this->notification = $notification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->SendFcmNotification($this->listTokens, $this->notification);
    }
}
