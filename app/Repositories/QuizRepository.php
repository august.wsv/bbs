<?php

namespace App\Repositories;

use App\Models\Quiz;
use App\Models\Question;
use Carbon\Carbon;
use App\Repositories\Contracts\IQuizRepository;

/**
 * QuizRepository class
 * Author: Hoai
 *
 */
class QuizRepository extends AbstractRepository implements IQuizRepository
{
    /**
     * QuizModel
     *
     * @var  string
     */
    protected $modelName = Quiz::class;

    public function getListQuiz()
    {
        return $this->model->all();
    }

    /**
     * Count user answered
     *
     * @return quizzes
     */

    public function countAnswerUser()
    {
        $userAnswers = $this->model
            ->select(
                'quizzes.id',
                'quizzes.start_date'
            )
            ->with(['answerUsers' => function ($query) {
                $query->groupBy('created_at');
            }])
            ->get();
        $quizzes = [];
        if (!empty($userAnswers)) {
            $userAnswers = $userAnswers->toArray();
            foreach ($userAnswers as $value) {
                $quizStartDate = $this->caculateStartDate($value['id']);
                $count = 0;
                foreach ($value["answer_users"] as $item) {
                    $answerUserSubmitedAt = $item['created_at'];
                    if (strtotime($answerUserSubmitedAt) >= strtotime((string)$quizStartDate)) {
                        $count++;
                    }
                }
                $quizzes[] = [
                    'id' => $value['id'],
                    'count' => $count
                ];
            }
        }
        return $quizzes;
    }

    /**
     * Get list users
     *
     * @param  int $quizId
     * @return collection
     */
    public function getUser($quizId)
    {
        $startDate = $this->caculateStartDate($quizId);
        $users = Question::select(
            'quizzes.participants',
            'users.name',
            'users.staff_code',
            'users.id as user_id',
            'answer_users.created_at as submited_at'
        )
            ->where('quiz_id', $quizId)
            ->join('answer_users', 'answer_users.question_id', 'questions.id')
            ->join('users', 'users.id', 'answer_users.user_id')
            ->join('quizzes', 'questions.quiz_id', 'quizzes.id')
            ->where('answer_users.created_at', ">=", $startDate)
            ->groupBy('answer_users.user_id')
            ->get();
        if (!$users->isEmpty()) {
            return $users;
        } else {
            return Quiz::select(
                'quizzes.participants'
            )
                ->where('id', $quizId)
                ->get();
        }
    }

    public function countQuestion($quizId)
    {
        return Question::where('quiz_id', $quizId)->count();
    }

    /**
     * Count correct answers of user
     *
     * @param  int $quizId
     * @return collection
     */
    public function countCorrectAnswer($quizId)
    {
        $startDate = $this->caculateStartDate($quizId);
        return Question::where('quiz_id', $quizId)
            ->join('answer_users', 'answer_users.question_id', 'questions.id')
            ->where('answer_users.created_at', ">=", $startDate)
            ->get();
    }

    /**
     * Get answers of user
     *
     * @param  int $quizId
     * @param  int $userId
     * @return collection
     */
    public function getAnswerUser($quizId, $userId)
    {
        $startDate = $this->caculateStartDate($quizId);
        return Question::select(
            'questions.*',
            'answer_users.answer',
            'answer_users.is_correct',
            'answer_users.created_at as submit_at'

        )
            ->with('answers')
            ->where('questions.quiz_id', $quizId)
            ->join('answer_users', 'answer_users.question_id', 'questions.id')
            ->where('answer_users.created_at', ">=", $startDate)
            ->where('answer_users.user_id', $userId)
            ->get();
    }

    /**
     * Caculate start date of quiz
     *
     * @param  int $quizId
     * @return StartDate
     */
    public function caculateStartDate($quizId)
    {
        $current = Carbon::now();
        $currentMonth = $current->month;
        $currentYear = $current->year;
        $quiz = $this->model->find($quizId);
        if ($quiz->type == array_search('Định kì', QUIZ_TYPES)) {
            $startDay = substr($quiz->start_date, -2);
            $startDate = "$currentYear/$currentMonth/$startDay";
            if (strtotime($startDate) > strtotime($current)) {
                $lastDate = Carbon::now()->subMonth();
                $startDate = "$lastDate->year/$lastDate->month/$startDay";
            }
        } else {
            $startDate = $quiz->start_date;
        }
        return $startDate;
    }
}
