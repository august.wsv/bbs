<?php

/**
 * Created by PhpStorm.
 * User: muatu
 * Date: 1/31/2019
 * Time: 4:32 PM
 */

namespace App\Repositories\Contracts;


interface IProjectRepository extends IBaseRepository
{
  /**
   * findBy
   *
   * @param  mixed $searchCriteria
   * @param  mixed $fields
   * @param  mixed $all
   * @param  mixed $request
   * @return void
   */
  public function findBy(array $searchCriteria = [], array $fields = ['*'], $all = false, $request = null);
}
