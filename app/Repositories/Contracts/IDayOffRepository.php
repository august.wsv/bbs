<?php

namespace App\Repositories\Contracts;

/**
 * DayOffRepository contract.
 * Author: jvb
 * Date: 2019/01/22 10:50
 */
interface IDayOffRepository extends IBaseRepository
{
  public function getTodayInfo($userIds);
}
