<?php

namespace App\Repositories\Contracts;

/**
 * PostRepository contract.
 * Author: jvb
 * Date: 2023/06/06 17:55
 */
interface IProjectSprintRepository extends IBaseRepository
{
}
