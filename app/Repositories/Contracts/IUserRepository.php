<?php

namespace App\Repositories\Contracts;

/**
 * UserRepository contract.
 * Author: jvb
 * Date: 2018/07/16 10:34
 */
interface IUserRepository extends IBaseRepository
{
    /**
     * @param $groupId
     * @param $teamId
     * @return mixed
     */
    public function getUserIdReport($groupId, $teamId);

    /**
     * @param array $ids
     * @return mixed
     */
    public function restore(array $ids = []);
}
