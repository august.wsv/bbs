<?php


namespace App\Repositories\Contracts;


interface IWorkTimesExplanationRepository extends IBaseRepository
{
    /**
     * @return mixed
     */
    public function permissionGetExplanation();

    /**
     * @param $userId
     * @param $workDay
     * @return mixed
     */
    public function getWorkTimeExplanation($userId, $workDay);
}
