<?php

namespace App\Repositories\Contracts;

/**
 * ProvidedDeviceRepository contract.
 * Author: jvb
 * Date: 2019/06/17 14:30
 */
interface IProvidedDeviceRepository extends IBaseRepository
{
    //
}
