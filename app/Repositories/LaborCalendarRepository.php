<?php

namespace App\Repositories;

use App\Models\LaborCalendar;
use App\Repositories\Contracts\ILaborCalendarRepository;
use App\User;
use Carbon\Carbon;

/**
 * MeetingRoomRepository class
 * Author: jvb
 * Date: 2019/03/11 06:46
 */
class LaborCalendarRepository extends AbstractRepository implements ILaborCalendarRepository
{
    /**
     * DeviceModel
     *
     * @var  string
     */
    protected $modelName = LaborCalendar::class;

    public function getListLaborCalendar($month, $userID)
    {
        $nowYear = Carbon::now()->year;
        $startDay = Carbon::parse($nowYear . '/' . $month . '/01')->startOfMonth();
        $endDay = Carbon::parse($nowYear . '/' . $month . '/01')->endOfMonth();
        $arrayLaborCalendars = [];

        $listLabor = $this->model->where('labor_date', '>=', $startDay)->where('labor_date', '<=', $endDay)->get();
        foreach ($listLabor as $info) {
            $laborUserName = '';
            $checkMyLabor = false;
            $arrayLaborCalendar = [];
            foreach ($info->laborCalendarDetail as $detail) {
                $user = User::withTrashed()->find($detail['user_id']);
                $laborUserName .= $user ? $user->name . ' </br>' : '';
                if ($detail['user_id'] == $userID) {
                    $checkMyLabor = true;
                }
            }
            $arrayLaborCalendar[Carbon::parse($info->labor_date)->format(DATE_MONTH_REPORT)] = $laborUserName;
            $arrayLaborCalendar['note'] = $info->note;
            $arrayLaborCalendar['checkSelf'] = $checkMyLabor;
            $arrayLaborCalendars[] = $arrayLaborCalendar;
        }

        return $arrayLaborCalendars;
    }

    public function getWorkContent($month)
    {
        $nowYear = Carbon::now()->year;
        $startDay = Carbon::parse($nowYear . '/' . $month . '/01')->startOfMonth();
        $endDay = Carbon::parse($nowYear . '/' . $month . '/01')->endOfMonth();
        $labor = $this->model->where('labor_date', '>=', $startDay)->where('labor_date', '<=', $endDay)->first();
        if ($labor) {
            return $labor->content;
        }
        return '';
    }

    public function updateWorkContent($month, $year, $content)
    {
        $dateStartUpdate = Carbon::parse($year . '/' . $month . '/01')->startOfMonth()->toDateString();
        $dateEndUpdate = Carbon::parse($year . '/' . $month . '/01')->endOfMonth()->toDateString();
        return LaborCalendar::where('labor_date', '>=', $dateStartUpdate)
            ->where('labor_date', '<=', $dateEndUpdate)->update(['content' => $content]);
    }

    public function getTodayInfo()
    {
        return LaborCalendar::where('labor_date', Carbon::now()->toDateString())->first();
    }
}
