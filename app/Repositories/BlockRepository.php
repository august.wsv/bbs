<?php

namespace App\Repositories;

use App\Models\Block;
use App\Repositories\Contracts\IBlockRepository;

/**
 * BlockRepository class
 * Author: jvb
 * Date: 2022/11/15
 */
class BlockRepository extends AbstractRepository implements IBlockRepository
{
    /**
     * WorkTimeModel
     *
     * @var  string
     */
    protected $modelName = Block::class;
}
