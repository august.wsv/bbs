<?php

namespace App\Repositories;


use App\Models\UserBlock;
use App\Repositories\Contracts\IUserBlockRepository;

class UserBlockRepository extends AbstractRepository implements IUserBlockRepository
{
    protected $modelName = UserBlock::class;
}
