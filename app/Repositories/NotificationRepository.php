<?php

namespace App\Repositories;

use App\Models\Notification;
use App\Repositories\Contracts\INotificationRepository;
use Illuminate\Http\Request;
use Auth;
use DB;

/**
 * RegulationRepository class
 * Author: jvb
 * Date: 2019/08/05
 */
class NotificationRepository extends AbstractRepository implements INotificationRepository
{
    protected $modelName = Notification::class;
}
