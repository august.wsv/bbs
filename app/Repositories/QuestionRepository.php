<?php

namespace App\Repositories;

use App\Models\Question;
use App\Models\Answer;
use App\Repositories\Contracts\IQuestionRepository;

/**
 * QuestionRepository class
 * Author: Hoai
 *
 */
class QuestionRepository extends AbstractRepository implements IQuestionRepository
{
    /**
     * QuestionModel
     *
     * @var  string
     */
    protected $modelName = Question::class;

    public function updateAnswer($answers, $arrTemp, $isCorrect, $questionId)
    {
        $sizeAnswer = count($answers);
        $count = count($arrTemp);
        $result = $sizeAnswer < $count ? $sizeAnswer : $count;
        for ($i = 0; $i < $result; $i++) {
            Answer::where('id', $arrTemp[$i]['id'])->update([
                'answer' => $answers[$i],
                'is_correct' => $isCorrect == $i ? CORRECT_ANSWER : WRONG_ANSWER,
            ]);
        }
        if ($sizeAnswer < $count) {
            Answer::where('question_id', $questionId)->orderBy('id', 'desc')->limit($count - $sizeAnswer)->forceDelete();
        }
        if ($sizeAnswer > $count) {
            for ($i = $count; $i < $sizeAnswer; $i++) {
                Answer::create([
                    'question_id' => $questionId,
                    'answer' => $answers[$i],
                    'is_correct' => $isCorrect == $i ? CORRECT_ANSWER : WRONG_ANSWER,
                ]);
            }
        }
    }
}
