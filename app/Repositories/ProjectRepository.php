<?php

namespace App\Repositories;

use App\Models\Project;
use App\Repositories\Contracts\IProjectRepository;

/**
 * Author: jvb
 * Date: 2019/01/31 05:00
 */
class ProjectRepository extends AbstractRepository implements IProjectRepository
{
    /**
     * ProjectModel
     *
     * @var  string
     */
    protected $modelName = Project::class;

    /**
     * findBy
     *
     * @param  mixed $searchCriteria
     * @param  mixed $fields
     * @param  mixed $all
     * @param  mixed $request
     * @return void
     */
    public function findBy(array $searchCriteria = [], array $fields = ['*'], $all = false, $request = null)
    {
        // it's needed for pagination
        $limit = !empty($searchCriteria['page_size']) ? (int)$searchCriteria['page_size'] : DEFAULT_PAGE_SIZE;

        $queryBuilder = $this->model->where(function ($query) use ($searchCriteria) {
            $this->applySearchCriteriaInQueryBuilder($query, $searchCriteria);
        })->select($fields)->searchOnAdmin($request);
        if (isset($request->userIds)){
            $queryBuilder->ofUser($request->userIds);
        }

        //order by
        $defaultOrderField = $this->model->getKeyName();

        $isValidOrders = isset($searchCriteria[self::ORDER_TXT]) && is_array($searchCriteria[self::ORDER_TXT]);
        $orders = $isValidOrders ? $searchCriteria[self::ORDER_TXT] : [$defaultOrderField => 'desc'];
        foreach ($orders as $field => $cond) {
            $cond = strtolower($cond) == 'asc' ? 'asc' : 'desc';
            $queryBuilder->orderBy($field, $cond);
        }
        $relations = isset($searchCriteria['with']) ? $searchCriteria['with'] : null;
        if ($relations) {
            $relations = explode(',', $relations);
            foreach ($relations as $relation) {
                if (method_exists($this, $relation)) {
                    $queryBuilder->with($relation);
                }
            }
        }
        if ($all) {
            return $queryBuilder->get();
        }
        return $queryBuilder->paginate($limit);
    }
}
