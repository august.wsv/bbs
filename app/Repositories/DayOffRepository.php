<?php

namespace App\Repositories;

use App\Models\DayOff;
use App\Repositories\Contracts\IDayOffRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * DayOffRepository class
 * Author: jvb
 * Date: 2019/01/22 10:50
 */
class DayOffRepository extends AbstractRepository implements IDayOffRepository
{
    /**
     * DayOffModel
     *
     * @var  string
     */
    protected $modelName = DayOff::class;

    public function getTodayInfo($userIds)
    {
        $now = Carbon::now()->format(DATE_TIME_FORMAT);
        return DayOff::select(
            'id',
            'user_id',
            'title',
            'reason',
            'number_off',
            'status',
            DB::raw('DATE_FORMAT(start_at, "%d/%m/%Y (%H:%i)") as start_date'),
            DB::raw('DATE_FORMAT(end_at, "%d/%m/%Y (%H:%i)") as end_date'),
            DB::raw('DATE_FORMAT(approver_at, "%d/%m/%Y (%H:%i)") as approver_date')
        )
            ->where(function ($q) use ($now) {
                $q->where('start_at', '<=', $now)
                    ->where('end_at', '>=', $now);
            })
            ->whereIn('user_id', $userIds)
            ->with('user:id,name,avatar')
            ->get();
    }
}
