<?php 
namespace App\Repositories;

use App\Models\OAuthClient;
use App\Repositories\Contracts\IOAuthClientRepository;
use Illuminate\Support\Str;
use App\Facades\AuthAdmin;

/**
* OAuthClientRepository class
* Author: jvb
* Date: 2019/05/25 14:31
*/
class OAuthClientRepository extends AbstractRepository implements IOAuthClientRepository
{
    /**
    * GroupModel
    *
    * @var  string
    */
    protected $modelName = OAuthClient::class;

    public function save(array $data)
    {
        $data['secret'] = Str::random(STR_RANDOM_40);
        if (property_exists($this->model, 'autoOrder') && $this->model->autoOrder && (!isset($data['order']))) {
            $data['order'] = $this->model->max('order') + 1;
        }

        if (property_exists($this->model, 'autoCreator') && $this->model->autoCreator && !isset($data['creator_id'])) {
            $data['creator_id'] = AuthAdmin::id();
        }
        return $this->model->create(
            $data
        )->makeVisible('secret');
    }
}
