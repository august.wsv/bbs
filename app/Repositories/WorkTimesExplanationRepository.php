<?php


namespace App\Repositories;


use App\Models\WorkTimesExplanation;
use App\Repositories\Contracts\IWorkTimesExplanationRepository;

class WorkTimesExplanationRepository extends AbstractRepository implements IWorkTimesExplanationRepository
{
    /**
     * WorkTimeModel
     *
     * @var  string
     */
    protected $modelName = WorkTimesExplanation::class;

    /**
     * @return mixed
     */
    public function permissionGetExplanation()
    {
        return $this->getModel()->where('type', '!=', TYPE_OVER_TIME);
    }

    /**
     * @param $userId
     * @param $workDay
     * @return mixed
     */
    public function getWorkTimeExplanation($userId, $workDay)
    {
        return $this->getModel()->where('user_id', $userId)->where('work_day', $workDay);
    }
}
