<?php

namespace App\Repositories;

use App\Models\QrCode;
use App\Repositories\Contracts\IQrCodeRepository;

/**
 * QrCodeRepository class
 * Author: jvb
 * Date: 2023/05/17 10:34
 */
class QrCodeRepository extends AbstractRepository implements IQrCodeRepository
{
    /**
     * PunishesModel
     *
     * @var  string
     */
    protected $modelName = QrCode::class;
}
