<?php

/**
 * PunishesModel class
 * Author: jvb
 * Date: 2019/04/22 08:21
 */

namespace App\Models;

use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Punishes extends Model
{
    use SoftDeletes, FillableFields, OrderableTrait, SearchLikeTrait;

    protected $table = 'punishes';

    const UNSUBMIT = 0;
    const SUBMITED = 1;

    protected $fillable = [
        'id',
        'rule_id',
        'week_num',
        'user_id',
        'infringe_date',
        'total_money',
        'detail',
        'is_confirmed',
        'is_submit',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Search for course title or subject name
     *
     * @param $query
     * @param $searchTerm
     *
     * @return mixed
     */
    public function scopeSearch($query, $searchTerm)
    {
        if ($searchTerm) {
            $query->where('punishes.total_money', 'like', '%' . $searchTerm . '%')
                ->orWhere(function ($q) use ($searchTerm) {
                    if (in_array($searchTerm, PUNISH_CONFIRMED_NAME)) {
                        $q->orWhere('punishes.is_confirmed', PUNISH_CONFIRMED_VALUE[$searchTerm]);
                    }
                })
                ->orWhere('punishes.infringe_date', 'like', '%' . $searchTerm . '%')
                ->orWhere(function ($q) use ($searchTerm) {
                    $q->orWhere('users.name', 'like', '%' . $searchTerm . '%')
                        ->orWhere('users.staff_code', 'like', '%' . $searchTerm . '%');
                })
                ->orWhere(function ($q) use ($searchTerm) {
                    if ($searchTerm == RULE_LATE_OF_WORK) {
                        $q->orWhere('punishes.rule_id', PUNISH_RULE_LATE);
                    } else {
                        $q->orWhere('rules.name', 'like', '%' . $searchTerm . '%');
                    }
                })
                ->join('users', 'users.id', 'user_id')
                ->leftJoin('rules', 'rules.id', 'rule_id')
                ->select('punishes.*')
                ->orderBy('punishes.id', 'desc');
        }

        return $query;
    }

    /**
     * searchBeetweenDate
     *
     * @param  mixed $query
     * @param  mixed $startDay
     * @param  mixed $endDay
     * @return void
     */
    public function scopeSearchBeetweenDate($query, $startDay, $endDay)
    {
        if ($startDay && $endDay) {
            $query = $query->whereDate('infringe_date', '>=', $startDay)
                ->whereDate('infringe_date', '<=', $endDay);
        } else {
            $haftYear = Carbon::now()->subMonth(6);
            $query = $query->whereDate('infringe_date', '>=', $haftYear);
        }
        return $query;
    }

    /**
     * scopeStatisticByGroup
     *
     * @param  mixed $query
     * @param  mixed $groupId
     * @return void
     */
    public function scopeStatisticByGroup($query, $groupId)
    {
        $userIds = [];
        if (!$groupId) {
            return $query;
        }
        $teams = Team::where('group_id', $groupId)->get(['id', 'leader_id']);
        foreach ($teams as $team) {
            $ids = $team->members->pluck('id')->toArray();
            $ids[] = $team->leader->id;
            $userIds = array_merge($userIds, $ids);
        }

        return $query->whereIn('punishes.user_id', $userIds);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rule()
    {
        return $this->belongsTo(Rules::class);
    }
}
