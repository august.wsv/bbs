<?php

namespace App\Models;

use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Database\Eloquent\Model;

class OAuthClient extends Model
{
    use FillableFields, OrderableTrait, SearchLikeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oauth_clients';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'secret',
        'redirect',
        'created_at',
        'updated_at',
    ];

    public function scopeSearch($query, $searchTerm)
    {
        return $query->where(function ($q) use ($searchTerm) {
            $q->orWhere('oauth_clients.name', 'like', '%' . $searchTerm . '%');
        })->select('oauth_clients.*');
    }
}
