<?php


namespace App\Models;


use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;

class TaskLogWork extends Model
{
    use FillableFields, OrderableTrait, SearchLikeTrait;

    protected $table = 'task_logwork';

    protected $primaryKey = 'id';

    protected $fillable = [
        'task_id',
        'time',
        'time_text',
        'time_start',
        'description',
        'time_start',
        'creator_id',
    ];

    public function projectTask()
    {
        return $this->belongsTo(ProjectTask::class, 'task_id');
    }
}