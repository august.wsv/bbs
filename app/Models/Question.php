<?php

namespace App\Models;

use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Question extends Model
{
    use Notifiable, SoftDeletes, FillableFields, OrderableTrait, SearchLikeTrait;



    protected $table = 'questions';

    protected $fillable = [
        'name',
        'type',
        'quiz_id',
        'question',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];


    /**
     * Search for course title or subject name
     *
     * @param $query
     * @param $searchTerm
     *
     * @return mixed
     */
    public function scopeSearch($query, $searchTerm)
    {
        return $query->where('question', 'like', '%' . $searchTerm . '%');
    }

    public function answers()
    {
        return $this->hasMany('App\Models\Answer', 'question_id', 'id');
    }

    public function answerUser()
    {
        return $this->hasMany(AnswerUser::class, 'question_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        Question::creating(function ($model) {
            $model->position = Question::max('position') + 1;
        });
    }
}
