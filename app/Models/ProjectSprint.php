<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectSprint extends Model
{
    protected $fillable = [
        'name',
        'description',
        'project_id',
        'start_date',
        'end_date',
        'status',
        'effort',
    ];

    public function scopeSearch($query, $searchTerm)
    {
        if ($searchTerm) {
            $query = $query->where('name', 'like', '%' . $searchTerm . '%')
                ->orwhere('description', 'like', '%' . $searchTerm . '%');
        }

        return $query;
    }
}
