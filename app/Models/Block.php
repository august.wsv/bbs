<?php

namespace App\Models;

use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\UserBlock;
use App\Models\User;

class Block extends Model
{
    use SoftDeletes, FillableFields, OrderableTrait, SearchLikeTrait;
    protected $table = 'blocks';

    protected $fillable = [
        'name',
        'description',
    ];

    public function users()
    {
        return $this->hasMany(UserBlock::class, 'block_id', 'id');
    }
    public function members()
    {
        return $this->hasManyThrough(User::class, UserBlock::class, 'block_id', 'id', 'id', 'user_id')->available();
    }
    public function scopeSearch($query, $searchTerm)
    {
        return $query->Where('name', 'like', '%' . $searchTerm . '%');
    }
}
