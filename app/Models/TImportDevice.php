<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TImportDevice extends Model
{
    protected $table = 't_import_devices';

    protected $fillable = [
        'id',
        'name',
        'specification',
        'quantity',
        'note',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'specification' => 'array',
    ];

    public function device()
    {
        return $this->belongsTo('App\Models\TDeviceType', 'device_type_id');
    }

    public function scopeSearchImportDevice($query, $searchString) {
        return $query->where('name', 'LIKE', '%'.$searchString.'%')
        ->orWhere('code', 'LIKE', '%'.$searchString.'%');
    }
}
