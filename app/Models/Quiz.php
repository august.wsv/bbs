<?php

namespace App\Models;

use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Quiz extends Model
{
    use Notifiable, SoftDeletes, FillableFields, OrderableTrait, SearchLikeTrait;



    protected $table = 'quizzes';

    protected $fillable = [
        'name',
        'description',
        'type',
        'participants',
        'status',
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];


    /**
     * Search for course title or subject name
     *
     * @param $query
     * @param $searchTerm
     *
     * @return mixed
     */
    public function scopeSearch($query, $searchTerm)
    {
        return $query->where('quizzes.name', 'like', '%' . $searchTerm . '%')->select('quizzes.*');
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function answerUsers()
    {
        return $this->hasManyThrough(AnswerUser::class, Question::class, 'quiz_id', 'question_id', 'id', 'id');
    }

    public function getGroupNameAttribute()
    {
        return $this['group']->implode('name', ', ');
    }
}
