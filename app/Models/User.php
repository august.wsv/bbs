<?php
/**
 * UserModel class
 * Author: jvb
 * Date: 2018/07/16 10:34
 */

namespace App\Models;

use App\Facades\ImageFacade;
use App\Notifications\UserResetPassword;
use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Team;
use App\Models\UserTeam;
use App\Models\Block;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes, FillableFields, OrderableTrait, SearchLikeTrait, HasApiTokens;

    const UN_ACTIVE = 0;
    const IS_ACTIVE = 1;
    const LEVEL_DEFAULT = 1;

    protected $table = 'users';

    protected $fillable = [
        'staff_code',
        'name',
        'email',
        'phone',
        'password',
        'remember_token',
        'avatar',
        'id_card',
        'id_addr',
        'address',
        'current_address',
        'fcm_token',
        'school',
        'birthday',
        'probation_at',
        'start_date',
        'end_date',
        'contract_type',
        'status',
        'jobtitle_id',
        'position_id',
        'gmail',
        'gitlab',
        'chatwork',
        'skills',
        'in_future',
        'hobby',
        'foreign_language',
        'sex',
        'is_remote',
        'is_remote_checkin',
        'last_activity_at',
        'is_remote_checkin_vpn',
        'official_contract_date'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activate_code', 'activate_code_time', 'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * Sends the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $when = now()->addSeconds(30);
        $this->notify((new UserResetPassword($token, $this->email, $this->name))->delay($when));
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'users.' . $this->id;
    }

    /**
     * Encrypt password
     *
     * @param $value
     *
     * @author  jvb
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = strlen($value) > 50 ? $value : bcrypt($value);
    }

    public function getAvatarAttribute()
    {
        if (empty($this->attributes['avatar']))
            return URL_IMAGE_NO_AVATAR;

        return ImageFacade::getImgThumbnail($this->attributes['avatar'], -1); // avatar
    }

    public function getCanReportPrivateAttribute()
    {
        return $this->attributes['contract_type'] != USER_ROLE['intership'];
    }

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get avaiable users
     *
     * @return mixed
     */
    public function availableUsers()
    {
        return $this->where('status', ACTIVE_STATUS)
            ->orderBy('staff_code');
    }

    public function scopeAvailable($query)
    {
        return $query->where('status', ACTIVE_STATUS)
            ->whereNull('end_date');
    }

    /**
     * Get users who can approve
     *
     * @param int $minJobTitle
     *
     * @return mixed
     */
    public function approverUsers($minJobTitle = MIN_APPROVE_JOB)
    {
        return $this->where('status', ACTIVE_STATUS)->where('jobtitle_id', '>=', $minJobTitle)
            ->orderBy('staff_code');
    }

    /**
     * Get users who is probation staff
     *
     * @return mixed
     */
    public function probationUsers()
    {
        return $this->where('status', ACTIVE_STATUS)->where('contract_type', CONTRACT_TYPES['probation']);
    }

    public function team()
    {
        if (isset($this->attributes) && !empty($this->attributes)) {
            if ($this->attributes['jobtitle_id'] == TEAMLEADER_ROLE || $this->attributes['jobtitle_id'] == MANAGER_ROLE) {
                $team = Team::where('leader_id', $this->attributes['id'])->first();
                if ($team) return $team;
            }
            $userTeam = UserTeam::where('user_id', $this->attributes['id'])->orderBy('id', 'desc')->first();

            if ($userTeam) {
                return Team::where('id', $userTeam->team_id)->first();
            }
        }
    }

    public function getGroupNameAttribute()
    {
        $team = $this->team();
        return $team->group->name ??  '';
    }

    public function getGroupIdAttribute()
    {
        $team = $this->team();
        return $team->group->id ?? '';
    }

    public function getUnitNameAttribute()
    {
        if(!$this->attributes) return '';

        $groupOrTeamName = $this->groupName;
        $name = $this->attributes['name'];

        if($groupOrTeamName){
            return "$name ($groupOrTeamName)";
        }
        return $name;
    }

    public function userTeam($team_id=null){
        if ($team_id){
            return UserTeam::where('team_id',$team_id)->pluck('user_id')->toArray();
        }
    }

    public function userBlock(){
        return $this->belongsTo(UserBlock::class,'id', 'user_id');
    }
    public function blocks()
    {
        return $this->belongsToMany(Block::class,'user_blocks','user_id','block_id');
    }

    public function getCurrentBlockName()
    {
        $userInBlock = $this->blocks()->first();
        return $userInBlock ? ' ' . $userInBlock->name : '';
    }


    public function isMaster()
    {
        return $this->attributes['jobtitle_id'] == MASTER_ROLE;
    }

    public function scopeNotMaster($query)
    {
        return $query->where('jobtitle_id', '!=', MASTER_ROLE);
    }

    public function isManager()
    {
        return $this->attributes['jobtitle_id'] >= MANAGER_ROLE;
    }

    public function scopeManager($query)
    {
        return $query->where('jobtitle_id', '>=', MANAGER_ROLE);
    }

    public function isGroupManager()
    {
        return Group::where('manager_id', $this->attributes['id'])->exists();
    }

    public function groupManage()
    {
        return $this->belongsTo(Group::class, 'id', 'manager_id');
    }

    public function isTeamLeader()
    {
        return $this->attributes['jobtitle_id'] == TEAMLEADER_ROLE;
    }
    public function teamLeaderToMaster()
    {
        return $this->attributes['jobtitle_id'] >= TEAMLEADER_ROLE;
    }

    /**
     * Search for course title or subject name
     *
     * @param $query
     * @param $searchTerm
     *
     * @return mixed
     */
    public function scopeSearchByGroup($query, $searchTerm)
    {
        if ($searchTerm) {
            $teams = Team::search($searchTerm)->select('teams.id', 'leader_id')->get();
            $teamIds = $teams->pluck('id')->toArray();
            $leaderIds = $teams->pluck('leader_id')->toArray();

            if (!empty($teamIds)) {
                $userIds = UserTeam::select('user_id')
                    ->whereIn('team_id', $teamIds)
                    ->pluck('user_id')->toArray();

                $query->orWhere(function ($p) use ($userIds, $leaderIds) {
                    $p->orWhereIn('id', $userIds)
                        ->orWhereIn('id', $leaderIds);
                });
            }
        }

        return $query;
    }
    
    public function scopeSearch($query, $searchTerm)
    {
        if ($searchTerm) {
            $query = $query->where('name', 'like', '%' . $searchTerm . '%')
                ->orwhere('staff_code', 'like', '%' . $searchTerm . '%')
                ->orwhere('phone', 'like', '%' . $searchTerm . '%')
                ->orWhere('email', 'like', '%' . $searchTerm . '%')
                ->orWhere(function ($q) use ($searchTerm) {
                    $q->whereNotNull('birthday')
                        ->where(function ($p) use ($searchTerm) {
                            $p->orWhereRaw("DATE_FORMAT(birthday, '%m/%d') like '%$searchTerm'")
                                ->orWhereRaw("DATE_FORMAT(birthday, '%m-%d') like '%$searchTerm'");
                        });

                });

        }

        return $query;
    }

    public function scopeSearchDevices($query, $searchTerm)
    {
        return $query->whereHas('device_user.device', function ($query) use ($searchTerm){
            $query->where('name', 'like', '%'.$searchTerm.'%');
        })
        ->orWhere('name', 'like', '%'.$searchTerm.'%')
        ->with('device_user.device.device_name');
    }

    public function scopeActive($query)
    {
        return $query->where('users.status', ACTIVE_STATUS)->whereNull('users.end_date');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeUserReport($query)
    {
        return $query->where('users.jobtitle_id', '<', TEAMLEADER_ROLE)
            ->whereNotIn('users.position_id', DONT_REPORT_POSITION_IDS);
    }

    public function scopeSearchTeamGroup($query, $groupId, $teamId){
        if ($groupId || $teamId){
            return $query->whereHas('teams' , function ($q) use ($groupId, $teamId){
                $q->when($teamId, function ($q) use ($teamId){
                    $q->where('teams.id', (int)$teamId);
                })->when($groupId, function ($q) use ($groupId){
                    $q->where('teams.group_id', (int)$groupId);
                });
            });
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function remain_dayoff()
    {
        return $this->hasOne(RemainDayoff::class, 'id', 'user_id');
    }

    public function workTimeRegisters()
    {
        return $this->hasMany(WorkTimeRegister::class)->where('day', '!=', 7);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function unread_notifications()
    {
        return $this->hasMany(Notification::class)->whereNull('read_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function firebase_tokens()
    {
        return $this->hasMany(UserFirebaseToken::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function active_firebase_tokens()
    {
        return $this->hasMany(UserFirebaseToken::class)->where('is_disabled', NOTIFICATION_ENABLE);
    }


    public function device_user()
    {
        return $this->hasMany('App\Models\TUserDevice', 'user_id');
    }

    public function getPositionAttribute()
    {
       return POSITIONS[$this->position_id] ?? '';
    }
    
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_members');
    }

    public function getProjectNamesAttribute()
    {
        $result = [];
        foreach ($this->projects()->inProgress()->pluck('name') as $project) {
            $result[] = $project;
        }
        return \implode(', ', $result);
    }

    public function getProjectIdsAttribute()
    {
        $result = [];
        foreach ($this->projects()->inProgress()->pluck('projects.id') as $project) {
            $result[] = $project;
        }
        return \implode(', ', $result);
    }

    public function group(array $columns = ['*'])
    {
        $columns = array_map(function ($col) {
            return "g.$col";
        }, $columns);
        
        $group = \DB::table("{$this->getTable()} as u")
        ->join('user_teams as ut', 'u.id', 'ut.user_id')
        ->join('teams as t', 'ut.team_id', 't.id')
        ->join('groups as g', 't.group_id', 'g.id')
        ->where('u.id', $this->getKey())->whereNull('ut.deleted_at')->first($columns);

        return $group;
    }

    public function getGroupNamesAttribute()
    {
        return optional($this->group()->name)->name ?? '';
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class, 'user_teams')->wherePivot('deleted_at', null);
    }

    public function userTeams()
    {
        return $this->hasMany(UserTeam::class)->whereNull('deleted_at');
    }
}
