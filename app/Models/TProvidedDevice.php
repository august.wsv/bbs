<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TProvidedDevice extends Model
{
    use SoftDeletes;

    protected $table = 't_provided_devices';

    protected $fillable = [
        'id',
        'device_id',
        'role',
        'user_id',
        'specification',
        'note',
        'return_date',
        'approval_manager',
        'manager_id',
        'status',
        'approved_at',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
}
