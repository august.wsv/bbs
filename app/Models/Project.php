<?php

/**
 * ProjectModel class
 * Author: jvb
 * Date: 2019/01/31 05:00
 */

namespace App\Models;

use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Project extends Model
{
    use Notifiable, SoftDeletes, FillableFields, OrderableTrait, SearchLikeTrait;

    const UN_ACTIVE = 0;
    const IS_ACTIVE = 1;
    const LEVEL_DEFAULT = 1;

    public $autoCreator = true;

    protected $table = 'projects';

    protected $fillable = [
        'name',
        'customer',
        'project_type',
        'scale',
        'amount_of_time',
        'technical',
        'tools',
        'leader_id',
        'description',
        'start_date',
        'end_date',
        'image_url',
        'status',
        'category',
        'group_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public function getTagArrsAttribute()
    {
        if (!empty($this->attributes['tags'])) {
            return explode(',', $this->attributes['tags']);
        }
        return [];
    }

    /**
     * Search for course title or subject name
     *
     * @param $query
     * @param $searchTerm
     *
     * @return mixed
     */
    public function scopeSearch($query, $searchTerm)
    {
        return $query->where('projects.name', 'LIKE', '%' . $searchTerm . '%')
            ->orWhere('customer', 'LIKE', '%' . $searchTerm . '%')
            ->orWhere('scale', 'LIKE', '%' . $searchTerm . '%')
            ->orWhere('technical', 'LIKE', '%' . $searchTerm . '%')
            ->orWhere('tools', 'LIKE', '%' . $searchTerm . '%')
            ->orWhere('description', 'LIKE', '%' . $searchTerm . '%')
            ->orWhereHas('leader', function ($query) use ($searchTerm) {
                $query->search($searchTerm);
            });
    }

    public function scopeOfUser($query, $userIds)
    {
        $userIds = !is_array($userIds) ? [$userIds] : $userIds;
        return $query->whereIn('leader_id', $userIds)->orWhereHas('projectMembers', function ($p) use ($userIds) {
            $p->whereIn('user_id', $userIds);
        });
    }

    public function leader()
    {
        return $this->hasOne('App\Models\User', 'id', 'leader_id');
    }

    public function projectMembers()
    {
        return $this->hasMany(ProjectMember::class);
    }

    public function currentMembers()
    {
        return $this->hasMany(ProjectMember::class)->where(function ($q) {
            $q->whereRaw('Date(time_start) <= CURDATE()')->orWhereNull('time_start');
        })->where(function ($q) {
            $q->whereRaw('Date(time_end) >= CURDATE()')->orWhereNull('time_end');
        })
            ->whereHas('user', function ($q) {
                $q->active();
            });
    }

    public function projectTasks()
    {
        return $this->hasMany(ProjectTask::class);
    }

    /**
     * @return mixed
     */
    public function getLeadersProject()
    {
        return User::where('jobtitle_id', TEAMLEADER_ROLE) //Leader
            ->orWhere('jobtitle_id', MANAGER_ROLE) //Leader
            ->orderBy('jobtitle_id')
            ->get();
    }

    /**
     * getProjectCategory
     *
     * @return void
     */
    public function getProjectCategory()
    {
        return $this->select('category')->whereNotNull('category')->distinct('category')->get();
    }

    public function getGroup()
    {
        return Group::select('name', 'id')->get();
    }


    /**
     * scopeSearchOnAdmin
     *
     * @param  mixed $query
     * @param  mixed $request
     * @return void
     */
    public function scopeSearchOnAdmin($query, $request)
    {
        $status = $request->get('status');
        if (isset($status)) {
            $query = $query->where('status', $status);
        }

        $category = $request->get('category');
        if (!empty($category)) {
            $query = $query->where('category', 'LIKE', '%' . $category . '%');
        }

        $groupId = $request->get('group_id');
        if (!empty($groupId)) {
            $query = $query->where('group_id', $groupId);
        }
        if ($request->has('sort')) {
            $query->orderBy($request->get('sort'), $request->get('is_desc') ? 'asc' : 'desc');
        } else {
            $query->orderBy('id', 'desc');
        }

        return $query;
    }

    /**
     * @param $query
     * @param User $user
     * @return mixed
     */
    public function scopeInProgress($query)
    {
        return $query->where('status', PROJECT_STATUS_DOING);
    }

    public function projectSprints()
    {
        return $this->hasMany(ProjectSprint::class);
    }
}
