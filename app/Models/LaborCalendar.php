<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LaborCalendar extends Model
{
    protected $table = 'labor_calendars';
    protected $fillable =
        [
            'creator_id',
            'content',
            'note',
            'status',
            'labor_date',
            'meme_user_id',
            'meme_at',
            'meme_path',
        ];

    public function laborCalendarDetail()
    {
        return $this->hasMany('App\Models\LaborCalendarDetail', 'labor_calendar_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'labor_calendar_details', 'labor_calendar_id', 'user_id');
    }

}
