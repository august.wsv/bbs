<?php

/**
 * ProjectTaskModel class
 * Author: jvb
 * Date: 2022/08/02 16:12
 */

namespace App\Models;

use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;

class ProjectTask extends Model
{
    use FillableFields, OrderableTrait, SearchLikeTrait;

    protected $table = 'project_tasks';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'project_id',
        'user_id',
        'creator_id',
        'updator_id',
        'status',
        'type',
        'name',
        'task_id',
        'deadline',
        'progress',
        'content',
        'total_log',
        'issue',
        'created_at',
        'updated_at',
        'count_log_work',
        'sprint_id',
        'task_code',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logWork()
    {

        return $this->hasMany(TaskLogWork::class, 'task_id', 'id');
    }

    public function getTotalLogAttribute()
    {
        return $this->attributes['total_log'] = custom_total_log_work($this->attributes['total_log']);
    }

    /**
     * @param $query
     * @param User $user
     * @return mixed
     */
    public function scopeOfUserTask($query, User $user)
    {
        return $query->when($user->jobtitle_id == STAFF_ROLE, function ($query) use ($user) {
            $query->where('user_id', $user->id);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sprint(){
        return $this->belongsTo(ProjectSprint::class, 'sprint_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function project(){
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
