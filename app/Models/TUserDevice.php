<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TUserDevice extends Model
{
    protected $table = 't_user_devices';

    protected $fillable = [
        'id',
        'device_id',
        'user_id',
        'start_date',
        'end_date',
        'status',
        'is_floating',
        'is_active'
    ];

    protected $casts = [
        'specification' => 'array',
    ];


    public function scopeSearch($query, $searchTerm)
    {
        $data = $query->where(function ($q) use ($searchTerm) {
            if ($searchTerm) {
                $q = $q->orWhere('t_user_devices.user_id', 'like', '%' . $searchTerm . '%');
            }
        });
        return $data;
    }

    public function device()
    {
        return $this->belongsTo('App\Models\TDevice', 'device_id');
    }


}
