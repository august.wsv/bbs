<?php
/**
 * AdminModel class
 * Author: jvb
 * Date: 2018/09/03 01:52
 */

namespace App\Models;

use App\Traits\Models\FillableFields;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use FillableFields, SoftDeletes;

    protected $table = 'admins';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'username',
        'password',
        'remember_token',
        'admin_role'
    ];

    public function getLogoPath()
    {
        return "/adminlte/img/avatar_" . rand(1, 5) . ".png";
    }

    public function scopeSearch($query, $searchAdmin)
    {
        return $query->where(function ($q) use ($searchAdmin) {
            $q->orWhere('admins.name', 'like', '%' . $searchAdmin . '%')
                ->orWhere('admins.username', 'like', '%' . $searchAdmin . '%');
        })->select('admins.*');
    }

    public function getRoleNameAttribute()
    {
        return ROLE_NAME[$this->admin_role] ?? '';
    }

    public function getRoleDescriptionAttribute()
    {
        return ROLE_DESCRIPTION[$this->admin_role] ?? '';
    }

    public function getSuperAdminAttribute()
    {
        return $this->admin_role === SUPERADMIN_ROLE;
    }

    /**
     * Encrypt password
     *
     * @param $value
     *
     * @author  jvb
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = strlen($value) > 50 ? $value : bcrypt($value);
    }

    public function isSuperAdmin()
    {
        return $this->admin_role == SUPERADMIN_ROLE;
    }

    public function isAdmin()
    {
        return $this->admin_role == ADMIN_ROLE;
    }

    public function isGM()
    {
        return $this->admin_role == GM_ROLE;
    }

    public function isContent()
    {
        return $this->admin_role == CONTENT_ROLE;
    }

    public function isItHelpDesk()
    {
        return $this->admin_role == IT_HELP_DESK_ROLE;
    }
}
