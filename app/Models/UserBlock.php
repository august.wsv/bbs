<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;

class UserBlock extends Model
{
    use Notifiable;

    protected $table = 'user_blocks';

    protected $fillable = [
        'name',
        'block_id',
        'user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

}
