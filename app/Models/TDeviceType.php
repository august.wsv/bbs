<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDeviceType extends Model
{
    protected $table = 't_device_types';

    protected $fillable = [
        'id',
        'name',
        'description'
    ];

}
