<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDeviceStock extends Model
{
    protected $table = 't_device_stocks';

    protected $fillable = [
        'id',
        'device_id',
        'import_user_id',
        'import_quantity',
        'import_date',
        'status',
        'note',
        'stock_number'
    ];

}
