<?php


namespace App\Models;


class CalendarOffJapan extends Model
{
    const REPEAT = 1;

    protected $table = 'calendar_offs_of_japan';

    protected $fillable = [
        'id',
        'date_name',
        'date_off_from',
        'date_off_to',
        'is_repeat',
    ];
    public function getDayOffJapanInMonth($param)
    {
        return CalendarOffJapan::whereMonth('date_off_from', $param)->orwhereMonth('date_off_to', $param)->get();
    }
}
