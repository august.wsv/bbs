<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accesses extends Model
{
    protected $table = 'accesses';
    protected $primaryKey = 'id';
    protected $fillable = [
      'id',
      'user_id',
      'access_date',
      'path',
      'created_at',
      'updated_at'
    ];
}
