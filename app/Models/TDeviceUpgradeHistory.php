<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TDeviceUpgradeHistory extends Model
{
    use SoftDeletes;


    protected $table = 't_device_upgrade_histories';

    protected $fillable = [
        'id',
        'device_first_id',
        'device_last_id',
        'level_upgrade',
        'user_id',
    ];
}
