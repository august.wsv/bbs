<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDeviceTypeAttribute extends Model
{
    protected $table = 't_device_types_attributes';
    protected $fillable = [
        'id',
        'device_type_id',
        'name',
        'field_name',
        'is_required',
        'is_mutiple',
        'note'
    ];

}
