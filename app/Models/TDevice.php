<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDevice extends Model
{
    protected $table = 't_devices';
    protected $fillable = [
        'id',
        'device_type_id',
        'name',
        'specification',
        'tag',
        'note',
        'quantity',
        'available_number'
    ];

    protected $casts = [
        'specification' => 'array',
    ];

    public function device_name()
    {
        return $this->belongsTo('App\Models\TDeviceType', 'device_type_id');
    }

}
