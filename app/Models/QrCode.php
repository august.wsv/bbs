<?php

namespace App\Models;

use App\Traits\Eloquent\OrderableTrait;
use App\Traits\Eloquent\SearchLikeTrait;
use App\Traits\Models\FillableFields;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class QrCode extends Model
{
    use Notifiable, FillableFields, OrderableTrait, SearchLikeTrait;

    protected $table = 'qr_codes';

    protected $fillable = [
        'id',
        'qr_code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
