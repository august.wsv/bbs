<?php

namespace App\Console\Commands;

use App\Repositories\Contracts\IProjectRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;

class ProjectFinishCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:project-finish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Project finished';

    protected $project;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IProjectRepository $project)
    {
        $this->project = $project;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $condition = [
                ['status', '=', PROJECT_ACTIVE],
                ['end_date', '<', date('Y-m-d')],
            ];
            $this->project->multiUpdate($condition, ['status' => PROJECT_UNACTIVE]);
        } catch (\Exception $e) {
            Log::error($e);
            echo $e->getMessage() . "\n";
        }
    }
}
