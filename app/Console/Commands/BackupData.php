<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Meeting;
use App\Models\WorkTime;
use App\Models\Report;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BackupData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'backup database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentYear = Carbon::now()->subYears(1)->format('Y');
        $agoYear = Carbon::now()->subYears(2)->format('Y');
        if ($currentYear % 2 == 0) {
            // backup dữ liệu bảng work_times
            \DB::statement("CREATE TABLE work_times_$agoYear LIKE work_times");
            DB::select(DB::raw("INSERT INTO work_times_$agoYear 
                SELECT * FROM work_times 
                WHERE year(created_at) BETWEEN $agoYear AND $currentYear"));
            Worktime::whereYear('created_at', '<', Carbon::now()->year)
                ->whereYear('created_at', '>=', $agoYear)->delete();
            // backup dữ liệu bảng reports
            \DB::statement("CREATE TABLE reports_$agoYear LIKE reports");
            DB::select(DB::raw("INSERT INTO reports_$agoYear 
                SELECT * FROM reports 
                WHERE year(created_at) BETWEEN $agoYear AND $currentYear"));
            Report::whereYear('created_at', '<', Carbon::now()->year)
                ->whereYear('created_at', '>=', $agoYear)->forceDelete();
            //backup dữ liệu bảng meetings
            \DB::statement("CREATE TABLE meetings_$agoYear LIKE meetings");
            DB::select(DB::raw("INSERT INTO meetings_$agoYear 
                SELECT * FROM meetings 
                WHERE year(created_at) BETWEEN $agoYear AND $currentYear"));
            Meeting::whereYear('created_at', '<', Carbon::now()->year)
                ->whereYear('created_at', '>=', $agoYear)->delete();
        }
    }
}
