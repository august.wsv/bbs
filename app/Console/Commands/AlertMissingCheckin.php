<?php

namespace App\Console\Commands;

use App\Helpers\NotificationHelper;
use App\Models\Notification;
use App\Models\WorkTime;
use Illuminate\Console\Command;

class AlertMissingCheckin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:alert_missing_checkin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Alert Missing Checkin/Checkout';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Ignore Sat and Sun
        if (in_array(date('D'), ['Sat', 'Sun'])) {
            return;
        }

        $fromDate = date('Y/m/01');
        $toDate = date('Y/m/d', strtotime('- 1 day'));
        $workingDays = getWorkingDays($fromDate, $toDate);

        $missingRecords = WorkTime::whereIn('work_day', $workingDays)->where(function ($q) {
            $q->where(function ($p) {
                $p->whereNull('start_at')->whereNotNull('end_at');
            })->orWhere(function ($p) {
                $p->whereNull('end_at')->whereNotNull('start_at');
            });
        })->whereHas('user', function ($q) {
            $q->notMaster()->available();
        })->get();
        $notifications = [];
        $title = __l('system.worktime');
        foreach ($missingRecords as $missingRecord) {
            $content = is_null($missingRecord->start_at)
                ? __l('worktime_miss_checkin', ['date' => $missingRecord->work_day])
                : __l('worktime_miss_checkout', ['date' => $missingRecord->work_day]);
            $notifications[] =
                NotificationHelper::generateNotify(
                    $missingRecord->user_id,
                    $title,
                    $content,
                    0,
                    NOTIFICATION_TYPE['worktime'],
                    route('work_time')
                );
        }

        Notification::insertAll($notifications);
        $total = count($notifications);
        $this->info("[AlertMissingCheckin] Total $total records is missing!");
    }
}
