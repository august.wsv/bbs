<?php

namespace App\Console\Commands;

use App\Models\QrCode;
use App\Repositories\Contracts\IQrCodeRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DeleteQrCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:delete_qr_code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete user by schedule';
    protected $repository;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IQrCodeRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $condition = [
                ['created_at', '<=', $now],
            ];
            $this->repository->multiDelete($condition);
        } catch (\Exception $e) {
            Log::error($e);
            echo $e->getMessage() . "\n";
        }
    }
}
