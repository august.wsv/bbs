<?php

namespace App\Console\Commands;

use App\Models\Booking;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MeetingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meeting:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new booking follow bookings';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $bookings = Booking::where('date' <= $now->format('Y-m-d'));
        foreach ($bookings as $booking) {
            $type = $booking->repeat_type;
            $days = $booking->days_repeat;

            switch ($type) {
                case WEEKLY:
                    if ($days == $now->dayOfWeek) {
                        $add = 1;
                    }
                    break;
                case MONTHLY:
                    if ($days == $now->day) {
                        $add = 1;
                    }
                    break;
                case YEARLY:
                    if ($days == $now->format('m-d')) {
                        $add = 1;
                    }
                    break;
                
                default:
                    # code...
                    break;
            }

            if (isset($add) && $add == 1) {
                $booking = [
                    'title' => $booking->title,
                    'content' => $booking->content,
                    'users_id' => $booking->users_id,
                    'meeting_room_id' => $booking->meeting_room_id,
                    'start_time' => $booking->start_time,
                    'end_time' => $booking->end_time,
                    'date' => $now->format('Y-m-d'),
                    'participants' => $booking->participants,
                    'is_notify' => $booking->is_notify,
                    'color' => $booking->color,
                ];
                DB::table('bookings')->insert($booking);
            }
        }
    }
}
