<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class GenerateModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:make-model {name} {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Eloquent model class';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $module = $this->argument('name');
        $tableName = $this->argument('table');

        //get fillable column
        $listColumn = Schema::getColumnListing($tableName);


        $moduleUcfirst = ucfirst(camel_case($module));
        $modelTpl = $this->loadView('command.generate_normal.model', [
            'module' => $module,
            'Module' => $moduleUcfirst,
            'table' => $tableName,
            'list_column' => $listColumn
        ]);

        //create file
        //model
        file_put_contents(app_path('Models/' . $moduleUcfirst . '.php'), $modelTpl);
    }

    private function loadView($view, $data = [])
    {
        return '<?php ' . PHP_EOL . view($view, $data);
    }
}
