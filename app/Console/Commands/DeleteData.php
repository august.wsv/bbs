<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Accesses;
use App\Models\AdminLog;
use App\Models\Notification;
use Carbon\Carbon;

class DeleteData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete accesses, admin_logs, notifications 6 months ago';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $month = Carbon::now()->subMonths(6);
        Accesses::whereDate('created_at', '<', $month)->delete();
        AdminLog::whereDate('created_at', '<', $month)->delete();
        Notification::whereDate('created_at', '<', $month)->delete();
    }
}
