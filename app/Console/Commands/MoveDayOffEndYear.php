<?php

namespace App\Console\Commands;

use App\Models\RemainDayoff;
use App\Models\User;
use Illuminate\Console\Command;

class MoveDayOffEndYear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:move_day_off_end_year';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'move day off end year';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereNull('end_date')->get();
        foreach ($users as $user) {
            $dayOffRemainPreYear = RemainDayoff::where('year', '=', ((int)date('Y')) - PRE_YEAR)
                ->where('user_id', $user->id)->first();
            $dayOffRemainYear = RemainDayoff::where('year', '=', date('Y'))->where('user_id', $user->id)->first();

            //reset day off pre year
            if (isset($dayOffRemainPreYear)) {
                $dayOffRemainPreYear->remain_pre_year = DAY_OFF_DEFAULT;
                $dayOffRemainPreYear->save();
            }
            //calculate day off end year
            if (isset($dayOffRemainYear) && $dayOffRemainYear->remain > DAY_OFF_DEFAULT) {
                $dayOffRemainYear->remain_pre_year = $dayOffRemainYear->remain;
                $dayOffRemainYear->save();
            }
            // create remain day off current year
            RemainDayoff::create([
                'user_id' => $user->id,
                'year' => (int)date('Y') + NEXT_YEAR,
                'remain' => DAY_OFF_DEFAULT,
                'remain_increment' => DAY_OFF_DEFAULT,
            ]);
        }
    }
}
