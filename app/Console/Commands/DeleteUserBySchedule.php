<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteUserBySchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete user by schedule';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$month = tháng hiện tại
        $month = date('m');
        if ($month == 1) {
            // nếu tháng = 1; $month = 13
            $month = 13;
        }
        // xóa tất cả user phù hợp điều kiện status = 0 ('user đã nghỉ việc')
        User::where('status', STATUS_DELETE_USER)
            // xóa user có tháng bắt đầu nghỉ việc < tháng hiện tại
            ->whereMonth('end_date', '<', $month)
            ->update(['deleted_at' => Carbon::now()]);
    }
}
