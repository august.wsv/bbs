<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class GenerateApi extends Command
{
    const MODULE_LOWER_CASE = 'module';
    const MODULE_TITLE_CASE = 'Module';
    const TABLE = 'table';
    const ROUTE_CONFIG = '/{id}\', [
			\'uses\' => \'';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:make-api-module {module} {table} {--skip-route}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create repository contract for model';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $module = $this->argument(self::MODULE_LOWER_CASE);
        $tableName = $this->argument(self::TABLE);
        $skipRoute = $this->option('skip-route');

        //get fillable column
        $listColumn = Schema::getColumnListing($tableName);


        $moduleCamelCase = ucfirst(camel_case($module));
        $contractTpl = $this->loadView(
            'command.generate_api.contract',
            [self::MODULE_LOWER_CASE => $module, self::MODULE_TITLE_CASE => $moduleCamelCase]
        );
        $eloquentTpl = $this->loadView(
            'command.generate_api.eloquent',
            [self::MODULE_LOWER_CASE => $module, self::MODULE_TITLE_CASE => $moduleCamelCase]
        );
        $serviceContractTpl = $this->loadView(
            'command.generate_api.service_contract',
            [self::MODULE_LOWER_CASE => $module, self::MODULE_TITLE_CASE => $moduleCamelCase]
        );
        $serviceTpl = $this->loadView(
            'command.generate_api.service',
            [self::MODULE_LOWER_CASE => $module, self::MODULE_TITLE_CASE => $moduleCamelCase]
        );
        $transformerTpl = $this->loadView(
            'command.generate_api.transformer',
            [self::MODULE_LOWER_CASE => $module, self::MODULE_TITLE_CASE => $moduleCamelCase]
        );
        $modelTpl = $this->loadView(
            'command.generate_api.model',
            [
                self::MODULE_LOWER_CASE => $module,
                self::MODULE_TITLE_CASE => $moduleCamelCase,
                self::TABLE => $tableName,
                'list_column' => $listColumn
            ]
        );
        $controllerTpl = $this->loadView(
            'command.generate_api.controller',
            [self::MODULE_LOWER_CASE => $module, self::MODULE_TITLE_CASE => $moduleCamelCase, self::TABLE => $skipRoute]
        );

        //create file
        //model
        file_put_contents(app_path('Models/' . $moduleCamelCase . '.php'), $modelTpl);
        file_put_contents(app_path('Repositories/Contracts/I' . $moduleCamelCase . 'Repository.php'), $contractTpl);
        file_put_contents(app_path('Repositories/' . $moduleCamelCase . 'Repository.php'), $eloquentTpl);
        file_put_contents(app_path('Transformers/' . $moduleCamelCase . 'Transformer.php'), $transformerTpl);
        file_put_contents(app_path('Services/Contracts/I' . $moduleCamelCase . 'Service.php'), $serviceContractTpl);
        file_put_contents(app_path('Services/' . $moduleCamelCase . 'Service.php'), $serviceTpl);
        if (!$skipRoute) {
            file_put_contents(app_path('Http/Controllers/API/' . $moduleCamelCase . 'Controller.php'), $controllerTpl);
        }

        //append to RepositoryServiceProvider
        $repo = $moduleCamelCase . 'Repository';
        $stringBind = '		$this->app->bind(I' . $repo . '::class, function () {
			return new ' . $repo . '(new ' . $moduleCamelCase . '());
		});';

        $stringUse = 'use App\Repositories\Contracts' . '\I' . $repo . ';' . "\n";
        $stringUse .= 'use App\Repositories\\' . $repo . ';' . "\n";
        $stringUse .= 'use App\Models\\' . $moduleCamelCase . ';';

        $stringProvides = '			I' . $repo . '::class,';

        $filePath = app_path('Providers/RepositoriesServiceProvider.php');
        $bindMarker = '##AUTO_INSERT_BIND##';
        $useMarker = '##AUTO_INSERT_USE##';
        $provideMarker = '##AUTO_INSERT_NAME##';
        $this->insertIntoFile($filePath, $bindMarker, $stringBind, true);
        $this->insertIntoFile($filePath, $useMarker, $stringUse, true);
        $this->insertIntoFile($filePath, $provideMarker, $stringProvides, true);

        //append route
        if (!$skipRoute) {
            $routePath = app_path() . '/../' . 'routes/api.php';
            $routeMarker = '##AUTO_INSERT_ROUTE##';
            $routeInsertText = '
		//' . $module . '
		Route::get(\'' . $module . '\', [
			//\'middleware\'=>\'permission:view\',
			\'uses\' => \'' . $moduleCamelCase . 'Controller@index\'
		]);
		Route::get(\'' . $module . self::ROUTE_CONFIG . $moduleCamelCase . 'Controller@get\'
		]);
		Route::post(\'' . $module . self::ROUTE_CONFIG . $moduleCamelCase . 'Controller@add\'
		]);
		Route::put(\'' . $module . self::ROUTE_CONFIG . $moduleCamelCase . 'Controller@put\'
		]);
		Route::delete(\'' . $module . self::ROUTE_CONFIG . $moduleCamelCase . 'Controller@remove\'
		]);';
            $this->insertIntoFile($routePath, $routeMarker, $routeInsertText);
        }
    }

    private function loadView($view, $data = [])
    {
        return '<?php ' . PHP_EOL . view($view, $data);
    }

    private function insertIntoFile($filePath, $insertMarker, $text, $after = true)
    {
        $contents = file_get_contents($filePath);
        $new_contents = str_replace(
            $insertMarker,
            ($after) ? $insertMarker . "\n" . $text : $text . "\n" . $insertMarker,
            $contents
        );
        return file_put_contents($filePath, $new_contents);
    }
}
