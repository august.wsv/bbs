<?php

namespace App\Console\Commands;

use App\Models\RemainDayoff;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AddDayOffMonth extends Command
{
    private $haftOfMonth = 15;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:add_day_off_month {--month=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'plus monthly holidays';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('[add_day_off_month] Start');
        $month = $this->option('month') ?? date('m');
        if (date('d') > $this->haftOfMonth) {
            Log::info('[add_day_off_month] Over date');
            return;
        }
        $checkDate = date("Y-$month-$this->haftOfMonth");
        Log::info("[add_day_off_month] check_date $checkDate");
        $users = User::select('id', 'name', 'sex', 'start_date')
            ->whereIn('contract_type', [CONTRACT_TYPES['staff'], CONTRACT_TYPES['probation']])
            ->where('status', ACTIVE_STATUS)
            ->where(function ($q) use ($checkDate) {
                $q->whereNull('start_date')
                    ->orWhere('start_date', '<', $checkDate);
            })
            ->whereNull('end_date')
            ->get();

        $totalAdd = 0;
        $totalUpdate = 0;

        foreach ($users as $user) {
            $dayOffRemain = RemainDayoff::where('year', '=', date('Y'))
                ->where('user_id', $user->id)->first();

            if ($dayOffRemain) {
                if ($dayOffRemain->add_day_offs_month < (int)$month) {
                    $totalUpdate++;
                    if ($user->sex == SEX['female']) {
                        $dayOffRemain->day_off_free_female = REMAIN_DAY_OFF_DEFAULT;
                        $dayOffRemain->check_add_free = REMAIN_DAY_OFF_DEFAULT;
                    }
                    $dayOffRemain->remain++;
                    $dayOffRemain->remain_increment++;
                    $dayOffRemain->add_day_offs_month = (int)$month;

                    $dayOffRemain->save();
                }
            } else {
                $totalAdd++;
                $data = [
                    [
                        'user_id' => $user->id,
                        'year' => (int)date('Y'),
                        'remain' => ADD_DAY_OFF_MONTH,
                        'remain_increment' => ADD_DAY_OFF_MONTH,
                        'add_day_offs_month' => (int)$month,
                    ]
                ];

                if ($user->sex == SEX['female']) {
                    $data['day_off_free_female'] = REMAIN_DAY_OFF_DEFAULT;
                    $data['check_add_free'] = REMAIN_DAY_OFF_DEFAULT;
                }

                RemainDayoff::create($data);
            }
        }

        $logContent = "[add_day_off_month - $month] Add: $totalAdd, Update: $totalUpdate";
        Log::info($logContent);
        $this->info($logContent);

    }

}
