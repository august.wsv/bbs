<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class GenerateNormal extends Command
{
    const MODULE_LOWER_CASE = 'module';
    const MODULE_TITLE_CASE = 'Module';
    const TABLE = 'table';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:make-module {module} {table} {--skip-route}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create repository contract for model';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $module = $this->argument(self::MODULE_LOWER_CASE);
        $tableName = $this->argument(self::TABLE);
        $skipRoute = $this->option('skip-route');

        //get fillable column
        $listColumn = Schema::getColumnListing($tableName);


        $moduleUcfirst = ucfirst(camel_case($module));
        $contractTpl = $this->loadView('command.generate_normal.contract', [
            self::MODULE_LOWER_CASE => $module,
            self::MODULE_TITLE_CASE => $moduleUcfirst
        ]);
        $eloquentTpl = $this->loadView('command.generate_normal.eloquent', [
            self::MODULE_LOWER_CASE => $module,
            self::MODULE_TITLE_CASE => $moduleUcfirst
        ]);
        $serviceContractTpl = $this->loadView('command.generate_normal.service_contract', [
            self::MODULE_LOWER_CASE => $module,
            self::MODULE_TITLE_CASE => $moduleUcfirst
        ]);
        $serviceTpl = $this->loadView('command.generate_normal.service', [
            self::MODULE_LOWER_CASE => $module,
            self::MODULE_TITLE_CASE => $moduleUcfirst
        ]);
        $transformerTpl = $this->loadView('command.generate_normal.transformer', [
            self::MODULE_LOWER_CASE => $module,
            self::MODULE_TITLE_CASE => $moduleUcfirst
        ]);
        $modelTpl = $this->loadView('command.generate_normal.model', [
            self::MODULE_LOWER_CASE => $module,
            self::MODULE_TITLE_CASE => $moduleUcfirst,
            self::TABLE => $tableName, 'list_column' => $listColumn
        ]);
        $controllerTpl = $this->loadView('command.generate_normal.controller', [
            self::MODULE_LOWER_CASE => $module,
            self::MODULE_TITLE_CASE => $moduleUcfirst,
            self::TABLE => $tableName
        ]);

        //create file
        //model
        file_put_contents(app_path('Models/' . $moduleUcfirst . '.php'), $modelTpl);
        file_put_contents(app_path('Repositories/Contracts/I' . $moduleUcfirst . 'Repository.php'), $contractTpl);
        file_put_contents(app_path('Repositories/' . $moduleUcfirst . 'Repository.php'), $eloquentTpl);
        file_put_contents(app_path('Transformers/' . $moduleUcfirst . 'Transformer.php'), $transformerTpl);
        file_put_contents(app_path('Services/Contracts/I' . $moduleUcfirst . 'Service.php'), $serviceContractTpl);
        file_put_contents(app_path('Services/' . $moduleUcfirst . 'Service.php'), $serviceTpl);
        if (!$skipRoute) {
            file_put_contents(app_path('Http/Controllers/Admin/' . $moduleUcfirst . 'Controller.php'), $controllerTpl);
        }

        //append to RepositoryServiceProvider
        $repo = $moduleUcfirst . 'Repository';
        $stringBind = '		$this->app->bind(I' . $repo . '::class, function () {
			return new ' . $repo . '(new ' . $moduleUcfirst . '());
		});';

        $stringUse = 'use App\Repositories\Contracts\I' . $repo . ';\n';
        $stringUse .= 'use App\Repositories\\' . $repo . ';\n';
        $stringUse .= 'use App\Models\\' . $moduleUcfirst . ';';

        $stringProvides = '			I' . $repo . '::class,';

        $filePath = app_path('Providers/RepositoriesServiceProvider.php');
        $bindMarker = '##AUTO_INSERT_BIND##';
        $useMarker = '##AUTO_INSERT_USE##';
        $provideMarker = '##AUTO_INSERT_NAME##';
        $this->insertIntoFile($filePath, $bindMarker, $stringBind, true);
        $this->insertIntoFile($filePath, $useMarker, $stringUse, true);
        $this->insertIntoFile($filePath, $provideMarker, $stringProvides, true);

        //append route
        if (!$skipRoute) {
            $routePath = app_path() . '/../routes/admin.php';
            $routeMarker = '##AUTO_INSERT_ROUTE##';
            $routeInsertText = '
		//' . $module . '
		Route::resource(\'' . $module . '\', \'' . $moduleUcfirst . 'Controller\');
		';
            $this->insertIntoFile($routePath, $routeMarker, $routeInsertText);
        }
    }

    private function loadView($view, $data = [])
    {
        return '<?php ' . PHP_EOL . view($view, $data);
    }

    private function insertIntoFile($filePath, $insertMarker, $text, $after = true)
    {
        $contents = file_get_contents($filePath);
        $newContents = str_replace($insertMarker, ($after) ?
            $insertMarker . "\n" . $text : $text . "\n" . $insertMarker, $contents);
        return file_put_contents($filePath, $newContents);
    }
}
