<?php

namespace App\Console;

use App\Console\Commands\AddDayOffFree;
use App\Console\Commands\AddDayOffMonth;
use App\Console\Commands\AlertMissingCheckin;
use App\Console\Commands\BackupDatabase;
use App\Console\Commands\DeleteFirebaseToken;
use App\Console\Commands\DeleteUserBySchedule;
use App\Console\Commands\HolidayAutoAdd;
use App\Console\Commands\MeetingCommand;
use App\Console\Commands\MoveDayOffEndYear;
use App\Console\Commands\NotifyMSTeam;
use App\Console\Commands\PostNotificationSender;
use App\Console\Commands\SentMailEvent;
use App\Console\Commands\SummaryNotification;
use App\Console\Commands\WeeklyReportCheck;
use App\Console\Commands\WeeklyReportReminder;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\BackupData;
use App\Console\Commands\DeleteData;
use App\Console\Commands\DeleteQrCode;
use App\Console\Commands\ProjectFinishCommand;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = array(
        SentMailEvent::class,
        AddDayOffMonth::class,
        MoveDayOffEndYear::class,
        HolidayAutoAdd::class,
        PostNotificationSender::class,
        MeetingCommand::class,
        WeeklyReportCheck::class,
        WeeklyReportReminder::class,
        SummaryNotification::class,
        DeleteFirebaseToken::class,
        BackupDatabase::class,
        DeleteUserBySchedule::class,
        AlertMissingCheckin::class,
        BackupData::class,
        DeleteData::class,
        ProjectFinishCommand::class,
        DeleteQrCode::class,
    );

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('db:backup')->cron('01 6,12,18 * * *');
        // Hện thống sẽ tự động check DB và gửi mail lúc 8h sáng
        $schedule->command('command:sent_mail_event')->cron('0 8 * * *');
        //Chạy từ ngày 1->15 hàng tháng cộng 1 ngày nghỉ cho nhân viên
        $schedule->command('command:add_day_off_month')->cron('01 00 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 * *');
        //Hệ thống tự động checkDB và thêm phòng họp vào lịch lúc 05:00 hằng ngày
        $schedule->command('meeting:create')->dailyAt('00:01');
        $schedule->command('command:move_day_off_end_year')->cron('45 23 31 12 *');
        $schedule->command('command:holiday')->monthly();
        $schedule->command('cron:post-notice')->everyThirtyMinutes();
        $schedule->command('weekly-report:check')->everyMinute();
        //Nhắc nhở gửi báo cáo tuần vào lúc 17h15 ngày thứ 6 hàng tuần
        $schedule->command('weekly-report:reminder')->cron('15 17 * * 5');
        $schedule->command('notify:summary')->everyMinute();
        $schedule->command('notify:clear_old_device')->dailyAt('00:00');
        // Hệ thống tự xóa user nghỉ việc vào ngày 10 hàng tháng lúc 2h
        $schedule->command('user:delete')->monthlyOn(10, '02:00');
        $schedule->command('command:alert_missing_checkin')->cron('01 14,16 * * *');
        // Hệ thống tự xóa accesses, admin_logs, notifications của 6 tháng trước
        $schedule->command('db:delete')->monthly();
        // Hệ thống tự backup dữ liệu work_times, meetings, reports mỗi 2 năm
        $schedule->command('data:backup')->yearly();
        // Hệ thống tự check trạng thái dự án hàng ngày vào lúc 0h
        $schedule->command('command:project-finish')->dailyAt('00:00');
        // Hệ thống tự xóa các qr hết hạn vào ngày 1 lúc 7 giờ hàng tháng
        $schedule->command('command:delete_qr_code')->monthlyOn(1, '07:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
