<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectSprintRequest;
use App\Services\Contracts\IProjectSprintService;
use Illuminate\Http\Request;

class ProjectSprintController extends Controller
{
    protected $service;
    
    public function __construct(IProjectSprintService $service)
    {
        $this->service = $service;
    }
    
    public function store(ProjectSprintRequest $request)
    {
        $project = $this->service->getProjectById($request->project_id);
        $this->authorize('crudSprint', $project);

        if (is_array($message = $this->service->store($request))) {
            flash()->error($message['message']);
        } else {
            flash()->success(__l('sprint_store_success'));
        }

        return back();
    }

    public function update(ProjectSprintRequest $request)
    {
        $project = $this->service->getProjectById($request->project_id);
        $this->authorize('crudSprint', $project);

        if (is_array($message = $this->service->update($request))) {
            flash()->error($message['message']);
        } else {
            flash()->success(__l('sprint_update_success'));
        }
        
        return back();
    }

    public function delete($id)
    {
        if (is_bool($message = $this->service->delete($id))) {
            flash()->success(__l('sprint_delete_success'));
        } else {
            flash()->error($message);
        }
        
        return back();
    }
}
