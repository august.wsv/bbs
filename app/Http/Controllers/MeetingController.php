<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingMeetingRequest;
use App\Models\Booking;
use App\Models\Meeting;
use App\Models\MeetingRoom;
use App\Models\Project;
use App\Models\User;
use App\Services\Contracts\IMeetingService;
use App\Traits\RESTActions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
 *
 */
class MeetingController extends Controller
{
    use RESTActions;

    protected $meetingService;

    public function __construct(IMeetingService $meetingService)
    {
        $this->meetingService = $meetingService;
    }

    public function calendar()
    {
        $groups = $this->meetingService->getUserTree();
        $meetingRooms = MeetingRoom::all();
        $projects = Project::where('status', ACTIVE_STATUS)->get();
        return view('end_user.meeting.calendar', compact('meetingRooms', 'groups', 'projects'));
    }

    public function getCalendar(Request $request)
    {
        $start = $request->start;
        $end = $request->end;
        $results1 = $this->meetingService->getMeetings($start, $end);
        $results2 = $this->meetingService->getBookings($start, $end);
        $meetingIds = collect($results1)->pluck('meeting_id')->toArray();
        $results = $results1;
        foreach ($results2 as $item) {
            if (!in_array($item['meeting_id'], $meetingIds)) {
                $meetingIds[] = $item['meeting_id'];
                $results[] = $item;
            }
        }
        return $this->respond(['bookings' => $results]);
    }

    public function booking(Request $request)
    {
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors(), 'status' => 422], 200);
        }
        $meetingRoomId = $request->meeting_room_id;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $daysRepeat = $request->days_repeat;

        $check = $this->check($daysRepeat, $meetingRoomId, $startTime, $endTime);

        if ($check == NO_DUPLICATE) {
            $room = MeetingRoom::find($meetingRoomId);
            DB::beginTransaction();
            $date = $request->days_repeat;
            $data = [
                'users_id' => \Auth::user()->id,
                'title' => $request->title,
                'participants' => implode(',', $request->participants),
                'content' => $request->get('content'),
                'meeting_room_id' => $meetingRoomId,
                'start_time' => $request->start_time,
                'end_time' => $request->end_time,
                'date' => $date,
                'color' => $room->color,
                'is_notify' => $request->is_notify,
            ];

            $meeting = new Meeting();
            if ($request->repeat_type == NO_REPEAT) {
                $meeting->fill($data);

                $meeting->save();
            } else {
                if ($request->repeat_type == YEARLY) {
                    $daysRepeat = date('m-d', strtotime($request->days_repeat));
                } elseif ($request->repeat_type == MONTHLY) {
                    $daysRepeat = (new \Carbon($request->days_repeat))->day;
                } elseif ($request->repeat_type == WEEKLY) {
                    $daysRepeat = (new \Carbon($request->days_repeat))->dayOfWeek;
                }
                $meeting->fill($data);
                $meeting->save();
                $data['repeat_type'] = $request->repeat_type;
                $data['days_repeat'] = $daysRepeat;
                $data['meeting_id'] = $meeting->id;
                Booking::insert($data);
            }

            DB::commit();
            return response()->json(["success" => true]);
        } else {
            return response()->json(["duplicate" => true, 'status' => 500], 200);
        }
    }

    public function update(Request $request, $id)
    {
        $booking = Meeting::find($id) ?? Booking::where('meeting_id', $id)->first();

        if ($booking->users_id != Auth::id()) {
            return response()->json(["unauthorized" => true, 'status' => 500], 200);
        }
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors(), 'status' => 422], 200);
        }
        $meetingRoomId = $request->meeting_room_id;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $daysRepeat = $request->days_repeat;
        $check = $this->check($daysRepeat, $meetingRoomId, $startTime, $endTime, $id);

        if ($check == NO_DUPLICATE) {
            $date = $request->days_repeat;
            $room = MeetingRoom::find($meetingRoomId);

            $data = [
                'title' => $request->title,
                'content' => $request->get('content'),
                'participants' => implode(',', $request->participants),
                'meeting_room_id' => $request->meeting_room_id,
                'start_time' => $request->start_time,
                'end_time' => $request->end_time,
                'date' => $date,
                'color' => $room->color,
                'is_notify' => $request->is_notify,
            ];

            $meeting = Meeting::where('id', $id)->first();
            $meeting->update($data);

            if ($request->repeat_type == NO_REPEAT) {
                Booking::where('meeting_id', $id)->delete();
            } else {
                $booking = Booking::where('meeting_id', $id)->withTrashed()->first();

                if ($booking) {
                    if ($request->repeat_type == YEARLY) {
                        $daysRepeat = date('m-d', strtotime($request->days_repeat));
                    } elseif ($request->repeat_type == MONTHLY) {
                        $daysRepeat = (new \Carbon($request->days_repeat))->day;
                    } elseif ($request->repeat_type == WEEKLY) {
                        $daysRepeat = (new \Carbon($request->days_repeat))->dayOfWeek;
                    }
                    $data['repeat_type'] = $request->repeat_type;
                    $data['days_repeat'] = $daysRepeat;
                    $data['deleted_at'] = null;
                    $booking->fill($data);
                    $booking->save();
                }
            }
            return response()->json(["success" => true]);
        } else {
            return response()->json(["duplicate" => true, 'status' => 500], 200);
        }
    }

    public function getMeeting(Request $request)
    {
        $id = $request->id;
        $booking_id = $request->booking_id;
        $meetingRoomId = $request->meeting_room_id;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $condition1 = [
            'id' => $id,
        ];
        $condition2 = [
            'id' => $booking_id,
            'meeting_room_id' => $meetingRoomId,
            'start_time' => $startTime,
            'end_time' => $endTime,
        ];
        $booking = Booking::where($condition2)->with('creator:id,name')->first();
        if (!$booking) {
            $booking = Meeting::where($condition1)->with('creator:id,name')->first();
            if ($booking) {
                $booking->booking_id = $booking_id;
            }
        } else {
            $booking->booking_id = $booking->id;
            $booking->id = $booking->meeting_id;
        }

        if ($booking) {
            $participantIds = $this->meetingService->getParticipantIds($booking);
            $users = User::whereIn('id', $participantIds)->active()
                ->orderBy('jobtitle_id', 'desc')->orderBy('staff_code')->pluck('name')->toArray();
            $meeting = MeetingRoom::find($meetingRoomId)->name ?? '';
            return response()->json(["booking" => $booking, "participants" => $users, "meeting" => $meeting]);
        }
    }

    public function deleteMeeting(Request $request)
    {
        if ($request->has('booking_id')) {
            Booking::where('id', $request->booking_id)->forceDelete();
        }
        $meeting = Meeting::find($request->meeting_id);

        if ($meeting && $meeting->users_id == Auth::id() && $meeting->date >= date(DATE_FORMAT)) {
            $meeting->delete();
        }
        return response()->json(["messages" => "success"]);
    }

    public function check($daysRepeat, $meetingRoomId, $startTime, $endTime, $id = null)
    {
        $date = $daysRepeat;
        $dateMonth = date('m-d', strtotime($daysRepeat));
        $day = (new \Carbon($daysRepeat))->day;
        $dayOfWeek = (new \Carbon($daysRepeat))->dayOfWeek;

        $check = NO_DUPLICATE;

        $meetingDefault = [['id', '<>', $id], ['meeting_room_id', $meetingRoomId]];
        $bookingDefault = [['meeting_id', '<>', $id], ['meeting_room_id', $meetingRoomId]];

        // check theo lich khong lap
        $booking = [];
        $meetingModel = new Meeting();
        $booking[0] = $meetingModel->where('date', $date)->where($meetingDefault);
        $model = Booking::where($bookingDefault);
        // check lich tuan
        $booking[1] = clone $model->where('repeat_type', WEEKLY)->where('days_repeat', $dayOfWeek);

        //check lich theo thang
        $booking[2] = clone $model->where('repeat_type', MONTHLY)->where('days_repeat', $day);

        //check lich theo nam
        $booking[3] = clone $model->where('repeat_type', YEARLY)->where('days_repeat', $dateMonth);

        for ($i = 0; $i < 4; $i++) {
            $bookings = $booking[$i];
            if (count($bookings->get()) > 0) {
                $bookings->where(function ($q) use ($startTime, $endTime) {
                    $q->where('start_time', '>', $startTime)->where('start_time', '<', $endTime)
                        ->orWhere('start_time', '<=', $startTime)->where('end_time', '>=', $endTime)
                        ->orWhere('end_time', '>', $startTime)->where('end_time', '<', $endTime);
                })->get();
                if ($bookings->count() > 0) {
                    return DUPLICATE;
                } else {
                    $check = NO_DUPLICATE;
                }
            } else {
                $check = NO_DUPLICATE;
            }
        }
        return $check;
    }

    private function validateRequest(Request $request)
    {
        $validateRequest = new BookingMeetingRequest();
        return Validator::make(
            $request->all(),
            $validateRequest->rules(),
            $validateRequest->messages(),
            $validateRequest->attributes()
        );
    }
}
