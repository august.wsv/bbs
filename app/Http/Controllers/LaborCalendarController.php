<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\ILaborCalendarRepository;
use App\Services\Contracts\ILaborCalendarService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LaborCalendarController extends Controller
{
    public $repository;
    public $service;

    public function __construct(ILaborCalendarRepository $repository, ILaborCalendarService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $month = Carbon::now()->month;
        $userID = Auth::guard('web')->user()->id;
        if ($request->has('month')) {
            $month = $request->get('month');
        }
        $listLaborCalendars = $this->repository->getListLaborCalendar($month, $userID);
        $timeSearch = Carbon::parse(Carbon::now()->year . '/' . $month . '/01');
        $workContent = $this->repository->getWorkContent($month);
        return view(
            'end_user.labor_calendar.index',
            compact(
                'month',
                'listLaborCalendars',
                'timeSearch',
                'workContent'
            )
        );
    }

    public function changeMeme(Request $request)
    {
        if (!$request->has('image')) {
            return response()->json(['status' => false, 'message' => 'image is required']);
        }
        $dayLaborCalendar = $this->repository->getTodayInfo();
        if (!$dayLaborCalendar) {
            return response()->json(['status' => false, 'message' => 'Labor is not existed']);
        }
        // upload meme image
        $date = date('Y/m');
        $memePath = $request->file('image')->store("$date/labor", 'images');

        $dayLaborCalendar->meme_path = image_path('/' . $memePath);
        $dayLaborCalendar->meme_user_id = Auth::id();
        $dayLaborCalendar->meme_at = Carbon::now();
        $dayLaborCalendar->save();

        return response()->json(['status' => true, 'path' => $dayLaborCalendar->meme_path]);
    }
}
