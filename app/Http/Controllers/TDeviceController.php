<?php

namespace App\Http\Controllers;

use App\Models\ProvidedDevice;
use App\Models\TDevice;
use App\Models\TDeviceType;
use App\Models\TDeviceTypeAttribute;
use App\Models\TImportDevice;
use App\Models\TUserDevice;
use App\Models\User;
use App\Services\DeviceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TDeviceController extends Controller
{
    protected $service;

    public function __construct(DeviceService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $optionDevices = TImportDevice::all();
        $users = User::all();
        $deviceAttributes = TDeviceTypeAttribute::all();
        $typeDevices = TDeviceType::all();
        $devices = $this->service->getOptionDevices();

        $response = [
            'users' => $users,
            'devices' => $devices,
            'device_attributes' => $deviceAttributes,
            'type_devices' => $typeDevices,
            'option_devices' => $optionDevices
        ];

        return view('end_user.t_device.index', $response);
    }

    public function deletes($id)
    {
        $deviceUser = TUserDevice::find($id);
        $device = TDevice::find($deviceUser->device_id);
        $deviceImport = TImportDevice::find($device->import_device_id);
        if ($deviceImport) {
            $deviceImportUpdate = TImportDevice::find($device->import_device_id);
            $deviceImportUpdate->quantity = $deviceImport->quantity + 1;
            $deviceImportUpdate->save();
        }
        $deviceUser->delete();
        return redirect()->back()->with('message', 'Bạn đã thu hồi thiết bị thành công!');
    }

    public function list(Request $request)
    {
        $user = Auth::user();
        if ($user->jobtitle_id == MASTER_ROLE) {
            $providedDevice = ProvidedDevice::whereNull('deleted_at');
        } elseif ($user->jobtitle_id == MANAGER_ROLE) {
            $providedDevice = ProvidedDevice::where('manager_id', Auth::id());
        } else {
            $providedDevice = ProvidedDevice::where('user_id', Auth::id());
        }
        $perPage = $request->get('page_size', DEFAULT_PAGE_SIZE);

        $providedDevice = $providedDevice->orderBy('id', 'desc')->paginate($perPage);

        $devices = $this->service->getDeviceDetailByUserId(Auth::user()->id);

        $response = ['devices' => $devices, 'providedDevice' => $providedDevice, 'perPage' => $perPage];

        return view('end_user.t_device.list', $response);
    }

    public function checkRoute()
    {
        $deviceUser = count(TUserDevice::where('user_id', Auth::user()->id)->get());
        if ($deviceUser > 0) {
            return redirect()->route('t_devices.t_list_device');
        } else {
            return redirect()->route('t_device.index');
        }
    }

    public function saveFormAdd(Request $request)
    {
        $devices = $this->service->saveFormDeviceService($request);
        if ($devices) {
            return redirect()->route('t_devices.t_list_device')
                ->with('message', 'Bạn đã khai báo thiết bị thành công!');
        }
        abort(404);
    }

    public function getDevice()
    {
        return $this->service->getDeviceOptions();
    }

    public function allDeviceImport()
    {
        return $this->service->allDeviceImportByService();
    }

    public function getDeviceImportById($id)
    {
        return TImportDevice::find($id);
    }

    public function getAttributeDevice($id)
    {
        return $this->service->getAttributeByTypeId($id);
    }
}
