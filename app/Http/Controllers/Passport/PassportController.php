<?php

namespace App\Http\Controllers\Passport;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PassportController extends Controller
{
    public $successStatus = 200;

    /**
     * get User
     */
    public function getGitlabUser()
    {
        $user = auth('passport')->user();

        return response()->json(
            $user,
            $this->successStatus
        );
    }

    /**
     * get User
     */
    public function getUser()
    {
        $user = auth('passport')->user();

        return response()->json(
            [
                'user' => $user
            ],
            $this->successStatus
        );
    }

    /**
     * get Ticket User
     */
    public function ticketUser()
    {
        $user = auth('passport')->user();
        if (!$user) {
            return response()->json(
                ['success' => false]
            );
        }
        $user->username = $user->email;
        $user->displayName = $user->name;

        return response()->json(
            $user,
            $this->successStatus
        );
    }
}
