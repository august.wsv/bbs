<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReportRequest;
use App\Http\Requests\ReplyReportRequest;
use App\Models\Report;
use App\Models\Team;
use App\Repositories\Contracts\IReportRepository;
use App\Services\Contracts\IReportService;
use App\Services\NotificationService;
use App\Traits\RESTActions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    use RESTActions;

    private $reportRepository;
    /**
     * @var NotificationService
     */
    private $notificationService;

    public function __construct(IReportService $service, IReportRepository $reportRepository, NotificationService $notificationService)
    {
        $this->service = $service;
        $this->reportRepository = $reportRepository;
        $this->notificationService = $notificationService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $reports = $this->service->search($request, $perPage, $search, $teamId);
        $teams = Team::pluck('name', 'id')->toArray();

        return view('end_user.report.index', compact('reports', 'search', 'perPage', 'data', 'teams', 'teamId'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $report = $this->service->getFromDraftOrTemplate();
        $receivers = $this->service->getReportReceiver();
        return view('end_user.report.create', compact('report', 'receivers'));
    }

    /**
     * @param $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReport(Request $request)
    {
        return $this->respond([
            'report' => $this->service->detail($request->get('id'))
        ]);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $report = $this->service->detail($id);

        if ($report) {
            return view('end_user.report.detail', compact('report'));
        }
        abort(404);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statistic()
    {
        return view('end_user.report.statistic');
    }

    /**
     * @param CreateReportRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveReport(CreateReportRequest $request)
    {
        $data = $request->all();
        $report = $this->service->create($request);
        if ($report) {
            if (!$data['is_new']) {
                flash()->success(__l('report_resent_successully'));
            } else if ($data['status'] == 0) {
                flash()->success(__l('report_drafted_successully'));
            } else {
                flash()->success(__l('report_created_successully'));
            }
        }

        return redirect(route('report'));
    }

    public function replyReport(ReplyReportRequest $request)
    {
        return $this->respond(['success' => $this->service->replyReport($request->report_id, $request)]);
    }

    public function deleteReport($id)
    {
        $report = Report::find($id);
        if ($report && Auth::user()->can('delete', $report)) {
            $report->delete();
            flash()->success(__l('report_deleted'));
        }
        return redirect(route('report'));
    }

}
