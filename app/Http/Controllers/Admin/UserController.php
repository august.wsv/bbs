<?php

namespace App\Http\Controllers\Admin;

use App\Models\RemainDayoff;
use App\Models\User;
use App\Models\UserTeam;
use App\Repositories\Contracts\IUserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * UserController
 * Author: jvb
 * Date: 2018/07/16 10:24
 */
class UserController extends AdminBaseController
{
    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.users';

    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::users';

    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = User::class;

    /**
     * @var  string
     */
    protected $resourceTitle = 'Nhân viên';

    protected $resourceSearchExtend = 'admin.users._search_extend';

    public function __construct(IUserRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'filled|max:255',
                'email' => 'email|unique:users,email',
                'staff_code' => 'filled|max:10|unique:users,staff_code,NULL,NULL,deleted_at,NULL',
                'birthday' => 'nullable|date|before:' . date('Y-m-d', strtotime('- 15 years')),
                'phone' => 'required|numeric|digits_between:10,30|unique:users,phone',
                'id_card' => 'required|digits_between:9,12|unique:users,id_card,NULL,id,deleted_at,NULL|numeric',
                'password' => 'required|min:8',
                'password_confirmation' => 'required|same:password',
                'start_date' => 'required|date|before_or_equal:today',
                'end_date' => "nullable|date|after:start_date",
                'sex' => "required|digits_between:0,1",
                'official_contract_date' => 'nullable|date'

            ],
            'messages' => [],
            'attributes' => [
                'phone' => 'số điện thoại',
                'start_date' => 'ngày vào công ty',
                'end_date' => 'ngày nghỉ việc',
                'staff_code' => 'mã nhân viên',
                'sex' => 'giới tính',
                'official_contract_date' => 'ngày kí hợp đồng chính thức'

            ],
            'advanced' => [],
        ];
    }

    public function resourceUpdateValidationData($record)
    {

        return [
            'rules' => [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users,email,' . $record->id,
                'staff_code' => 'filled|max:10|unique:users,staff_code,' . $record->id,
                'birthday' => 'nullable|date|before:' . date('Y-m-d', strtotime('- 15 years')),
                'phone' => 'nullable|numeric|digits_between:10,30|unique:users,phone,' . $record->id,
                'id_card' => 'required|digits_between:9,12|unique:users,id_card,' . $record->id,
                'password' => 'nullable|min:8',
                'password_confirmation' => 'same:password',
                'start_date' => 'required|date|before_or_equal:today',
                'end_date' => "nullable|date|after:start_date",
                'sex' => "required|digits_between:0,1",
                'official_contract_date' => 'nullable|date'

            ],
            'messages' => [],
            'attributes' => [
                'phone' => 'số điện thoại',
                'start_date' => 'ngày vào công ty',
                'end_date' => 'ngày nghỉ việc',
                'staff_code' => 'mã nhân viên',
                'sex' => 'giới tính',
                'official_contract_date' => 'ngày kí hợp đồng chính thức'

            ],
            'advanced' => [],
        ];
    }

    public function getSearchRecords(Request $request, $perPage = 15, $search = null)
    {
        $model = $this->getResourceModel()::search($search);

        $jobtitleId = $request->get('jobtitle');
        if (isset($jobtitleId)) {
            $model = $model->where('jobtitle_id', $jobtitleId);
        }
        $positionId = $request->get('position');

        if (isset($positionId)) {
            $model = $model->where('position_id', $positionId);
        }
        $contractType = $request->get('contract_type');
        if (isset($contract_type)) {
            $model = $model->where('contract_type', $contractType);
        }

        $isRemoteCheckin = $request->get('is_remote_checkin');
        if (isset($isRemoteCheckin)) {
            $model = $model->where('is_remote_checkin', $isRemoteCheckin);
        }

        if ($request->has('sort')) {
            $model->orderBy($request->get('sort'), $request->get('is_desc') ? 'asc' : 'desc');
        } else {
            $model->orderBy('id', 'desc');
        }

        return $model->paginate($perPage);
    }

    public function resetPassword(Request $request)
    {
        $isAll = $request->get('is_all', false);
        if ($isAll) {
            $users = User::all();
        } else {
            $this->validate($request, ['user_ids' => 'required']);

            $userIds = $request->get('user_ids');
            if (!is_array($userIds)) {
                $userIds = [$userIds];
            }
            $users = User::whereIn('id', $userIds)->get();
        }
        if ($users->isNotEmpty()) {
            foreach ($users as $user) {
                $user->password = $user->staff_code;
                $user->save();
            }
            flash()->success('Reset mật khẩu thành công');
        } else {
            flash()->error('Không tìm thấy nhân viên');
        }
        return redirect(route('admin::users.index'));
    }

    public function getRedirectAfterSave($record, $request, $isCreate = true)
    {
        $isFemaleStaff = $record->sex == SEX['female'] && $record->contract_type == CONTRACT_TYPES['staff'];
        $isActiveFemaleStaff = $isFemaleStaff && $record->status == ACTIVE_STATUS;

        if ($isCreate) {
            $dayOff = new RemainDayoff();
            $dayOff->user_id = $record->id;
            $day = Carbon::createFromFormat('Y-m-d', $request->start_date)->day;

            if ($day <= HALF_MONTH && $isActiveFemaleStaff) {
                $dayOff->day_off_free_female = REMAIN_DAY_OFF_DEFAULT;
            } else {
                $dayOff->day_off_free_female = DEFAULT_VALUE;
            }
            
            $dayOff->remain = DEFAULT_VALUE;
            $dayOff->remain_increment = DEFAULT_VALUE;
            $dayOff->year = date('Y');
            $dayOff->save();
        } else {
            if ($request->status == ACTIVE_STATUS) {
                $record->end_date = null;
                $record->save();
            }
            $dayOff = RemainDayoff::where('user_id', $record->id)->where('year', date('Y'))->first();
            if ($dayOff) {
                $day = (int)date('d');
                if ($day <= HALF_MONTH && $isActiveFemaleStaff && $dayOff->check_add_free == DEFAULT_VALUE) {
                    $dayOff->day_off_free_female = REMAIN_DAY_OFF_DEFAULT;
                    $dayOff->check_add_free = REMAIN_DAY_OFF_DEFAULT;
                }
                $dayOff->save();
            }
        }
        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }

    public function getValuesToSave(Request $request, $record = null)
    {
        if (!$request->password) {
            $request->offsetUnset('password');
        }
        if (!isset($request->status)) {
            $request->merge(['status' => '0']);
        }
        if (!isset($request->is_remote)) {
            $request->merge(['is_remote' => '0']);
        }
        if (!isset($request->is_remote_checkin)) {
            $request->merge(['is_remote_checkin' => '0']);
        }
        if (!isset($request->is_remote_checkin_vpn)) {
            $request->merge(['is_remote_checkin_vpn' => '0']);
        }
        return $request->only($this->getResourceModel()::getFillableFields());
    }
}
