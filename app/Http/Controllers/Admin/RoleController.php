<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RoleRequest;
use App\Models\Admin;
use App\Repositories\Contracts\IAdminRepository;
use App\Repositories\Contracts\IBaseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends AdminBaseController
{
    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.role';

    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::role';

    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = Admin::class;

    /**
     * @var  string
     */
    protected $resourceTitle = 'Phân quyền';

    /**
     * Controller construct
     */
    public function __construct(IAdminRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function getValuesToSave(Request $request, $record = null)
    {
        if (!$request->password) {
            $request->offsetUnset('password');
        }
        return $request->only($this->getResourceModel()::getFillableFields());
    }

    public function resourceStoreValidationData()
    {
        return $this->validationData();
    }

    public function resourceUpdateValidationData($record)
    {
        return $this->validationData($record->id);
    }

    private function validationData($recordId = null)
    {
        $questionRequest = new RoleRequest();
        return [
            'rules' => $questionRequest->rules($recordId),
            'messages' => $questionRequest->messages(),
            'attributes' => $questionRequest->attributes(),
            'advanced' => [],
        ];
    }

    public function deletes(Request $request)
    {
        $this->authorize('deletes');
        $this->validate($request, [
            'ids' => 'required|array'
        ]);
        $ids = $request->get('ids');
        if ($this->repository->multiDelete([[
            function ($q) use ($ids) {
                $q->whereIn('id', $ids)->where('admin_role', '!=', SUPERADMIN_ROLE);
            }
        ]])) {
            flash()->success('Đã xóa những bản ghi được chọn.');
        } else {
            flash()->info('Không xóa được những bản ghi được chọn');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }
}
