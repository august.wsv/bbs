<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TDeviceType;
use App\Models\TDeviceTypeAttribute;
use App\Models\TUserDevice;
use App\Models\User;
use App\Repositories\Contracts\IDeviceRepository;
use App\Services\DeviceService;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DeviceExport;
use App\Exports\TDeviceExport;
use App\Models\TDevice;
use App\Models\TImportDevice;
use Maatwebsite\Excel\Excel as ExcelExcel;
use App\Exports\DeviceAlmostExpireExport;
use App\Http\Requests\SaveDeviceImportRequest;
use App\Http\Requests\SaveFormAddRequest;

class TDeviceController extends Controller
{
    protected $resourceAlias = 'admin.t_devices';

    protected $resourceRoutesAlias = 'admin::t_devices';

    protected $resourceTitle = 'Thiết bị';

    protected $resourceModel = TUserDevice::class;

    protected $service;

    public function __construct(IDeviceRepository $repository, DeviceService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function listUserDevice(Request $request, $device = null, $role = null)
    {
        $devices = $this->service->getDevicesService($request, $device, $role);
        return view('admin.t_devices.t_list_devices', ['devices' => $devices, 'role' => $role, 'device' => $device]);
    }

    public function getDeviceImportById($id)
    {
        return TImportDevice::find($id);
    }

    public function allDeviceImport()
    {
        return $this->service->allDeviceImportByService();
    }

    public function addDevice($userId)
    {
        $optionDevices = TImportDevice::all();
        $users = User::all();
        $deviceAttributes = TDeviceTypeAttribute::all();
        $typeDevices = TDeviceType::all();
        $devices = $this->service->getOptionDevices();

        $response = [
            'users' => $users,
            'devices' => $devices,
            'device_attributes' => $deviceAttributes,
            'type_devices' => $typeDevices,
            'option_devices' => $optionDevices,
            'user_id' => $userId
        ];

        return view('admin.t_devices.t_form_add', $response);
    }

    public function saveFormAdd(SaveFormAddRequest $request)
    {
        $devices = $this->service->saveFormDeviceService($request);
        if ($devices) {
            return redirect()->route('admin::t_devices.t_list_devices')
                ->with('message', 'Bạn đã thêm mới thiết bị thành công!');
        }
        abort(404);
    }

    public function detailDevice($id)
    {

        $user = $this->service->getDeviceDetailByUserId($id);
        return view('admin.t_devices.t_detail_devices', ['user' => $user]);
    }

    public function getDevice()
    {
        return $this->service->getDeviceOptions();
    }

    public function getAttributeDevice($id)
    {
        return $this->service->getAttributeByTypeId($id);
    }

    public function removeAttrDevice($id)
    {
        TDeviceTypeAttribute::find($id)->delete();
        return redirect()->back()->with('message', 'Bạn đã xóa thuộc tính thành công');
    }


    public function deletes($id)
    {
        $deviceUser = TUserDevice::find($id);
        $device = TDevice::find($deviceUser->device_id);
        $deviceImport = TImportDevice::find($device->import_device_id);
        if ($deviceImport) {
            $deviceImportUpdate = TImportDevice::find($device->import_device_id);
            $deviceImportUpdate->quantity = $deviceImport->quantity + 1;
            $deviceImportUpdate->save();
        }
        $deviceUser->delete();
        return redirect()->back()->with('message', 'Bạn đã thu hồi thiết bị thành công!');
    }

    public function updateDevcie($id, $role)
    {
        $deviceUpgrade = $this->service->updateDeviceService($id, $role);
        $deviceImport = TImportDevice::where('device_type_id', $deviceUpgrade->device->device_type_id)->get();

        $response = ['device_upgrade' => $deviceUpgrade, 'role' => $role, 'device_import' => $deviceImport];

        return view('admin.t_devices.t_device_edit', $response);
    }

    public function saveFormUpgrade(Request $request, $id, $role)
    {
        if ($role == 'UPGRADE') {
            $message = 'Bạn đã nâng cấp thiết bị thành công!';
        } else {
            $message = 'Bạn đã đổi thiết bị thành công!';
        }
        $deviceUpgrade = $this->service->saveFormUpgradeService($request, $id, $role);
        if ($deviceUpgrade) {
            return redirect()->route('admin::t_devices.t_list_devices')->with('message', $message);
        }
        abort(404);
    }

    public function export(TDeviceExport $export)
    {
        $date = date('d-m-y');
        return Excel::download(new TDeviceExport, $date . '-' . 'devices.xlsx');
    }

    // attribute

    public function listDeviceAttribute()
    {
        $deviceAttributes = $this->service->getListDeviceAttribute();
        return view('admin.t_devices.t_list_attribute', ['device_attributes' => $deviceAttributes]);
    }

    public function addAttributeDevice()
    {
        return view('admin.t_devices.form-add-attribute');
    }

    public function saveFormAttribute(Request $request)
    {
        $this->service->saveFormAttributeService($request);
        return redirect()->route('admin::t_devices.attribute')->with('message', 'Bạn đã thêm thiết bị thành công');
    }

    public function addAttrForm($id)
    {
        $device = TDeviceType::find($id);
        $deviceAttributes = TDeviceTypeAttribute::where('device_type_id', $device->id)->get()->toArray();
        return view('admin.t_devices.add-attribute', ['device' => $device, 'device_attributes' => $deviceAttributes]);
    }


    public function saveAddAttr(Request $request, $id)
    {
        $this->service->saveAddAttrInSerevice($request, $id);
        return redirect()->back()->with('message', 'Bạn đã sửa thiết bị thành công');
    }


    public function listImportDevice(Request $request, $device = null)
    {
        $devices = $this->service->getListImportDevice($request, $device);
        return view('admin.t_devices.list_import', ['devices' => $devices]);
    }

    public function detailDeviceImport($id)
    {
        $deviceImport = $this->service->getDetailDeviceImport($id);
        return view('admin.t_devices.detail_import_device', ['device_import' => $deviceImport]);
    }

    public function thanhLy($id, Request $request)
    {
        if ($this->service->thanhLyThietBi($id, $request)) {
            return redirect()->route('admin::t_devices.t_import_device_index')
                ->with('message', 'Bạn đã thanh lý thiết bị thành công');
        } else {
            return redirect()->route('admin::t_devices.t_import_device_index')
                ->with('error', 'Thanh lý thất bại! Vui lòng thử lại!');
        }
    }

    public function removeImportDevice($id)
    {
        $this->service->removeImportDeviceService($id);
        return redirect()->route('admin::t_devices.t_import_device_index')
            ->with('message', 'Bạn đã xóa thiết bị thành công');
    }

    public function addDeviceImport()
    {
        return view('admin.t_devices.add_device_import');
    }

    public function saveDeviceImport(SaveDeviceImportRequest $request)
    {
        $this->service->saveDeviceImportInService($request);
        return redirect()->route('admin::t_devices.t_import_device_index')
            ->with('message', 'Bạn đã thêm thiết bị thành công');
    }

    public function editNameImportDevice(Request $request)
    {
        $this->service->editNameImportDeviceByService($request);
        return redirect()->route('admin::t_devices.t_import_device_index')
            ->with('message', 'Bạn đã sửa thiết bị thành công');
    }

    public function exportData(Request $request, $device = null)
    {
        try {
            $devices = $this->service->getListAlmostExpireDevice();
            return Excel::download(new DeviceAlmostExpireExport($devices), "device-almost-expire.xlsx");
        } catch (\Exception $e) {
            abort(404);
        }
    }

    public function listUser(Request $request)
    {
        $importDeviceID = $request->get('import_device_id');
        try {
            $listUserUseDevice = $this->service->listUserUseDevice($importDeviceID);
            $tableUser = view('admin.t_devices.t_device_list_user', compact('listUserUseDevice'))->render();
            return response()->json(['status' => 200, 'table_user' => $tableUser]);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return response()->json([
                'code' => 500
            ]);
        }
    }
}
