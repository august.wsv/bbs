<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Punishes;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * AdminController
 * Author: jvb
 * Date: 2018/09/03 01:52
 */
class MasterController extends Controller
{
    /**
     * Controller construct
     */
    public function __construct()
    {
    }

    public function index()
    {
        $probationStaffs = (new User())->probationUsers()->paginate(8);
        $events = Event::where('status', ACTIVE_STATUS)->orderBy('id', 'desc')->get();
        $haftYear = Carbon::now()->subMonth(6);
        $punishes = Punishes::select(
            DB::raw('SUM(total_money) as total'),
            DB::raw('DATE_FORMAT(infringe_date, "%m") as month')
        )
            ->whereDate('infringe_date', '>=', $haftYear)
            ->groupBy('month')
            ->orderBy('infringe_date')
            ->get();

        $userCount = \App\Models\User::where('status', ACTIVE_STATUS)->count();

        $totalLate = Punishes::select('total_money')
            ->where('rule_id', LATE_RULE_ID)
            ->whereDate('infringe_date', '>=', $haftYear)
            ->sum('total_money');

        $totalReport = Punishes::select('total_money')
            ->where('rule_id', WEEKLY_REPORT_RULE_ID)
            ->whereDate('infringe_date', '>=', $haftYear)
            ->sum('total_money');

        $totalSubmit = Punishes::select('total_money')
            ->where('is_submit', PUNISH_SUBMIT['submitted'])
            ->whereDate('infringe_date', '>=', $haftYear)
            ->sum('total_money');

        return view(
            'admin.master',
            compact('probationStaffs', 'events', 'punishes', 'totalLate', 'totalReport', 'totalSubmit', 'userCount')
        );
    }

    public function download(Request $request)
    {
        if (Storage::exists($request->file_path))
            return Storage::download($request->file_path);
    }
}
