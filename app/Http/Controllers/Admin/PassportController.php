<?php

namespace App\Http\Controllers\Admin;

use App\Models\OAuthClient;
use App\Repositories\Contracts\IOAuthClientRepository;

class PassportController extends AdminBaseController
{
    //
    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.passport';
    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::passport';
    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = OAuthClient::class;

    /**
     * @var  string
     */
    protected $resourceTitle = 'Quản lí Client Passport';
    
    public function __construct(IOAuthClientRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required|max:255',
                'redirect' => 'required|url',
            ],
            'messages' => [],
            'attributes' => [
                'name' => 'tên client',
                'redirect' => 'Đường dẫn chuyển hướng'
            ],
            'advanced' => [],
        ];
    }

    public function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required|max:255',
                'redirect' => 'required|url',
            ],
            'messages' => [],
            'attributes' => [
                'name' => 'tên client',
                'redirect' => 'Đường dẫn chuyển hướng'
            ],
            'advanced' => [],
        ];
    }
}
