<?php

namespace App\Http\Controllers\Admin;

use App\Models\Quiz;
use App\Repositories\Contracts\IQuizRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\QuizResquest;
use App\Services\Contracts\IQuizService;
use App\Services\UserService;
use App\Services\Contracts\IMeetingService;

class QuizController extends AdminBaseController
{

    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.quizzes';

    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::quizzes';

    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = Quiz::class;

    /**
     * @var  string
     */
    protected $resourceTitle = 'Quiz';

    public $repository;
    protected $service;
    protected $meetingService;
    protected $userService;

    public function __construct(
        IQuizRepository $repository,
        IQuizService $service,
        IMeetingService $meetingService,
        UserService $userService
    ) {
        $this->repository = $repository;
        $this->service = $service;
        $this->meetingService = $meetingService;
        $this->userService = $userService;
        parent::__construct();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function resourceUpdateValidationData($record)
    {
        return $this->validationData();
    }

    /**
     * Validate request
     *
     * @return array rules request must follow
     */
    private function validationData()
    {
        $questionRequest = new QuizResquest();
        return [
            'rules' => $questionRequest->rules(),
            'messages' => $questionRequest->messages(),
            'attributes' => $questionRequest->attributes(),
            'advanced' => [],
        ];
    }

    /**
     * validating the resource before storing to db
     *
     * @return array rules to follow
     */
    public function resourceStoreValidationData()
    {
        return $this->validationData();
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewList', $this->getResourceModel());
        if ($request->has('is_export') && in_array($request->path(), EXPORT_PATHS)) {
            return $this->exportData($request);
        } else {
            $records = $this->searchRecords($request, $perPage, $search);
            foreach ($records as $quiz) {
                $participants = $this->service->getParticipants($quiz->participants);
                $quiz->participants = $participants;
            }
            return view(
                $this->getResourceIndexPath(),
                $this->filterSearchViewData($request, [
                    'records' => $records,
                    'search' => $search,
                    'resourceAlias' => $this->getResourceAlias(),
                    'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
                    'resourceTitle' => $this->getResourceTitle(),
                    'perPage' => $perPage,
                    'resourceSearchExtend' => $this->resourceSearchExtend,
                    'addVarsForView' => $this->addVarsSearchViewData()
                ])
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', $this->getResourceModel());
        $class = $this->getResourceModel();
        return view(
            $this->getResourceCreatePath(),
            $this->filterCreateViewData([
                'record' => new $class(),
                'resourceAlias' => $this->getResourceAlias(),
                'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
                'resourceTitle' => $this->getResourceTitle(),
                'addVarsForView' => $this->addVarsCreateViewData(),
                'groups' => $this->userService->getUserTree()
            ])
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory
     * |\Illuminate\Http\RedirectResponse
     * |\Illuminate\Routing\Redirector
     * |\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $record = $this->repository->findOne($id);
        $record->participants = explode(",", $record->participants);
        $this->authorize('update', $record);
        return view(
            $this->getResourceEditPath(),
            $this->filterEditViewData($record, [
                'record' => $record,
                'resourceAlias' => $this->getResourceAlias(),
                'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
                'resourceTitle' => $this->getResourceTitle(),
                'addVarsForView' => $this->addVarsEditViewData(),
                'groups' => $this->userService->getUserTree()
            ])
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $record = $this->repository->findOne($id);
        $this->authorize('update', $record);
        $valuesToSave = $this->getValuesToSave($request, $record);
        $valuesToSave['participants'] = is_array($request->participants) ? implode(',', $request->participants) : null;
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'update', $record);
        if ($this->repository->update($record, $this->alterValuesToSave($request, $valuesToSave))) {
            flash()->success('Cập nhật thành công.');
            return $this->getRedirectAfterSave($record, $request, false);
        } else {
            flash()->info('Cập nhật thất bại.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->getResourceModel());
        $valuesToSave = $this->getValuesToSave($request);
        $valuesToSave['participants'] = is_array($request->participants) ? implode(',', $request->participants) : "";
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'store');
        if ($record = $this->repository->save($this->alterValuesToSave($request, $valuesToSave))) {
            flash()->success('Thêm mới thành công.');
            return $this->getRedirectAfterSave($record, $request, true);
        } else {
            flash()->info('Thêm mới thất bại.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $record = $this->getResourceModel()::findOrFail($id);

        $this->authorize('delete', $record);
        if (!$this->checkDestroy($record)) {
            return redirect(route($this->getResourceRoutesAlias() . '.index'));
        }

        if ($record->delete()) {
            flash()->success('Xóa thành công.');
        } else {
            flash()->info('Không thể xóa bản ghi.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }

    /**
     * Statistic quiz
     *
     * @return mixed
     */
    public function statisticQuiz()
    {
        $quiz = $this->service->getListQuiz();
        $userAnswers = $this->repository->countAnswerUser();
        $sumUserAnswers = [];
        foreach ($userAnswers as $userAnswer) {
            $sumUserAnswers[$userAnswer['id']] = $userAnswer['count'];
        }
        return view('admin.quiz_statistic.index', ['quizzes' => $quiz, 'sumUserAnswers' => $sumUserAnswers]);
    }

    /**
     * Get user
     *
     * @param  Request $request
     * @return mixed
     */
    public function getUser(Request $request)
    {
        $quizId = $request->id;
        $users = $this->service->getListUsers($quizId);
        $amountQuestion = $this->repository->CountQuestion($quizId);
        $amountCorrectAnswers = $this->service->countCorrectAnswer($quizId);

        $response = [
            'users' => $users,
            'amountQuestion' => $amountQuestion,
            'amountCorrectAnswers' => $amountCorrectAnswers,
            'quizId' => $quizId
        ];

        return view('admin.quiz_statistic.detail', $response);
    }

    /**
     * Get answers of user
     *
     * @param  Request $request
     * @return mixed
     */
    public function answerUser(Request $request)
    {
        $answerUser = $this->repository->getAnswerUser($request->quizId, $request->id);
        $response = ['questions' => $answerUser, 'quizId' => $request->quizId, 'userId' => $request->id];
        return view('admin.quiz_statistic.answer_user', $response);
    }
}
