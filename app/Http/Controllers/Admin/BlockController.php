<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserBlockRequest;
use App\Models\Block;
use App\Repositories\Contracts\IBlockRepository;
use App\Services\Contracts\IBlockService;
use Illuminate\Http\Request;

class BlockController extends AdminBaseController
{

    //
    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.blocks';
    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::blocks';
    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = Block::class;

    /**
     * @var  string
     */
    protected $resourceTitle = 'Khu vực';

    /**
     * __construct
     *
     * @param  mixed $repository
     * @return void
     */
    public function __construct(IBlockRepository $repository, IBlockService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        parent::__construct();
    }

    /**
     * getResourceManageMemberPath
     *
     * @return void
     */
    public function getResourceManageMemberPath()
    {
        return 'admin.blocks.user_block';
    }

    /**
     * resourceStoreValidationData
     *
     * @return void
     */
    public function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required|max:255',
            ],
            'messages' => [],
            'attributes' => [
                'name' => 'tên khu vực',
            ],
            'advanced' => [],
        ];
    }

    /**
     * resourceUpdateValidationData
     *
     * @param  mixed $record
     * @return void
     */
    public function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required|max:255',
            ],
            'messages' => [],
            'attributes' => [
                'name' => 'tên khu vực',
            ],
            'advanced' => [],
        ];
    }

    /**
     * manageMember
     *
     * @param  mixed $id
     * @return void
     */
    public function manageMember($id)
    {
        $record = $this->repository->findOne($id);
        // list id old member of block
        $listUserIdOfTeam = $this->service->getCurrentBlockUserIdList($record);
        $listUserInTeam = $this->service->getListUserInTeam();

        return view(
            $this->getResourceManageMemberPath(),
            $this->filterShowViewData($record, [
                'record' => $record,
                'resourceAlias' => $this->getResourceAlias(),
                'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
                'resourceTitle' => $this->getResourceTitle(),
                'listUserInTeam' => $listUserInTeam,
                'listUserIdOfTeam' => json_encode($listUserIdOfTeam)
            ])
        );
    }

    /**
     * saveUserBlock
     *
     * @param  mixed $request
     * @return void
     */
    public function saveUserBlock(UserBlockRequest $request)
    {
        $this->service->saveUserBlock($request);
        return redirect()->action('Admin\BlockController@manageMember', ['id' => $request->id]);
    }
}
