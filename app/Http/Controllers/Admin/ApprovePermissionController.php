<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ApprovePermissionExport;
use App\Models\WorkTimesExplanation;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Services\Contracts\IWorkTimeExplanationService;

class ApprovePermissionController extends AdminBaseController
{
    //
    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.approve_permission';
    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::approve_permission';
    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = WorkTimesExplanation::class;

    protected $meetingService;

    /**
     * @var  string
     */
    protected $resourceTitle = 'Xin phép';
    protected $resourceSearchExtend = 'admin.approve_permission._search_extend';

    private $service;

    /**
     * Controller construct
     *
     * @param IWorkTimeExplanationService    $service
     */
    public function __construct(IWorkTimeExplanationService $service)
    {
        $this->service = $service;
        parent::__construct();
    }

    public function exportData(Request $request, $search = null)
    {
        if ($request->path() === 'admin/approve_permission') {
            $approvePermissions = $this->getResourceModel()::search($request['search'])->get();
            return Excel::download(new ApprovePermissionExport($approvePermissions), "approve-permission.xlsx");
        } else {
            abort(404);
        }
    }

    public function getSearchRecords(Request $request, $perPage = 15, $search = null)
    {
        if (!$request->has('year')) {
            $request->merge(['year' => date('Y')]);
        }
        if (!$request->has('month')) {
            $request->merge(['month' => date('n')]);
        }

        return $this->service->search($request, $perPage, $search);
    }

    /**
     * show modal detail WorkTimesExplanation
     *
     * @param int $id
     *
     * @return Response
     */
    public function modalDetail($id)
    {
        // Get $workTimesExplanation
        $workTimesExplanation = WorkTimesExplanation::where(['id' => $id])->first();

        return view('elements.admin.approve_permission.modal_detail', [
            'workTimesExplanation' => $workTimesExplanation
        ]);
    }
}
