<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PunishRequest;
use App\Models\Punishes;
use App\Models\Rules;
use App\Models\User;
use App\Repositories\Contracts\IPunishesRepository;
use App\Services\Contracts\IPunishesService;
use App\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * PunishesController
 * Author: jvb
 * Date: 2019/04/22 08:21
 */
class PunishesController extends AdminBaseController
{
    public $defaultPageSize = 50;

    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.punishes';

    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::punishes';

    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = Punishes::class;

    /**
     * @var  string
     */
    protected $resourceTitle = 'Danh sách vi phạm';

    protected $resourceSearchExtend = 'admin.punishes._search_extend';

    /**
     * @var \App\Services\Contracts\IPunishesService $service
     */
    protected $service;

    /**
     * Controller construct
     */
    public function __construct(IPunishesRepository $repository, IPunishesService $service)
    {
        $this->repository = $repository;
        $this->service = $service;

        parent::__construct();
    }

    public function getSearchRecords(Request $request, $perPage = 15, $search = null)
    {
        $model = $this->getSearchModel($request, $search);

        $ruleId = $request->get('rule_id', '');
        if ($ruleId != '') {
            $model = $model->where('rule_id', $ruleId);
        }
        $isConfirmed = $request->get('is_confirmed', '');

        if ($isConfirmed != '') {
            $model = $model->where('is_confirmed', $isConfirmed);
        }
        $userId = $request->get('user_id');
        if (!empty($userId)) {
            $model = $model->where('user_id', $userId);
        }
        if ($request->has('sort')) {
            $model->orderBy($request->get('sort'), $request->get('is_desc') ? 'asc' : 'desc');
        } else {
            $model->orderBy('id', 'desc');
        }

        return $model->paginate($perPage);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function filterCreateViewData($data = [])
    {
        return $this->makeRelationData($data);
    }

    /**
     * @param       $record
     * @param array $data
     *
     * @return array
     */
    public function filterEditViewData($record, $data = [])
    {

        return $this->makeRelationData($data);
    }

    public function resourceStoreValidationData()
    {
        return $this->validationData();
    }

    /**
     * @param         $record
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getRedirectAfterSave($record, $request, $isCreate = null)
    {
        if ($record->status == STATUS_DAY_OFF['active']) {
            $this->service->calculateDayOff($request, $record->id);
        }
        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }


    public function resourceUpdateValidationData($record)
    {
        return $this->validationData();
    }

    /**
     * @param Request $request
     * @param         $values
     *
     * @return mixed
     */
    public function alterValuesToSave(Request $request, $values)
    {
        if (empty($values['approver_at']) && !empty($values['approver_id'])) {
            $values['approver_at'] = Carbon::now();
        }

        return $values;
    }

    private function validationData()
    {
        $questionRequest = new PunishRequest();
        return [
            'rules' => $questionRequest->rules(),
            'messages' => $questionRequest->messages(),
            'attributes' => $questionRequest->attributes(),
            'advanced' => [],
        ];
    }

    private function makeRelationData($data = [])
    {
        $userModel = new User();
        $data['users'] = $userModel->availableUsers()
            ->select(DB::raw('CONCAT(staff_code, " - ", name) as name'), 'id')
            ->pluck('name', 'id')->toArray();
        $data['rules'] = Rules::select(DB::raw('CONCAT(name, " - ", FORMAT(penalize, 0)) as rule'), 'id')
            ->pluck('rule', 'id')->toArray();

        return $data;
    }

    public function submit(Request $request)
    {
    }

    public function changeSubmitStatus($id)
    {
        $punish = Punishes::find($id);

        if ($punish) {
            $punish->is_submit ^= true;
            if ($punish->is_submit)
                $punish->is_confirmed = PUNISH_CONFIRMED;
            $punish->save();
            flash()->success('Cập nhật thành công!');
        } else {
            flash()->error('Không tìm thấy bản ghi!');
        }
        return redirect(route('admin::punishes.index'));
    }

    public function submits(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);
        $ids = $request->get('ids');
        Punishes::whereIn('id', $ids)->update([
            'is_confirmed' => PUNISH_CONFIRMED,
            'is_submit' => Punishes::SUBMITED
        ]);
        flash()->success('Cập nhật thành công!');

        return redirect(route('admin::punishes.index'));
    }

    public function changeConfirmStatus($id)
    {
        $punish = Punishes::find($id);

        if ($punish) {
            $punish->is_confirmed ^= true;
            if (!$punish->is_confirmed) {
                $punish->is_submit = false;
            } else {
                $this->sendNotification(collect([$punish]));
            }
            $punish->save();
            flash()->success('Cập nhật thành công!');
        } else {
            flash()->error('Không tìm thấy bản ghi!');
        }
        return redirect(route('admin::punishes.index'));
    }

    public function confirms(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);
        $ids = $request->get('ids');
        Punishes::whereIn('id', $ids)->update([
            'is_confirmed' => PUNISH_CONFIRMED
        ]);
        $this->sendNotification(Punishes::whereIn('id', $ids)->orderBy('week_num')->get());
        flash()->success('Cập nhật thành công!');

        return redirect(route('admin::punishes.index'));
    }

    private function sendNotification($punishes)
    {
        /** @var NotificationService $notificationService */
        $notificationService = app()->make(NotificationService::class);
        // report only
        $punishes = $punishes->where('rule_id', WEEKLY_REPORT_RULE_ID);
        $punisheByWeeks = $punishes->groupBy('week_num');
        $users = User::all();
        foreach ($punisheByWeeks as $week => $items) {
            $users = $users->filter(function ($item) use ($items) {
                return in_array($item->id, $items->pluck('user_id')->toArray());
            });
            $notificationService->dontSentWeeklyReport($users, $week);
        }
    }

    /**
     * @param Request $request
     * @param         $search
     *
     * @return mixed
     */
    protected function getSearchModel(Request $request, $search)
    {
        $model = $this->getResourceModel()::search($search);
        $year = $request->get('year');
        $month = $request->get('month');
        if ($year) {
            $model->whereYear('infringe_date', $year);
        }

        if ($month) {
            $model->whereMonth('infringe_date', $month);
        }
        $isSubmit = $request->get('is_submit');
        if (isset($isSubmit)) {
            $model->where('is_submit', $isSubmit);
        }
        $isRecaculate = $request->get('is_recaculate');
        if (isset($isRecaculate)) {
            // caculate punish money
            $userId = $request->get('user_id');
            $calYear = isset($year) ? $year : date('Y');
            $calMonth = isset($month) ? $month : date('m');
            $userIds = isset($userId) ? [$userId] : [];

            $this->service->calculateLateTime($calYear, $calMonth, $userIds);
        }
        return $model;
    }
}
