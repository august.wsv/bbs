<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Exports\ExportPunishes;
use App\Http\Requests\StatisticPunishedRequest;
use App\Models\Group;
use App\Models\Rules;
use App\Services\Contracts\IPunishesService;
use Illuminate\Http\Request;

class StatisticPunishedController extends Controller
{
    protected $service;

    /**
     * __construct__
     *
     * @param  mixed $service
     * @return void
     */
    public function __construct(IPunishesService $service)
    {
        $this->service = $service;
    }

    /**
     * index
     *
     * @param  mixed $request
     * @return void
     */
    public function index(StatisticPunishedRequest $request)
    {
        $rules = Rules::select('name', 'id')->get();
        $groupId = $request->group ?? null;
        $startDate = $request->start_day ?? null;
        $endDate = $request->end_day ?? null;
        $groups = Group::all();
        $totalLate = $this->service->getTotalLate($startDate, $endDate, $groupId);
        $punishUsers = $this->service->getUserList($startDate, $endDate, $groupId, 10);
        $punishTypes = $this->service->getPunishTypes($startDate, $endDate, $groupId, $rules);
        return view(
            'admin.statistic_punished.index',
            compact(
                'totalLate',
                'rules',
                'punishTypes',
                'startDate',
                'endDate',
                'punishUsers',
                'groups',
                'groupId'
            )
        );
    }
    /**
     * exportPunishes
     *
     * @param  mixed $request
     * @return void
     */
    public function exportPunishes(Request $request)
    {
        $startDate = $request->startDate ?? null;
        $endDate = $request->endDate ?? null;
        $groupId = $request->group ?? null;
        $punishUsers = $this->service->getUserList($startDate, $endDate, $groupId);
        $title = ($startDate && $endDate)
            ? 'Danh_sách_tiền_phạt_từ_' . $startDate . '_đến_' . $endDate . '.xlsx'
            : 'Danh_sách_tiền_phạt_6_tháng_gần_nhất.xlsx';

        ob_end_clean();
        ob_start();

        return \Maatwebsite\Excel\Facades\Excel::download(new ExportPunishes($punishUsers), $title);
    }
}
