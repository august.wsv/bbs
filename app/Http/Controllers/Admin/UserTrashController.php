<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Contracts\IUserRepository;

class UserTrashController extends AdminBaseController
{
    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.user_trash';

    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::user_trash';

    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = User::class;

    /**
     * @var  string
     */
    protected $resourceTitle = 'Nhân viên';

    protected $resourceSearchExtend = 'admin.user_trash._search_extend';

    public function __construct(IUserRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function deletes(Request $request)
    {
        return 1;
    }

    public function getSearchRecords(Request $request, $perPage = 15, $search = null)
    {
        $model = $this->getResourceModel()::search($search);
        if ($request->has('sort')) {
            $model->orderBy($request->get('sort'), $request->get('is_desc') ? 'asc' : 'desc');
        } else {
            $model->orderBy('id', 'desc');
        }
        return $model->onlyTrashed()->paginate($perPage);
    }

    public function edit($id)
    {
        $record = User::onlyTrashed()->where('id', $id)->first();

        $this->authorize('update', $record);

        $response = [
            'record' => $record,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'resourceTitle' => $this->getResourceTitle(),
            'addVarsForView' => $this->addVarsEditViewData()
        ];

        return view($this->getResourceEditPath(), $this->filterEditViewData($record, $response));
    }


    public function show($id)
    {
        $record = User::onlyTrashed()->where('id', $id)->first();

        $this->authorize('update', $record);
        $allocateUsers = null;
        if (isset($this->resourceAllocate)) {
            $allocateUsers = $this->deviceUserService->getRecordByDeviceId($record->id);
        }

        $response = [
            'record' => $record,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'resourceTitle' => $this->getResourceTitle(),
            'addVarsForView' => $this->addVarsShowViewData(),
            'allocateUsers' => $allocateUsers
        ];

        return view($this->getResourceShowPath(), $this->filterShowViewData($record, $response));
    }

    public function update(Request $request, $id)
    {
        $record = User::onlyTrashed()->where('id', $id)->first();

        $this->authorize('update', $record);

        $valuesToSave = $this->getValuesToSave($request, $record);
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'update', $record);
        if ($this->repository->update($record, $this->alterValuesToSave($request, $valuesToSave))) {

            flash()->success('Cập nhật thành công.');

            return $this->getRedirectAfterSave($record, $request, false);
        } else {
            flash()->info('Cập nhật thất bại.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }

    public function restore(Request $request)
    {
        $request->validate([
            'user_ids' => 'required|array'
        ]);

        $result = $this->repository->restore($request->user_ids);

        if ($result) {
            flash()->success(__l('user_restore_success'));
        } else {
            flash()->info(__l('user_restore_fail'));
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }


    public function destroy($id)
    {
        $record = User::onlyTrashed()->where('id', $id)->first();

        $this->authorize('delete', $record);

        if (!$this->checkDestroy($record)) {
            return redirect(route($this->getResourceRoutesAlias() . '.index'));
        }

        if ($record->forceDelete()) {
            flash()->success('Xóa thành công.');
        } else {
            flash()->info('Không thể xóa bản ghi.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }
}
