<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\Models\Question;
use App\Models\Answer;
use Illuminate\Http\Request;
use App\Repositories\Contracts\IQuestionRepository;

class QuestionController extends AdminBaseController
{

    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.questions';

    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::questions';

    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = Question::class;

    public function getResourceIndexPath()
    {
        return 'admin.questions.table';
    }

    public function getResourceEditPath()
    {
        return 'admin.questions.edit';
    }

    public function getResourceCreatePath()
    {
        return 'admin.questions.form';
    }

    /**
     * @var  string
     */
    protected $resourceTitle = 'Question';

    public function __construct(IQuestionRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewList', $this->getResourceModel());
        if ($request->has('is_export') && in_array($request->path(), EXPORT_PATHS)) {
            return $this->exportData($request);
        }
        $records = $this->searchRecords($request, $perPage, $search);
        return view(
            $this->getResourceIndexPath(),
            ['id' => $request->quiz_id],
            $this->filterSearchViewData($request, [
                'records' => $records,
                'search' => $search,
                'resourceAlias' => $this->getResourceAlias(),
                'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
                'resourceTitle' => $this->getResourceTitle(),
                'perPage' => $perPage,
                'resourceSearchExtend' => $this->resourceSearchExtend,
                'addVarsForView' => $this->addVarsSearchViewData()
            ])
        );
    }

    /**
     * Classes using this trait have to implement this method.
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $perPage
     * @param string|null              $search
     *
     * @return \Illuminate\Support\Collection
     */
    public function getSearchRecords(Request $request, $perPage = 15, $search = null)
    {
        $model = $this->getResourceModel()::where('quiz_id', $request->id)->search($search);
        if ($request->has('sort')) {
            $model->orderBy($request->get('sort'), $request->get('is_desc') ? 'asc' : 'desc');
        } else {
            $model->orderBy('position', 'asc');
        }
        return $model->paginate($perPage);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = $this->repository->findOne($id);

        $this->authorize('update', $record);
        $allocateUsers = null;
        $answer = Answer::where('question_id', $id)->get();
        if (isset($this->resourceAllocate)) {
            $allocateUsers = $this->deviceUserService->getRecordByDeviceId($record->id);
        }
        return view(
            $this->getResourceShowPath(),
            $this->filterShowViewData($record, [
                'record' => $record,
                'answers' => $answer,
                'resourceAlias' => $this->getResourceAlias(),
                'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
                'resourceTitle' => $this->getResourceTitle(),
                'addVarsForView' => $this->addVarsShowViewData(),
                'allocateUsers' => $allocateUsers
            ])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->getResourceModel());
        $valuesToSave = $this->getValuesToSave($request);
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'store');
        $answers = $request->get('answers');
        $isCorrect = $request->get('is_correct');
        if ($record = $this->repository->save($this->alterValuesToSave($request, $valuesToSave))) {
            if ($answers != null) {
                $questionId = $record->id;
                foreach ($answers as $key => $item) {
                    $answer = new Answer();
                    $answer->answer = $item;
                    $answer->question_id = $questionId;
                    $answer->is_correct = $key == $isCorrect ? CORRECT_ANSWER : WRONG_ANSWER;
                    $answer->save();
                }
            }
            flash()->success('Thêm mới thành công.');
            switch ($request->save_question) {
                case 'save-continue':
                    return $this->redirectBackTo(
                        route($this->getResourceRoutesAlias() . '.create', ['id' => $request->quiz_id])
                    );
                    break;
                case 'save':
                    return redirect()->route($this->getResourceRoutesAlias() . '.index', ['id' => $request->quiz_id]);
                    break;
            }
        } else {
            flash()->info('Thêm mới thất bại.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index', ['id' => $request->quiz_id]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory
     * |\Illuminate\Http\RedirectResponse
     * |\Illuminate\Routing\Redirector
     * |\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $record = $this->repository->findOne($id);
        $answer = Answer::where('question_id', $id)->get();
        $this->authorize('update', $record);
        return view(
            $this->getResourceEditPath(),
            $this->filterEditViewData($record, [
                'record' => $record,
                'resourceAlias' => $this->getResourceAlias(),
                'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
                'resourceTitle' => $this->getResourceTitle(),
                'addVarsForView' => $this->addVarsEditViewData(),
                'answer' => $answer
            ])
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $record = $this->repository->findOne($id);
        $this->authorize('update', $record);
        $valuesToSave = $this->getValuesToSave($request, $record);
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'update', $record);
        DB::beginTransaction();
        try {
            $this->repository->update($record, $this->alterValuesToSave($request, $valuesToSave));
            if ($request->type == FILL_IN_BLANKS) {
                Answer::where('question_id', $request->question_id)->delete();
            } else {
                $answers = array_values($request->get('answers'));
                $isCorrect = $request->get('is_correct');
                $arrTemp = Answer::where('question_id', $id)->get()->toArray();
                $this->repository->updateAnswer($answers, $arrTemp, $isCorrect, $id);
            }
            DB::commit();
            flash()->success('Cập nhật thành công.');
            if ($request->save_question == 'save-continue') {
                return $this->redirectBackTo(
                    route($this->getResourceRoutesAlias() . '.create', ['id' => $request->quiz_id])
                );
            }
            return redirect()->route($this->getResourceRoutesAlias() . '.index', ['id' => $request->quiz_id]);
        } catch (\Exception $e) {
            DB::rollback();
            flash()->info('Cập nhật thất bại.');
        }
        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index', ['id' => $request->quiz_id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $record = $this->getResourceModel()::findOrFail($id);
        $record->answers()->delete();
        $this->authorize('delete', $record);
        if (!$this->checkDestroy($record)) {
            return redirect(route($this->getResourceRoutesAlias() . '.index'));
        }

        if ($record->delete()) {
            flash()->success('Xóa thành công.');
        } else {
            flash()->info('Không thể xóa bản ghi.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }

    /**
     * Sortable list questions
     *
     * @param  mixed $request
     * @return void
     */
    public function sortableList(Request $request)
    {
        $questions = Question::all();
        foreach ($questions as $question) {
            foreach ($request->order as $order) {
                if ($order['id'] == $question->id) {
                    $question->position = $order['position'];
                    $question->save();
                }
            }
        }
        return response('Update Successfully.', 200);
    }
}
