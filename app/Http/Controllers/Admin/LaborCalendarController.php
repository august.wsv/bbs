<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\DateTimeHelper;
use App\Models\LaborCalendar;
use App\Models\LaborCalendarDetail;
use App\Repositories\Contracts\ILaborCalendarRepository;
use App\Repositories\Contracts\IUserRepository;
use App\Services\Contracts\ILaborCalendarService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LaborCalendarController extends AdminBaseController
{
    protected $resourceAlias = 'admin.labor_calendar';

    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::labor_calendar';

    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = LaborCalendar::class;

    protected $resourceTitle = 'Lịch trực nhật';

    protected $resourceSearchExtend = 'admin.labor_calendar._search_extend';

    protected $userRepository;

    protected $service;

    public $repository;

    public function __construct(
        IUserRepository $userRepository,
        ILaborCalendarService $service,
        ILaborCalendarRepository $repository
    ) {
        $this->userRepository = $userRepository;
        $this->service = $service;
        $this->repository = $repository;
        parent::__construct();
    }

    public function index(Request $request)
    {
        $now = Carbon::now();
        $searchMonth = $now->month;
        $searchYear = $now->year;
        $nowDate = $now->toDateString();
        if ($request->has('month') || $request->has('year')) {
            $searchMonth = $request->get('month');
            $searchYear = $request->get('year');
            $nowDate = Carbon::parse($searchYear . '/' . $searchMonth . '/1');
        }
        $laborDate = $searchYear . '/' . $searchMonth . '/01';
        $firstLaborCalendarOfMonth = LaborCalendar::where('labor_date', $laborDate)->get();
        $contentWork = $firstLaborCalendarOfMonth->isNotEmpty() ? $firstLaborCalendarOfMonth[0]->content : '';
        $laborCalendars = $this->service->getLaborCalendar($searchMonth, $searchYear);
        $listLaborCalendarOfAllEmployee = $this->service->getListLaborCalendarAll($searchYear);
        $listLaborUserInMonthChose = $this->service->getListLaborUserInMonth($searchMonth, $searchYear);
        $listLaborUserInMonthUnChose = $this->service->getListLaborUserInMonthUnChose($searchMonth, $searchYear);
        return view(
            $this->resourceAlias . '._layout',
            [
                'resourceAlias' => $this->resourceAlias,
                'resourceSearchExtend' => $this->resourceSearchExtend,
                'resourceRoutesAlias' => $this->resourceRoutesAlias,
                'resourceTitle' => $this->resourceTitle,
                'searchMonth' => $searchMonth,
                'searchYear' => $searchYear,
                'laborCalendars' => $laborCalendars,
                'nowDate' => $nowDate,
                'listLaborCalendarOfAllEmployee' => $listLaborCalendarOfAllEmployee,
                'listLaborUserInMonthChose' => $listLaborUserInMonthChose,
                'listLaborUserInMonthUnChose' => $listLaborUserInMonthUnChose,
                'contentWork' => $contentWork
            ]
        );
    }

    public function create(Request $request)
    {
        $laborUserList = $this->userRepository->getLaborUserList();
        $listBeforeSixMonth = DateTimeHelper::getBeforeSixMonth();
        $currentMonth = Carbon::now()->month;
        $currentYear = Carbon::now()->year;
        $countUserChose = DateTimeHelper::countUserChose($currentMonth, $currentYear);
        return view(
            $this->resourceAlias . '.create',
            [
                'resourceAlias' => $this->resourceAlias,
                'resourceSearchExtend' => $this->resourceSearchExtend,
                'resourceRoutesAlias' => $this->resourceRoutesAlias,
                'resourceTitle' => $this->resourceTitle,
                'laborUserList' => $laborUserList,
                'listBeforeSixMonth' => $listBeforeSixMonth,
                'currentMonth' => $currentMonth,
                'currentYear' => $currentYear,
                'countUserChose' => $countUserChose
            ]
        );
    }

    public function countUserChose()
    {
        $month = request()->get('month');
        $year = request()->get('year');
        $countUser = DateTimeHelper::countUserChose($month, $year);
        return $countUser;
    }

    public function store(Request $request)
    {
        $month = $request->get('month');
        $year = $request->get('year');
        $content = $request->get('content');
        $listUserID = $request->get('list_user_id');
        try {
            $this->service->saveLaborCalendar($month, $year, $listUserID, $content);
            flash()->success('Tạo mới thành công.');
            return redirect()->route('admin::labor_calendar.index', ['month' => $month, 'year' => $year]);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            flash()->info('Tạo mới thất bại.');
            return redirect()->route('admin::labor_calendar.index');
        }
    }

    public function edit($laborCalendarID)
    {
        $laborCalendar = $this->service->getInfoLaborCalendar($laborCalendarID);
        return view(
            $this->resourceAlias . '.edit',
            $this->filterEditViewData(
                $laborCalendar,
                [
                    'record' => $laborCalendar,
                    'resourceAlias' => $this->getResourceAlias(),
                    'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
                    'resourceTitle' => $this->getResourceTitle(),
                    'addVarsForView' => $this->addVarsEditViewData()
                ]
            )
        );
    }

    public function update(Request $request, $id)
    {
        $updateInfo = $request->only(['status', 'note']);
        $updateLabor = $this->repository->findOne($id);
        $laborDate = $updateLabor->labor_date;
        $month = Carbon::parse($laborDate)->month;
        $year = Carbon::parse($laborDate)->year;

        try {
            $this->repository->update($updateLabor, $updateInfo);
            flash()->success('Cập nhật thành công.');
            return redirect()->route('admin::labor_calendar.index', ['month' => $month, 'year' => $year]);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            flash()->info('Cập nhật thất bại.');
            return redirect()->route('admin::labor_calendar.index');
        }
    }

    public function pairingEmployee(Request $request)
    {
        $month = $request->get('month');
        $year = $request->get('year');
        $content = $request->get('content');
        $this->service->randomLaborCalendar($month, $year, $content);
        return redirect()->route('admin::labor_calendar.index', ['month' => $month, 'year' => $year]);
    }

    public function infoPairUserChose(Request $request)
    {
        $laborCalendarID = $request->get('laborCalendarID');
        $listID = LaborCalendarDetail::select('user_id')->where('labor_calendar_id', $laborCalendarID)->get();
        $laborCalendar = LaborCalendar::find($laborCalendarID);
        if (empty($laborCalendar)) {
            abort(404);
        }
        $date = Carbon::parse($laborCalendar->labor_date)->format(DATE_TIME_FORMAT_VI);
        return response()->json([
            'id' => $listID,
            'date' => $date,
            'laborCalendarID' => $laborCalendarID
        ]);
    }

    public function updatePairUser(Request $request)
    {
        $laborDetail = LaborCalendarDetail::where('labor_calendar_id', $request->get('labor_calendar_id'))->get();
        if ($laborDetail->isEmpty()) {
            abort(404);
        }
        $laborDetail[0]->user_id = $request->get('userID_1');
        $laborDetail[0]->save();
        $laborDetail[1]->user_id = $request->get('userID_2');
        $laborDetail[1]->save();
        return redirect()->route(
            'admin::labor_calendar.index',
            ['month' => $request->get('month_search'), 'year' => $request->get('year_search')]
        );
    }

    public function statusPairUserChose(Request $request)
    {
        try {
            $laborCalendarID = $request->get('laborCalendarID');
            $laborCalendar = LaborCalendar::find($laborCalendarID);
            $status = $laborCalendar->status;
            $note = $laborCalendar->note;
            $date = Carbon::parse(LaborCalendar::find($laborCalendarID)->labor_date)->format(DATE_TIME_FORMAT_VI);
            return response()->json([
                'status' => $status,
                'note' => $note,
                'date' => $date,
                'laborCalendarID' => $laborCalendarID,
                'status_response' => 200
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status_response' => 500
            ]);
        }
    }

    public function updateStatusPairUserChose(Request $request)
    {
        $laborCalendar = LaborCalendar::find($request->get('labor_calendar_id'));
        if (empty($laborCalendar)) {
            abort(404);
        }
        $laborCalendar->status = $request->get('status');
        $laborCalendar->note = $request->get('note');
        $laborCalendar->save();
        flash()->success('Cập nhật thành công.');
        return redirect()->route(
            'admin::labor_calendar.index',
            ['month' => $request->get('month_search'), 'year' => $request->get('year_search')]
        );
    }

    public function updateWorkContent(Request $request)
    {
        $month = $request->get('month');
        $year = $request->get('year');
        $content = $request->get('content');
        try {
            $this->repository->updateWorkContent($month, $year, $content);
            return redirect()->route('admin::labor_calendar.index', ['month' => $month, 'year' => $year]);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            flash()->info('Cập nhật thất bại.');
            return redirect()->route('admin::labor_calendar.index');
        }
    }
}
