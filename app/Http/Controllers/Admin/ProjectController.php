<?php

/**
 * Created by PhpStorm.
 * User: muatu
 * Date: 1/31/2019
 * Time: 3:15 PM
 */

namespace App\Http\Controllers\Admin;

use App\Models\Project;
use App\Repositories\Contracts\IProjectRepository;
use Illuminate\Http\Request;

class ProjectController extends AdminBaseController
{
    /**
     * @var  string
     */
    protected $resourceAlias = 'admin.projects';
    /**
     * @var  string
     */
    protected $resourceRoutesAlias = 'admin::projects';
    /**
     * Fully qualified class name
     *
     * @var  string
     */
    protected $resourceModel = Project::class;

    protected $resourceSearchExtend = 'admin.projects._search_extend';
    /**
     * @var  string
     */
    protected $resourceTitle = 'Dự án';

    public function __construct(IProjectRepository $repository) {
        $this->repository = $repository;
        parent::__construct();
    }


    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->getResourceModel());
        $this->validationData($request);
        $valuesToSave = $this->getValuesToSave($request);
        $request->merge($valuesToSave);

        if ($record = $this->repository->save($this->alterValuesToSave($request, $valuesToSave))) {
            flash()->success('Thêm mới thành công.');

            return $this->getRedirectAfterSave($record, $request, true);
        } else {
            flash()->info('Thêm mới thất bại.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $record = $this->repository->findOne($id);

        $this->authorize('update', $record);

        $valuesToSave = $this->getValuesToSave($request, $record);
        $request->merge($valuesToSave);
        $this->validationData($request, $id);

        if ($this->repository->update($record, $this->alterValuesToSave($request, $valuesToSave))) {

            flash()->success('Cập nhật thành công.');

            return $this->getRedirectAfterSave($record, $request, false);
        } else {
            flash()->info('Cập nhật thất bại.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias() . '.index'));
    }

    /**
     * validationData
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function validationData($request, $id = null)
    {
        $status = $request->status;
        if ($status == 0) {
            $startDateRule = '|after_or_equal:now';
        } elseif ($status == 1) {
            $startDateRule = '|before_or_equal:now|required';
        } elseif ($status == 2) {
            $endDateRule = '|before_or_equal:now|required';
        }

        $rules = [
            'name' => 'required|max:255|unique:users,email' . ($id ? (',' . $id) : ''),
                'customer' => 'required|max:255',
                'scale' => 'nullable|numeric|min:1',
                'start_date' => 'nullable|date' . (isset($startDateRule) ? $startDateRule : ''),
                'end_date' => 'nullable|date|after_or_equal:start_date' . (isset($endDateRule) ? $endDateRule : ''),
        ];

        $messages = [
            'end_date.after_or_equal' => 'Trường ngày kết thúc phải là một ngày sau ngày bắt đầu.',
            'end_date.before_or_equal' => 'Dự án đã kết thúc, trường ngày kết thúc phải là một ngày trước hiện tại.',
            'start_date.after_or_equal' => 'Trường ngày bắt đầu phải là một ngày sau ngày hiện tại.',
            'start_date.before_or_equal' => 'Trường ngày bắt đầu phải là một ngày trước ngày hiện tại.',
        ];

        $attributes = [
            'name' => 'tên dự án',
            'customer' => 'khách hàng',
            'start_date' => 'ngày bắt đầu',
            'end_date' => 'ngày kết thúc',
            'scale' => 'quy mô dự án',
        ];

        $this->validate($request, $rules, $messages, $attributes);
    }

    /**
     * getSearchRecords
     *
     * @param  mixed $request
     * @param  mixed $perPage
     * @param  mixed $search
     * @return void
     */
    public function getSearchRecords(Request $request, $perPage = 15, $search = null)
    {
        $model = $this->getResourceModel()::search($search);
        $model->searchOnAdmin($request);

        return $model->paginate($perPage);
    }
}
