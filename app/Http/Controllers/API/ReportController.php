<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\Api\ReportStatisticsRequest;
use App\Http\Requests\CreateReportRequest;
use App\Http\Requests\ReplyReportRequest;
use App\Models\Report;
use App\Services\Contracts\IReportService;
use App\Transformers\ReportTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends ApiBaseController
{
    /**
     * ReportController constructor.
     *
     * @param IReportService    $service
     * @param ReportTransformer $transformer
     */
    public function __construct(
        IReportService $service,
        ReportTransformer $transformer
    ) {
        $this->service = $service;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @api              {get} /reports
     * @apiName          List report
     * @apiDescription   ReportList
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search reports.
     *
     * @apiSuccess {Object[]} reports List of report.
     * @apiSuccess {Object} meta Meta Pagination.
     */
    public function index(Request $request)
    {
        $request->merge(['is_api' => true]);
        $reports = $this->service->search($request, $perPage, $search, $teamId);
        return $this->respondTransformer($reports);
    }

    /**
     * @api              {get} /reports/:id
     * @apiName          Report detail
     * @apiDescription   GetReport
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} report Report info.
     */
    public function detail($id)
    {
        $report = $this->service->detailApi($id);
        if ($report != null) {
            return $this->respondTransformer($report);
        }
        return $this->respondNotfound();
    }

    /**
     * @api              {get} /report-receivers
     * @apiName          Report receivers
     * @apiDescription   GetReportReceiver
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {List} list receiver.
     */
    public function getReportReceiver()
    {
        $receivers = $this->service->getReportReceiver();
        return $this->respond($receivers);
    }

    /**
     * @api              {post} /report
     * @apiName          Report create
     * @apiDescription   SaveReport
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {Number} choose_week Week number.
     * @apiParam {Number} status Save draft or final.
     * @apiParam {Array} to_ids Receiver Ids.
     * @apiParam {String} content Report content.
     *
     * @apiSuccess {Object} success.
     */
    public function saveReport(CreateReportRequest $request)
    {
        $report = $this->service->create($request);
        unset($report->user);
        return $this->respond($report);
    }

    /**
     * @api              {post} /reply-report
     * @apiName          Report reply
     * @apiDescription   ReportReply
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {Number} report_id Report Id.
     * @apiParam {String} content Report content.
     *
     * @apiSuccess {Object} success.
     */
    public function replyReport(ReplyReportRequest $request, $id)
    {
        $this->service->replyReport($id, $request);
        return $this->responseSuccess();
    }

    /**
     * @api              {delete} /report/:id
     * @apiName          Report remove
     * @apiDescription   ReportRemove
     * @apiGroup         Report
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} success.
     */
    public function deleteReport($id)
    {
        $report = Report::find($id);
        if ($report && Auth::user()->can('delete', $report)) {
            $report->delete();
            $this->responseSuccess();
        }

        $this->responseFail();
    }

    /**
     * @param ReportStatisticsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function statistics(ReportStatisticsRequest $request)
    {
        return $this->respond($this->service->statisticsReport($request));
    }
}
