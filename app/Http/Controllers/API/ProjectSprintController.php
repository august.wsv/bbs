<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\Api\UpdateProjectSprintRequest;
use App\Http\Requests\Api\StoreProjectSprintRequest;
use App\Http\Requests\Api\ProjectSprintRequest;
use App\Services\Contracts\IProjectSprintService;
use App\Transformers\ProjectSprintTransformer;
use Illuminate\Support\Facades\Auth;

class ProjectSprintController extends ApiBaseController
{
    /**
     * @var \App\Services\Contracts\IProjectSprintService
     */
    protected $service;

    protected $transformer;
    
    public function __construct(IProjectSprintService $service, ProjectSprintTransformer $transformer)
    {
        $this->service = $service;
        $this->transformer = $transformer;
    }
    
    public function index(ProjectSprintRequest $request)
    {
        return $this->respondTransformer($this->service->get($request));
    }

    public function storeSprint(StoreProjectSprintRequest $request)
    {
        if (!$project = $this->canCrudSprint($request->project_id)) {
            return $this->respondAuthFail();
        }

        $result = $this->service->store($request);

        if (is_array($result)) {
            return $this->respondFail($result['message'], null, $result['code']);
        }

        return $this->respondTransformer($result);
    }

    public function updateSprint(StoreProjectSprintRequest $request, $id)
    {
        if (!$project = $this->canCrudSprint($request->project_id)) {
            return $this->respondAuthFail();
        }

        $result = $this->service->update($request, $id);

        if (is_array($result)) {
            return $this->respondFail($result['message'], null, $result['code']);
        }
        return $this->respondTransformer($result);
    }

    public function deleteSprint(ProjectSprintRequest $request, $id)
    {
        if (!$this->canCrudSprint($request->project_id)) {
            return $this->respondAuthFail();
        }

        $result = $this->service->update($request, $id);

        if (is_array($result)) {
            return $this->respondFail($result['message'], null, $result['code']);
        }
        
        return $this->responseSuccess();
    }

    private function canCrudSprint($projectId)
    {
        $project = $this->service->getProjectById($projectId);
        if (!Auth::user()->can('crudSprint', $project)) {
            return null;
        }

        return $project;
    }
}
