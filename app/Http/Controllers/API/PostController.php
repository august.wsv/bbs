<?php

namespace App\Http\Controllers\API;

use App\Services\Contracts\IPostService;
use App\Transformers\PostTransformer;
use Illuminate\Http\Request;

class PostController extends ApiBaseController
{

    /**
     * @var IPostService
     */
    private $postService;

    /**
     * PostController constructor.
     *
     * @param IPostService    $postService
     * @param PostTransformer $transformer
     */
    public function __construct(
        IPostService $postService,
        PostTransformer $transformer
    )
    {
        $this->postService = $postService;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @api              {get} /posts
     * @apiName          PostList
     * @apiDescription   Post list
     * @apiGroup         Post
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search posts.
     *
     * @apiSuccess {Object[]} posts List of post.
     * @apiSuccess {Object} meta Meta Pagination.
     */
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $posts = $this->postService->search($request, $perPage, $search);
        return $this->respondTransformer($posts);
    }

    /**
     * @api              {get} /posts/:id
     * @apiName          Post detail
     * @apiDescription   GetPost
     * @apiGroup         Post
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} post Post info.
     */
    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $post = $this->postService->detail($id);
        if ($post != null) {
            return $this->respondTransformer($post);
        }
        return $this->respondNotfound();
    }

}
