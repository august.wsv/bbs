<?php

namespace App\Http\Controllers\API;

use App\Events\DayOffNoticeEvent;
use App\Http\Requests\Api\ApproveDayOffRequest;
use App\Http\Requests\Api\ProjectStatisticRequest;
use App\Http\Requests\AskPermissionRequest;
use App\Http\Requests\DayOffRequest;
use App\Http\Requests\CreateDayOffRequest;
use App\Models\Group;
use App\Models\Team;
use App\Repositories\Contracts\IUserRepository;
use App\Services\Contracts\IDayOffService;
use App\Services\Contracts\IUserService;
use App\Services\NotificationService;
use App\Transformers\DayOffTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * UserController
 * Author: jvb
 * Date: 2018/07/16 10:34
 */
class UserController extends ApiBaseController
{
    private $userDayOffService;
    public $notificationService;

    /**
     * UserController constructor.
     *
     * @param \App\Repositories\Contracts\IUserRepository $repository
     * @param \App\Transformers\UserTransformer           $transformer
     */
    public function __construct(
        IUserRepository $repository,
        IUserService $service,
        UserTransformer $transformer,
        IDayOffService $userDayOffService
    ) {
        $this->repository = $repository;
        $this->service = $service;
        $this->transformer = $transformer;
        $this->userDayOffService = $userDayOffService;
        $this->notificationService = app()->make(NotificationService::class);
        parent::__construct();
    }

    /**
     * @api              {get} /users
     * @apiName          UserList
     * @apiDescription   User List
     * @apiGroup         User
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search users.
     *
     * @apiSuccess {Object[]} users List of user.
     * @apiSuccess {Object} meta Meta Pagination.
     */

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $collections = $this->service->getContact($request, $perPage, $search, false);
        return $this->respondTransformer($collections);
    }

    /**
     * @api              {get} /users/:id
     * @apiName          UserDetail
     * @apiDescription   User detail
     * @apiGroup         User
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} user User info.
     */

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $report = $this->service->detail($id);
        if ($report != null) {
            return $this->respondTransformer($report);
        }
        return $this->respondNotfound();
    }

    /**
     * @param DayOffRequest $request
     * @param null $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function dayOff(DayOffRequest $request, $status = null)
    {
        $availableDayLeft = $this->userDayOffService->getDayOffUser($request, Auth::id(), true, true);
        return  $this->respondTransformer($availableDayLeft, new DayOffTransformer(), 'day_offs');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserManager()
    {
        $userManager = $this->service->getUserManager();
        return  $this->respond($userManager);
    }

    /**
     * @param AskPermissionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function askPermissionCreate(AskPermissionRequest $request)
    {
        try {
            $message = $this->service->askPermissionCreate($request) ? __l('success') : __l('approved');
            return $this->respond([], Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            Log::error($e);
            return $this->respondInternal($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function detailPermission(Request $request)
    {
        try {
            $request->validate([
                'data' => 'required|date',
                'type' => 'required|between:1,4',
            ]);
            return $this->respond($this->service->detailOTWorkTimesExplanation($request));
        } catch (\Exception $e) {
            Log::error($e);
            return $this->respondInternal($e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOtAndAskPermission()
    {
        return $this->respond($this->service->getOtAndAskPermission());
    }

    /**
     * @param CreateDayOffRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dayOffCreate(CreateDayOffRequest $request)
    {
        try {
            $this->service->dayOffSave($request);
            return $this->responseSuccess();
        } catch (\Exception $e) {
            Log::error($e);
            return $this->respondInternal($e->getMessage());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function dayOffDetail($id)
    {
        return $this->respond($this->userDayOffService->getOneData($id));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function group()
    {
        $data = Group::with('teams:id,name')->select('id', 'name')->get();
        return $this->respond($data);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function teans()
    {
        return $this->respond(Team::select('name', 'id')->get()->toArray());
    }

    /**
     * @param ApproveDayOffRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function approveDayOff(ApproveDayOffRequest $request, $id)
    {
        try {
            $dayOff = $this->userDayOffService->findDayOff($id);
            if ($dayOff->approver_id != Auth::id()) {
                return $this->respond([], Response::HTTP_FORBIDDEN, __l('dont_permission'));
            }
            if ($dayOff->status != STATUS_DAY_OFF['abide']) {
                return $this->respond([], Response::HTTP_BAD_REQUEST, __l('approved'));
            }
            if ($request->status == STATUS_DAY_OFF['active']) {
                $this->userDayOffService->calculateDayOff($request, $id);
            } else {
                $dayOff->approve_comment = $request->approve_comment;
                $dayOff->status = STATUS_DAY_OFF['noActive'];
                $dayOff->save();
                event(new DayOffNoticeEvent($dayOff, Auth::user(), NOTIFICATION_DAY_OFF['close']));
            }
            return $this->respond();
        } catch (\Exception $e) {
            Log::error($e);
            return $this->respondInternal($e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function dayOffApprove()
    {
        return $this->respond($this->userDayOffService->getDayOffApprove());
    }

    public function getUserList(Request $request)
    {
        $collections = $this->service->getUserList($request, $perPage, $search);
        return $this->respond($collections);
    }

    /**
     * @param ProjectStatisticRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function projectStatistic(ProjectStatisticRequest $request){
        return $this->respond($this->service->projectStatistic($request->all()));
    }

    public function userStatisticPerformance(Request $request){
        $request->validate([
            'day_number' => 'required|integer',
            'user_id' => 'required|integer|exists:users,id',
        ]);
        return $this->respond($this->service->userStatisticPerformance($request->all()));
    }
}
