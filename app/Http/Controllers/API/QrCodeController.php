<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Transformers\QrCodeTransformer;
use App\Services\Contracts\IQrCodeService;

class QrCodeController extends ApiBaseController
{
    protected $service;
    public function __construct(
        IQrCodeService $service,
        QrCodeTransformer $transformer
    ) {
        $this->service = $service;
        $this->transformer = $transformer;
        parent::__construct();
    }

    public function refresh(Request $request)
    {
        $qrCode = $this->service->refresh($request);

        return $this->respond(['qr_code' => $qrCode]);
    }
}
