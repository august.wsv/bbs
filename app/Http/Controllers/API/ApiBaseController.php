<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Traits\ParseRequestSearch;
use App\Traits\RESTActions;

/**
 * UserController
 * Author: jvb
 * Date: 2018/09/16 10:34
 */
class ApiBaseController extends Controller
{
    use RESTActions;
    use ParseRequestSearch;

    public function __construct()
    {
    }

    public function responseSuccess()
    {
        return $this->respond([
            'success' => true
        ]);
    }

    public function responseFail($error = '')
    {
        return $this->respond([
            'success' => false,
            'error' => $error
        ]);
    }
}
