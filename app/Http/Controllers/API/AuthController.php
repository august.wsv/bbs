<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\RegisterFormRequest;
use App\Http\Resources\UserDetailResource;
use App\Models\User;
use App\Repositories\Contracts\IUserRepository;
use App\Services\Contracts\IAuthService;
use App\Services\Contracts\IPotatoService;
use App\Services\Contracts\IUserService;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException as ValidationExceptionAlias;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * UserController
 * Author: jvb
 * Date: 2018/09/16 10:34
 */
class AuthController extends ApiBaseController
{
    public $successStatus = 200;

    protected $authService;

    /**
     * AuthController constructor.
     *
     * @param \App\Repositories\Contracts\IUserRepository $repository
     * @param \App\Services\Contracts\IUserService        $service
     * @param \App\Transformers\UserTransformer           $transformer
     */
    public function __construct(
        IUserRepository $repository,
        IUserService $service,
        UserTransformer $transformer,
        IAuthService $authService
    ) {
        $this->repository = $repository;
        $this->service = $service;
        $this->transformer = $transformer;
        $this->authService = $authService;
        parent::__construct();
    }

    /**
     * @param \App\Http\Requests\RegisterFormRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterFormRequest $request)
    {
        $user = $this->service->register($request);

        return $this->respond($user);
    }

    /**
     * @api              {post} /login
     * @apiName          Login
     * @apiDescription   Login
     * @apiGroup         Auth
     *
     * @apiParam {String} email Email of User.
     * @apiParam {String} password Password of User.
     *
     * @apiSuccess {Object} User User info.
     * @apiSuccess {Object} Meta meta.token for jwt authentication.
     *
     * @apiError         LoginFail http status code 422
     */

    /**
     * @param Request $request
     *
     * @return UserDetailResource
     * @throws ValidationExceptionAlias
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $token = $this->authService->login($request);
        
        if (!$token) {
            return response()->json([
                'errors' => [
                    'email' => ['Sai tên đăng nhập hoặc mật khẩu']
                ]
            ], 401);
        }
        
        $userDetail = $request->user();

        return (new UserDetailResource($userDetail))
            ->additional([
                'meta' => [
                    'token' => $token,
                    'fcm_token' => $userDetail->fcm_token,
                ]
            ]);
    }

    /**
     * @api              {post} /logout
     * @apiName          Logout
     * @apiDescription   Logout
     * @apiGroup         Auth
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     */

    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        \auth()->logout();
    }

    //AuthController
    public function refreshToken()
    {
        $token = JWTAuth::getToken();
        if (!$token) {
            $this->respondFail('Token not provided');
        }
        try {
            $token = JWTAuth::refresh($token);
        } catch (TokenInvalidException $e) {
            $this->respondFail('The token is invalid');
        }
        return $this->respond(['token' => $token]);
    }

    /**
     * @api              {get} /logout
     * @apiName          UserInfo
     * @apiDescription   UserInfo
     * @apiGroup         Auth
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} User User info.
     */
    public function user(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return (new UserDetailResource($user));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function info(Request $request)
    {
        $request->merge(['get_all' => true]);
        $potatos = $this->potatoService->showList(Auth::id(), $request);
        $user = User::where('id', Auth::id())
            ->with('level:id,name,introduce,image_url', 'test_histories.question_set:id,name')
            ->first();

        $data = [
            'user' => $user,
            'current_potato' => $this->potatoService->currentPotato(Auth::id()),
            'potatos' => $potatos,
        ];

        return $this->respond($data);
    }

    /**
     * details user api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();

        return response()->json(
            [
                'success' => $user
            ],
            $this->successStatus
        );
    }
}
