<?php

namespace App\Http\Controllers\API;
use App\Services\NotificationService;
use Illuminate\Http\Response;

class NotificationController extends ApiBaseController
{

    private $notificationService;
    public function __construct(
        NotificationService $notificationService
    ){
        $this->notificationService = $notificationService;
        $this->middleware('auth:api');
    }

    public function getUserNotifications()
    {
        return $this->respond(
            $this->notificationService->getUserNotifications()
        );
    }

    public function markRead()
    {
        $this->notificationService->markRead();
        return $this->respond();
    }
}