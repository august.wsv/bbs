<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\Api\CheckinOrCheckoutNormalRequest;
use App\Http\Requests\Api\CheckinOrCheckoutQrCodeRequest;
use App\Models\User;
use App\Services\Contracts\IWorkTimeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

/**
 * WorktimeController
 * Author: jvb
 * Date: 2022/07/16 10:34
 */
class WorktimeController extends ApiBaseController
{

    /**
     * UserController constructor.
     * @param IWorkTimeService $service
     */
    public function __construct(IWorkTimeService $service)
    {
        $this->service = $service;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sync(Request $request)
    {
        $allData = $request->all();

        try {
            $collectionData = collect($allData);
            $userIds = User::select('id')->whereIn('staff_code', $collectionData
                ->pluck('StaffCode')->toArray())->get()->pluck('id')->toArray();
            $startDate = \Carbon::createFromFormat(DATE_TIME_FORMAT_VI, $collectionData
                ->min('Workday'))->format(DATE_FORMAT);
            $endDate = \Carbon::createFromFormat(DATE_TIME_FORMAT_VI, $collectionData
                ->max('Workday'))->format(DATE_FORMAT);

            // create collection data
            $groupByDate = $collectionData->groupBy('Workday');
            // start row heading
            $allItems = ['', ''];

            foreach ($groupByDate as $workDate => $worktimes) {
                $items = [];
                foreach ($worktimes->groupBy('StaffCode') as $userCode => $data) {
                    if (!in_array($userCode, $items)) {
                        array_push($items, $userCode);
                        array_push(
                            $allItems,
                            collect([
                                // STT
                                '',
                                $userCode,
                                // Phong ban
                                '',
                                // Chuc danh
                                '',
                                $workDate,
                                // Thu
                                '',
                                $data->min('CheckIn'),
                                $data->max('CheckOut'),
                            ])
                        );
                    }
                }
            }
            $workTimeImport = new \App\Imports\WorkTimeImport($startDate, $endDate, $userIds, 'd/m/Y');
            // WorkTimeImport.collection to import data

            $workTimeImport->collection(collect($allItems));

            return $this->respond([
                'success' => true
            ]);
        } catch (\Exception $e) {
            return $this->respondFail([
                'success' => false,
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $userId = $request->user_id ?? Auth::id();
        return $this->respond($this->service->workTimeUser($request, $userId));
    }

    public function checkin(CheckinOrCheckoutNormalRequest $request)
    {
        return $this->respond([], Response::HTTP_OK, $this->service->doCheckinOrCheckout($request));
    }

    public function checkQRCode(CheckinOrCheckoutQrCodeRequest $request)
    {
        $request->merge(['user_id' => Auth::id()]);
        return $this->respond([], Response::HTTP_OK, $this->service->doCheckinOrCheckoutByQRCode($request));
    }
}
