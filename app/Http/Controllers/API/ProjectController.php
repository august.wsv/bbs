<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\AddProjectTaskRequest;
use App\Http\Requests\Api\ProjectTaskProgressRequest;
use App\Http\Requests\Api\TaskLogTimeRequest;
use App\Http\Requests\Api\ProjectOverviewRequest;
use App\Http\Requests\UpdateProjectTaskRequest;
use App\Services\Contracts\IProjectService;
use App\Transformers\ProjectTaskTransformer;
use App\Transformers\ProjectTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class ProjectController extends ApiBaseController
{

    /**
     * @var IProjectService
     */
    private $projectService;

    /**
     * ProjectController constructor.
     *
     * @param IProjectService    $projectService
     * @param ProjectTransformer $transformer
     */
    public function __construct(
        IProjectService $projectService,
        ProjectTransformer $transformer
    ) {
        $this->projectService = $projectService;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @api              {get} /projects
     * @apiName          List project
     * @apiDescription   ProjectList
     * @apiGroup         Project
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiParam {String} [search] Keyword to search projects.
     *
     * @apiSuccess {Object[]} projects List of project.
     * @apiSuccess {Object} meta Meta Pagination.
     */
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $projects = $this->projectService->search($request, $perPage, $search);
        return $this->respondTransformer($projects);
    }

    /**
     * @api              {get} /projects/joined
     * @apiName          List Joined project
     * @apiDescription   ProjectJoined
     * @apiGroup         Project
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object[]} projects List of project.
     * @apiSuccess {Object} meta Meta Pagination.
     */
    public function joined(Request $request)
    {
        $projects = $this->projectService->joinedList(\Auth::user());
        return $this->respondTransformer($projects);
    }

    /**
     * @api              {get} /projects/:id
     * @apiName          ProjectDetail
     * @apiDescription   GetProject
     * @apiGroup         Project
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} project Project info.
     */
    public function detail($id)
    {
        $project = $this->projectService->detail($id);
        if ($project != null) {
            return $this->respondTransformer($project);
        }
        return $this->respondNotfound();
    }

    /**
     * @api              {post} /projects/:id/tasks
     * @apiName          ProjectCreateTask
     * @apiDescription   createTask
     * @apiGroup         Project
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} project Project info.
     */
    public function createTask(AddProjectTaskRequest $request, $id)
    {
        try {
            $project = $this->projectService->detail($id);
            $this->authorize('create_task', $project);
            $data = $request->all();
            $taskNames = array_values(array_filter(array_map('trim', explode("\n", $request->name))));
            $tasks = collect([]);
            foreach ($taskNames as $taskName) {
                $data['name'] = $taskName;
                $task = $this->projectService->createTask(\Auth::user(), $project, $data);
                $tasks->push($task);
            }
            return $this->respond($tasks);
        } catch (\Exception $e) {
            Log::error($e);
            return $this->respondInternal($e->getMessage());
        }
    }

    /**
     * @api              {put} /projects/:id/tasks/:taskId
     * @apiName          ProjectUpdateTask
     * @apiDescription   updateTask
     * @apiGroup         Project
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} project Project info.
     */
    public function updateTask(UpdateProjectTaskRequest $request, $id, $taskId)
    {
        try {
            $project = $this->projectService->detail($id);
            $this->authorize('edit_task', $project);
            $data = $request->all();
            $task = $this->projectService->updateTask(\Auth::user(), $project, $taskId, $data);
            return $this->respond($task);
        } catch (\Exception $e) {
            Log::error($e);
            return $this->respondInternal($e->getMessage());
        }
    }

    /**
     * @api              {put} /projects/:id/tasks/:taskId
     * @apiName          ProjectDeleteTask
     * @apiDescription   deleteTask
     * @apiGroup         Project
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} project Project info.
     */
    public function deleteTask($id, $taskId)
    {
        try {
            $task = $this->projectService->deleteTask(\Auth::user(), $id, $taskId);
            return $this->respond($task);
        } catch (\Exception $e) {
            Log::error($e);
            return $this->respondInternal($e->getMessage(), $e->getCode());
        }
    }


    /**
     * @api              {get} /projects/:id
     * @apiName          ProjectOverview
     * @apiDescription   GetProject
     * @apiGroup         Project
     *
     * @apiHeader (Bearer Header) {String} Authorization Bearer token.
     *
     * @apiSuccess {Object} project Project info.
     */
    public function overview(ProjectOverviewRequest $request, $id)
    {
        $project = $this->projectService->overview($request->all(), \Auth::user(), $id);
        if ($project != null) {
            return $this->respondTransformer($project);
        }
        return $this->respond();
    }

    /**
     * @param TaskLogTimeRequest $request
     * @param $taskId
     * @return \Illuminate\Http\JsonResponse
     */
    public function logWork(TaskLogTimeRequest $request, $taskId)
    {
        return $this->respond($this->projectService->logWork($request, $taskId));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function taskDetail($id)
    {
        return $this->respond($this->projectService->taskDetail($id));
    }

    /**
     * @param ProjectTaskProgressRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTaskProgress(ProjectTaskProgressRequest $request, $id){
        return $this->respondTransformer(
            $this->projectService->getTaskProgress($request->all(), $id, \Auth::user()),
            new ProjectTaskTransformer(),
            'project_tasks'
        );
    }

    public function overviewTaskLogWork(Request $request)
    {
        return $this->respond($this->projectService->overviewTaskLogWork($request));
    }
}
