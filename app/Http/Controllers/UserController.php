<?php

namespace App\Http\Controllers;

use App\Events\AskPermissionPrivateNoticeEevnt;
use App\Events\DayOffNoticeEvent;
use App\Helpers\DateTimeHelper;
use App\Http\Requests\Admin\WorkTimeRequest;
use App\Http\Requests\ApprovedRequest;
use App\Http\Requests\ApprovePermissionRequest;
use App\Http\Requests\AskPermissionRequest;
use App\Http\Requests\CreateDayOffRequest;
use App\Http\Requests\DayOffRequest;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\WorkTimeCalendarRequest;
use App\Models\AnswerUser;
use App\Models\CalendarOff;
use App\Models\CalendarOffJapan;
use App\Models\DayOff;
use App\Models\Group;
use App\Models\OverTime;
use App\Models\Project;
use App\Models\Question;
use App\Models\Quiz;
use App\Models\RemainDayoff;
use App\Models\User;
use App\Models\Team;
use App\Models\WorkTime;
use App\Models\WorkTimesExplanation;
use App\Repositories\Contracts\IDayOffRepository;
use App\Services\Contracts\IDayOffService;
use App\Services\Contracts\IUserService;
use App\Services\Contracts\IWorkTimeService;
use App\Services\NotificationService;
use App\Transformers\DayOffTransformer;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_encode;

class UserController extends Controller
{
    private $userService;
    private $userDayOffService;
    private $dayOffRepository;
    public $notificationService;
    private $workTimeService;

    public function __construct(
        IUserService $userService,
        IDayOffService $userDayOffService,
        IDayOffRepository $dayOffRepository,
        IWorkTimeService $workTimeService
    ) {
        $this->notificationService = app()->make(NotificationService::class);
        $this->dayOffRepository = $dayOffRepository;
        $this->userService = $userService;
        $this->userDayOffService = $userDayOffService;
        $this->workTimeService = $workTimeService;
    }

    public function index()
    {
        return view('end_user.user.index');
    }

    public function profile()
    {
        $user = Auth::user();
        return view('end_user.user.profile', compact('user'));
    }

    public function saveProfile(ProfileRequest $request)
    {
        $only = [
            'current_address',
            'gmail', 'gitlab',
            'chatwork',
            'skills',
            'in_future',
            'hobby',
            'foreign_language',
            'school',
        ];

        $data = $request->only($only);

        if ($request->hasFile('avatar')) {
            $avatar = request()->file('avatar');
            $extension = $avatar->getClientOriginalExtension();
            $avatarName = Auth::user()->staff_code . '-' . rand() . '.' . $extension;
            $destinationPath = public_path(URL_IMAGE_AVATAR);
            $data['avatar'] = URL_IMAGE_AVATAR . $avatarName;
            $avatar->move($destinationPath, $avatarName);
        }

        User::updateOrCreate([
            'id' => Auth::id(),
        ], $data);

        return redirect(route('profile'))->with('success', 'Thiết lập hồ sơ thành công!');
    }

    public function changePassword()
    {
        return view('end_user.user.change_password');
    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();

        $this->validate(
            $request,
            [
                'current_password' => ['required', function ($attribute, $value, $fail) use ($user) {
                    if (!\Hash::check($value, $user->password)) {
                        return $fail(__('auth.current_password_incorrect'));
                    }
                },],
                'password' => [
                    'required',
                    'min:8',
                    'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@]).*$/',
                    'confirmed',
                    'different:current_password'
                ],
            ],
            [
                'different' => 'Mật khẩu mới phải khác mật khẩu cũ'
            ],
            ['password' => 'mật khẩu mới']
        );

        $user->password = $request->get('password');
        $user->save();
        Auth::logout();

        return redirect('/login')->with('notification_change_pass', __('messages.notification_change_pass'));
    }

    public function workTime()
    {
        $userss = $this->userService->getUserByGroup();
        return view('end_user.user.work_time', compact('userss'));
    }

    /**
     * Show work time calendar
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function workTimeAPI(Request $request)
    {
        $userId = $request->user_id ?? Auth::id();
        $lastyear = (int)date('Y') - 1;
        $this->validate($request, [
            'year' => "required|min:" . $lastyear . "|integer|max:" . date('Y'),
            'month' => 'required|integer|max:12',
        ]);
        $calendarData = $this->workTimeService->workTimeUser($request, $userId);

        $explanationCalendar = WorkTimesExplanation::where('user_id', $userId)
            ->whereYear('work_day', $request->year)
            ->whereMonth('work_day', $request->month);
        $calendarOffModel = new CalendarOff();
        $calendarOffJapanModel = new CalendarOffJapan();
        $listDayOffInMonth = $calendarOffModel->getDayOffInMonth($request->month);
        $allDateOffInThisMonth = $this->userService->getDayOff($listDayOffInMonth, $request->month, $request->year);
        $listDayOffJapanInMonth = $calendarOffJapanModel->getDayOffJapanInMonth($request->month);

        $allDateOffJapanInThisMonth = $this->userService->getDayOff(
            $listDayOffJapanInMonth,
            $request->month,
            $request->year
        );

        $calendarDataModal = [];
        if (isset($explanationCalendar)) {
            foreach ($explanationCalendar->get()->toArray() as $item) {
                $calendarDataModal[] = [
                    'id' => $item['id'],
                    'work_day' => $item['work_day'],
                    'type' => $item['type'],
                    'ot_type' => $item['ot_type'],
                    'status' => $item['status'],
                    'note' => $item['note'],
                    'user_id' => $item['user_id'],
                ];
            }
        }
        return response([
            'success' => true,
            'message' => 'success',
            'data' => $calendarData,
            'dataModal' => $calendarDataModal,
            'all_date_off_in_this_month' => $allDateOffInThisMonth,
            'all_date_off_japan_in_this_month' => $allDateOffJapanInThisMonth
        ]);
    }

    /**
     * Create or edit work time calendar
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function dayOffCreateCalendar(WorkTimeRequest $request)
    {
        $reason = $request['reason'];
        $otType = $request['ot_type'];
        $explanationType = $request['explanation_type'];
        $workTimeExplanation = $this->getWorkTimeExplanation($request['work_day'])
            ->where('type', $explanationType)->first();
        if ($workTimeExplanation) {
            $workTimeExplanation->update(['note' => $reason, 'ot_type' => $otType, 'type' => $explanationType]);
            return back()->with('day_off_success', '');
        } else {
            WorkTimesExplanation::create([
                'user_id' => Auth::id(),
                'work_day' => $request['work_day'],
                'type' => $explanationType,
                'ot_type' => $otType,
                'note' => $reason
            ]);
            return back()->with('day_off_success', 'day_off_success');
        }
    }

    public function workTimeDetailAskPermission(Request $request)
    {
        if ($request->has('explanationOtType')) {
            $workTimeExplanation = OverTime::where('creator_id', Auth::id())
                ->where('work_day', $request['work_day'])
                ->whereIn('status', array_values(WORK_TIME_OT_STATUS))
                ->where('ot_type', $request['explanationOtType'])->first();
        } elseif ($request->has('explanationType')) {
            $workTimeExplanation = $this->getWorkTimeExplanation($request['work_day'])
                ->whereIn('status', array_values(WORK_TIME_OT_STATUS))
                ->where('type', $request['explanationType'])->first();
        }
        $projectName = $this->projectActive()->toArray();
        $explanationRes = [];
        if ($workTimeExplanation) {
            $explanationRes = $workTimeExplanation->toArray();
            $explanationRes['project'] = $projectName;
            return $explanationRes;
        } else {
            return $projectName;
        }
    }

    public function workTimeAskPermission(WorkTimeCalendarRequest $request)
    {

        if ($request->user_login == Auth::id()) {
            $minute = DateTimeHelper::getMinutesBetweenTwoTime($request['start_at'], $request['end_at']);
            $reason = $request['reason'];
            $workDay = $request['work_day'];
            $lateWork = array_search('Đi muộn', WORK_TIME_TYPE);
            $earlyLeave = array_search('Về sớm', WORK_TIME_TYPE);

            if ($request->has('ot_type')) {
                $projectType = 1;
                //default is by project
                $otType = $request->get('ot_type', $projectType);
                if ($otType == $projectType) {
                    $project = Project::find($request['project_id'])->name;
                } else {
                    $project = null;
                }
                $workTimeExplanation = OverTime::where('creator_id', Auth::id())
                    ->where('work_day', $workDay)->where('status', '!=', array_search('Từ chối', OT_STATUS))->first();
                if ($workTimeExplanation) {
                    $workTimeExplanation->update([
                        'reason' => $reason,
                        'minute' => $minute,
                        'ot_type' => $otType,
                        'project_id' => $request['project_id'],
                        'project_name' => $project
                    ]);
                    $responseBack = back()->with('wt_permission_success', '');
                } else {
                    OverTime::create([
                        'creator_id' => Auth::id(),
                        'minute' => $minute,
                        'work_day' => $request['work_day'],
                        'status' => array_search('Chưa duyệt', OT_STATUS),
                        'reason' => $reason,
                        'start_at' => $request['start_at'],
                        'end_at' => $request['end_at'],
                        'ot_type' => $otType,
                        'project_id' => $request['project_id'],
                        'project_name' => $project,
                    ]);
                    $responseBack = back()->with('wt_permission_success', 'wt_permission_success');
                }
            } elseif (in_array($request['explanation_type'], [$lateWork, $earlyLeave])) {
                $workTimeExplanation = $this->getWorkTimeExplanation($workDay)
                    ->where('type', $request['explanation_type'])->first();
                if ($workTimeExplanation) {
                    $workTimeExplanation->update(['type' => $request['explanation_type'], 'note' => $reason]);
                    $responseBack = back()->with('wt_permission_success', '');
                } else {
                    WorkTimesExplanation::create([
                        'user_id' => Auth::id(),
                        'work_day' => $request['work_day'],
                        'type' => $request['explanation_type'],
                        'note' => $reason
                    ]);
                    $responseBack = back()->with('wt_permission_success', 'wt_permission_success');
                }
            }
        } else {
            abort(404);
        }

        return $responseBack;
    }

    public function workTimeGetProject(Request $request)
    {
        return $this->projectActive()->toArray();
    }

    public function askPermission(Request $request, $status = null)
    {
        $autoShowModal = $request->has('t');
        $queryDate = date('Y-m-01', strtotime('- 90 days'));
        $query = WorkTimesExplanation::select(
            'work_times_explanation.id',
            'work_times_explanation.work_day',
            'work_times_explanation.type',
            'work_times_explanation.ot_type',
            'work_times_explanation.note',
            'work_times_explanation.status as work_times_explanation_status',
            'work_times_explanation.user_id',
            'work_times_explanation.approver_id',
            'work_times_explanation.reason_reject',
            'ot_times.creator_id',
            'ot_times.id as id_ot_time',
            'ot_times.status as ot_time_status',
            'users.name as approver',
            'work_times_explanation.option_time'
        )
            ->leftJoin('ot_times', function ($join) {
                $join->on('ot_times.creator_id', '=', 'work_times_explanation.user_id')
                    ->on('ot_times.work_day', '=', 'work_times_explanation.work_day');
            })->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'ot_times.approver_id');
            })
            ->whereDate('work_times_explanation.work_day', '>=', $queryDate);

        $queryLeader = clone $query;
        $dataLeader = $queryLeader->groupBy(
            'work_times_explanation.work_day',
            'work_times_explanation.type',
            'work_times_explanation.ot_type',
            'work_times_explanation.note',
            'work_times_explanation.user_id',
            'work_times_explanation.option_time',
            'ot_times.creator_id'
        )
            ->where('user_id', '!=', Auth::id())
            ->orderBy('work_times_explanation.status', 'asc')
            ->orderBy('work_times_explanation.updated_at', 'desc')->get();
        $askPermission = $this->permissionGetExplanation()
            ->where('user_id', Auth::id())
            ->askPermissionLastSixMonth()
            ->get();
        $otTimes = $this->permissionGetOverTime()
            ->where('creator_id', Auth::id())
            ->askPermissionLastSixMonth()
            ->orderBy('work_day', 'desc')->get();

        $groups = Group::all();
        $managerApproveOther = $this->permissionGetExplanation()
            ->select(
                'work_times_explanation.id',
                'work_times_explanation.work_day',
                'work_times_explanation.user_id',
                'work_times_explanation.type',
                'work_times_explanation.note',
                'work_times_explanation.reason_reject',
                'work_times_explanation.status',
                'groups.name as groups'
            )
            ->join('user_teams', 'work_times_explanation.user_id', '=', 'user_teams.user_id')
            ->join('teams', 'teams.id', '=', 'user_teams.team_id')
            ->join('groups', 'groups.id', '=', 'teams.group_id')
            ->with('creator:id,name')
            ->where('user_teams.deleted_at', null)
            ->showApproving()
            ->orderBy('work_times_explanation.status', 'asc')->orderBy('work_times_explanation.updated_at', 'desc')
            ->get();

        $managerApproveOT = OverTime::select(
            'ot_times.id',
            'ot_times.work_day',
            'ot_times.creator_id',
            'ot_times.ot_type',
            'ot_times.project_id',
            'ot_times.reason',
            'ot_times.note_respond',
            'ot_times.status',
            'ot_times.start_at',
            'ot_times.end_at',
            'ot_times.minute',
            'groups.name as groups'
        )
            ->join('user_teams', 'ot_times.creator_id', '=', 'user_teams.user_id')
            ->join('teams', 'teams.id', '=', 'user_teams.team_id')
            ->join('groups', 'groups.id', '=', 'teams.group_id')
            ->with('authorizer:id,name')
            ->where('user_teams.deleted_at', null)
            ->onApproving()
            ->orderBy('ot_times.work_day', 'desc')
            ->get();

        $assigns = $this->userService->getUsersAssginOt();
        $projects = Project::where('status', ACTIVE_STATUS)->orderBy('name')
            ->get();

        $dataDayOff = $this->userDayOffService->getDataDate($request, $status ?? ALL_DAY_OFF);

        $response = [
            'askPermission' => $askPermission,
            'otTimes' => $otTimes,
            'dataLeader' => $dataLeader,
            'managerApproveOther' => $managerApproveOther,
            'managerApproveOT' => $managerApproveOT,
            'assigns' => $assigns,
            'projects' => $projects,
            'groups' => $groups,
            'autoShowModal' => $autoShowModal,
            'dataDayOff' => $dataDayOff
        ];

        return view('end_user.user.ask_permission', $response);
    }

    public function getDayOff(Request $request, $status = ALL_DAY_OFF)
    {
        return $this->userDayOffService->getDayOff($request, $status);
    }

    public function approveDetail(Request $request)
    {
        $datas = [];
        if ($request['permission-type'] == 'ot') {
            // ##
            $user = Auth::user();
            $overTime = OverTime::find($request['id']);
            if ($overTime && $user->can('approverOt', $overTime)) {
                $datas['id'] = $overTime['id'];
                $datas['creator_id'] = $overTime->creator->name;
                $datas['project_name'] = $overTime->project->name ?? '';
                $datas['start_at'] = $overTime['start_at'];
                $datas['end_at'] = $overTime['end_at'];
                $datas['minute'] = $overTime['minute'];
                $datas['reason'] = $overTime['reason'];
                $datas['status'] = $overTime['status'];
                $datas['work_day'] = $overTime['work_day'];
                $datas['caculator_time'] = $overTime->description_time;
            } else {
                return response()->json(['error' => true], 403);
            }
        } elseif ($request['permission-type'] == 'other') {
            $workTimeExplanation = WorkTimesExplanation::find($request['id']);
            $datas['id'] = $workTimeExplanation['id'];
            $datas['work_day'] = $workTimeExplanation['work_day'];
            $datas['option_time'] = $workTimeExplanation['hours_permission'];
            $datas['user_id'] = $workTimeExplanation->creator->name ?? '';
            $datas['note'] = $workTimeExplanation['note'];
            $datas['status'] = $workTimeExplanation['status'];
        }
        return $datas;
    }

    /**
     * @param AskPermissionRequest $request
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function askPermissionCreate(AskPermissionRequest $request)
    {
        $data = $this->userService->askPermissionCreate($request);
        return $data
            ? redirect(route('ask_permission'))
            ->with('create_permission_success', 'create_permission_success')
            : back()->with('permission_error', '');
    }

    public function askPermissionEarly(Request $request)
    {
        $workTimeExplanation = $this->getWorkTimeExplanation($request['work_day'])->first();
        if ($workTimeExplanation) {
            $workTimeExplanation->update([
                'ot_type' => $request['ot_type'],
                'note' => $request['note'],
                'work_day' => $request['work_day']
            ]);
        } else {
            WorkTimesExplanation::create([
                'user_id' => Auth::id(),
                'work_day' => $request['work_day'],
                'type' => $request['type'],
                'ot_type' => $request['ot_type'],
                'note' => $request['note'],
            ]);
        }
        return back()->with('create_permission_success', '');
    }

    public function askPermissionModal(Request $request)
    {
        $request->validate([
            'data' => 'required|date',
            'type' => 'required|between:1,4',
        ]);
        $datas = $this->userService->detailOTWorkTimesExplanation($request);
        $projects = Project::where('status', ACTIVE_STATUS)->orderBy('name')->get();
        if ($datas) {
            return [$datas, $projects];
        } else {
            return ['', $projects];
        }
    }

    public function approvePermission(ApprovePermissionRequest $request)
    {
        $permissionType = $request['permission_type'];

        if ($permissionType == 'ot') {
            try {
                $overTime = OverTime::findOrFail($request['id']);
                $overTime->status = $request['approve_type'];
                $overTime->note_respond = $request['reason_approve'];
                $overTime->approver_id = Auth::id();
                $overTime->approver_at = Carbon::now();
                $overTime->save();
                $statusType = $overTime->status == ACTIVE_STATUS ? ACTIVE_STATUS : UNACTIVE_STATUS;
                event(new AskPermissionPrivateNoticeEevnt($overTime, $permissionType, $statusType));

                $message = $overTime->status == ACTIVE_STATUS
                    ? __l('success_approve_active')
                    : __l('success_approve_unactive');
                $responseJson = response()->json(['status' => 'success', 'message' => $message]);
            } catch (\Exception $e) {
                $responseJson = response()->json(['status' => 'error', 'message' => __l('error_approve')]);
            }
        } elseif ($permissionType == 'other') {
            try {
                $workTimesExplanation = WorkTimesExplanation::findOrFail($request['id']);

                $workTimesExplanation->status = $request['approve_type'];
                $workTimesExplanation->approver_id = Auth::id();
                $workTimesExplanation->approver_at = Carbon::now();
                $workTimesExplanation->reason_reject = $request['reason_approve'];
                $workTimesExplanation->save();
                $statusType = $workTimesExplanation->status == ACTIVE_STATUS ? ACTIVE_STATUS : UNACTIVE_STATUS;
                event(new AskPermissionPrivateNoticeEevnt($workTimesExplanation, $permissionType, $statusType));

                $message = $workTimesExplanation->status == ACTIVE_STATUS
                    ? __l('success_approve_active')
                    : __l('success_approve_unactive');
                $responseJson = response()->json(['status' => 'success', 'message' => $message]);
            } catch (\Exception $e) {
                $responseJson = response()->json(['status' => 'error', 'message' => __l('error_approve')]);
            }
        }

        return $responseJson;
    }

    public function approved(ApprovedRequest $request)
    {
        $workTimesExplanationID = $request['id'];
        if ($workTimesExplanationID) {
            WorkTimesExplanation::where('id', $workTimesExplanationID)
                ->update([
                    'status' => array_search('Đã duyệt', OT_STATUS),
                    'approver_id' => Auth::id(),
                    'approver_at' => Carbon::now()
                ]);
            return back()->with('approver_success', '');
        }
    }

    public function approvedOT(ApprovedRequest $request)
    {
        $workTimesExplanationID = $request['id'];
        if ($workTimesExplanationID) {
            WorkTimesExplanation::where('id', $workTimesExplanationID)
                ->update([
                    'status' => array_search('Đã duyệt', OT_STATUS),
                    'approver_id' => Auth::id(),
                    'approver_at' => Carbon::now()
                ]);
        }
        OverTime::create([
            'creator_id' => $request['user_id'],
            'reason' => $request['reason'],
            'status' => array_search('Đã duyệt', OT_STATUS),
            'approver_id' => Auth::id(),
            'approver_at' => now(),
            'work_day' => $request['work_day'],
        ]);
        return back()->with('approver_success', '');
    }

    //
    //
    //  DAY OFF SECTION
    //
    //

    public function dayOff(DayOffRequest $request, $status = null)
    {
        $countDayOff = $this->userDayOffService->countDayOffUserLogin();
        $userManager = $this->userService->getUserManager();
        $availableDayLeft = $this->userDayOffService->getDayOffUser($request, Auth::id(), true);
        $autoShowModal = $request->has('t');
        $response = [
            'availableDayLeft' => $availableDayLeft,
            'userManager' => $userManager,
            'countDayOff' => $countDayOff,
            'autoShowModal' => $autoShowModal,
        ];

        if (isset($request->status_search) || isset($request->search_end_at) || isset($request->search_start_at)) {
            $searchStartDate = $request->search_start_at;
            $searchEndDate = $request->search_end_at;
            $statusSearch = $request->status_search;

            $dayOff = $this->userDayOffService->searchStatus($searchStartDate, $searchEndDate, $statusSearch);
            $response = array_merge(
                $response,
                [
                    'dayOff' => $dayOff,
                    'statusSearch' => $statusSearch,
                    'searchEndDate' => $searchEndDate,
                    'searchStratDate' => $searchStartDate
                ]
            );
            return view('end_user.user.day_off', $response);
        }
        return view('end_user.user.day_off', $response);
    }

    public function dayOffListApprovalAPI(Request $request)
    {
        $response = [
            'success' => false,
            'message' => NOT_AUTHORIZED
        ];
        if (!$request->ajax() || !Auth::check()) {
            return response($response);
        }
        $user = Auth::user();
        $dataResponse = $this->userDayOffService->listApprovals((int)$user->jobtitle_id + 1);

        return response([
            'success' => true,
            'message' => "Danh Sách người phê duyệt",
            'data' => $dataResponse->toArray()
        ]);
    }

    public function dayOffApprove(DayOffRequest $request, $status = null)
    {
        $dataDayOff = $this->userDayOffService->showList($status);
        return view(
            'end_user.user.day_off_approval',
            compact('dataDayOff')
        );
    }

    public function dayOffApprove_get(Request $request, $id)
    {
        if (!$request->ajax() || !Auth::check() || $id === null) {
            return null;
        }

        $responseObject = $this->userDayOffService->getRecordOf($id);
        if ($responseObject == null) {
            return null;
        }
        $transformer = new DayOffTransformer();

        return $transformer->transform($responseObject);
    }

    public function dayOffApprove_AcceptAPI(Request $request)
    {
        $response = [
            'success' => false,
            'message' => NOT_AUTHORIZED
        ];

        //	     Checking authorize for action
        $isApproval = Auth::user()->jobtitle_id >= \App\Models\Report::MIN_APPROVE_JOBTITLE;

        if (!$isApproval || !$request->ajax()) {
            return response($response);
        }

        $arrRequest = $request->all();
        $recievingObject = (object)$arrRequest;

        $targetRecordResponse = $this->userDayOffService->updateStatusDayOff(
            $recievingObject->id,
            Auth::id(),
            $recievingObject->approve_comment,
            $recievingObject->number_off
        );

        if ($targetRecordResponse) {
            $response['message'] = 'Cập nhật thành công.';
            $response['success'] = true;
        } else {
            $response['message'] = 'Cập nhật thất bại. Đơn xin không tồn tại hoặc có lỗi xảy ra với server';
            $response['success'] = false;
        }
        return response($response);
    }

    //
    //
    //  CONTACT
    //
    //

    public function contact(Request $request)
    {
        $users = $this->userService->getContact($request, $perPage, $search);
        $groups = Group::all();
        return view('end_user.user.contact', compact('users', 'groups', 'search', 'perPage'));
    }

    /**
     * Create or edit day off
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function dayOffCreatevacationVacation(CreateDayOffRequest $request)
    {
        if ($request->id_hid) {
            $dayOff = DayOff::FindOrFail($request->id_hid);
        } else {
            $dayOff = new DayOff();
        }
        $dayOff->fill($request->all());
        $dayOff->start_at = $request->start_at . SPACE . CHECK_TIME_DAY_OFF_START_DATE;
        $dayOff->end_at = $request->end_at . SPACE . CHECK_TIME_DAY_OFF_END_DATE;
        $dayOff->user_id = Auth::id();
        $dayOff->save();
        if ($request->id_hid) {
            return redirect(route('day_off'))->with('day_off_edit_success', '');
        } else {
            event(new DayOffNoticeEvent($dayOff, Auth::user(), NOTIFICATION_DAY_OFF['create']));
            return redirect(route('day_off'))->with('day_off_success', '');
        }
    }

    public function dayOffCreate(CreateDayOffRequest $request)
    {
        $this->userService->dayOffSave($request);
        return $request->id_hid
            ? redirect(route('day_off'))->with('day_off_edit_success', '')
            : redirect(route('day_off'))->with('day_off_success', '');
    }

    public function dayOffSearch(Request $request)
    {
        $start = $request->search_start_at ?? '';
        $end = $request->search_end_at;
        $status = $request->status;
        $search = $request->search;
        $dataDayOff = $this->userDayOffService->showList(null);
        $getDayOff = $this->userDayOffService
            ->getDataDate($request, $status, ['user.projects', 'user', 'user.teams', 'user.teams.group']);

        $response = [
            'getDayOff' => $getDayOff,
            'dataDayOff' => $dataDayOff,
            'start' => $start,
            'end' => $end,
            'status' => $status,
            'search' => $search,
        ];

        return view('end_user.user.day_off_approval', $response);
    }

    public function dayOffShow($status)
    {
        $dataDayOff = $this->userDayOffService->showList($status);

        $response = [
            'dataDayOff' => $dataDayOff,
            'status' => $status,
        ];

        return view('end_user.user.day_off_approval', $response);
    }

    public function dayOffDetail($id, $check = false)
    {
        $dayOff = $this->userDayOffService->getOneData($id);
        if (!$dayOff) {
            return response()->json([]);
        }
        if ($dayOff->status == STATUS_DAY_OFF['abide'] && $check) {
            $dayOff->status = STATUS_DAY_OFF['noActive'];
            $dayOff->save();
            return back()->with('close', '');
        }
        $numOff = $dayOff->number_off ? checkNumber($dayOff->number_off) : DEFAULT_VALUE;
        $absent = $dayOff->absent ? checkNumber($dayOff->absent) : DEFAULT_VALUE;

        if ($dayOff->title != REMAIN_DAY_OFF_DEFAULT) {
            $timeStart = DateTimeHelper::checkTileDayOffGetDate($dayOff->start_at);
            $timeEnd = DateTimeHelper::checkTileDayOffGetDate($dayOff->end_at);
            $time = $timeStart . ' - ' . $timeEnd;
        }
        $numOffApprove = $this->userDayOffService
            ->checkDateUsable($dayOff->start_at, $dayOff->end_at, null, null, true);

        $dayOffPreYear = RemainDayoff::where('user_id', $dayOff->user_id)
            ->where('year', date('Y') - PRE_YEAR)->first()->remain ?? DEFAULT_VALUE;
        $dayOffYear = RemainDayoff::where('user_id', $dayOff->user_id)->where('year', date('Y'))->first();
        $remainDayoffCurrentYear = $dayOffYear->remain ?? DEFAULT_VALUE;
        $dayOffFreeCurrentYear = $dayOffYear->day_off_free_female ?? DEFAULT_VALUE;
        $totalAbsent = $numOffApprove[0] - ($dayOffPreYear + $remainDayoffCurrentYear + $dayOffFreeCurrentYear);

        return response()->json([
            'data' => $dayOff,
            'numoff' => $numOff,
            'approver' => User::find($dayOff->approver_id)->name ?? '',
            'userdayoff' => User::find($dayOff->user_id)->name ?? '',
            'absent' => $absent + $numOff,
            'approver_id' => User::find($dayOff->approver_id)->id ?? '',
            'timeStartEdit' => Carbon::createFromFormat(DATE_TIME_FORMAT, $dayOff->start_at)->format('Y/m/d'),
            'timeEndEdit' => Carbon::createFromFormat(DATE_TIME_FORMAT, $dayOff->end_at)->format('Y/m/d'),
            'time' => $time ?? DEFAULT_VALUE,
            'approver_num' => $numOffApprove[0],
            'totalRemain' => $dayOffPreYear + $remainDayoffCurrentYear + $dayOffFreeCurrentYear,
            'totalAbsent' => $totalAbsent > 0 ? $totalAbsent : 0,
            'project_names' => $dayOff->user->project_names,
            'group' => optional($dayOff->user->group())->name
        ]);
    }

    public function editDayOffDetail(Request $request, $id)
    {
        $this->validate($request, [
            'number_off' => 'required|numeric|min:0',
            'approve_comment' => 'nullable|min:1|max:255'
        ]);
        $dayOff = DayOff::findOrFail($id);
        if ($dayOff->approver_id == Auth::id()) {
            $this->userDayOffService->calculateDayOff($request, $id);
            return back()->with('success', __('messages.edit_day_off_successully'));
        } else {
            abort(404);
        }
    }

    public function deleteOrCloseDayOff(Request $request)
    {
        if (isset($request->day_off_id)) {
            DayOff::findOrFail($request->day_off_id)->delete();
            return back()->with('delete_day_off', '');
        } elseif (isset($request->id_close)) {
            $dayOff = DayOff::findOrFail($request->id_close);
            if ($dayOff->status == STATUS_DAY_OFF['abide']) {
                $dayOff->approve_comment = $request->approval_coment;
                $dayOff->status = STATUS_DAY_OFF['noActive'];
                $dayOff->save();
                event(new DayOffNoticeEvent($dayOff, Auth::user(), NOTIFICATION_DAY_OFF['close']));
                return back()->with('close', '');
            }
        } else {
            abort(404);
        }
    }

    public function checkUsable(Request $request)
    {
        $dayOffPreYear = RemainDayoff::where('user_id', Auth::id())
            ->where('year', date('Y') - PRE_YEAR)->first()->remain ?? DEFAULT_VALUE;
        $dayOffYear = RemainDayoff::where('user_id', Auth::id())->where('year', date('Y'))->first();
        $remainDayoffCurrentYear = $dayOffYear->remain ?? DEFAULT_VALUE;
        $dayOffFreeCurrentYear = $dayOffYear->day_off_free_female ?? DEFAULT_VALUE;
        $numOff = $this->userDayOffService
            ->checkDateUsable($request->start_date, $request->end_date, $request->start_time, $request->end_time);
        if (is_array($numOff) && $numOff[0] > ($dayOffPreYear + $remainDayoffCurrentYear + $dayOffFreeCurrentYear)) {
            $absent = $numOff[0] - ($dayOffPreYear + $remainDayoffCurrentYear + $dayOffFreeCurrentYear);
            return response()->json([
                'check' => true,
                'absent' => $absent,
                'flag' => $numOff[1]
            ]);
        } else {
            return response()->json([
                'check' => false,
                'flag' => true
            ]);
        }
    }

    public function multiApprover(Request $request, $check = null)
    {
        $this->validate($request, [
            'ids' => 'required|array',
            'to_id' => 'nullable|integer',
            'assgin_comment' => 'nullable|max:255'
        ]);
        if ($check == 'yes') {
            $this->doApprove($request, ACTIVE_STATUS);
        } elseif ($check == 'no') {
            $this->doApprove($request, REJECT_STATUS);
        } else {
            $this->doApprove($request, null, true);
        }
        return back()->with('multiApprover_successully', '');
    }

    private function doApprove($request, $status = null, $assgin = false)
    {
        $idsUnique = array_unique($request->ids);

        if ($assgin) {
            $arrUpdate = ['assign_id' => $request->to_id, 'assgin_comment' => $request->assgin_comment];
        } else {
            $arrUpdate = ['status' => $status, 'approver_at' => now()];
        }
        OverTime::whereIn('id', $idsUnique)->where('status', DEFAULT_VALUE)->update($arrUpdate);
    }

    private function getWorkTimeExplanation($workDay)
    {
        return WorkTimesExplanation::where('user_id', Auth::id())->where('work_day', $workDay);
    }

    private function permissionGetExplanation()
    {
        return WorkTimesExplanation::where('type', '!=', 4);
    }

    private function permissionGetOverTime()
    {
        return OverTime::where('id', '!=', null);
    }

    private function projectActive()
    {
        return Project::where('status', Project::IS_ACTIVE)->get();
    }

    public function checkOut()
    {
        $user = Auth::user();
        if ($user && $user->is_remote_checkin === IS_REMOTE_STAFF) {
            $now = Carbon::now();
            $date = $now->format('Y-m-d');
            $workTime = WorkTime::where([
                'user_id' => $user->id,
                'work_day' => $date,
            ])->first();

            if ($workTime) {
                $workTime->end_at = $now->format('H:i:s');
            } else {
                $workTime = new WorkTime([
                    'user_id' => $user->id,
                    'work_day' => $date,
                    'end_at' => $now->format('H:i:s'),
                ]);
            }

            $workTime->save();
        }
        return redirect('/');
    }
    public function organizationalStructure()
    {
        $users = Team::select(
            'groups.name as group',
            'teams.name as team',
            'teams.banner',
            'teams.color',
            'leader.name as name_leader',
            'leader.jobtitle_id as jobtitle_leader',
            'leader.avatar as avatar_leader',
            'leader.id as id_leader',
            'member.id as id_menber',
            'member.name as name_menber',
            'member.jobtitle_id',
            'member.position_id',
            'member.avatar'
        )
            ->join('groups', 'groups.id', '=', 'teams.group_id')
            ->join('user_teams', 'teams.id', '=', 'user_teams.team_id')
            ->join('users as member', 'member.id', '=', 'user_teams.user_id')
            ->join('users as leader', 'teams.leader_id', '=', 'leader.id')
            ->where('teams.deleted_at', '=', null)
            ->where('user_teams.deleted_at', '=', null)
            ->distinct('user_teams.user_id')
            ->get();
        $dataGroup = [];
        foreach ($users as $value) {
            $dataGroup[$value->group][$value->team]['color'] = $value->color;
            $dataGroup[$value->group][$value->team]['image'] = $value->banner;
            $dataGroup[$value->group][$value->team]['leader'] = [
                'name_leader' => $value->name_leader,
                'jobtitle_leader' => $value->jobtitle_leader,
                'avatar_leader' => $value->avatar_leader,
            ];
            $dataGroup[$value->group][$value->team]['member'][] = [
                'name' => $value->name_menber,
                'jobtitle_member' => $value->jobtitle_id,
                'position_member' => $value->position_id,
                'avatar' => $value->avatar,
            ];
        }
        $directors = User::where('jobtitle_id', 3)->take(2)->get();
        return view('end_user.user.org_chart', compact('dataGroup', 'directors'));
    }

    /**
     * List quizzes must do
     *
     * @param  mixed $request
     * @return void
     */
    public function getQuizMustDo(Request $request)
    {
        $current = Carbon::now();
        $userId = $request->user_id;
        $activeQuizzes = Quiz::where('status', array_search('Đã kích hoạt', STATUS_QUIZ))
            ->whereDate('start_date', '<=', $current)->with('questions')->get()->toArray();
        $quizzes = [];
        foreach ($activeQuizzes as $quiz) {
            if ($quiz["questions"]) {
                $quizzes[$quiz["id"]] = $quiz["participants"];
            }
        }
        $quizMustDo = [];
        foreach ($quizzes as $quizId => $participants) {
            $participants = explode(',', $participants);
            if (in_array($userId, $this->userService->getParticipantIds($participants))) {
                array_push($quizMustDo, $quizId);
            }
        }
        $quizzesDone = AnswerUser::select(
            'quizzes.id',
            'quizzes.start_date',
            'quizzes.type',
            DB::raw("MAX(answer_users.created_at) as time_submit")
        )
            ->join('questions', 'questions.id', '=', 'answer_users.question_id')
            ->join('quizzes', 'quizzes.id', 'questions.quiz_id')
            ->where('answer_users.user_id', Auth::id())
            ->where('quizzes.status', array_search('Đã kích hoạt', STATUS_QUIZ))
            ->whereColumn('quizzes.start_date', '<=', 'answer_users.created_at')
            ->groupBy('quizzes.id')
            ->orderBy('quizzes.id', 'asc')
            ->get()
            ->toArray();
        $currentDay = $current->day;
        $currentMonth = $current->month;
        $currentYear = $current->year;
        $arrQuizDone = [];
        foreach ($quizzesDone as $quizDone) {
            if ($quizDone['type'] == array_search('Một lần', QUIZ_TYPES)) {
                array_push($arrQuizDone, $quizDone['id']);
            } else {
                $startDay = substr($quizDone['start_date'], -2);
                $timeSubmit = strtotime($quizDone['time_submit']);
                if ($startDay <= $currentDay) {
                    $lastDate = $current->endOfMonth();
                    $endDate = strtotime($lastDate);
                    $startDate = strtotime("$currentYear/$currentMonth/$startDay");
                    if ($startDate <= $timeSubmit && $timeSubmit <= $endDate) {
                        array_push($arrQuizDone, $quizDone['id']);
                    }
                } else {
                    $firstDay = $current->startOfMonth();
                    $startDate = strtotime($firstDay);
                    $endDate = strtotime("$currentYear/$currentMonth/$startDay");
                    if ($startDate <= $timeSubmit && $timeSubmit <= $endDate) {
                        array_push($arrQuizDone, $quizDone['id']);
                    } else {
                        $lastDate = $current->subMonth()->endOfMonth();
                        $endDate = strtotime($lastDate);
                        $lastMonth = $lastDate->month;
                        $lastYear = $lastDate->year;
                        $startDate = strtotime("$lastYear/$lastMonth/$startDay");
                        if ($startDate <= $timeSubmit && $timeSubmit <= $endDate) {
                            array_push($arrQuizDone, $quizDone['id']);
                        }
                    }
                }
            }
        }
        $quizMustDo = array_values(array_diff($quizMustDo, array_unique($arrQuizDone)));
        if (empty($quizMustDo)) {
            return response()->json(['quiz_must_do' => $quizMustDo]);
        }
        $detailQuiz = Quiz::where('id', $quizMustDo[0])->get();
        return response()->json(['quiz_must_do' => $quizMustDo, 'detail_quiz' => $detailQuiz]);
    }

    /**
     * Get questions of quiz 
     *
     * @param  mixed $request
     * @return void
     */
    public function getQuiz(Request $request)
    {
        $questionAnswer = Question::where('questions.quiz_id', $request->quiz_id)
            ->with('answers')->OrderBy('position', 'asc')->get()->toArray();
        return response()->json(['question_answer' => $questionAnswer, 'quiz_id' => $request->quiz_id]);
    }

    /**
     * Store answers of user
     *
     * @param  mixed $request
     * @return void
     */
    public function answerStore(Request $request)
    {
        $answerCorrects = Question::where('quiz_id', $request->quiz_id)
            ->with([
                'answers' => function ($query) {
                    $query->where('answers.is_correct', CORRECT_ANSWER);
                }
            ])->get();
        $isCorrect = [];
        foreach ($answerCorrects as $answerCorrect) {
            if ($answerCorrect->type == CORRECT_ANSWER) {
                $isCorrect[$answerCorrect->id] = CORRECT_ANSWER;
            } else {
                foreach ($answerCorrect->answers as $answer) {
                    if ($request->questions[$answerCorrect->id] == $answer->answer) {
                        $isCorrect[$answerCorrect->id] = CORRECT_ANSWER;
                    } else {
                        $isCorrect[$answerCorrect->id] = WRONG_ANSWER;
                    }
                }
            }
        }
        foreach ($request->questions as $question_id => $answer) {
            AnswerUser::create([
                'user_id' => Auth::id(),
                'question_id' => $question_id,
                'answer' => $answer,
                'is_correct' => $isCorrect[$question_id],
            ]);
        }
        return back();
    }
}
