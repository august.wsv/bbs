<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\Contracts\IAuthService;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Cookie;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Service.
     *
     * @var \App\Services\Contracts\IAuthService
     */
    protected $authService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IAuthService $authService)
    {
        $this->middleware('guest')->except('logout');
        $this->authService = $authService;
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string|exists:users,email,end_date,NULL',
            'password' => 'required|string',
        ], [
            'email.exists' => 'Thông tin tài khoản không tìm thấy trong hệ thống.'
        ]);
    }


    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $ttl = config('jwt.ttl');
        $path = '/';
        $domain = '.hatoq.com';
        Cookie::queue(cookie(
            'auth._token.local',
            'Bearer ' . $this->authService->login($request),
            $ttl,
            $path,
            $domain,
            false,
            false
        ));
        Cookie::queue(cookie(
            'auth._token_expiration.local',
            strtotime("+ $ttl minutes") * 1000,
            $ttl,
            $path,
            $domain,
            false,
            false
        ));
        Cookie::queue(cookie(
            'auth.strategy',
            'local',
            $ttl,
            $path,
            $domain,
            false,
            false
        ));
    }
}
