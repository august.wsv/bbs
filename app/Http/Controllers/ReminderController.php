<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\IReminderRepository;
use App\Repositories\Contracts\IReminderMemberRepository;
use App\Http\Requests\ReminderRequest;
use Illuminate\Http\Request; 
use App\Models\Reminder;
use App\Models\ReminderMember;
use App\User; 
use Auth;
use App\Services\NotificationService;
use App\Services\Contracts\IReportService;
use Illuminate\Support\Facades\Validator;

class ReminderController extends Controller
{
    private $reminder_repository;
    private $reminder_member_repository;

    /**
     * @var NotificationService
     */
    private $notificationService;
    
    /**
     * ReminderController constructor.
     *
     * @param IReminder_repository  $reminder_repository
     * @param IReminderMemberRepository  $reminder_member_repository
     */
    public function __construct(IReportService $service, IReminderRepository $reminder_repository, IReminderMemberRepository $reminder_member_repository, NotificationService $notificationService)
    {
        $this->reminder_repository = $reminder_repository;
        $this->reminder_member_repository = $reminder_member_repository;
        $this->notificationService = $notificationService;
        $this->service = $service; 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $receivers = $this->service->getReportReceiver();
        $reminders = $this->reminder_repository->getStatusUnsuccess();
        return view('end_user.reminder.index',compact('reminders', 'receivers'));
    }

    public function detail($id)
    {   
        $reminder = $this->reminder_repository->findOne($id);
        $reminder_users = $reminder->users()->get();
        $count = $reminder_users->count();
        if ($reminder|| $reminder_users|| $count) {
            return view('end_user.reminder.detail',compact('reminder', 'count', 'reminder_users'));
        }
        abort(404);        
    }

    /**
     * Display a listing of the resource(status = 1).
     * @return \Illuminate\Http\Response
     */
    public function reminderStatusSuccess()
    {
        $successfulls = $this->reminder_repository->getStatusSuccess();
        return view('end_user.reminder.successfull',compact('successfulls'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReminderRequest $request)
    {
        $data = $request->all();
        $reminder = $this->reminder_repository->save($data);
        $id = $reminder->id;
        if($request->user_id != null){
            $user_reminder = $request->user_id;
            array_push($user_reminder, Auth::id());
            foreach ($user_reminder as $item) {
                if(isset($item)){
                    $newReminder = new ReminderMember();
                    $newReminder->reminder_id = $id;
                    $newReminder->user_id = $item;
                    $newReminder->save();
                }
            }
        }
        return redirect()->route('reminder_index')->with(['level' => 'success', 'message' => 'Thêm nhắc việc thành công!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOne($id)
    {   
        $reminder = $this->reminder_repository->findOne($id);
        if ($reminder) {
            return view('end_user.reminder.edit',compact('reminder'));
        }
        abort(404);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSave($id, Request $request)
    {
        $data = $request->all();
        $reminder = $this->reminder_repository->updateOneBy(['id' => $id], $data);
        return redirect()->route('reminder_index')->with(['level' => 'success', 'message' => 'Cập nhật nhắc việc thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteReminder($id)
    {
        $reminder = Reminder::find($id);
        $reminder->delete($id);
        return redirect()->route('reminder_index')->with(['level' => 'success', 'message' => 'Xóa nhắc việc thành công!']);
    }
}
