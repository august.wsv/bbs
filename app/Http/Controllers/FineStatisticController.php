<?php

namespace App\Http\Controllers;

use App\Services\Contracts\IPunishesService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FineStatisticController extends Controller
{
    private $punishesService;
    public function __construct(IPunishesService $punishesService)
    {
        $this->punishesService = $punishesService;
    }

    public function index(Request $request)
    {
        if($request->has('month')) {
            $dayCurrent = Carbon::createFromDate(date('Y'), $request->get('month'), 1)->startOfMonth();
        }
        else {
            $dayCurrent = Carbon::now()->startOfMonth();
        }
        $day = $dayCurrent->dayOfWeek;
        $dayStartProcess = $dayCurrent->addDays(abs($day - 8));
        $keySearch = $request->has('search') ? $request->get('search') : '';

        if (Carbon::now() >= $dayStartProcess) {
            $startBeforeMonth =  $dayCurrent->toDateString();
            $endBeforeMonth = $dayCurrent->endOfMonth()->toDateString();
            $monthYear = $dayCurrent->format(MONTH_YEAR_FORMAT);
        } else {
            $startBeforeMonth =  $dayCurrent->subMonth()->toDateString();
            $endBeforeMonth = $dayCurrent->subMonth()->endOfMonth()->toDateString();
            $monthYear = $dayCurrent->subMonth()->format(MONTH_YEAR_FORMAT);
        }

        $perPage =  $request->has('page_size') ? $request->get('page_size') : DEFAULT_PAGE_SIZE;

        $listPunishesOnMonth = $this->punishesService->getListPunishesOnMonth($startBeforeMonth, $endBeforeMonth, $perPage, $keySearch);
        $listTopPunishes = $this->punishesService->getListTopPunishes($startBeforeMonth, $endBeforeMonth, $request->get('top', 5));
        $listTypePunishes = $this->punishesService->getListTypePunishes($startBeforeMonth, $endBeforeMonth);


        return view('end_user.fine_statistic.index',compact('listPunishesOnMonth', 'perPage', 'listTopPunishes', 'listTypePunishes', 'monthYear', 'keySearch'));
    }
}
