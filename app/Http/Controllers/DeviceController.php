<?php

namespace App\Http\Controllers;

use App\Events\ProvidedDeviceNoticeEvent;
use App\Http\Requests\ProvidedDeviceRequest;
use App\Models\ProvidedDevice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeviceController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        if ($user->jobtitle_id == MASTER_ROLE) {
            $providedDevice = ProvidedDevice::whereNull('deleted_at');
        } elseif ($user->jobtitle_id == MANAGER_ROLE) {
            $providedDevice = ProvidedDevice::where('manager_id', Auth::id());
        } else {
            $providedDevice = ProvidedDevice::where('user_id', Auth::id());
        }
        $perPage = $request->get('page_size', DEFAULT_PAGE_SIZE);

        $providedDevice = $providedDevice
            ->orderBy('id', 'desc')
            ->paginate($perPage);

        return view('end_user.device.index', compact('providedDevice'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(ProvidedDeviceRequest $request)
    {
        $team = Auth::user()->team();
        $managerId = $team->group->manager_id ?? null;
        if ($managerId) {
            $providedDevice = new ProvidedDevice();
            if ($request->id_check) {
                $providedDevice = ProvidedDevice::FindOrFail($request->id_check);
            }
            $providedDevice->user_id = Auth::id();
            $providedDevice->manager_id = $managerId;
            $providedDevice->type_device = $request->type_device;
            $providedDevice->status = DEVICE_STATUS_ABIDE;
            $providedDevice->fill($request->all());
            $providedDevice->save();
            if ($request->id_check) {
                return back()->with('success', __l('device_edit_success'));
            }
            event(new ProvidedDeviceNoticeEvent($providedDevice, TYPE_DEVICE['send']));
            return back()->with('success', __l('device_create_success'));
        }
        return back()->with('error', __l('device_create_error'));
    }

    public function delete(Request $request)
    {
        if ($request->id) {
            ProvidedDevice::find($request->id)->delete();
            return back()->with('delete_success', __l('device_delete_success'));
        }
        abort(404);
    }

    public function edit($id)
    {
        $userLogin = Auth::user();
        $providedDevice = ProvidedDevice::findOrFail($id);
        if ($providedDevice && $userLogin->can('view', $providedDevice)) {
            return response([
                'success' => 200,
                'data' => $providedDevice->toArray(),
                'name' => $providedDevice->user->name,
                'date_create' => Carbon::createFromFormat(DATE_TIME_FORMAT, $providedDevice->created_at)
                    ->format(DATE_TIME_FORMAT_VI),
                'return_date' => $providedDevice->return_date
                    ? Carbon::createFromFormat(DATE_FORMAT, $providedDevice->return_date)->format(DATE_TIME_FORMAT_VI)
                    : '',
            ]);
        }
        abort(404);
    }

    public function approval(Request $request)
    {
        $providedDevice = ProvidedDevice::findOrFail($request->id_check);
        if ($request->has('done')) {
            $providedDevice->status = STATUS_DEVICE['done'];
            $providedDevice->save();
            return back()->with('success', __l('device_done'));
        }
        $data = $request->all();
        $status = $request->get('status');
        
        if ($status == STATUS_DEVICE['approved']) {
            $data['approved_at'] = Carbon::now();
        }

        $providedDevice->fill($data);
        $providedDevice->save();
        event(new ProvidedDeviceNoticeEvent($providedDevice, TYPE_DEVICE['manager_approval']));
        return back()->with('success', __l('device_approvel_success'));
    }
}
