<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Post;
use App\Models\Project;
use App\Models\User;
use App\Repositories\Contracts\ILaborCalendarRepository;

class HomeController extends Controller
{
    /**
     * @var ILaborCalendarRepository
     */
    private $repository;

    public function __construct(ILaborCalendarRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $posts = Post::select('id', 'name', 'introduction', 'image_url')
            ->where('status', ACTIVE_STATUS)
            ->orderBy('updated_at', 'desc')
            ->take(2)
            ->get();

        $events = Event::select('id', 'name', 'place', 'event_date', 'event_end_date', 'introduction', 'image_url', 'content', 'created_at', 'deadline_at')
            ->where('status', ACTIVE_STATUS)
//            ->whereDate('event_date', '>=', date(DATE_FORMAT))
            ->orderBy('event_date', 'desc')
            ->take(3)->get();
        $projects = Project::select('id', 'name', 'technical', 'image_url')
            ->where('status', ACTIVE_STATUS)
            ->orderBy('id', 'desc')
            ->take(10)
            ->get();

        $dayLaborCalendar = $this->repository->getTodayInfo();
        $laborUserName = '';
        $laborMeme = '';
        if ($dayLaborCalendar) {
            foreach ($dayLaborCalendar->laborCalendarDetail as $detail) {
                $user = User::withTrashed()->find($detail['user_id']);
                if ($user)
                    $laborUserName .= $user->name . ' - ';
            }
            $laborUserName = substr($laborUserName, 0, strlen($laborUserName) - 2);
            $laborMeme = $dayLaborCalendar->meme_path;
        }


        return view('end_user.home', compact('posts', 'events', 'projects', 'laborUserName', 'laborMeme'));
    }


}
