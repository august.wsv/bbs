<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class QuizResquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'participants'  => 'required|string',
            'start_date' => 'required|date',
            'end_date' => 'date|nullable',
        ];
    }
    public function attributes()
    {
        return [
            'name' => 'tiêu đề',
            'participants' => 'đối tượng',
            'start_date' => 'thời gian bắt đầu',
            'end_date' => 'thời gian kết thúc',
        ];
    }
}
