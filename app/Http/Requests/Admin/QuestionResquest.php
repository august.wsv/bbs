<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class QuestionResquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'question' => 'required',
            // 'answers' => 'required|array|min:2',
            'answers.*' => 'required|required_if:type,==,0',

        ];
    }
    public function messages()
    {
        return [
            // 'answers.required_if:type,==,0' => 'Câu trả lời không dược để trống.',
            'answers.*.required' => 'Câu trả lời không dược để trống.'

        ];
    }
    public function attributes()
    {
        return [
            'question' => 'câu hỏi',
            'answers' => 'câu trả lời',
        ];
    }
}
