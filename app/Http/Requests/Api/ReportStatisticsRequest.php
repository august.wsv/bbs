<?php


namespace App\Http\Requests\Api;


use Illuminate\Foundation\Http\FormRequest;

class ReportStatisticsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year' => 'nullable|integer',
            'month' => 'nullable|integer|between:1,12',
            'group_id' => 'nullable|integer|exists:groups,id',
            'team_id' => 'nullable|integer|exists:teams,id',
        ];
    }
}
