<?php


namespace App\Http\Requests\Api;


use Illuminate\Foundation\Http\FormRequest;

class ApproveDayOffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required|integer|in:1,2',
            'approve_comment' => 'nullable|min:1|max:255'
        ];
    }
}
