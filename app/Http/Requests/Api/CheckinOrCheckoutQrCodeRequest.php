<?php


namespace App\Http\Requests\Api;

use Composer\DependencyResolver\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;

class CheckinOrCheckoutQrCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'time' => 'required|date_format:Y-m-d H:i:s',
            'qr_code' => 'required|exists:qr_codes,qr_code'
        ];
    }
}
