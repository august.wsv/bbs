<?php


namespace App\Http\Requests\Api;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class TaskLogTimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    public function validationData()
    {
        if (Route::input('task_id')) {
            return array_merge($this->request->all(), [
                'task_id' => Route::input('task_id'),
            ]);
        }
        return $this->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'time_text' => 'required|regex:/^\d+d\d+h$/',
            'description' => 'nullable|string|max:255',
            'task_id' => 'nullable|integer|exists:project_tasks,id',
            'time_start' => 'nullable|date_format:' . DATE_TIME_FORMAT_SHORT,
        ];
    }
}
