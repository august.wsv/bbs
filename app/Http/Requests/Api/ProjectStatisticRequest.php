<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class ProjectStatisticRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'day_number' => 'nullable|integer',
            'group_id' => 'nullable|integer|exists:groups,id',
            'sort_column' => 'nullable|string|in:' . PROJECT_STATISTIC_COLUMN_SORT,
            'sort_type' => 'nullable|string|in:' . SORT_TYPE,
        ];
    }

    public function messages()
    {
//        return parent::messages(); // TODO: Change the autogenerated stub
        return [
            'sort_column.in' => 'The :attribute field does not exist in: ' . PROJECT_STATISTIC_COLUMN_SORT,
            'sort_type.in' => 'The :attribute field does not exist in: ' . SORT_TYPE,
        ];
    }
}
