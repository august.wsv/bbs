<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectSprintRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required|integer|exists:projects,id',
            'name' => 'required|string|unique:project_sprints,name,NULL,id,project_id,' . $this->project_id,
            'description' => 'string',
            'status' => 'required|integer|in:' . implode(',', SPRINT_STATUS),
            'effort' => 'nullable|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
        ];
    }

    public function messages()
    {
        $statusMessage = substr(array_reduce(array_keys(SPRINT_STATUS), function ($init, $key) {
            return $init . SPRINT_STATUS[$key] . ": $key, ";
        }), 0, -2);

        return [
            'start_date.date' => 'Trường start_date không phải là định dạng của Y-m-d',
            'end_date.date' => 'Trường end_date không phải là định dạng của Y-m-d',
            'name.unique' => 'Tên sprint đã tồn tại trong dự án này',
            'status.in' => "Giá trị của trường status phải nằm trong khoảng ($statusMessage)",
        ];
    }
}
