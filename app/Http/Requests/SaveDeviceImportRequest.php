<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveDeviceImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|array',
            'sum_total' => 'required|array',
            'guarantee' => 'required|array',
            'date_import' => 'required|array',
            'code.*' => 'required',
            'sum_total.*' => 'required',
            'guarantee.*' => 'required',
            'date_import.*' => 'required|date_format:Y-m-d',
        ];
    }
}
