<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProjectTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'task_id' => 'nullable|max:191',
            'deadline' => 'nullable|date',
            'progress' => 'nullable|integer|between:0,100',
            'type' => 'nullable|integer|in:' . implode(',', TASK_TYPE),
            'sprint_id' => 'nullable|integer|exists:project_sprints,id',
        ];
    }
}
