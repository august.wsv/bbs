<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveFormAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_type_id' => 'required|array',
            'user_id' => 'required|exists:users,id',
            'date_import' => 'required|array',
            'device_select' => 'required|array',
            'date_import.*' => 'required|date_format:Y-m-d',
            'device_type_id.*' => 'required',
            'device_select.*' => 'required',
        ];
    }
}
