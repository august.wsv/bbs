<?php

namespace App\Http\Requests;

use App\Facades\AuthAdmin;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AuthAdmin::check() && AuthAdmin::user()->isSuperAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($id = null)
    {
        return [
            'name' => 'required|min:3|max:50',
            'username' => 'required|min:3|max:50|regex:/^[A-Za-z0-9_]+$/|unique:admins,username' . ($id ? (','. $id) : ''),
            'password' => 'nullable|min:8|max:20',
            'admin_role' => 'required|integer',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'tên admin',
            'username' => 'tên đăng nhập',
            'admin_role' => 'phân quyền'
        ];
    }
}
