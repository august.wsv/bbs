<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShareRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titleDocument' => 'required|string|max:255',
            'fileDocument' => 'required|file|max:51200'
        ];
    }

    public function messages()
    {
        return [
            'fileDocument.max' => 'Dung lượng file tải lên không được lớn hơn :max KB.'
        ];
    }
}
