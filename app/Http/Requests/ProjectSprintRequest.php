<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;

class ProjectSprintRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'project_id' => 'required',
            'status' => 'required|integer',
            'start_date' => 'required',
            'end_date' => 'required',
            'effort' => 'required|numeric',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $routeName = request()->route()->getName();
        preg_match('/(update|store)/', $routeName, $matches);

        throw (new ValidationException($validator))
            ->errorBag($matches[0] ?? $this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
