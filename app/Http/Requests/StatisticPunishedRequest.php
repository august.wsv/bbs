<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class StatisticPunishedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_day' => 'date|nullable|before_or_equal:end_day',
            'end_day' => 'date|nullable|before_or_equal:' . Carbon::now(),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'start_day.before_or_equal' => 'Ngày bắt đầu phải nhỏ hơn ngày kết thúc',
            'end_day.before_or_equal' => 'Ngày kết thúc phải nhỏ hơn hiện tại'
        ];
    }
}
