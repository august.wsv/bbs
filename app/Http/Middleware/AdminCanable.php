<?php

namespace App\Http\Middleware;

use App\Facades\AuthAdmin;
use Closure;

class AdminCanable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $ability)
    {
        if (AuthAdmin::user()->can($ability)) {
            return $next($request);
        } else {
            abort(403);
        }
    }
}
