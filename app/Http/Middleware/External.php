<?php

namespace App\Http\Middleware;

use Closure;

class External
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('app.bbs_public_key')) {
            $header = $request->header('PublicKey');
            if ($header != config('app.bbs_public_key')) {
                abort(403);
            }
        }

        return $next($request);
    }
}
