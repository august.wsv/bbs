<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;

class EncryptCookies extends BaseEncrypter
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        'jwt_cookie',
        // For Nuxt
        'auth._token.local',
        'auth._token_expiration.local',
        'auth.strategy',
    ];
}
