<?php

namespace App\Http\Middleware;

use App\Models\Accesses;
use App\Models\WorkTime;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class ActivityHistoryUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $timeNow = (int)Carbon::now()->format('H');
            if ($timeNow >= TIME_CHECKIN){
                $user = Auth::user();
                if ($user->is_remote_checkin === IS_REMOTE_STAFF) {
                    if (($user->is_remote_checkin_vpn == REMOTE_CHECKIN_VPN && in_array($request->ip(), IP_CHECKIN)) || $user->is_remote_checkin_vpn == REMOTE_CHECKIN_ONLINE){
                        $now = Carbon::now();
                        $date = $now->format('Y-m-d');
                        if (!WorkTime::where([
                            'user_id' => $user->id,
                            'work_day' => $date])->first()) {
                            $workTime = new WorkTime([
                                'user_id' => $user->id,
                                'work_day' => $date,
                                'start_at' => $now->format('H:i:s'),
                                'checkin_type' => WORK_TIME_CHECKIN_TYPE['checkin_online'],
                            ]);
                            $workTime->save();
                        }
                    }
                }
            }
            Auth::user()->update([
                'last_activity_at' => Carbon::now()
            ]);

            //save history access
            if ($request->isMethod('get')) {
                $accessHistory = new Accesses();
                $accessHistory->user_id = Auth::id();
                $accessHistory->access_date = now();
                $accessHistory->path = $request->path();
                $accessHistory->save();
            }
        }
        return $next($request);
    }
}
