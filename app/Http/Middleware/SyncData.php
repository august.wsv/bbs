<?php

namespace App\Http\Middleware;

use Closure;

class SyncData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('app.bbs_secret_key')) {
            $header = $request->header('SecretKey');
            if ($header != config('app.bbs_secret_key')) {
                abort(403);
            }
        }

        return $next($request);
    }
}
