<?php

/**
 * Created by PhpStorm.
 * User: batri
 * Date: 10/22/2018
 * Time: 12:18 AM
 */

namespace App\Handlers;

use App\Facades\AuthAdmin;

class ConfigHandler
{
    public function userField()
    {
        if (AuthAdmin::check()) {
            $adminId = AuthAdmin::user()->id;
            $path = public_path("uploads/photos/$adminId");
            \File::isDirectory($path) || \File::makeDirectory($path, 0775, true, true);
            return $adminId;
        } else {
            abort(401);
        }
    }
}
