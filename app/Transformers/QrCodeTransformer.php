<?php

namespace App\Transformers;

use App\Models\QrCode;
use League\Fractal;

/**
 * QrCodeTransformer class
 * Author: jvb
 * Date: 2023/05/17 08:38
 */
class QrCodeTransformer extends Fractal\TransformerAbstract
{
    public function transform(QrCode $item)
    {
        return $item->toArray();
    }
}
