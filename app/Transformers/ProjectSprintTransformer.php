<?php

namespace App\Transformers;

use App\Models\ProjectSprint;
use League\Fractal;

class ProjectSprintTransformer extends Fractal\TransformerAbstract
{
    public function transform(ProjectSprint $item)
    {
        return $item->toArray();
    }
}
