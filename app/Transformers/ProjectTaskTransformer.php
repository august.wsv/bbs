<?php


namespace App\Transformers;

use App\Models\ProjectTask;
use League\Fractal;
class ProjectTaskTransformer extends Fractal\TransformerAbstract
{
    public function transform(ProjectTask $item)
    {
        return $item->toArray();
    }
}