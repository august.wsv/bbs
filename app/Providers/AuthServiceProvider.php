<?php

namespace App\Providers;

use App\Policies\PotatoPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Project' => 'App\Policies\ProjectPolicy',
        'App\Models\Report' => 'App\Policies\ReportPolicy',
        'App\Models\ProvidedDevice' => 'App\Policies\DevicePolicy',
        'App\Models\OverTime' => 'App\Policies\OverTimePolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        Gate::define('master', 'App\Policies\UsersPolicy@master');
        Gate::define('manager', 'App\Policies\UsersPolicy@manager');
        Gate::define('team-leader', 'App\Policies\UsersPolicy@teamLeader');
        Gate::define('HCNS', 'App\Policies\UsersPolicy@HCNS');
        Gate::define('BRSE', 'App\Policies\UsersPolicy@BRSE');
        Gate::define('staff', 'App\Policies\UsersPolicy@staff');
        Gate::define('seeWorkingTime', 'App\Policies\UsersPolicy@seeWorkingTime');
        Gate::define('editShareDocument', 'App\Policies\UsersPolicy@editShareDocument');

        Gate::define('super-admin', 'App\Policies\AdminPolicy@superAdmin');
        Gate::define('admin', 'App\Policies\AdminPolicy@admin');
        Gate::define('content', 'App\Policies\AdminPolicy@content');
        Gate::define('itHelpDesk', 'App\Policies\AdminPolicy@itHelpDesk');

        Gate::define('crudSprint', 'App\Policies\ProjectPolicy@crudSprint');
    }
}
