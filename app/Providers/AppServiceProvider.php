<?php

namespace App\Providers;

use App\Models\Reminder;
use App\Models\ReminderMember;
use App\Repositories\Contracts\IReminderMemberRepository;
use App\Repositories\ReminderMemberRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Contracts\IBlockRepository;
use App\Repositories\BlockRepository;
use App\Models\Block;
use App\Models\UserBlock;
use App\Repositories\Contracts\IUserBlockRepository;
use App\Repositories\UserBlockRepository;
use App\Services\BlockService;
use App\Services\Contracts\IBlockService;
use App\Repositories\NotificationRepository;

//Reminder nhac viec

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);
        try {
            $config = \App\Models\Config::firstOrNew(['id' => 1]);
            view()->share('config', $config);
        } catch (\Exception $exception) {

        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('settings', function () {
            return Config::all();
        });
        $this->app->bind(\App\Repositories\Contracts\IReminderRepository::class, function () {
            return new \App\Repositories\ReminderRepository(new Reminder());
        });
        $this->app->bind(IReminderMemberRepository::class, function () {
            return new ReminderMemberRepository(new ReminderMember());
        });
        $this->app->bind(IBlockRepository::class, function () {
            return new BlockRepository(new Block());
        });
        $this->app->bind(IUserBlockRepository::class, function () {
            return new UserBlockRepository(new UserBlock());
        });
        $this->app->bind(IBlockService::class, function () {
            return app()->make(BlockService::class);
        });
    }
}
