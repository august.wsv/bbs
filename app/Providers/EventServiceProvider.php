<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserRegistered' => [
            'App\Listeners\SendMailUserRegistered',
        ],
        'App\Events\SearchEvent' => [
            'App\Listeners\WriteSearchLog',
        ],
        'App\Events\EventSentMail' => [
            'App\Listeners\EventSendMail',
        ],
        'App\Events\CaculateLateTimeEvent' => [
            'App\Listeners\CaculateLateTimeListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
