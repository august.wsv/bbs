<?php

namespace App\Observers;

use App\Models\ProjectTask;
use App\Models\TaskLogWork;

class TaskLogWorkObserver
{
    /**
     * Handle to the user "creating" event.
     *
     * @param \App\Models\TaskLogWork $logWork
     *
     * @return void
     */
    public function creating(TaskLogWork $logWork)
    {

    }

    /**
     * Handle to the user "created" event.
     *
     * @param \App\Models\TaskLogWork $logWork
     *
     * @return void
     */
    public function created(TaskLogWork $logWork)
    {
        //
    }

    /**
     * Handle the topic "updating" event.
     *
     * @param TaskLogWork $logWork
     *
     * @return void
     */
    public function updating(TaskLogWork $logWork)
    {
    }

    /**
     * Handle the user "updated" event.
     *
     * @param \App\Models\TaskLogWork $logWork
     *
     * @return void
     */
    public function updated(TaskLogWork $logWork)
    {

    }

    /**
     * Handle the user "deleted" event.
     *
     * @param \App\Models\TaskLogWork $logWork
     *
     * @return void
     */
    public function deleted(TaskLogWork $logWork)
    {
        //
    }

    /**
     * @param TaskLogWork $log
     */
    public function saved(TaskLogWork $log){
        $task = ProjectTask::find($log->task_id);
        if ($task){
            $task->total_log = $task->logWork->sum('time');
            $task->count_log_work = $task->logWork->count();
            $task->save();
        }
    }
}
