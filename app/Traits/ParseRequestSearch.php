<?php

/**
 * Created by PhpStorm.
 * User: batri
 * Date: 1/14/2018
 * Time: 10:55 PM
 */

namespace App\Traits;

use Illuminate\Http\Request;

trait ParseRequestSearch
{
    /**
     * @param Request $request
     * @param array   $fieldOperator Eg: ['name'=>'like']
     *
     * @return array
     */
    private function parseRequest(Request $request, $fieldOperator = [])
    {
        $result = [];
        $skipPagingOrderField = $this->skipRequestSearchParams();
        if (!$fieldOperator) {
            foreach ($request->all() as $field => $value) {
                if (in_array($value, ['null', null, false])) {
                    continue;
                }
                $result[$field] = in_array($field, $skipPagingOrderField) ?
                    $result[$field] : ['field' => $field, 'operator' => '=', 'value' => $value];
            }
        } else {
            foreach ($fieldOperator as $field => $operator) {
                $value = $request->get($field);
                if ($value === null) {
                    continue;
                }
                $result[$field] = ['field' => $field, 'operator' => $operator, 'value' => $value];
            }
        }
        return $result;
    }

    public function skipRequestSearchParams()
    {
        return ['page', 'page_size', 'orders', 'with'];
    }
}
