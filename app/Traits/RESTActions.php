<?php

namespace App\Traits;

use App\Repositories\Contracts\IBaseRepository;
use App\Services\Contracts\IBaseService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\TransformerAbstract;

trait RESTActions
{
    /**
     * @var IBaseRepository
     */
    protected $repository;

    /**
     * @var TransformerAbstract
     */
    protected $transformer;

    /**
     * @var IBaseService
     */
    protected $service;

    //get
    public function all()
    {
        $list = $this->repository->findBy([], true);
        return $this->respondTransformer($list);
    }

    //get
    public function show($id)
    {
        $model = $this->repository->findOne($id);
        return $this->respondTransformer($model);
    }

    //post
    public function store(Request $request)
    {
        $model = $this->repository->save($request->all());
        return $this->respondTransformer($model);
    }

    //put
    public function update(Request $request, $id)
    {
        $model = $this->repository->findOne($id);
        if (is_null($model)) {
            return $this->respondNotfound();
        }
        $model = $this->repository->update($model, $request->all());
        return $this->respondTransformer($model);
    }

    //delete
    public function destroy($id)
    {
        $model = $this->repository->findOne($id);
        if (is_null($model)) {
            return $this->respondNotfound();
        }
        $this->repository->delete($model);
        return $this->respond([
            MESSAGE => 'Success',
            'code' => 200
        ], Response::HTTP_OK);
    }

    protected function respond($data = [], $status = Response::HTTP_OK, $message = '')
    {
        return self::respondFinal($message, $status, $data);
    }

    protected function respondTransformer($data, $transformer = null, $resourceName = '')
    {
        if (!empty($transformer)) {
            return $this->respond($this->transformer($data, $transformer, $resourceName));
        }
        return $this->respond($this->transformer($data, $this->transformer, $resourceName));
    }

    /**
     * @param Model|array         $model
     * @param TransformerAbstract $transformer
     * @param string              $resourceName
     *
     * @return array
     */
    protected function transformer($model, TransformerAbstract $transformer, $resourceName = '')
    {
        $ref = new \ReflectionClass($transformer);
        if (empty($resourceName)) {
            $resourceName = strtolower(str_replace('Transformer', '', $ref->getShortName()));
        }

        //cannot replace data to resource name???
        $dataTransform = fractal($model, $transformer)->withResourceName($resourceName)->toArray();
        if (isset($dataTransform['data']) && isset($dataTransform['data'][0])) {
            $dataTransform[str_plural($resourceName)] = $dataTransform['data'];
            unset($dataTransform['data']);
        }

        if ($model instanceof Model) {
            $dataTransform[$resourceName] = $dataTransform['data'];
            unset($dataTransform['data']);
        }
        return $dataTransform;
    }

    protected
    function respondFail($message = '', $errors = null, $status = Response::HTTP_BAD_REQUEST)
    {
        return self::respondFinal($message, $status, null, $errors);
    }

    protected
    function respondFailValidation($message = '', $errors = null, $status = Response::HTTP_UNPROCESSABLE_ENTITY)
    {
        return self::respondFinal($message ?: Response::$statusTexts[$status], $status, null, $errors);
    }

    protected
    function respondAuthFail($message = '', $status = Response::HTTP_UNAUTHORIZED)
    {
        return self::respondFinal($message, $status);
    }

    protected
    function respondNotfound($message = '', $status = Response::HTTP_NOT_FOUND)
    {
        return self::respondFinal($message, $status);
    }

    protected
    function respondDBFail($message = '', $status = Response::HTTP_UNPROCESSABLE_ENTITY)
    {
        return self::respondFinal($message, $status);
    }

    protected
    function respondFrameworkException($httpCode, $requestCode, $message = '', $data = [])
    {
        $dataResponse = [
            MESSAGE => $message,
            'code' => $requestCode,
            'data' => $data
        ];
        return response()->json($dataResponse, $httpCode);
    }

    private
    function respondFinal($message, $status, $data = null, $errors = null)
    {
        $dataResponse = [
            MESSAGE => $message ?: Response::$statusTexts[$status],
            'code' => $status,
            'data' => $data
        ];
        if ($errors) {
            $dataResponse['errors'] = $errors;
        }

        return response()->json($dataResponse, $status);
    }

    protected function respondInternal($message = '', $status = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        return self::respondFinal($message, $status);
    }
}
