<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;

trait SendFcmNotificationTrait
{
    public function sendFcmNotification($listFcmTokens, $notification)
    {

        if (!$listFcmTokens || !$notification) {
            return false;
        }

        $preparedData = [
            'registration_ids' => array_values($listFcmTokens),
            'priority' => 'high',
            'apns' => [
                'payload' => [
                    'aps' => [
                        'mutable-content' => 1,
                        'content-available' => 1,
                    ],
                ],
                'headers' => [
                    //"apns-push-type": "background", // This line prevents background notifications to be displayed
                    "apns-priority" => "10",
                ],
            ],
            'notification' => [
                'title' => isset($notification['title']) ? $notification['title'] : '',
                'body' => isset($notification['body']) ? $notification['body'] : ''
            ],
            'data' => $notification
        ];

        $dataString = json_encode($preparedData);

        $headers = [
            'Authorization: key=' . env('FIREBASE_TOKEN', ''),
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $result = curl_exec($ch);
        curl_close($ch);

        Log::info($result);
        Log::info(['log notification data' => $notification]);
        Log::info(['Fcm Token' => $listFcmTokens]);

        return $result;
    }
}
