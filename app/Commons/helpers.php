<?php

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;

if (!function_exists('number_collapse')) {
    /**
     * Collapse number
     *
     * @param integer $number
     * @param string  $separator
     *
     * @return string
     */
    function number_collapse($number, $separator = '')
    {
        $result = '';
        if ($number && is_numeric($number)) {
            if ($number > 1000000000) {
                $result = floor($number / 1000000000) . 'b';
                if ($number % 1000000000 != 0) {
                    $result .= $separator . number_collapse($number % 1000000000, ',');
                }
            } else if ($number > 1000000) {
                $result = floor($number / 1000000) . 'm';
                if ($number % 1000000 != 0 && empty($separator)) {
                    $result .= $separator . number_collapse($number % 1000000, ',');
                }
            } else if ($number > 1000) {
                $result = floor($number / 1000) . 'k';
                if ($number % 1000 != 0 && empty($separator)) {
                    $result .= $separator . number_collapse($number % 1000, ',');
                }
            } else {
                if (empty($separator))
                    $result = $number;
            }
        }
        return $result;
    }
}

/**
 * make path with datetime version
 *
 * @param      $path
 * @param bool $secure
 *
 * @return string
 * @author: trinhnv
 *
 */
if (!function_exists('asset_ver')) {
    function asset_ver($path, $secure = null)
    {
        return get_asset_ver($path, $secure);
    }
}

if (!function_exists('image_path')) {
    function image_path($path)
    {
        if (starts_with($path, '/')) {
            $path = substr($path, 1);
        }
        return url('images/' . $path);
    }
}

/**
 * make path for image by env
 *
 * @param      $path
 * @param bool $secure
 *
 * @return string
 * @author: trinhnv
 *
 */
if (!function_exists('asset_image')) {
    function asset_image($path)
    {
        $url = starts_with($path, '/') ? $path : '/' . $path;

        return env('IMAGE_URL', '') . $url . (config('app.debug') ? ('?v=' . date('Ymdhis')) : '');
    }
}

/**
 * @param      $path
 * @param null $secure
 * @param bool $asset_link
 *
 * @return string
 */
if (!function_exists('get_asset_ver')) {
    function get_asset_ver($path, $secure = null, $asset_link = false)
    {
        $url = '/' . $path;

        return $url . (config('app.debug') ? ('?v=' . date('Ymdhis')) : '?v=2022');
    }
}

if (!function_exists('trustedproxy_config')) {
    /**
     * Get Trusted Proxy value
     *
     * @param string $key
     * @param string $env_value
     *
     * @return mixed
     */
    function trustedproxy_config($key, $env_value)
    {
        if ($key === 'proxies') {
            if ($env_value === '*' || $env_value === '**') {
                return $env_value;
            }

            return $env_value ? explode(',', $env_value) : null;
        } elseif ($key === 'headers') {
            if ($env_value === 'HEADER_X_FORWARDED_AWS_ELB') {
                return Request::HEADER_X_FORWARDED_AWS_ELB;
            } elseif ($env_value === 'HEADER_FORWARDED') {
                return Request::HEADER_FORWARDED;
            }

            return Request::HEADER_X_FORWARDED_ALL;
        }

        return null;
    }
}

if (!function_exists('redirect_back_field')) {
    /**
     * Generate a redirect back url form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
    function redirect_back_field()
    {
        return new HtmlString('<input type="hidden" name="_redirect_back" value="' . old('_redirect_back', back()->getTargetUrl()) . '">');
    }
}

if (!function_exists('redirect_back_to')) {
    /**
     * Get an instance of the redirector.
     *
     * @param string|null $callbackUrl
     * @param int         $status
     * @param array       $headers
     * @param bool        $secure
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    function redirect_back_to($callbackUrl = null, $status = 302, $headers = [], $secure = null)
    {
        $to = request()->input('_redirect_back', back()->getTargetUrl());
        if ($callbackUrl) {
            if (!starts_with($to, $callbackUrl)) {
                $to = $callbackUrl;
            }
        }

        return redirect($to, $status, $headers, $secure);
    }
}

if (!function_exists('lfm_thumbnail')) {
    /**
     * Get image thumbnail LFM
     *
     * @param $imagePath
     *
     * @return string
     */
    function lfm_thumbnail($imagePath)
    {
        $img_thumb = explode('/', $imagePath);
        $directory = dirname($imagePath);

        return $directory . '/thumbs/' . end($img_thumb);
    }
}

if (!function_exists('unicode_encode')) {
    /**
     * Encode unicode
     *
     * @param $str
     *
     * @return string
     */
    function unicode_encode($str)
    {
        return preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }, $str);
    }
}
if (!function_exists('get_day_of_week')) {

    function get_day_of_week()
    {
        return [
            "1" => "Thứ 2",
            "2" => "Thứ 3",
            "3" => "Thứ 4",
            "4" => "Thứ 5",
            "5" => "Thứ 6",
            "6" => "Thứ 7",
            "7" => "Chủ nhật",
        ];
    }
}
if (!function_exists('get_week_number')) {
    /**
     * Get week number
     *
     * @param $ddate
     *
     * @return string
     */
    function get_week_number($ddate = null)
    {
        if ($ddate == null)
            $ddate = date(DATE_FORMAT);
        $date = new DateTime($ddate);

        return $date->format("W");
    }
}
if (!function_exists('get_week_info')) {
    /**
     * 0: this week, -1: next week; 1: last week
     *
     * @param $type
     * @param $week_number
     *
     * @return string
     */
    function get_week_info($type = 0, &$week_number = null)
    {
        [$firstDay, $lastDay] = get_first_last_day_in_week($type, $day);
        $week_number = get_week_number($day);
        $year = date('Y');
        return "$week_number-$year [$firstDay - $lastDay]";
    }
}

if (!function_exists('get_first_last_day_in_week')) {
    /**
     * 0: this week, -1: next week; 1: last week
     *
     * @param $type
     * @param $day
     *
     * @return array
     */
    function get_first_last_day_in_week($type = 0, &$day = null)
    {
        switch ($type) {
            case 1;
                $day = date(DATE_FORMAT, strtotime('-7 days'));
                break;
            case -1;
                $day = date(DATE_FORMAT, strtotime('+7 days'));
                break;
            default:
                $day = date(DATE_FORMAT);
                break;
        }

        $firstDay = date(DATE_MONTH_REPORT, strtotime("monday this week", strtotime($day)));
        $lastDay = date(DATE_MONTH_REPORT, strtotime("saturday this week", strtotime($day)));

        return [$firstDay, $lastDay];
    }
}

if (!function_exists('getMonthFormWeek')) {
    function getMonthFormWeek($week, $year = null)
    {
        if (!$year) $year = date('Y');
        $weekDays = getStartAndEndDate($week, $year);
        $week_start = $weekDays['week_start'];
        return explode('-', $week_start)[1];
    }
}

if (!function_exists('getStartAndEndDate')) {
    function getStartAndEndDate($week, $year, $getWeekend = true)
    {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('Y-m-d');
        $dto->modify($getWeekend ? '+6 days' : '+4 days');
        $ret['week_end'] = $dto->format('Y-m-d');

        return $ret;
    }
}

if (!function_exists('getWorkingDays')) {
    function getWorkingDays($startDate, $endDate, $format = DATE_FORMAT)
    {
        $startTimestamp = strtotime($startDate);
        $endTimestamp = strtotime($endDate);
        $workingDays = [];
        for ($i = $startTimestamp; $i <= $endTimestamp; $i = $i + (60 * 60 * 24)) {
            if (date("N", $i) <= 5) {
                $workingDays[] = date($format, $i);
            }
        }
        return $workingDays;
    }
}

if (!function_exists('getStartAndEndDateOfMonth')) {
    function getStartAndEndDateOfMonth($month, $year)
    {
        $dto = new DateTime();
        $dto->setDate($year, $month, 1);;

        return [
            $dto->format('Y-m-01'),
            $dto->format('Y-m-t'),
        ];
    }
}

if (!function_exists('get_years')) {
    /**
     * Encode unicode
     *
     * @param integer $num
     * @param string  $prefix
     * @param bool    $isDesc
     *
     * @param bool    $istatic
     *
     * @return array
     */
    function get_years($num = 5, $prefix = '', $isDesc = true, $istatic = false)
    {
        $result = [];
        $currentYear = date('Y');
        $minYear = $currentYear - $num;
        $start = $isDesc ? $currentYear : $minYear;
        $step = $isDesc ? -1 : 1;

        while ($start > $minYear && $start <= $currentYear) {
            $result[$start] = $prefix . $start;
            $start = $start + $step;
        }
        if ($istatic) {
            return $result;
        }
        return ['' => 'Chọn năm'] + $result;
    }
}

if (!function_exists('get_months')) {
    /**
     * Get months
     *
     * @param string $prefix
     * @param bool   $isDesc
     *
     * @return string
     *
     */
    function get_months($prefix = '', $isDesc = false)
    {
        $result = [];
        $start = $isDesc ? 12 : 1;
        $step = $isDesc ? -1 : 1;

        while ($start >= 1 && $start <= 12) {
            $result[$start] = $prefix . $start;
            $start = $start + $step;
        }

        return ['' => 'Chọn tháng'] + $result;
    }
}
if (!function_exists('get_month')) {
    /**
     * Get month
     *
     * @param $date
     *
     * @return string
     */
    function get_month($date)
    {
        return date('m', strtotime($date));
    }
}
if (!function_exists('__l')) {
    /**
     * Encode unicode
     *
     * @param $key
     * @param $attr
     *
     * @return string
     */
    function __l($key, $attr = [])
    {
        if (\Lang::has("messages.$key")) {
            return __("messages.$key", $attr);
        } else {
            \Log::info("messages.$key not exists");
            return $key;
        }
    }
}

if (!function_exists('__admin_sortable')) {
    /**
     * Echo sortable url
     *
     * @param $field
     *
     * @return string
     */
    function __admin_sortable($field)
    {
        echo '<a href="' . \Request::fullUrlWithQuery(['sort' => $field, 'is_desc' => !request('is_desc', false)]) . '"class="ic-ca">';
        if (request('sort') == $field && request('is_desc') == 1) {
            echo '<span class="dropup"><span class="caret"></span></span>';
        } else {
            echo '<span class="caret"></span>';
        }
        echo '</a>';
    }
}

if (!function_exists('statistics')) {
    /**
     * statistics
     *
     * @return array {array}
     */
    function statistics()
    {
        return [
            '1' => 'Tất cả công ty',
            '2' => 'Team',
            '3' => 'Cá nhân',
        ];
    }
}

if (!function_exists('team')) {
    /**
     * get all team
     *
     * @return array {array}
     */
    function team()
    {
        $teams = DB::table('teams')->select('id', 'name')->pluck('name', 'id')->toArray();
        return ['' => 'Chọn team'] + $teams;
    }
}

if (!function_exists('users')) {
    /**
     * get all users
     *
     * @return array {array}
     */
    function users($check = false)
    {
        $teams = DB::table('users')->select('id', 'name');
        if ($check) {
            $teams = $teams->where('status', ACTIVE_STATUS)->whereIN('jobtitle_id', [MANAGER_ROLE, MASTER_ROLE]);
        }

        return ['' => 'Chọn nhân viên'] + $teams->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
    }
}

if (!function_exists('to_work_time')) {
    /**
     *
     *
     * @return string
     */
    function to_work_time($time)
    {
        $times = explode(':', $time);
        if (count($times) == 3) {
            return $times[0] . ':' . $times[1];
        }
        return $time;
    }
}
if (!function_exists('get_date_list')) {
    /**
     *
     *
     * @param        $startDate
     * @param        $endDate
     * @param string $format
     *
     * @return array
     */
    function get_date_list($startDate, $endDate, $format = DATE_FORMAT)
    {
        $results = [];
        try {
            $period = new DatePeriod(
                new DateTime($startDate),
                new DateInterval('P1D'),
                new DateTime($endDate)
            );

            foreach ($period as $key => $value) {
                $results[] = $value->format($format);
            }
            $results[] = (new DateTime($endDate))->format($format);
        } catch (Exception $e) {
        }
        return $results;
    }
}
if (!function_exists('get_day_name')) {
    /**
     *
     *
     * @param      $date
     * @param bool $isShort
     *
     * @return string
     */
    function get_day_name($date, $isShort = false)
    {
        $day = date_format(date_create($date), 'N') + 1;
        if ($isShort) {
            return $day == 8 ? 'CN' : ('T' . $day);
        } else {
            return $day == 8 ? 'Chủ nhật' : ('Thứ ' . $day);
        }
    }
}
if (!function_exists('to_work_time_name')) {
    /**
     *
     *
     * @return string
     */
    function to_work_time_name($startAt, $endAt, &$type = 0)
    {
        if ($startAt >= SWITCH_TIME) {
            $type = 2;
            return 'Chiều';
        } else if ($startAt == OFF_TIME) {
            $type = 0;
            return 'Nghỉ';
        } else if ($endAt <= SWITCH_TIME) {
            $type = 1;
            return 'Sáng';
        }
        $type = 3;
        return "Cả ngày";
    }
}

if (!function_exists('checkNumber')) {

    /**
     * @param $number
     *
     * @return number
     */

    function checkNumber($number)
    {
        if (!$number) {
            return 0;
        }
        $explode = explode('.', $number);
        if (count($explode) > 1 && $explode[1] > 0) {
            return $number;
        } else {
            return $explode[0];
        }
    }
}

if (!function_exists('get_punish_image')) {

    /**
     * @param $number
     *
     * @return number
     */

    function get_punish_image($number)
    {
        $unit = 1000;
        //10M
        if ($number > 10000 * $unit) {
            return '/img/pigs/pig-4.png';
        } else if ($number > 5000 * $unit) {
            return '/img/pigs/pig-3.png';
        } else if ($number > 2000 * $unit) {
            return '/img/pigs/pig-2.png';
        } else {
            return '/img/pigs/pig-1.png';
        }
    }
}
if (!function_exists('show_timeout_event')) {

    /**
     * @param      $endAt
     * @param null $startAt
     *
     * @return string
     */

    function show_timeout_event($endAt, $startAt = null)
    {
        if (!$startAt) $startAt = date(DATE_TIME_FORMAT);
        $start = date_create($endAt);
        $end = date_create($startAt);
        $diff = date_diff($end, $start);
        $month = $diff->format('%m');

        if ($month > 0) {
            return $endAt;
        } else {
            $day = $diff->format('%d');
            if ($day > 0) {
                return $day . ' ngày';
            } else {
                $hour = $diff->format('%h');
                if ($hour > 0) {
                    return $hour . ' giờ';
                } else {
                    $minute = $diff->format('%i');
                    if ($minute > 0) {
                        return $minute . ' phút';
                    } else {
                        return 'vài giây nữa thôi !!!';
                    }
                }
            }
        }
    }
}

if (!function_exists('get_beautiful_time')) {

    /**
     * @param      $endAt
     * @param null $startAt
     *
     * @return string
     */

    function get_beautiful_time($time)
    {
        $startAt = date(DATE_TIME_FORMAT);

        $start = date_create($time);
        $end = date_create($startAt);
        $diff = date_diff($end, $start);
        $month = $diff->format('%m');
        $sendDate = $start->format(DATE_FORMAT);

        if ($month > 1) {
            return $time;
        } else if ($month == 1) {
            return 'Tháng trước';
        } else {
            $day = $diff->format('%d');

            if ($time->format(DATE_FORMAT) == date(DATE_FORMAT, strtotime('yesterday'))) {
                return 'Hôm qua';
            } else if ($day >= 1) {
                $currentWeek = get_week_number();
                $week = get_week_number($time);
                $subWeek = $currentWeek - $week;
                if ($subWeek > 1) {
                    return $subWeek . ' tuần trước';
                } elseif ($subWeek == 1) {
                    return get_day_name($time) . ', tuần trước';
                } else {
                    return $day . ' ngày trước';
                }
            } else {
                $hour = $diff->format('%h');

                if ($hour > 2) {
                    if ($sendDate != $end->format(DATE_FORMAT)) {
                        return 'Hôm qua';
                    } else {
                        return $hour . ' giờ trước';
                    }
                } elseif ($hour >= 1) {
                    return $hour . ' giờ trước';
                } else {
                    $minute = $diff->format('%i');
                    if ($minute > 0) {
                        return $minute . ' phút trước';
                    } else {
                        return 'Vừa xong';
                    }
                }
            }
        }
    }
}
if (!function_exists('get_year')) {
    function get_year($time)
    {
        return (int)Carbon::createFromFormat(DATE_FORMAT, $time)->year;
    }
}
if (!function_exists('check_day_of_week')) {
    function check_day_of_week($day)
    {
        return array_key_exists(Carbon::parse($day)->dayOfWeek, WEEKDAY);
    }
}

if (!function_exists('check_day_holiday')) {
    function check_day_holiday($day)
    {
        return DB::table('calendar_offs')->whereDate('date_off_from', $day)->orwhereDate('date_off_to', $day)->exists();
    }
}

if (!function_exists('paginate_links')) {
    /**
     * @param  array  $mergeUrls
     * @return array
     */
    function paginate_links($mergeUrls)
    {
        return request()->except(['per_page', 'page'] + array_keys($mergeUrls)) + $mergeUrls;
    }
}

if (!function_exists('change_log_time')) {

    /**
     * @param $logTime // format 10d2h
     * @return false|float|int|string
     */
    function change_log_time($logTime)
    {
        $dayOffset = strpos($logTime, 'd');
        $hourOffset = strpos($logTime, 'h');
        $day = DEFAULT_VALUE;
        $hour = DEFAULT_VALUE;
        if ($dayOffset !== false) {
            $day = substr($logTime, DEFAULT_VALUE, strpos($logTime, 'd'));
            if ($hourOffset !== 'false') $dayOffset++;
        }
        if ($hourOffset !== false) {
            if ($dayOffset === 'false') $dayOffset = DEFAULT_VALUE;
            $hour = substr($logTime, $dayOffset, strpos($logTime, 'h') - $dayOffset);
        }
        return $day * TIME_DAY + $hour;
    }

    if (!function_exists('get_week_date_list')) {
        /**
         *
         *
         * @param        $firtDate
         * @param        $lastDate
         *
         * @return array
         */
        function get_week_date_list($firtDate, $lastDate)
        {
            $firtDate = Carbon::parse($firtDate);
            $lastDate = Carbon::parse($lastDate);

            $dates = [];
            for($i = 1; $firtDate->lte($lastDate); $i++){
                $start = clone $firtDate->startOfWeek(Carbon::SUNDAY);
                $end = clone $firtDate->endOfWeek(Carbon::SATURDAY);
                $dates[$firtDate->weekOfYear] = get_date_list($start,$end);
                $firtDate->addDays(1);
            }

            return $dates;
        }
    }

    if (!function_exists('custom_total_log_work')) {
        /**
         * format adbh (ex: 1d3h)
         * @param  $time
         *
         * @return string
         */
        function custom_total_log_work($time)
        {
            if ($time < TIME_DAY) {
                $time = $time . 'h';
            } else {
                $day = floor($time / TIME_DAY);
                $hour = $time % TIME_DAY;
                $time = $day . 'd' . $hour . 'h';
            }
            return $time;
        }
    }

    if (!function_exists('codeQr')) {
        /**
         * codeQr
         *
         * @return string
         */
        function codeQr()
        {
            return date_format(Carbon::now(),'Ymd').Str::random(5);
        }
    }
}
