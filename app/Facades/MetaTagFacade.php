<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static bool getMetaTag($backUrl)
 *
 * @see \App\Helper\MetaTagHelper
 */
class MetaTagFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'meta_img_helper';
    }
}
