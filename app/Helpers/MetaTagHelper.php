<?php

namespace App\Helpers;

use App\Models\Event;
use App\Models\Post;
use Illuminate\Http\Request;

class MetaTagHelper
{
    public static function getMetaTagByRoute($backUrl)
    {
        $route = app('router')->getRoutes()->match(Request::create($backUrl));
        $id = $route->parameter('id');
        $content = url(JVB_LOGO_PATH);
        if ($id) {
            $routeName = $route->getName();

            switch ($routeName) {
                case 'event_detail' :
                    $event = Event::find($id);
                    if ($event) {
                        $content = url($event->image_url);
                    }
                    break;
                case 'post_detail':
                    $post = Post::find($id);
                    if ($post) {
                        $content = url($post->image_url);
                    }
                    break;
                default:
                    break;
            }
        }
        return $content;
    }
}
