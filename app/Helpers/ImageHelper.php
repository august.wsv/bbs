<?php

namespace App\Helpers;

/**
 * Class for image handling and manipulation
 * User: jvb
 * Date: 9/20/2017
 * Time: 11:48 AM
 */
class ImageHelper
{
    const SUFFIX_AVATAR = '_a'; // 240px
    const AVATAR_WIDTH = 240;

    const SUFFIX_SMALL = '_s';
    const SMALL_WIDTH = 480;
    const SMALL_HEIGHT = 320;
    const SUFFIX_NORMAL = '_m';
    const NORMAL_WIDTH = 900;
    const NORMAL_HEIGHT = 600;
    const SUFFIX_LARGE = '_l';
    const LARGE_WIDTH = 1920;
    const LARGE_HEIGHT = 1080;
    const THUMBNAIL_SUFFIX = 'thumb';
    const FILE_EXTENSION = 'png';
    const RATIO = 1.5;

    /**
     * @param string $imagePath
     * @param string $fileName
     * @param string $fileExt
     */
    public static function createThumbnail($imagePath, $fileName, $fileExt = self::FILE_EXTENSION)
    {
        self::generateThumbnailSmall($imagePath, $fileName, $fileExt);
        self::generateThumbnailNormal($imagePath, $fileName, $fileExt);
        self::generateThumbnailLarge($imagePath, $fileName, $fileExt);
    }

    /**
     * Get thumbnail small name
     *
     * @param $imagePath
     *
     * @return string
     */
    public static function getThumbnailSmall($imagePath = null)
    {
        return self::getThumnailName($imagePath, self::SUFFIX_SMALL);
    }

    /**
     * Get thumbnail Normal name
     *
     * @param $imagePath
     *
     * @return string
     */
    public static function getThumbnailNormal($imagePath = null)
    {
        return self::getThumnailName($imagePath, self::SUFFIX_NORMAL);
    }

    /**
     * Get thumbnail large name
     *
     * @param $imagePath
     *
     * @return string
     */
    public static function getThumbnailLarge($imagePath = null)
    {
        return self::getThumnailName($imagePath, self::SUFFIX_LARGE);
    }

    private static function generateThumbnailAvatar($uploadImage, $fileName, $fileExt = self::FILE_EXTENSION)
    {
        list($width, $height) = getimagesize($uploadImage);
        $w = $h = self::AVATAR_WIDTH;

        self::generateThumbnail(
            $uploadImage,
            $fileName,
            self::SUFFIX_AVATAR,
            $fileExt,
            $w,
            $h,
            $width,
            $height,
            0,
            0,
            0,
            0
        );
    }

    /**
     * Generate small thumbnail from uploaded image
     *
     * @param string $uploadImage path of image upload
     * @param string $fileName     name of image
     * @param string $filExt     extention of image
     */
    private static function generateThumbnailSmall($uploadImage, $fileName, $filExt = self::FILE_EXTENSION)
    {
        list($width, $height) = getimagesize($uploadImage);
        if (($width / $height) > self::RATIO) {
            //Caculate resize image height
            $resizeWidth = (int)(self::SMALL_WIDTH * $height / self::SMALL_HEIGHT);
            $srcX = ($width - $resizeWidth) / 2;
            self::generateThumbnail(
                $uploadImage,
                $fileName,
                self::SUFFIX_SMALL,
                $filExt,
                self::SMALL_WIDTH,
                self::SMALL_HEIGHT,
                $resizeWidth,
                $height,
                0,
                0,
                $srcX,
                0
            );
        } else {
            //Caculate resize image width
            $resizeHeight = (int)(self::SMALL_HEIGHT * $width / self::SMALL_WIDTH);
            $srcY = ($height - $resizeHeight) / 2;
            self::generateThumbnail(
                $uploadImage,
                $fileName,
                self::SUFFIX_SMALL,
                $filExt,
                self::SMALL_WIDTH,
                self::SMALL_HEIGHT,
                $width,
                $resizeHeight,
                0,
                0,
                0,
                $srcY
            );
        }
    }

    /**
     * Generate vertical thumbnail from uploaded image with largesize
     *
     * @param string $uploadImage path of image upload
     * @param string $fileName     name of image
     * @param string $fileExt     extention of image
     */
    private static function generateThumbnailNormal($uploadImage, $fileName, $fileExt = self::FILE_EXTENSION)
    {
        list($width, $height) = getimagesize($uploadImage);
        if (($width / $height) > self::RATIO) {
            //Caculate resize image height
            $resizeWidth = (int)(self::NORMAL_WIDTH * $height / self::NORMAL_HEIGHT);
            $srcX = ($width - $resizeWidth) / 2;
            self::generateThumbnail(
                $uploadImage,
                $fileName,
                self::SUFFIX_NORMAL,
                $fileExt,
                self::NORMAL_WIDTH,
                self::NORMAL_HEIGHT,
                $resizeWidth,
                $height,
                0,
                0,
                $srcX,
                0
            );
        } else {
            //Caculate resize image width
            $resizeHeight = (int)(self::NORMAL_HEIGHT * $width / self::NORMAL_WIDTH);
            $srcY = ($height - $resizeHeight) / 2;
            self::generateThumbnail(
                $uploadImage,
                $fileName,
                self::SUFFIX_NORMAL,
                $fileExt,
                self::NORMAL_WIDTH,
                self::NORMAL_HEIGHT,
                $width,
                $resizeHeight,
                0,
                0,
                0,
                $srcY
            );
        }
    }

    /**
     * Generate thumbnail from uploaded image with largesize
     *
     * @param string $uploadImage path of image upload
     * @param string $fileName     name of image
     * @param string $fileExt     extention of image
     */
    private static function generateThumbnailLarge($uploadImage, $fileName, $fileExt = self::FILE_EXTENSION)
    {
        list($width, $height) = getimagesize($uploadImage);
        if (($width / $height) > self::RATIO) {
            //Caculate resize image height
            $resizeWidth = (int)(self::LARGE_WIDTH * $height / self::LARGE_HEIGHT);
            $srcX = ($width - $resizeWidth) / 2;
            self::generateThumbnail(
                $uploadImage,
                $fileName,
                self::SUFFIX_LARGE,
                $fileExt,
                self::LARGE_WIDTH,
                self::LARGE_HEIGHT,
                $resizeWidth,
                $height,
                0,
                0,
                $srcX,
                0
            );
        } else {
            //Caculate resize image width
            $resizeHeight = (int)(self::LARGE_HEIGHT * $width / self::LARGE_WIDTH);
            $srcY = ($height - $resizeHeight) / 2;
            self::generateThumbnail(
                $uploadImage,
                $fileName,
                self::SUFFIX_LARGE,
                $fileExt,
                self::LARGE_WIDTH,
                self::LARGE_HEIGHT,
                $width,
                $resizeHeight,
                0,
                0,
                0,
                $srcY
            );
        }
    }

    /**
     * Generate thumbnail from uploaded image
     *
     * @param string $image       path of image upload
     * @param string $fileName    name of image
     * @param string $suffix      suffix of image
     * @param string $fileExt    extension of image
     * @param int    $thumbWidth  width of thumbnail image
     * @param int    $thumbHeight height of thumbnail image
     * @param int    $width       width of source image
     * @param int    $height      height of source image
     * @param int    $dstX       x-coordinate of destination point.
     * @param int    $dstY       y-coordinate of destination point.
     * @param int    $srcX       x-coordinate of source point.
     * @param int    $srcY       y-coordinate of source point.
     */
    private static function generateThumbnail(
        $image,
        $fileName,
        $suffix,
        $fileExt,
        $thumbWidth,
        $thumbHeight,
        $width,
        $height,
        $dstX = 0,
        $dstY = 0,
        $srcX = 0,
        $srcY = 0
    ) {

        $fileExtension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
        $fileName = pathinfo($fileName, PATHINFO_FILENAME);

        $tail = '_' . self::THUMBNAIL_SUFFIX . $suffix . '.' . $fileExt;
        $thumbnail = pathinfo($image, PATHINFO_DIRNAME) . '/' . $fileName . $tail;
        $thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);

        $whiteBg = imagecolorallocate($thumbImage, 255, 255, 255);
        imagefill($thumbImage, 0, 0, $whiteBg);

        try {
            switch ($fileExtension) {
                case 'jpg':
                    $source = imagecreatefromjpeg($image);
                    break;
                case 'jpeg':
                    $source = imagecreatefromjpeg($image);
                    break;
                case 'png':
                    $source = imagecreatefrompng($image);
                    break;
                default:
                    $source = imagecreatefrompng($image);
            }
        } catch (\Exception $e) {
            $source = imagecreatefromjpeg($image);
        };

        imagecopyresampled(
            $thumbImage,
            $source,
            $dstX,
            $dstY,
            $srcX,
            $srcY,
            $thumbWidth,
            $thumbHeight,
            $width,
            $height
        );
        switch ($fileExt) {
            case 'jpg' || 'jpeg':
                imagejpeg($thumbImage, $thumbnail, 100);
                break;
            case 'png':
                imagepng($thumbImage, $thumbnail, 100);
                break;
            default:
                imagepng($thumbImage, $thumbnail, 100);
        }
        imagedestroy($thumbImage);
    }

    /**
     * Get name of thumbnail
     *
     * @param $imagePath
     * @param $suffix
     *
     * @return string/false
     */
    private static function getThumnailName($imagePath, $suffix)
    {
        if (!$imagePath) {
            return FALSE;
        }
        try {
            $imagePathInfo = pathinfo($imagePath);
            $tail = '_' . self::THUMBNAIL_SUFFIX . $suffix . '.' . self::FILE_EXTENSION;
            $thumbPath = $imagePathInfo['dirname'] . '/' . $imagePathInfo['filename'] . $tail;
            if (!file_exists($thumbPath)) {
                return FALSE;
            }
            return $thumbPath;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public static function generateImageUrl($imagePath)
    {
        if (!$imagePath) {
            $imagePath = URL_IMAGE_NO_AVATAR;
        }
        if (starts_with($imagePath, '/')) {
            $imagePath = ltrim($imagePath, '/');
        }
        if (!file_exists($imagePath)) {
            $imagePath = URL_IMAGE_NO_AVATAR;
        }
        return $imagePath;
    }

    public static function getImgThumbnail($imagePath, $type = null)
    {
        $imagePath = static::generateImageUrl($imagePath);

        if ($type === -1) {
            $suffix = self::SUFFIX_AVATAR;
        } elseif ($type === 0) {
            $suffix = self::SUFFIX_SMALL;
        } elseif ($type == 1) {
            $suffix = self::SUFFIX_NORMAL;
        } elseif ($type == 2) {
            $suffix = self::SUFFIX_LARGE;
        } else {
            $suffix = null;
        }

        if (empty($suffix)) {
            return $imagePath;
        }
        try {
            $imagePathInfo = pathinfo($imagePath);
            $tail = '_' . self::THUMBNAIL_SUFFIX . $suffix . '.' . self::FILE_EXTENSION;
            $thumbPath = $imagePathInfo['dirname'] . '/' . $imagePathInfo['filename'] . $tail;
            if (!file_exists($thumbPath)) {
                if ($type == -1) {
                    self::generateThumbnailAvatar($imagePath, $imagePathInfo['basename']);
                } else if ($type == 0) {
                    self::generateThumbnailSmall($imagePath, $imagePathInfo['basename']);
                } else if ($type == 1) {
                    self::generateThumbnailNormal($imagePath, $imagePathInfo['basename']);
                } else if ($type == 2) {
                    self::generateThumbnailLarge($imagePath, $imagePathInfo['basename']);
                } else {
                    return $imagePath;
                }
            }
            $path = '/' . $thumbPath;
        } catch (\Exception $e) {
            $path = $imagePath;
        }

        return $path;
    }
}
