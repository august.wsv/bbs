<?php

namespace App\Helpers;

use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class CollectionHelper
{
    public static function paginate(Collection $results, $pageSize)
    {
        $page = Paginator::resolveCurrentPage('page');
        $total = $results->count();
        $data = array_values($results->forPage($page, $pageSize)->toArray());
        return static::paginator($data, $total, $pageSize, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);
    }

    /**
     * Create a new length-aware paginator instance.
     *
     * @param \Illuminate\Support\Collection $items
     * @param int $total
     * @param int $perPage
     * @param int $currentPage
     * @param array $options
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    protected static function paginator($items, $total, $perPage, $currentPage, $options)
    {
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $paginator = Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
            'items', 'total', 'perPage', 'currentPage', 'options'
        ));
        return [
            'meta' => [
                "pagination" => [
                    "total"=> $paginator->total(),
                    "per_page"=> $paginator->perPage(),
                    "current_page"=> $paginator->currentPage(),
                    "total_pages"=> ceil($paginator->total()/$paginator->perPage()),
                    "links"=> [
                        "next"=> $paginator->nextPageUrl()
                    ],
                ]
            ],
            'data' => $paginator->items()
        ];
    }

    /**
     * @param Collection $results
     * @param $sortKey
     * @param $sortType
     * @return Collection
     */
    public static function sortBy(Collection $results, $sortKey, $sortType){
        return in_array($sortType, IN_SORT_DESC) ? $results->sortByDesc($sortKey) : $results->sortBy($sortKey) ;
    }
}
