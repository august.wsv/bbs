<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function superAdmin($admin)
    {
        return $admin->isSuperAdmin();
    }

    public function admin($admin)
    {
        return $this->superAdmin($admin) || $admin->isAdmin();
    }

    public function groupManager($admin)
    {
        return $this->admin($admin) || $admin->isGM();
    }

    public function content($admin)
    {
        return $this->admin($admin) || $admin->isContent();
    }

    public function itHelpDesk($admin)
    {
        return $this->superAdmin($admin) || $this->admin($admin) || $admin->isItHelpDesk();
    }
}
