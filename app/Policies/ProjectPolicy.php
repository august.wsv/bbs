<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create_task(User $user, Project $project)
    {
        return $user->isManager() || in_array($user->id, $this->memberProject($project)) || $user->id === $project->leader_id;
    }

    public function edit_task(User $user, Project $project)
    {
        return $user->isManager() || in_array($user->id, $this->memberProject($project)) || $user->id === $project->leader_id;
    }

    public function edit(User $user, Project $project)
    {
        return $user->isManager() || $user->id === $project->leader_id || $this->checkEdit($project);
    }

    public function crudSprint(User $user, Project $project = null)
    {
        if (!$project) {
            return false;
        }

        return $user->isManager() || in_array($user->id, $this->memberProject($project));
    }

    private function checkEdit($project)
    {
        $ids = $this->memberProject($project);
        if ((Auth::user()->jobtitle_id == TEAMLEADER_ROLE || Auth::user()->jobtitle_id == MANAGER_ROLE) && in_array(Auth::id(), $ids)) {
            return true;
        } else {
            return false;
        }
    }

    private function memberProject($project){
        return $project->projectMembers->pluck('user_id')->toArray();
    }
}
