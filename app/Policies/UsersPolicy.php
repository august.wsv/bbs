<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class UsersPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function master($user)
    {
        if ($user->jobtitle_id == MASTER_ROLE) {
            return true;
        }
        return null;
    }

    public function manager($user)
    {
        if ($user->jobtitle_id >= MANAGER_ROLE) {
            return true;
        }
        return null;
    }

    public function teamLeader($user)
    {
        if ($user->jobtitle_id >= TEAMLEADER_ROLE) {
            return true;
        }
        return null;
    }

    public function HCNS($user)
    {
        $team=$user->team()->name ?? 'team';
        if ($team == TEAM_HCNS) {
            return true;
        }
        return null;
    }

    public function BRSE($user)
    {
        if ($user->jobtitle_id == BRSE) {
            return true;
        }
        return null;
    }

    public function staff($user)
    {
        if ($user->jobtitle_id >= STAFF) {
            return true;
        }
        return null;
    }

    public function seeWorkingTime($user)
    {
        $team=$user->team()->name ?? 'team';
        $jobtitle=$user->jobtitle_id;
        if ($team == TEAM_HCNS || $jobtitle==MASTER_ROLE || $jobtitle==MANAGER_ROLE || $jobtitle==TEAMLEADER_ROLE) {
            return true;
        }
        return null;
    }

    public function editShareDocument($user, $share)
    {
        $team=$user->team()->name ?? 'team';
        $jobtitle=$user->jobtitle_id;
        return $team == TEAM_HCNS || $jobtitle >= MANAGER_ROLE || $share->creator_id == $user->id;
    }
}
