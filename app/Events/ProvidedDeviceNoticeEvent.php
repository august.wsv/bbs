<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class ProvidedDeviceNoticeEvent extends NotificationBroadCast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    private $dayoff;
    private $user;
    private $type;
    private $toId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($providedDevice, $type)
    {
        $user = Auth::user();
        if ($user) {
            $url = route('device_index') . "#$providedDevice->id";
            $logoId = NOTIFICATION_TYPE['device'];
            $logoUrld = NOTIFICATION_LOGO[NOTIFICATION_TYPE['device']];
            if ($type == TYPE_DEVICE['send']) {
                $title = $user->name . SPACE . __l('device_suggest');
                $this->toId = $providedDevice->manager_id;
                $content = $providedDevice->title ?? '';
            } elseif ($type == TYPE_DEVICE['manager_approval']) {
                $title = $user->name . SPACE . __l('device_manager_approvel');
                $this->toId = $providedDevice->user_id;
                $content = $providedDevice->approval_manager ?? '';
            } else {
                $title = __l('device_administrative');
                $this->toId = $providedDevice->user_id;
                $content = $providedDevice->approval_hcnv ?? '';
                $jvbLogo = JVB_LOGO_URL;
            }
            $this->data = [
                'id' => $providedDevice->id,
                'name' => $user->name,
                'title' => $title,
                'content' => $content,
                'image_url' => $jvbLogo ?? $user->avatar,
                'logo_url' => $logoUrld,
                'logo_id' => $logoId,
                'url' => $url,
                'from_id' => $user->id,
                'to_id' => $this->toId,
            ];
        }
        parent::__construct();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('users.' . $this->toId);
    }
}
