<?php

namespace App\Events;

use App\Helpers\NotificationHelper;
use App\Jobs\SendFcmNotificationJob;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotificationBroadCast implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var array
     */
    public $data;

    /**
     * Create a new event instance.
     */
    public function __construct()
    {
        if ($this->data) {
            $notification = NotificationHelper::generateNotify(
                $this->data['to_id'],
                $this->data['title'],
                $this->data['content'],
                $this->data['from_id'],
                $this->data['logo_id'],
                $this->data['url']
            );
            Notification::insertAll([$notification]);
            if ($this->data['to_id']) {
                $fcmToken = User::find($this->data['to_id']);
                if (isset($fcmToken->fcm_token)) {
                    $job = new SendFcmNotificationJob([$fcmToken->fcm_token], $notification);
                    dispatch($job)->onQueue(QUEUE_HIGH);
                }
            }
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('bbs');
    }
}
