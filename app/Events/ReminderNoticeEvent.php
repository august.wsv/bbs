<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Auth;
use App\Models\User;

class ReminderNoticeEvent extends NotificationBroadCast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    private $userId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($reminder)
    {

        $this->userId = $reminder->user_id;
        $this->data = [
            'to_id' => $reminder->user_id,
            'title' => $reminder->name . ', Thông báo nhắc việc!',
            'logo_url' => NOTIFICATION_LOGO[NOTIFICATION_TYPE['reminder']],
            'logo_id' => NOTIFICATION_TYPE['reminder'],
            'content' => $reminder->content,
            'url' => route($reminder->route_detail_reminder, $reminder->id),
            'from_id' => $reminder->user_id,
        ];
        parent::__construct();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('users.' . $this->userId);
    }
}
