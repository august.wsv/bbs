<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsChannel;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsMessage;
use Illuminate\Support\Facades\Auth;

class SentMSTeam extends Notification
{
    use Queueable;
    private $contents;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $type;
    private $webhook_url;

    /**
     * Create a new notification instance.
     *
     * @param        $webhook_url
     * @param        $content
     * @param string $title
     * @param string $type
     */
    public function __construct($webhook_url, $content, $title = 'Thông báo', $type = 'success')
    {
        if(Auth::check()) {
            $userId = Auth::id();
            $content = "User $userId: \n" . $content;
        }
        $this->webhook_url = $webhook_url;
        $this->contents = $content;
        $this->title = $title;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [MicrosoftTeamsChannel::class];
    }

    /**
     * @param $notifiable
     *
     * @return MicrosoftTeamsMessage
     * @throws \NotificationChannels\MicrosoftTeams\Exceptions\CouldNotSendNotification
     */
    public function toMicrosoftTeams($notifiable)
    {
        if (!$this->webhook_url) {
            return;
        }
        return MicrosoftTeamsMessage::create()
            ->to($this->webhook_url)
            ->type($this->type)
            ->title($this->title)
            ->content($this->contents);
    }

}
