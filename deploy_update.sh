#!/bin/bash
set -xe

composer update

php artisan migrate

php artisan optimize