<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1',
    'as' => 'api::',
], function () {

    Route::post('/login', 'AuthController@login')->name('login');
    Route::group([
        'middleware' => ['jwt.auth'],
    ], function () {
        Route::get('/user', 'AuthController@user')->name('user');
        Route::get('/logout', 'AuthController@logout')->name('logout');
        Route::get('/auth/logout', 'AuthController@logout')->name('auth_logout');
        Route::get('/users', 'UserController@index')->name('users');
        Route::get('/users/{id}', 'UserController@detail')->name('users_detail');
        Route::get('/users-manager', 'UserController@getUserManager')->name('users_manager');
        Route::get('/group', 'UserController@group');
        Route::get('/teams', 'UserController@teans');

        Route::get('/punishes', 'PunishController@index')->name('punishes');

        Route::get('/events', 'EventController@index')->name('events');
        Route::get('/events/{id}', 'EventController@detail')->name('events_detail');

        Route::get('/posts', 'PostController@index')->name('posts');
        Route::get('/posts/{id}', 'PostController@detail')->name('posts_detail');

        Route::get('/reports', 'ReportController@index')->name('reports');
        Route::get('/reports/receivers', 'ReportController@getReportReceiver')->name('reports.report_receivers');
        Route::post('/reports', 'ReportController@saveReport')->name('reports.save_report');
        Route::get('/reports/{id}', 'ReportController@detail')->name('reports_detail');
        Route::post('/reports/{id}/reply', 'ReportController@replyReport')->name('reports.reply_report');
        Route::delete('/reports/{id}', 'ReportController@deleteReport')->name('reports_delete');
        Route::get('/statistics-report', 'ReportController@statistics');

        Route::get('/projects', 'ProjectController@index')->name('projects');
        Route::get('/projects/joined', 'ProjectController@joined')->name('joined_projects');
        Route::get('/projects/statistic', 'UserController@projectStatistic');
        Route::get('/projects/user-statistic', 'UserController@userStatisticPerformance');
        Route::get('/projects/{id}', 'ProjectController@detail')->name('projects_detail');
        Route::get('/projects/{id}/overview', 'ProjectController@overview')->name('projects_overview');
        Route::get('/projects/{id}/tasks', 'ProjectController@getTaskProgress');
        Route::post('/projects/{id}/tasks', 'ProjectController@createTask')->name('projects_create_task');
        Route::put('/projects/{id}/tasks/{taskId}', 'ProjectController@updateTask')->name('projects_update_task');
        Route::get('/tasks/{id}', 'ProjectController@taskDetail')->name('projects_task_detail');
        Route::put('/tasks/{task_id}/log-work', 'ProjectController@logWork')->name('task_log_work');
        Route::delete('/projects/{id}/tasks/{taskId}', 'ProjectController@deleteTask')->name('projects_delete_task');
        Route::get('/overview-task-log-work', 'ProjectController@overviewTaskLogWork')->name('overview-task-log-work');

        Route::prefix('project-sprint')->group(function() {
            Route::get('/', 'ProjectSprintController@index');
            Route::post('/', 'ProjectSprintController@storeSprint');
            Route::put('/{id}', 'ProjectSprintController@updateSprint');
            Route::delete('/{id}', 'ProjectSprintController@deleteSprint');
        });

        Route::get('/regulations', 'RegulationController@index')->name('regulations');
        Route::get('/regulations/{id}', 'RegulationController@detail')->name('regulations_detail');

        Route::get('/share/document', 'ShareController@document')->name('share_document');
        Route::get('/share/experience', 'ShareController@experience')->name('share_experience');
        Route::get('/share-detail/{id}', 'ShareController@detail')->name('share_detail');

        //xin nghỉ
        Route::get('/ngay-phep', 'UserController@dayOff')->middleware('delete.cache');
        Route::post('/ngay-phep', 'UserController@dayOffCreate');
        Route::get('/ngay-phep/{id}', 'UserController@dayOffDetail');
        Route::put('/duyet-ngay-nghi/{id}', 'UserController@approveDayOff');
        Route::get('/phe-duyet-ngay-phep', 'UserController@dayOffApprove');
        //xin đi muộn về sớm xin-phep/chi-tiet-don
        Route::get('xin-phep/chi-tiet-don', 'UserController@detailPermission');
        //đăng kí OT, đi muộn về sớm
        Route::post('xin-phep', 'UserController@askPermissionCreate');
        //danh sách xin phép OT, xin đi muộn về sớm với user đăng nhâp
        Route::get('xin-phep', 'UserController@getOtAndAskPermission');
        Route::post('checkin-qr-code', 'WorktimeController@checkQRCode')->name('checkin_qr_code');
        Route::get('/notifications', 'NotificationController@getUserNotifications');
        Route::post('/notification/mark-read', 'NotificationController@markRead');

    });

    Route::get('/worktime', 'WorktimeController@index');
    Route::group([
        'prefix' => 'worktime',
        'middleware' => ['sync-data'],
    ], function () {
        Route::post('sync', 'WorktimeController@sync')->name('worktime_sync');
        Route::post('checkin', 'WorktimeController@checkin')->name('worktime');    
        Route::get('users', 'UserController@index')->name('worktime_users');
        Route::post('qr/refresh', 'QrCodeController@refresh');
    });

    Route::post('details', 'AuthController@details');

    Route::group([
        'prefix' => 'public',
        'middleware' => ['external'],
    ], function () {
        Route::get('user-list', 'UserController@getUserList');
    });

});
