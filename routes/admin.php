<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

$this->get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
$this->post('login', 'Auth\LoginController@login');
$this->any('logout', 'Auth\LoginController@logout')->name('admin.logout');

Route::group([
    'middleware' => ['admin'],
    'as' => 'admin::'
], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'MasterController@index']);
    Route::middleware('admin_can:admin')->group(function () {
        Route::get('/download', ['as' => 'download', 'uses' => 'MasterController@download']);
        //punishes
        Route::post('punishes-submit', ['as' => 'punishes.submit', 'uses' => 'PunishesController@submits']);
        Route::post('punishes-confirm', ['as' => 'punishes.confirms', 'uses' => 'PunishesController@confirms']);
        Route::get('punishes/confirm/{id}', ['as' => 'punishes.confirm', 'uses' => 'PunishesController@changeConfirmStatus']);
        Route::get('punishes/status/{id}', ['as' => 'punishes.status', 'uses' => 'PunishesController@changeSubmitStatus']);
        Route::post('punishes/deletes', ['as' => 'punishes.deletes', 'uses' => 'PunishesController@deletes']);
        Route::resource('punishes', 'PunishesController');

        //rules
        Route::post('rules/deletes', ['as' => 'rules.deletes', 'uses' => 'RulesController@deletes']);
        Route::resource('rules', 'RulesController');

        //DeviceUser
        Route::get('devices/{id}/allocate', ['as' => 'deviceusers.allocate', 'uses' => 'DeviceUserController@allocate']);
        Route::post('deviceusers/deletes', ['as' => 'deviceusers.deletes', 'uses' => 'DeviceUserController@deletes']);
        Route::resource('deviceusers', 'DeviceUserController');

        //ActionDevice
//    Route::resource('ActionDevice', 'ActionDeviceController');

        //Device
        Route::post('devices/deletes', ['as' => 'devices.deletes', 'uses' => 'DeviceController@deletes']);
        Route::resource('devices', 'DeviceController');
        //event_attendance
        Route::resource('event_attendance', 'EventAttendanceController');


        //event_attendance
        Route::resource('event_attendance', 'EventAttendanceController');


        //project
        Route::resource('project', 'ProjectController');

        //OverTime
        Route::post('over_times/deletes', ['as' => 'over_times.deletes', 'uses' => 'OverTimeController@deletes']);
        Route::get('over_times/{id}/undo', ['as' => 'over_times.undo', 'uses' => 'OverTimeController@undo']);
        Route::resource('over_times', 'OverTimeController');

        // Ask Permission
        Route::post('approve_permission/deletes', ['as' => 'approve_permission.deletes', 'uses' => 'ApprovePermissionController@deletes']);
        Route::resource('approve_permission', 'ApprovePermissionController');
    Route::get('approve_permission/modal_detail/{id?}', 'ApprovePermissionController@modalDetail')
        ->name('approve_permission.modal_detail');

        //feedback
        Route::resource('feedback', 'FeedbackController');

        //WorkTimeDetail
        Route::resource('work_time_details', 'WorkTimeDetailController');

        //WorkTime
        Route::post('work_times/deletes', ['as' => 'work_times.deletes', 'uses' => 'WorkTimeController@deletes']);
        Route::get('work_times/download-template', ['as' => 'work_times.download_template', 'uses' => 'WorkTimeController@downloadTemplate']);
        Route::get('work_times/import', ['as' => 'work_times.import', 'uses' => 'WorkTimeController@import']);
        Route::post('work_times/import', ['as' => 'work_times.importData', 'uses' => 'WorkTimeController@importData']);
        Route::post('work_times/import', ['as' => 'work_times.importData', 'uses' => 'WorkTimeController@importData']);
        Route::get('work_times/user/{id}', ['as' => 'work_times.user', 'uses' => 'WorkTimeController@byUser'])->where(['id' => '\d+']);
        Route::resource('work_times', 'WorkTimeController');
        Route::get('work_time/update_time',  'WorkTimeController@updateTime')->name('work_time.update_time');


        //DayOff
        Route::post('day_offs/deletes', ['as' => 'day_offs.deletes', 'uses' => 'DayOffController@deletes']);
        Route::get('day_offs/user/{id}', ['as' => 'day_offs.user', 'uses' => 'DayOffController@byUser'])->where(['id' => '\d+']);
        Route::resource('day_offs', 'DayOffController');

        //report
        Route::resource('report', 'ReportController');

        //config
        Route::get('configs', 'ConfigController@index')->name('configs.index');
        Route::post('configs', 'ConfigController@store')->name('configs.store');
        Route::get('configs-dayoff', 'ConfigController@dayoffCreate')->name('configs.dayoff');
        Route::get('configs-dayoff-delete', 'ConfigController@dayoffDelete')->name('configs.delete_dayoff');
        Route::get('configs-dayadd', 'ConfigController@additionalDateCreate')->name('configs.dayadd');
        Route::get('configs-dayadd-delete', 'ConfigController@additionalDateDelete')->name('configs.delete_dayadd');
        Route::get('configs-dayoffjapan-delete', 'ConfigController@dayoffjapanDelete')->name('configs.delete_dayoffjapan');

        //regulation
        Route::post('regulations/deletes', ['as' => 'regulations.deletes', 'uses' => 'RegulationController@deletes']);
        Route::resource('regulations', 'RegulationController');

        //admin
        Route::resource('admins', 'AdminController');

        Route::resource('projects', 'ProjectController');
        Route::post('projects/deletes', ['as' => 'projects.deletes', 'uses' => 'ProjectController@deletes']);

        //users
        Route::post('user_trash/deletes', ['as' => 'user_trash.deletes', 'uses' => 'UserTrashController@deletes']);
        Route::patch('user_trash/restore', ['as' => 'user_trash.restore', 'uses' => 'UserTrashController@restore']);
        Route::resource('user_trash', 'UserTrashController');


        Route::get('users/import/{setId}', ['as' => 'users.import', 'uses' => 'UserController@import']);
        Route::get('users/download-template', ['as' => 'users.download-template', 'uses' => 'UserController@downloadTemplate']);
        Route::post('users/import', ['uses' => 'UserController@importData']);
        Route::post('users/deletes', ['as' => 'users.deletes', 'uses' => 'UserController@deletes']);
        Route::get('users/reset-password', 'UserController@resetPassword');
        Route::resource('users', 'UserController');

        //blocks
        Route::resource('blocks', 'BlockController');
        Route::post('blocks/deletes', ['as' => 'blocks.deletes', 'blocks' => 'BlockController@deletes']);
        Route::get('blocks/manage-member/{id}', ['as' => 'blocks.manage-member', 'uses' => 'BlockController@manageMember']);
        Route::post('blocks/save-member', ['uses' => 'BlockController@saveUserBlock']);

        Route::post('teams/save-member', ['uses' => 'TeamController@saveUserTeam']);
        Route::get('teams/manage-member/{id}', ['uses' => 'TeamController@manageMember']);
        Route::put('teams/{id}', ['uses' => 'TeamController@updateTmp'])->name('teame.update123');
        Route::post('teams/deletes', ['as' => 'teams.deletes', 'uses' => 'TeamController@deletes']);
        Route::resource('teams', 'TeamController');

        //Quiz
        Route::post('quizzes/deletes', ['as' => 'quizzes.deletes', 'uses' => 'QuizController@deletes']);
        Route::resource('quizzes', 'QuizController');

        //Question
        Route::post('questions/deletes', ['as' => 'questions.deletes', 'uses' => 'QuestionController@deletes']);
        Route::get('quizzes/list_question/{id?}', ['as' => 'questions.index', 'uses' => 'QuestionController@index']);
        Route::post('sortable', ['as' => 'questions.sortable', 'uses' => 'QuestionController@sortableList']);
        Route::resource('questions', 'QuestionController', ['except' =>  'index']);
        
        //Thống kê kết quả quiz
        Route::get('quiz_statistic', ['as' => 'quizzes.statistic', 'uses' => 'QuizController@statisticQuiz']);
        Route::get('quiz_statistic/detail/{id}', 'QuizController@getUser')->name('quiz.get_user');
        Route::get('quiz_statistic/detail/answer_user/{id}', 'QuizController@answerUser')->name('quiz.answer_user');

        //register work time
        Route::post('work_time_register/deletes', ['as' => 'work_time_register.deletes', 'uses' => 'WorkRegisterController@deletes']);
        Route::resource('work_time_register', 'WorkRegisterController');

        //statistic work time
        Route::post('work_time_statistic/deletes', ['as' => 'work_time_statistic.deletes', 'uses' => 'StatisticController@deletes']);
        Route::get('work_time_statistic/export', ['as' => 'work_time_statistic.export', 'uses' => 'StatisticController@export']);
        Route::resource('work_time_statistic', 'StatisticController');
        //rooms
        Route::post('meeting_rooms/deletes', ['as' => 'meeting_rooms.deletes', 'uses' => 'MeetingRoomController@deletes']);
        Route::resource('meeting_rooms', 'MeetingRoomController');

        // manager-group
        Route::resource('group', 'GroupController');
        Route::post('group/deletes', ['as' => 'group.deletes', 'uses' => 'GroupController@deletes']);

        // suggestions
        Route::resource('suggestions', 'SuggestionController');
        Route::post('suggestions/deletes', ['as' => 'suggestions.deletes', 'uses' => 'SuggestionController@deletes']);
        Route::get('suggestions/detail/{id}', 'SuggestionController@detailSuggestions')->name('events.suggestions');

        //labor-calendar
        Route::post('count_user_chose_of_month','LaborCalendarController@countUserChose')->name('count.user.chose');
        Route::resource('labor_calendar','LaborCalendarController');
        Route::get('pairing_employee', 'LaborCalendarController@pairingEmployee')->name('labor.pairing');
        Route::post('pairing_chose', 'LaborCalendarController@infoPairUserChose')->name('labor.pair.chose');
        Route::post('update_pairing_user', 'LaborCalendarController@updatePairUser')->name('labor.update.pair.user');
        Route::post('pairing_status', 'LaborCalendarController@statusPairUserChose')->name('labor.pair.status');
        Route::post('update_pairing_status', 'LaborCalendarController@updateStatusPairUserChose')->name('labor.update.pair.status');
        Route::post('update_work_content', 'LaborCalendarController@updateWorkContent')->name('labor.update.work.content');
        // statistic-punished
        Route::resource('statistic_punished','StatisticPunishedController');
        Route::get('/export-punishes', ['as' => 'export-punishes', 'uses' => 'StatisticPunishedController@exportPunishes']);

        // music
//    Route::post('music/deletes', ['as' => 'music.deletes', 'uses' => 'MusicController@deletes']);
//    Route::resource('music', 'MusicController');
//    Route::resource('questions', 'QuestionsController');
//    Route::post('questions/deletes', ['as' => 'questions.deletes', 'uses' => 'QuestionsController@deletes']);
//    Route::get('questions/{id}/detail', 'QuestionsController@questionDetail')->name('questions.home.detail');
//    Route::get('comment/{id}/deletes/{role}', 'QuestionsController@deleteComment')->name('questions.comment.delete');
    });

    Route::middleware(['admin_can:content'])->group(function () {
        //post
        Route::get('posts/broadcast', ['as' => 'posts.broadcast', 'uses' => 'PostController@broadcast']);
        Route::post('posts/sendBroadcast', ['as' => 'posts.sendBroadcast', 'uses' => 'PostController@sendBroadcast']);
        Route::post('posts/deletes', ['as' => 'posts.deletes', 'uses' => 'PostController@deletes']);
        Route::resource('posts', 'PostController');

        //event
        Route::post('events/deletes', ['as' => 'events.deletes', 'uses' => 'EventController@deletes']);
        Route::resource('events', 'EventController');
        Route::get('events/detail/{id}', 'EventController@detailEvent')->name('events.detailEvent');
        Route::get('events/dowload-excel-list-user/{id}', 'EventController@dowloadExcelListUserJoin')->name('events.dowloadExcelListUserJoin');
    });

    // Role
    Route::middleware(['admin_can:super-admin'])->group(function () {
        Route::resource('role', 'RoleController');
        Route::post('role/deletes', ['as' => 'role.deletes', 'uses' => 'RoleController@deletes']);
    });

    Route::middleware(['admin_can:itHelpDesk'])->group(function () {
        // device
        Route::get('t_devices/devices-list/{device?}/{role?}','TDeviceController@listUserDevice')->name('t_devices.t_list_devices');
        Route::get('t_devices/t_export_device','TDeviceController@export')->name('t_devices.t_export_device');
        Route::get('t_device/add-device/{user_id}', 'TDeviceController@addDevice')->name('t_devices.t_add_devices');
        Route::get('t_device/detail/{id}', 'TDeviceController@detailDevice')->name('t_devices.t_detail_device');
        Route::post('t_device/save-device', 'TDeviceController@saveFormAdd')->name('t_devices.t_save_form_device');
        Route::get('t_device/get-device', 'TDeviceController@getDevice');
        Route::get('t_device/get-device/attribute/{id}', 'TDeviceController@getAttributeDevice');
        Route::get('t_devices/upgrade/{id}/{role}', 'TDeviceController@updateDevcie')->name('t_devices.upgrade');
        Route::post('t_devices/save-upgrade/{id}/{role}', 'TDeviceController@saveFormUpgrade')->name('t_devices.saveFormUpgrade');
        Route::get('t_devices/deletes/{id}', ['as' => 't_devices.deletes', 'uses' => 'TDeviceController@deletes']);
        Route::get('t_device/export/almost-expire', 'TDeviceController@exportData')->name('t_devices.export.almost_expire');
        Route::post('t_device/list-user', 'TDeviceController@listUser')->name('t_device.list_user');

        Route::get('t_devices/get-device-import/{id}', 'TDeviceController@getDeviceImportById');
        Route::get('t_devices/list-device-import', 'TDeviceController@allDeviceImport');
        Route::get('t_devices/list-device/import/{device?}', 'TDeviceController@listImportDevice')->name('t_devices.t_import_device_index');
        Route::get('t_devices/detail/import-device/{id}', 'TDeviceController@detailDeviceImport')->name('t_devices.device_import_detail');
        Route::post('t_devices/thanh-ly-thiet-bi/{id}', 'TDeviceController@thanhLy')->name('t_devices.thanhly');
        Route::get('t_devices/remove-import-device/{id}', 'TDeviceController@removeImportDevice');
        Route::get('t_devices/add_device/import', 'TDeviceController@addDeviceImport')->name('t_devices.add_device_import');
        Route::post('t_devices/save/device_import', 'TDeviceController@saveDeviceImport')->name('t_devices.save_device_import');
        Route::post('t_devices/edit-name/device_import', 'TDeviceController@editNameImportDevice')->name('t_devices.edit_name_import_device');
        // device attr
        Route::get('t_devices/devices-attribute','TDeviceController@listDeviceAttribute')->name('t_devices.attribute');
        Route::get('t_device/add-attribute-device', 'TDeviceController@addAttributeDevice')->name('t_devices.t_add_attribute_devices');
        Route::post('t_device/save-attribute', 'TDeviceController@saveFormAttribute')->name('t_devices.t_save_form_attribute');
        Route::get('t_device/remove-attr-device/{id}', 'TDeviceController@removeAttrDevice');
        Route::get('t_device/them-moi-thuoc-tinh/{id}', 'TDeviceController@addAttrForm')->name('t_devices_add_attr');
        Route::post('t_device/save-attr-device/{id}', 'TDeviceController@saveAddAttr')->name('t_devices.t_save_attribute');
        Route::resource('t_devices', 'TDeviceController');
        
        //DeviceUser
        Route::post('provided_device/deletes', ['as' => 'provided_device.deletes', 'uses' => 'ProvidedDeviceController@deletes']);
        Route::resource('provided_device', 'ProvidedDeviceController');

        //Client Passport
        Route::resource('passport', 'PassportController');
        Route::post('passport/deletes', ['as' => 'passport.deletes', 'uses' => 'PassportController@deletes']);
    });
});
