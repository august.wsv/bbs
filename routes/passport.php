<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => 'v1',
    'as' => 'passport::',
], function () {
    Route::get('/gitlab-user', 'PassportController@getGitlabUser');
    Route::get('/get-user', 'PassportController@getUser');
    Route::get('/ticket-user', 'PassportController@ticketUser');
});

