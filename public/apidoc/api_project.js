define({
  "name": "BBS",
  "version": "1.0.0",
  "description": "BBS api documentation",
  "title": "Api list",
  "url": "https://bbs.hatoq.com/api/v1",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-08-08T03:15:22.856Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
