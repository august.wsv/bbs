define({ "api": [
  {
    "type": "post",
    "url": "/login",
    "title": "",
    "name": "Login",
    "description": "<p>Login</p>",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of User.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "User",
            "description": "<p>User info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Meta",
            "description": "<p>meta.token for jwt authentication.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "LoginFail",
            "description": "<p>http status code 422</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/AuthController.php",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/logout",
    "title": "",
    "name": "Logout",
    "description": "<p>Logout</p>",
    "group": "Auth",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/AuthController.php",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/logout",
    "title": "",
    "name": "UserInfo",
    "description": "<p>UserInfo</p>",
    "group": "Auth",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "User",
            "description": "<p>User info.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/AuthController.php",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/events",
    "title": "",
    "name": "EventList",
    "description": "<p>List event</p>",
    "group": "Event",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keyword to search events.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "events",
            "description": "<p>List of event.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta Pagination.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/EventController.php",
    "groupTitle": "Event"
  },
  {
    "type": "get",
    "url": "/events/:id",
    "title": "",
    "name": "GetEvent",
    "description": "<p>Event detail</p>",
    "group": "Event",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "event",
            "description": "<p>Event info.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/EventController.php",
    "groupTitle": "Event"
  },
  {
    "type": "get",
    "url": "/posts",
    "title": "",
    "name": "PostList",
    "description": "<p>Post list</p>",
    "group": "Post",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keyword to search posts.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "posts",
            "description": "<p>List of post.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta Pagination.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/PostController.php",
    "groupTitle": "Post"
  },
  {
    "type": "get",
    "url": "/posts/:id",
    "title": "",
    "name": "Post_detail",
    "description": "<p>GetPost</p>",
    "group": "Post",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "post",
            "description": "<p>Post info.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/PostController.php",
    "groupTitle": "Post"
  },
  {
    "type": "get",
    "url": "/projects",
    "title": "",
    "name": "List_project",
    "description": "<p>ProjectList</p>",
    "group": "Project",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keyword to search projects.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "projects",
            "description": "<p>List of project.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta Pagination.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ProjectController.php",
    "groupTitle": "Project"
  },
  {
    "type": "get",
    "url": "/projects/:id",
    "title": "",
    "name": "ProjectDetail",
    "description": "<p>GetProject</p>",
    "group": "Project",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "project",
            "description": "<p>Project info.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ProjectController.php",
    "groupTitle": "Project"
  },
  {
    "type": "get",
    "url": "/punishes",
    "title": "",
    "name": "List_punish",
    "description": "<p>PunishList</p>",
    "group": "Punish",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keyword to search punishes.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "punishes",
            "description": "<p>List of punish.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta Pagination.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/PunishController.php",
    "groupTitle": "Punish"
  },
  {
    "type": "get",
    "url": "/punishes/:id",
    "title": "",
    "name": "Punish_detail",
    "description": "<p>GetPunish</p>",
    "group": "Punish",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "punish",
            "description": "<p>punish info.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/PunishController.php",
    "groupTitle": "Punish"
  },
  {
    "type": "get",
    "url": "/regulations",
    "title": "",
    "name": "List_regulation",
    "description": "<p>RegulationList</p>",
    "group": "Regulation",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keyword to search regulations.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "regulations",
            "description": "<p>List of regulation.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta Pagination.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/RegulationController.php",
    "groupTitle": "Regulation"
  },
  {
    "type": "get",
    "url": "/regulations/:id",
    "title": "",
    "name": "Regulation_detail",
    "description": "<p>GetRegulation</p>",
    "group": "Regulation",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "regulation",
            "description": "<p>Regulation info.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/RegulationController.php",
    "groupTitle": "Regulation"
  },
  {
    "type": "get",
    "url": "/reports",
    "title": "",
    "name": "List_report",
    "description": "<p>ReportList</p>",
    "group": "Report",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keyword to search reports.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "reports",
            "description": "<p>List of report.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta Pagination.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ReportController.php",
    "groupTitle": "Report"
  },
  {
    "type": "get",
    "url": "/new-report",
    "title": "",
    "name": "New_report",
    "description": "<p>GetNewReport</p>",
    "group": "Report",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Report",
            "optional": false,
            "field": "Report.",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ReportController.php",
    "groupTitle": "Report"
  },
  {
    "type": "post",
    "url": "/report",
    "title": "",
    "name": "Report_create",
    "description": "<p>SaveReport</p>",
    "group": "Report",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "choose_week",
            "description": "<p>Week number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Save draft or final.</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "to_ids",
            "description": "<p>Receiver Ids.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Report content.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "success.",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ReportController.php",
    "groupTitle": "Report"
  },
  {
    "type": "get",
    "url": "/reports/:id",
    "title": "",
    "name": "Report_detail",
    "description": "<p>GetReport</p>",
    "group": "Report",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "report",
            "description": "<p>Report info.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ReportController.php",
    "groupTitle": "Report"
  },
  {
    "type": "get",
    "url": "/report",
    "title": "",
    "name": "Report_detail",
    "description": "<p>ReportDetail</p>",
    "group": "Report",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Report Id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Report",
            "optional": false,
            "field": "Report.",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ReportController.php",
    "groupTitle": "Report"
  },
  {
    "type": "get",
    "url": "/report-receivers",
    "title": "",
    "name": "Report_receivers",
    "description": "<p>GetReportReceiver</p>",
    "group": "Report",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "List",
            "optional": false,
            "field": "list",
            "description": "<p>receiver.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ReportController.php",
    "groupTitle": "Report"
  },
  {
    "type": "delete",
    "url": "/report/:id",
    "title": "",
    "name": "Report_remove",
    "description": "<p>ReportRemove</p>",
    "group": "Report",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "success.",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ReportController.php",
    "groupTitle": "Report"
  },
  {
    "type": "post",
    "url": "/reply-report",
    "title": "",
    "name": "Report_reply",
    "description": "<p>ReportReply</p>",
    "group": "Report",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "report_id",
            "description": "<p>Report Id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Report content.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "success.",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ReportController.php",
    "groupTitle": "Report"
  },
  {
    "type": "get",
    "url": "/share/document",
    "title": "",
    "name": "List_document_share",
    "description": "<p>Sharing document list</p>",
    "group": "Share",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keyword to search shares.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "shares",
            "description": "<p>List of share.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta Pagination.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ShareController.php",
    "groupTitle": "Share"
  },
  {
    "type": "get",
    "url": "/share/experience",
    "title": "",
    "name": "List_experience_share",
    "description": "<p>Sharing experience list</p>",
    "group": "Share",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keyword to search shares.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "shares",
            "description": "<p>List of share.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta Pagination.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ShareController.php",
    "groupTitle": "Share"
  },
  {
    "type": "get",
    "url": "/shares/:id",
    "title": "",
    "name": "Share_detail",
    "description": "<p>GetShare</p>",
    "group": "Share",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "share",
            "description": "<p>Share info.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/ShareController.php",
    "groupTitle": "Share"
  },
  {
    "type": "get",
    "url": "/users/:id",
    "title": "",
    "name": "UserDetail",
    "description": "<p>User detail</p>",
    "group": "User",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User info.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users",
    "title": "",
    "name": "UserList",
    "description": "<p>User List</p>",
    "group": "User",
    "header": {
      "fields": {
        "Bearer Header": [
          {
            "group": "Bearer Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keyword to search users.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "users",
            "description": "<p>List of user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta Pagination.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "bbs/app/Http/Controllers/API/UserController.php",
    "groupTitle": "User"
  }
] });
