$(function () {
    if($("#answer4").length == 0){
        $(".btn-add-answer").show();
    }else{
            $(".btn-add-answer").hide();
    }
    var max_fields = 4;
    var x = $(".form-check").length;
    $(".form_2.data_info").on("click",".btn-add-answer", function(e){ 
        e.preventDefault();
        console.log(x);
        if(x < max_fields){ 
            x++;
            $(".group-answer").append('<div class="form-check" style="margin-top: 10px;">'+
                                        '<input class="form-check-input correct-answer" type="radio" name="is_correct" value="' + (x-1) + '" id="radio' + x + '">'+
                                        '<input  class="form-control answer" type="text" name="answers[]" id="answer'+ x + '">' +
                                        '<a href="#" class="btn btn-danger btn-sm btn-delete" data-id="'+(x-1)+'" style="margin-left: 10px;"><i class="fa fa-trash-o"></i></a>'+
                                      '</div>'
                                     );
            if(x==4){
                $(".btn-add-answer").hide();
            }
        }
    })
    $(".form_2.data_info").on("click",".btn-delete", function(e){ 
        $(this).parent('div').remove();
        x--;
        let valAnswer = $(this).parent().children(':first').val()
        if (valAnswer < 3) {
            $(`input[value=${Number(valAnswer) + 1}]`).val(2);
        }
        $(".btn-add-answer").show();

    })
    var clone = $(".add_answer").clone();
    $(".form_1_btns .btn_next").click( function(){
        $(".form_1").css("display", "none");
        $(".form_2").css("display", "block");
        $(".form_1_btns").css("display", "none");
        $(".form_2_btns").css("display", "flex");
        $(".form_2_progessbar").addClass("active");
        if ($("#essay").prop("checked")) {
            $(".add_answer").detach();
        }else if($('.add_answer').length == 0){
            $(".add_question").append(clone);
        }
    });
    $(".form_2_btns .btn_next, .btn-save").click( function(){
        var flag = 0;
		$('.err-answer, .err-is-correct, .err-question').remove();
        if ($(".question").val() == "") {
            $('.question').after( "<div class='err-question margin-t-5' style='color:red'>Vui lòng điền câu hỏi</div>" );
            flag++;
        }
        if (!($('.add_answer').find('input:radio').is(':checked')) && $('.answer').length != 0 ) {
            $('.group-answer').prepend( "<div class='err-is-correct' style='color:red'>Vui lòng chọn đáp án đúng</div>" );
            flag++;
        }
		$('[name^="answers[]"]').each(function() {
			if($(this).val() == ""){
				$(this).parent().append( "<div class='err-answer margin-t-5' style='color:red; padding-left:30px'>Vui lòng điền câu trả lời</div>" );
				flag++;	
			}
		});
		if(flag != 0){
            event.preventDefault();
        }else{
			$(".form_2").css("display", "none");
			$(".form_3").css("display", "block");
			$(".form_2_btns").css("display", "none");
			$(".form_3_btns").css("display", "flex");
			$(".form_3_progessbar").addClass("active");
			$(".question").append("<span class='question_result'>" + $(".question").val() + "</span>");
			$("<div class='input_wrap answer_sample'></div>").insertAfter(".question_sample");
			var html =""
			$('[name^="answers[]"]').each(function() {
				html += '<div class="answer_sample">';
                if ($(this).siblings().is(":checked")) {
                    html +=	'<div class="form-check correct-answer-quiz">';
                    html +=	'<input class="form-check-input" type="radio" name="questions" id="exampleRadios" value="" disabled checked>';
                    html +=	'<label class="form-check-label margin-l-15 label-answer" for="exampleRadios" style="height:35px">' + $(this).val() +'</label>'; 
                    html +=	'</div>'; 
                    html +=	'</div>';
                } else{
                    html +=	'<div class="form-check">';
                    html +=	'<input class="form-check-input" type="radio" name="questions" id="exampleRadios" value="" disabled>';
                    html +=	'<label class="form-check-label margin-l-15 label-answer" for="exampleRadios" style="height:35px">' + $(this).val() +'</label>'; 
                    html +=	'</div>'; 
                    html +=	'</div>';
                }

			});
			$(".input_wrap.answers_sample").html(html);
		}
	});
    $(".form_2_btns .btn_back").click( function(){
        $(".form_2").css("display", "none");
        $(".form_1").css("display", "block");
        $(".form_2_btns").css("display", "none");
        $(".form_1_btns").css("display", "flex");
        $(".form_2_progessbar").removeClass("active");
    });
    $(".form_3_btns .btn_back").click( function(){
        $(".form_3").css("display", "none");
        $(".form_2").css("display", "block");
        $(".form_3_btns").css("display", "none");
        $(".form_2_btns").css("display", "flex");
        $(".form_3_progessbar").removeClass("active");
        $(".answer_sample").remove();
        $(".question_result").remove();
    });

    $('#sidebar-form').on('submit', function (e) {
        e.preventDefault();
    });
    var noticeIdx = 0;
    $.each($('.notification'), function () {
        let $notification = $(this);
        let fadeOutTime = 3 * 1000 + (noticeIdx++ * 1000); //5s
        let totalTime = fadeOutTime;
        let notificationInterval = window.setInterval(function () {
            totalTime -= 50;
            $notification.children('.notification-spinner').css('width', totalTime / fadeOutTime * 100 + '%');
            if (totalTime === 0) {
                clearInterval(notificationInterval);
                $notification.slideUp();
            }
        }, 50);

        $notification.hover(function () {
            totalTime = fadeOutTime;
        });
    });


    $('.sidebar-menu li.active').data('lte.pushmenu.active', true);

    $('#search-input').on('keyup', function () {
        var term = $('#search-input').val().trim();

        if (term.length === 0) {
            $('.sidebar-menu li').each(function () {
                $(this).show(0);
                $(this).removeClass('active');
                if ($(this).data('lte.pushmenu.active')) {
                    $(this).addClass('active');
                }
            });
            return;
        }

        $('.sidebar-menu li').each(function () {
            if ($(this).text().toLowerCase().indexOf(term.toLowerCase()) === -1) {
                $(this).hide(0);
                $(this).removeClass('pushmenu-search-found', false);

                if ($(this).is('.treeview')) {
                    $(this).removeClass('active');
                }
            } else {
                $(this).show(0);
                $(this).addClass('pushmenu-search-found');

                if ($(this).is('.treeview')) {
                    $(this).addClass('active');
                }

                var parent = $(this).parents('li').first();
                if (parent.is('.treeview')) {
                    parent.show(0);
                }
            }

            if ($(this).is('.header')) {
                $(this).show();
            }
        });

        $('.sidebar-menu li.pushmenu-search-found.treeview').each(function () {
            $(this).find('.pushmenu-search-found').show(0);
        });
    });
});

$(function () {
    //Initialize Select2 Elements
    $('select.select2').select2();

    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].square-green, input[type="radio"].square-green').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    $('input[type="checkbox"].square-blue, input[type="radio"].square-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
    });
    $('input[type="checkbox"].square-yellow, input[type="radio"].square-yellow').iCheck({
        checkboxClass: 'icheckbox_square-yellow',
        radioClass: 'iradio_square-yellow'
    });
});

$(function () {
    //Initialize Datetimepicker Elements
    $('input.datetimepicker').datetimepicker({
        //locale: 'es', locale: 'en',
        format: 'YYYY/MM/DD h:mm A',
        showTodayButton: true,
        showClear: true,
        icons: {
            today: "fa fa-thumb-tack",
            clear: "fa fa-trash"
        }
    });

    $('input.datetimepicker-single').datetimepicker({
        //locale: 'es', locale: 'en',
        format: 'YYYY-MM-DD',
        showTodayButton: true,
        showClear: true,
        icons: {
            today: "fa fa-thumb-tack",
            clear: "fa fa-trash"
        }
    });

    $('input.datetimepicker-search').datetimepicker({
        //locale: 'es', locale: 'en',
        format: 'YYYY-MM-DD',
        showTodayButton: true,
        showClear: true,
        icons: {
            today: "fa fa-thumb-tack",
            clear: "fa fa-trash"
        }
    });

    $('#date-range-select-search').on('select2:select', function (e) {
        var data = e.params.data;
        var fromElem = $('#date-range-search-from');
        var toElem = $('#date-range-search-to');
        var index = data.id ? data.id : 'custom';
        var handler = null;

        var ranges = {
            'custom': function () {
                fromElem.data('DateTimePicker').clear();
                toElem.data('DateTimePicker').clear();
            },
            'today': function () {
                fromElem.data('DateTimePicker').date(moment());
                toElem.data('DateTimePicker').date(moment());
            },
            'yesterday': function () {
                var start = moment().subtract(1, 'days');
                var end = moment().subtract(1, 'days');
                fromElem.data('DateTimePicker').date(start);
                toElem.data('DateTimePicker').date(end);
            },
            'last_7_days': function () {
                var start = moment().subtract(6, 'days');
                var end = moment();
                fromElem.data('DateTimePicker').date(start);
                toElem.data('DateTimePicker').date(end);
            },
            'this_month': function () {
                var start = moment().startOf('month');
                var end = moment().endOf('month');
                fromElem.data('DateTimePicker').date(start);
                toElem.data('DateTimePicker').date(end);
            },
            'last_month': function () {
                var start = moment().subtract(1, 'month').startOf('month');
                var end = moment().subtract(1, 'month').endOf('month');
                fromElem.data('DateTimePicker').date(start);
                toElem.data('DateTimePicker').date(end);
            },
            'last_30_days': function () {
                var start = moment().subtract(29, 'days');
                var end = moment();
                fromElem.data('DateTimePicker').date(start);
                toElem.data('DateTimePicker').date(end);
            },
            'last_60_days': function () {
                var start = moment().subtract(59, 'days');
                var end = moment();
                fromElem.data('DateTimePicker').date(start);
                toElem.data('DateTimePicker').date(end);
            },
            'last_90_days': function () {
                var start = moment().subtract(89, 'days');
                var end = moment();
                fromElem.data('DateTimePicker').date(start);
                toElem.data('DateTimePicker').date(end);
            },
            'months': function (numb) {
                var start = moment().subtract(numb, 'months').startOf('month');
                var end = moment().subtract(numb, 'months').endOf('month');
                fromElem.data('DateTimePicker').date(start);
                toElem.data('DateTimePicker').date(end);
            },
            'years': function (numb) {
                var start = moment().subtract(numb, 'years').startOf('year');
                var end = moment().subtract(numb, 'years').endOf('year');
                fromElem.data('DateTimePicker').date(start);
                toElem.data('DateTimePicker').date(end);
            }
        };

        if (ranges.hasOwnProperty(index) && typeof (ranges[index]) === 'function') {
            handler = ranges[index];
            handler.call(index);
        } else {
            var fnData = index.split(' ');
            if (fnData.length === 2) {
                var numb = fnData[0];
                index = fnData[1];
                if (!isNaN(numb) && ranges.hasOwnProperty(index) && typeof (ranges[index]) === 'function') {
                    handler = ranges[index];
                    handler.call(index, numb);
                }
            }
        }
    });
});
