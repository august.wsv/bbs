$(function () {
    let table;
    
    let absenceColumns = {
        no: 0,
        name: 1,
        group: 2,
        date: 3,
        reason: 4,
        projects: 5,
        status: 6,
        detail: 7,
    }
    
    function initTable(total) {
        total = total > 10 ? true : null;
        let count = 1;
        const $table = $('#tablePreview');
        $table.find('tr th').css('width', 0);
        $table.find('tr th:nth-child(2)').css('max-width', '100px');

        const columns = Array.prototype.map.call(Object.entries(absenceColumns), (col) => {
            return { data: col[0] };
        });
        
        table = $table.DataTable({
            columns,
            language: {
                lengthMenu: '',
                zeroRecords: 'Không tìm thấy dữ liệu',
                info: 'Trang _PAGE_/_PAGES_',
                infoEmpty: 'Dữ liệu trống',
                infoFiltered: '(_TOTAL_ kết quả từ _MAX_ bản ghi)',
                sSearch: 'Tìm kiếm',
            },
            paginate: total ?? $table.find('tbody tr').length > 10, // enable pagination if length > 10
            pageLength: 10, // set number of rows per page
            lengthChange: false, // hide page length dropdown
            searching: false, // disable search feature
            ordering: true, // enable sorting
            info: true, // show table information summary
            createdRow: function (row, data, dataIndex) {
                $(row).find('td:first-child').text(count++);
            },
        });

    }

    $('.calendar-search').on('click', function () {
        $(this)
            .prev()
            .datepicker({
                format: 'yyyy/mm/dd',
            })
            .datepicker('show');
    });

    $('#search-day-off-form').on('submit', function (e) {
        e.preventDefault();
        const id = $('#panelAbsence .search-day-off').val();
        const params = {};
        const errorHTML = '<small class="text-danger error position-absolute">Định dạng yyyy/mm/dd</small>';
        let isError = false;

        $('.search-date').each(function (index, item) {
            if (item.value) {
                if (/^\d{4}\/\d{2}\/\d{2}$/g.test(item.value)) {
                    params[$(this).attr('name')] = item.value;
                    $(item).parent().find('.error').remove();
                } else {
                    $(item).parent().append(errorHTML);
                    isError = true;
                }
            }
        });

        if (!isError) {
            getDayOff(urlGetDayOff + id, params);
        }
    });

    $('.search-date').on('input changeproperties', function () {
        $(this).parent().find('.error').remove();
    });

    $('#panelAbsence .search-day-off').on('change', function() {
        $('#search-day-off-form').trigger('submit');
    });

    function getDayOff(url, params) {
        $.ajax({
            url,
            data: params,
            success(response) {
                renderTable(response);
            },
        });
    }

    function renderTable(response) {
        table.clear().draw();
        table.destroy();
        initTable(response.length);
        $.each(response, function (index, item) {
            const rowNode = table.row
                .add({
                    no: index ?? '',
                    name: item.user?.name ?? '',
                    group: item.user?.teams[0]?.group.name ?? '',
                    date: typeof item.start_at == 'string' ? item.start_at.split(' ')[0] : '',
                    reason: item.reason ?? '',
                    projects: item.user?.projects.map((item) => item.name).join(', ') ?? '',
                    status: statusIcons[item.status] ?? '',
                    detail: `<a href="${urlApproveDayOff}#${item.id}" class="btn btn-link btn-px-0 btn-panelAbsence">Chi tiết >></a>`,
                })
                .draw()
                .node();

            const lastRow = $(rowNode).attr({
                'data-id': item.id,
                'data-number-off': item.total_day_off ?? 0,
            });

            lastRow.find('td:last-child').attr('data-id', item.id).addClass('p-0');
            $(table.cell(lastRow, absenceColumns.no).node()).addClass('d-none d-lg-table-cell');
            $(table.cell(lastRow, absenceColumns.group).node()).addClass('d-none d-xl-table-cell');
            $(table.cell(lastRow, absenceColumns.reason).node()).addClass('truncate reason');
            $(table.cell(lastRow, absenceColumns.projects).node()).addClass('d-none d-lg-table-cell');
            $(table.cell(lastRow, absenceColumns.status).node()).addClass('d-none d-md-table-cell status-approve-icon');
            $(table.cell(lastRow, absenceColumns.detail).node()).addClass('w-0');
        });
    }

    initTable();
});
