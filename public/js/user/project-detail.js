$(function () {
    console.clear();
    tinymce.init({
        selector: '.sprint-editor',
        height: '200px',
        plugins: ['advlist lists'],
        toolbar1: 'bullist numlist outdent indent | bold italic',
    });

    $('.calendar-search').each(function() {
        $(this)
            .prev()
            .datepicker({
                format: 'yyyy/mm/dd',
            });
        $(this).on('click', function() {
            $(this)
            .prev().datepicker('show');
        })
    });

    $('.calendar-search').on('click', function () {
        $(this)
            .prev()
            .datepicker({
                format: 'yyyy/mm/dd',
            })
            .datepicker('show');
    });

    $('#form-add-sprint').on('submit', function (e) {
        if (!validate.call(this, $(this).serializeArray())) {
            e.preventDefault();
        }
    });

    $('#table-sprints td.truncate').each(function () {
        $(this).tooltip({
            title: $(this).text(),
        });
    });

    $('.btn-edit.sprint').on('click', function () {
        const sprintId = $(this).data('sprint-id');
        const info = getInfoBySprintId(sprintId);

        const $modal = $('#modal-update-sprint').modal('show');
        $modal.find('[name=sprint_id]').val(sprintId);
        $modal.find('[name=project_id]').val(info.sprintProjectId);
        $modal.find('.modal-title').text(`Sửa ${info.sprintName.trim()}`);
        $modal.find('[name=status]').val(info.sprintStatus);
        $('#form-edit-input-print-name').val(info.sprintName.trim());
        $('#form-edit-input-start-date').val(info.sprintStartDate);
        $('#form-edit-input-end-date').val(info.sprintEndDate);
        $('#form-edit-input-print-effort').val(info.sprintEffort);

        tinymce.get('form-edit-input-print-desc').setContent(info.sprintDescription);
    });

    $('.btn-delete.sprint').on('click', function () {
        if (!$(this).data('url')) {
            return Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Có lỗi xảy ra!',
                footer: '<a href="#" onclick="location.reload()">Tải lại trang</a>',
            });
        }

        const $modal = $('#modal-delete-sprint').modal('show');
        $modal.find('form').attr('action', $(this).data('url'));
    });

    $('#form-edit-sprint').on('submit', function(e) {
        if (!validate.call(this, $(this).serializeArray())) {
            e.preventDefault();
        }

        const sprintId = $(this).find('[name=sprint_id]').val();
        const $nameError = $('#form-edit-input-print-name').parents('.group-input').find('.error');

        // Lấy mảng tên đã tồn tại
        const existingNames = $('#table-sprints').find('tbody tr').map(function() {
            if ($(this).data('sprint-id') != sprintId) {
                return $(this).find('.sprint-name').text().trim();
            }
        }).toArray();
        
        // Unique name validate
        $(this).serializeArray().forEach((item) => {
            if (item.name == 'name') {
                if (existingNames.includes(item.value)) {
                    e.preventDefault();
                    $nameError.text('Sprint này đã tồn tại trong dự án.');
                }
                return; // thoát vòng lặp
            }
        });
    });

    function getInfoBySprintId(id) {
        const $sprint = $('#table-sprints').find(`tr[data-sprint-id=${id}]`);
        if ($sprint.length <= 0) {
            return {};
        }
    
        return {
            sprintProjectId: $sprint.data('project-id'),
            sprintStatus: $sprint.data('sprint-status'),
            sprintName: $sprint.find('.sprint-name').text().trim(),
            sprintStartDate: $sprint.find('.sprint-time-start').val().trim(),
            sprintEndDate: $sprint.find('.sprint-time-end').val().trim(),
            sprintEffort: $sprint.find('.sprint-effort').text().trim(),
            sprintDescription: $sprint.find('.sprint-description').html().trim(),
        };
    }

    function validate(params) {
        let isSuccess = true;
        let isInvalidDesc = false;

        for (let param of params) {
            const $input = $(this).find(`[name='${param.name}']`);

            if (param.name == 'description') {
                let $elementTmp = $('<div>');
                $elementTmp.html(tinymce.get($input.attr('id')).getContent());
                isInvalidDesc = !$elementTmp.text();
            }

            if (!$input.val() || isInvalidDesc) {
                isSuccess = false;
                isInvalidDesc = false;
                $input.parents('.group-input').find('.error').text('Trường này là bắt buộc.');
            } else {
                $input.parents('.group-input').find('.error').text('');
            }
        }

        return isSuccess;
    }

    function showModalWithErrors($modal, errors = {}) {
        $modal.modal('show');
        const info = getInfoBySprintId(oldSprintId);

        if (info.sprintName) {
            $modal.find('.modal-title').text(info.sprintName);
        }
    
        for (let i in errors) {
            $modal.find(`[name=${i}]`).parents('.mb-3').find('.error').text(errors[i][0]);
        }
    }

    for (let i in bagErrors) {
        showModalWithErrors($(`#modal-${i}-sprint`), bagErrors[i]);
    }
});
