window.addEventListener("load", function () {
  // selectMonths= {"01":"Tháng 1" , "02": "Tháng 2", "03":"Tháng 3", "04":"Tháng 4", "05":"Tháng 5", "06":"Tháng 6", "07": "Tháng 7", "08":"Tháng 8", "09":"Tháng 9","10":"Tháng 10", "11":"Tháng 11", "12":"Tháng 12"};
  var currentYear = new Date().getFullYear();
  var currentMonth = new Date().getMonth() + 1;
  var currentDate = new Date().getDate();
  $('#year').text(currentYear);

  for (var i = 1; i <= 12; i++) {
    var text = (i < 10) ? "Tháng 0" + i : "Tháng " + i;

    $('#month')
      .append($('<option>', {value: i})
        .text(text));
  }

  $('#month option[value=' + currentMonth + ']').attr('selected', 'selected');
  var i;
  for (i = 1; i <= 31; i++) {
    var text = (i < 10) ? "0" + i : i;
    $('#date').append($('<option>', {value: i})
      .text(text));
  }
  $('#date option[value=' + currentDate + ']').attr('selected', 'selected');

});
var selectInit = false;


$(function () {
  window.start_date = null;
  window.end_date = null;
  var $calendar = $('#calendar-meeting');
  var $month = $('#month');
  var $date = $('#date');
  var filterRoomId = null;
  var filterHasMe = false;
  var filterProject = false;
  var filterName = false;
  var bookings = [];

  function showByRoom(roomId) {
    var renderBookings = bookings;
      filterRoomId = roomId;
      renderBookings = bookings.filter(function (item) {
        for (let i = 0; i < roomId.length; i++) {
          if (item.meeting_room_id == roomId[i]) {
            return item;
          }
        }
      });

    $calendar.fullCalendar('removeEvents');
    $calendar.fullCalendar('addEventSource', renderBookings);
    $calendar.fullCalendar('rerenderEvents');
  }

  $('.meeting_room_btn').click(function () {
    let roomID = listRoomChecked();
    showByRoom(roomID);
  });

  function listRoomChecked() {
    let roomIDs = [];
    $('#select-room').find("input[name='room_name']:checked").each(function() {
      roomIDs.push($(this).val());
    });
    return roomIDs;
  }

  // show by join
  function showByJoin(check) {
    var renderBookings = bookings;
    filterHasMe = check;
    if (check) {
      renderBookings = bookings.filter(function (item) {
        return item.has_me === true;
      });
    } else {
      renderBookings = bookings.filter(function (item) {
        return item;
      });
    }

    $calendar.fullCalendar('removeEvents');
    $calendar.fullCalendar('addEventSource', renderBookings);
    $calendar.fullCalendar('rerenderEvents');
  }

  $('#is_join').click(function () {
    showByJoin($('input[name="is_join"]').is(':checked'));
  });

  function filterByName(bookings, name) {
    return bookings.filter(function (item) {
      return searchBookings(item, name);
    });
  }

  function showByName(name) {
    var renderBookings = bookings;
    if (name.trim()) {
      filterName = name.trim();
      renderBookings = filterByName(bookings, filterName);
    } else {
      filterName = false;
    }
    $calendar.fullCalendar('removeEvents');
    $calendar.fullCalendar('addEventSource', renderBookings);
    $calendar.fullCalendar('rerenderEvents');
  }

  $('#txtSearch').keyup(function () {
    showByName($(this).val());
  });


  // show by project

  function filterByProject(booking, projectIds) {
    var result = false;
    projectIds.forEach(function (projectId) {
      result = booking.participants.includes(projectId);
      if (result) return;
    });
    return result;
  }

  function showByProject(projectIds) {
    var renderBookings = bookings;
    if (projectIds[0] && projectIds[0].indexOf('Tất cả') > -1) {
      filterProject = false;
    } else {
      filterProject = projectIds;
      renderBookings = bookings.filter(function (booking) {
        return filterByProject(booking, projectIds)
      });
    }
    $calendar.fullCalendar('removeEvents');
    $calendar.fullCalendar('addEventSource', renderBookings);
    $calendar.fullCalendar('rerenderEvents');
  }

  $('#selectProject').change(function () {
    showByProject($(this).val());
  });

  function renderCalendar() {
    var data = {
        start: window.start_date,
        end: window.end_date,
      },
      month = $month.children("option:selected").val();
    date = $date.children("option:selected").val();
    data.month = month;
    data.date = date;
    $.ajax({
      url: '/get_calendar-booking',
      method: 'GET',
      dataType: 'JSON',
      data: data,
      success: function (response) {
        if (response.code == 200 && response.data.bookings) {
          $calendar.fullCalendar('removeEvents');
          bookings = response.data.bookings;
          var renderBookings = bookings.filter(function (item) {
            var result = true;
            if (filterRoomId) {
              result = item.meeting_room_id == filterRoomId;
            }
            if (filterHasMe) {
              result = result && item.has_me == true;
            }
            if (filterName) {
              result = result && searchBookings(item, filterName);
            }
            if (filterProject) {
              result = result && filterByProject(item, filterProject);
            }
            return result;
          });
          $calendar.fullCalendar('addEventSource', renderBookings);
          $calendar.fullCalendar('rerenderEvents');
        }
      }
    });
  }

  $month.change(function () {
    renderCalendar();
  });
  $date.change(function () {
    renderCalendar();
  });

  function resetModal() {
    $("#addMeeting .notice-error").hide();
    $("#addMeeting .error").removeClass('error');
    $("#addMeeting")[0].reset();
    if (selectInit)
      $('.selectpicker').selectpicker('refresh');
  }

  function clickEvent(calEvent) {
    var data = {
      'id': calEvent.id,
      'booking_id': calEvent.booking_id,
      'meeting_room_id': calEvent.meeting_room_id,
      'start_time': calEvent.start.format('HH:mm:00'),
      'end_time': calEvent.end.format('HH:mm:00'),
      'date': calEvent.start.format('YYYY-MM-DD'),
    };
    $.ajax({
      url: '/get-booking',
      data: data,
      type: 'GET',
      success: function (data) {
        if (data) {
          buttonEffects();
          var meeting = data.booking;
          $('#id_booking').val(calEvent.id);
          $('#start_date').val(calEvent.start.format('YYYY-MM-DD'));
          $('#show-title').text(meeting.title);
          $('#show-content').html(meeting.content.replace(/(?:\r\n|\r|\n)/g, '<br>'));
          $('#show-object').text(data.participants.join(', '));
          $('#show-meeting').text(data.meeting);
          $('#show-creator').text(meeting.creator.name);
          $('#show-date-create').text(meeting.created_at);
          $('#time').text(meeting.start_time.substring(0, 5) + ' - ' + meeting.end_time.substring(0, 5));

          if (meeting.users_id == userId) {
            $('#showModal').find('.modal-footer').show();
            $('#edit').one('click', function () {
              $('#meeting_id').val(meeting.meeting_id || meeting.id);
              $('#booking_id').val(meeting.id);
              $('#title').val(meeting.title);
              $('#content').val(meeting.content);
              $('#participants').val(meeting.participants);
              $('.selectpicker').selectpicker('refresh');
              $('#meeting_room_id').val(meeting.meeting_room_id);
              $('#show-creator').val(meeting.creator.name);
              $('#show-date-create').val(meeting.created_at);
              $('#start_time').val(moment(calEvent.start).format('HH:mm'));
              $('#end_time').val(moment(calEvent.end).format('HH:mm'));

              //reset the timepicker
              $('#start_time').timepicker('remove');
              $('#end_time').timepicker('remove');

              $('#start_time').timepicker({
                // 12 or 24 hour
                showInputs: false,
                showMeridian: false
              });
              $('#end_time').timepicker({
                // 12 or 24 hour
                showInputs: false,
                showMeridian: false
              });

              $('#days_repeat').val(meeting.date);
              $('input[name="is_notify"]').attr('checked', meeting.is_notify != 0);
              if (data.type == 'PAST') {
                $('#repeat').css('display', 'none');
              } else {
                $('input:radio[name="repeat_type"]').filter('[value=' + meeting.repeat_type + ']').attr('checked', true);
              }
              $('#showModal').modal('hide');
              $('#addModal').modal();

            });

            if (booking.repeat_type || calEvent.start.format('YYYY-MM-DD') >= moment().format('YYYY-MM-DD')) {
              // delete
              $('#deleteMessage').show();
              $('#delete').one('click', function () {
                $.ajax({
                  url: 'delete-booking',
                  data: {
                    meeting_id: meeting.id, //
                    booking_id: meeting.booking_id,
                  },
                  type: 'GET',
                  success: function (data) {
                    if (data && data.messages == 'success') {
                      $('#deleteModal').modal('hide');
                      $('#message').text('Bạn đã hủy thành công buổi họp!')
                      $('#deleteSuccessModal').modal();
                    }
                  },
                  fail: function () {
                    $('#deleteModal').modal('hide');
                    $('#message').text('Thao tác hủy không thành công!')
                    $('#deleteSuccessModal').modal();
                  }
                });

              });
            } else {
              $('#deleteMessage').hide();
            }
          } else {
            $('#showModal').find('.modal-footer').hide();
          }
          $('#showModal').modal();
        }
      }
    });
  }

  $calendar.fullCalendar({
    header: {
      left: 'prev,next today agendaDay,agendaWeek',
      center: 'title',
      right: ''
    },
    defaultView: 'agendaWeek',
    allDaySlot: false,
    minTime: "07:00",
    maxTime: "21:00",

    defaultDate: moment().format('YYYY-MM-DD'),
    locale: 'vi',
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    height: 720,
    eventRender: function (eventObj, $el) {

    },
    viewRender: function (newView, oldView) {
      window.start_date = newView.start.format('Y/M/D');
      window.end_date = newView.end.format('Y/M/D');

      renderCalendar();
    },
    selectable: true,
    select: function (start, end, allDay = false) {
      resetModal();
      //do something when space selected
      $('#start_time').val(moment(start).format('HH:mm'));
      $('#end_time').val(moment(end).format(' HH:mm'));

      //reset the timepicker
      $('#start_time').timepicker('remove');
      $('#end_time').timepicker('remove');

      $('#start_time').timepicker({
        // 12 or 24 hour
        showInputs: false,
        showMeridian: false
      });
      $('#end_time').timepicker({
        // 12 or 24 hour
        showInputs: false,
        showMeridian: false
      });

      $('#days_repeat').val(moment(start).format('YYYY-MM-DD'));

      //Show 'add event' modal
      $('#addModal').modal('show');
      if (!selectInit)
        setTimeout(function () {
          $(".btn-light").click();
          selectInit = true;
        }, 300)
    },
    eventClick: function (calEvent, jsEvent, view) {
      clickEvent(calEvent);
      
    },
    eventAllow: function (dropLocation, item) {
      return item.user_id == userId && item.end.diff(new Date()) >= 0;
    },
    eventDrop: function (calEvent, next) {
      $('#days_repeat').val(calEvent.start.format('YYYY-MM-DD'));
    },
  });
})

function buttonEffects() {
  $('#deleteMessage').click(function () {
    $('#showModal').modal('hide');
    $('#deleteModal').modal();
  });


  $('#ok').click(function () {
    $('#deleteSuccessModal').modal('hide');
    location.reload();
  });
  $('#cancel').click(function () {
    $('#deleteModal').modal('hide');
    $('#showModal').modal();
  });
}

$('#booking').click(function (event) {
  event.preventDefault();
  var repeat_type = $('[name="repeat_type"]:checked').val();
  var days_repeat = $('#days_repeat').val();
  var participants = [];
  $.each($(".selectpicker option:selected"), function () {
    participants.push($(this).val());
  });

  var meeting_room_id = $('#meeting_room_id').val();
  if (meeting_room_id == 1) $('#color').val('blue');
  else if (meeting_room_id == 2) $('#color').val('green');
  else if (meeting_room_id == 3) $('#color').val('orange');
  var start_time = $('#start_time').val();
  var end_time = $('#end_time').val();
  var title = $('#title').val();
  var content = $('#content').val();
  var is_notify = $('input[name="is_notify"]:checked').val();
  is_notify = (is_notify) ? is_notify : "0";
  var _token = $('#_token').val();
  var color = $('#color').val();
  var id = $('#meeting_id').val();
  var booking_id = $('#booking_id').val();
  if (id > 0) var url = '/sua-phong-hop/' + id;
  else var url = '/them-phong-hop';
  $("#addMeeting .error").removeClass('error');
  $("#addMeeting .notice-error").hide();

  $.ajax({
    url: url,
    type: "post",
    data: {
      "_token": _token,
      "participants[]": participants,
      "booking_id": booking_id,
      "meeting_room_id": meeting_room_id,
      "title": title,
      "content": content,
      "start_time": start_time,
      "end_time": end_time,
      "repeat_type": repeat_type,
      "is_notify": is_notify,
      "days_repeat": days_repeat,
      "color": color
    },

    success: function (data) {
      if (data.status == 422) {
        if (data.errors.participants) {
          $('.btn-light').addClass("error");
          $('.selectpicker').change(function () {
            $('.btn-light').removeClass("error");
          });

        }
        $.each(data.errors, function (i, error) {
          var el = $('#' + i);
          el.addClass("error");
          el.change(function () {
            $(this).removeClass("error");
          });
        });
        $("#addMeeting .notice-error").show();
      } else if (data.status == 500) {
        if (data.duplicate) {
          $('#meeting_room_id').addClass("error");
          $('#alert_message').html('Phòng họp đã sử dụng trong thời gian được chọn. <br /> Vui lòng chọn lại!')
          $('#alertModal').modal();
        } else if (data.unauthorized) {
          $('#alert_message').html('Bạn không có quyền sửa lịch họp này!')
          $('#alertModal').modal();
        }
      } else if (data.success) {
        console.log(data.success)
        location.reload();
      }
    },
    error: function (data) {

    }
  });
  buttonEffects();
});

function change_alias(alias) {
  var str = alias;
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
  str = str.replace(/ + /g, " ");
  str = str.trim();
  return str;
}

function toCompareText(text) {
  if (text.trim() != '') {
    text = change_alias(text.toLowerCase());
    return text.replace(/\W/g, '');
  }
}


// show by name
function searchBookings(meeting, search) {
  var searchs = search.split('');
  var totalMatch = 0;
  searchs.forEach(function (text) {
    if (toCompareText(meeting.title).indexOf(toCompareText(text)) > -1) {
      totalMatch++;
    }
  });
  return totalMatch > 0;
}
