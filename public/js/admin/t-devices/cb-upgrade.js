$(document).ready(function() {
    $('.add_input').each(function() {
        $(this).click(function() {
            let data_key_last = Number($(this).attr('data-key-last'));
            let data_name = $(this).attr('data-name')
            let required_data = $(this).attr('data-required')
            let mutiple_data = $(this).attr('data-mutiple')
            $(this).parent().parent().find('.append_input').append(
                `
                <input style="margin-top:12px" name="${data_name}[${data_key_last + 1}][name]" type="text" value="" class="form-control">
                <input name="${data_name}[${data_key_last + 1}][required]" type="hidden" value="${required_data}">
                <input name="${data_name}[${data_key_last + 1}][mutiple]" type="hidden" value="${mutiple_data}">
                `

            )

            $(this).parent().parent().parent().find('.append_note_input').append(
                `
                <input style="margin-top:12px" name="${data_name}[${data_key_last + 1}][note]" type="text" value="" class="form-control">
                `
            )
            $(this).attr('data-key-last', data_key_last + 1);
        })
    })




    $('.select_device').select2();


    $('.select_device').change(function() {
        var id_device_import = $(this).val();
        var id_device_attr = $('#device_type_id').val()
        if (id_device_import == 'MANUALLY') {
            var id_device = $('.select_device').val()
            $('.attribute_device').children().remove();
            $('.name_device').val('');
            $('.name_device').attr('readonly', false)
            loadAttributeDeviceFirst(id_device)

            $('.slect_type').change(function() {
                loadAttributeDeviceFirst($(this).val())
            })

            function loadAttributeDeviceFirst(id_device) {
                $.ajax({
                        url: "../../../t_device/get-device/attribute/" + id_device_attr,
                    })
                    .done(function(data_attributes) {
                        $('.attribute_device').children().remove();
                        $.each(data_attributes, function(key_attr, value_attr) {
                            $('.attribute_device').append(`
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group row col-md-6">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">${value_attr.name}</label>
                                        <div class="col-sm-8 add_input_js">
                                            <input name="specification[${value_attr.name}][0][name]" data-name-error="${value_attr.name}" type="text" class="validate i form-control" ${value_attr.is_required == 0 ? 'data-required=" REQUIRED "' : ''}>
                                            <input name="specification[${value_attr.name}][0][required]" value="${value_attr.is_required}" type="hidden">
                                            <input name="specification[${value_attr.name}][0][mutiple]" value="${value_attr.is_mutiple}" type="hidden">
                                        </div>
                                        <div class="col-sm-2">
                                        ` +
                                (value_attr.is_mutiple == 0 ? '<button data-count="0" data-mutiple="' + value_attr.is_mutiple + '" data-required="' + value_attr.is_required + '" data-name-attr="' + value_attr.name + '" type="button" class="btn btn-primary btn-sm btn_add_js"><i class="fa fa-plus"></i></button>' : '') +
                                `
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
                                        <div class="col-sm-8 add_note_js">
                                            <input name="specification[${value_attr.name}][0][note]" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `);

                        });

                        $('.attribute_device').find('.btn_add_js').click(function() {
                            let data_name_attr = $(this).attr('data-name-attr')

                            let count_input = Number($(this).attr('data-count'));
                            let required_data = $(this).attr('data-required')
                            let mutiple_data = $(this).attr('data-mutiple')
                            $(this).parent().parent().find('.add_input_js').append(`
                            <input style="margin-top:12px" data-name-error="${data_name_attr}" name="specification[${data_name_attr}][${count_input + 1}][name]" ${required_data == 0 ? 'data-required=" REQUIRED "' : ''} type="text" value="" class="validate form-control">
                            <input name="specification[${data_name_attr}][${count_input + 1}][required]" type="hidden" value="${required_data}">
                            <input name="specification[${data_name_attr}][${count_input + 1}][mutiple]" type="hidden" value="${mutiple_data}">
                        `);
                            $(this).parent().parent().parent().find('.add_note_js').append(`
                            <input style="margin-top:12px" name="specification[${data_name_attr}][${count_input + 1}][note]" type="text" value="" class="form-control">
                        `);
                            $(this).attr('data-count', count_input + 1);
                        })
                    });
            }
        } else {
            $.ajax({
                    url: '../../../t_devices/get-device-import/' + id_device_import
                })
                .done(function(response) {
                    $('.attribute_device').children().remove();
                    $('.name_device').val(response.name);
                    $.each(response.specification, function(key, value) {
                        $.each(value, function(key_v, value_v) {
                            $('.attribute_device').append(`
                            <div class="form-group row col-md-6 parent_attribute">
                                <label for="staticEmail" class="col-sm-2 col-form-label">${key}</label>
                                <div class="col-sm-8 append_input">
                                    <input readonly name="specification[${key}][${key_v}][name]" type="text" value="${value_v['name'] ?? ''}" class="form-control">
                                    <input name="specification[${key}][${key_v}][required]" type="hidden" value="${value_v['required'] ?? ''}">
                                    <input name="specification[${key}][${key_v}][mutiple]" type="hidden" value="${value_v['mutiple'] ?? ''}">
                                </div>
                            </div>
                            <div class="form-group row col-md-6">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
                                <div class="col-sm-8">
                                    <input readonly name="specification[${key}][${key_v}][note]" value="${value_v['note'] ?? ''}" type="text" class="form-control">
                                </div>
                            </div>
                        `)
                        })
                    })
                })
        }

    })

    $("#formSubmit").submit(function(e) {
        //stop submitting the form to see the disabled button effect
        $.each($('#find_validate').find('.validate'), function(key, value) {

            var error_name_attr = $(value).attr('data-name-error')
            if ($(value).attr('data-required') == ' REQUIRED ') {
                if ($(value).val() == '') {
                    e.preventDefault();

                    $('.error').find('.error-notification').remove()
                    $('.error').append(`
                        <p class="text-danger error-notification">Bạn không được bỏ trồng trường ${error_name_attr}!</p>
                    `)
                }
            }
        })

        //disable the submit button
        $("#btn_submit").attr("disabled", true);

        return true;

    });
})