$(document).ready(function() {
    $('.user_device').select2();
    var count = 2;

    $('#add_device').click(function() {
        count++;

        var select = `<div class="border border-append border_${count}" data-count="">
        <div class="col-md-12" style="border: 1px solid #cccccc; padding-top: 12px; padding-bottom: 12px; margin-bottom: 12px">
            <div>
                <label for="staticEmail" class="">Thiết bị : <span class="name_device_title_${count}">Tự thêm thiết bị</span></label>
                <button data-count="${count}" type="button" class="btn btn-danger btn-sm pull-right btn-remove-device"><i class="fa fa-times"></i></button>
            </div>
            <hr>
            <div class="row">
                <div class="form-group row col-md-6">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Tìm thiết bị</label>
                    
                    <div class="col-sm-8">
                        <select data-key="${count}" class="form-control select_dev_ipt append_select_element_${count}" name="device_select[${count}]">
                            <option value="MANUALLY">Tự thêm</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group row col-md-6">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Loại thiết bị</label>
                    <div class="col-sm-8">
                        <select class="form-control select_device_append_${count}" name="device_type_id[${count}]">

                        </select>
                    </div>
                </div>
            </div>
            <div class="append_attribute_${count}">

            </div>
        </div></div>`;





        $.ajax({
                url: "../../t_device/get-device",
            })
            .done(function(data) {
                $('#append_item').append(select);
                $('#append_item').find('.border-append').each(function() {
                    var remove = $(this);
                    $(this).find('.btn-remove-device').click(function() {
                        $(remove).remove();
                    })
                });

                selectElement()
                $.each(data, function(key, value) {
                    $('.select_device_append_' + count).append(`<option value="${value.id}">${value.name}</option>`);
                });
                let dom_append_attribute = $('#append_item').find('.append_attribute_' + count);
                let val_option = $($('#append_item').find('.select_device_append_' + count), "option:selected").val();
                loadAttributeDevice(dom_append_attribute, val_option)
                $('#append_item').find('.select_device_append_' + count).change(function() {
                    let val_id_change = $(this).val();
                    loadAttributeDevice(dom_append_attribute, val_id_change)
                });


                function selectElement() {
                    $.ajax({
                            url: '../../t_devices/list-device-import'
                        })
                        .done(function(response) {
                            $('#append_item').find('.append_select_element_' + count).select2()
                            $.each(response, function(key, value) {
                                var select_content = ""
                                select_content = value.name
                                if (value.device_type != '') {
                                    $('#append_item').find('.append_select_element_' + count).append(`
                                        <optgroup class="append_lable_${value.id}" label="${select_content}"></optgroup>
                                `)
                                }
                                $.each(value.device_type, function(key_type, value_type) {
                                    if (value_type != null) {
                                        $('#append_item').find('.append_select_element_' + count).find('.append_lable_' + value_type.device_type_id).append(`<option value="${value_type.id}">${value_type.name}</option>`)
                                    }
                                })
                            })

                            $('#append_item').find('.select_dev_ipt').change(function() {
                                var data_key_import = $(this).attr('data-key');
                                var id_device_import = $(this).val();
                                if (id_device_import == 'MANUALLY') {
                                    console.log('1', id_device_import)

                                    let dom_append_attribute = $('#append_item').find('.append_attribute_' + data_key_import);
                                    let val_option = $($('#append_item').find('.select_device_append_' + data_key_import), "option:selected").val();
                                    $('#append_item').find('.select_device_append_' + data_key_import).attr('disabled', false)
                                    $('#append_item').find('.name_device_title_' + data_key_import).text('Tự thêm thiết bị')
                                    loadAttributeDevice(dom_append_attribute, val_option)
                                    $('#append_item').find('.select_device_append_' + data_key_import).change(function() {
                                        let val_id_change = $(this).val();
                                        loadAttributeDevice(dom_append_attribute, val_id_change)
                                    });
                                } else {
                                    $.ajax({
                                            url: '../../t_devices/get-device-import/' + id_device_import
                                        })
                                        .done(function(response) {
                                            $('#append_item').find('.border_' + data_key_import).append(`<input type="hidden" value="${response.device_type_id}" name="device_type_id[${data_key_import}]">`)
                                            $('.append_attribute_' + data_key_import).children().remove();
                                            $('.name_device_' + data_key_import).val(response.name);
                                            $('#append_item').find('.name_device_title_' + data_key_import).text(response.name)
                                            $('#append_item').find('.select_device_append_' + data_key_import).val(response.device_type_id);
                                            $('#append_item').find('.select_device_append_' + data_key_import).attr('disabled', true)
                                                // $('.slect_type_' + data_key_import).find('option:selected').removeAttr('selected')
                                                // $('.slect_type_' + data_key_import).find('option[value="' + response.device_type_id + '"]').attr("selected", true);
                                            $.each(response.specification, function(key, value) {
                                                $.each(value, function(key_v, value_v) {
                                                    $('.append_attribute_' + data_key_import).append(`
                                                    <div class="row">   
                                                        <div class="form-group row col-md-6 parent_attribute">
                                                            <label for="staticEmail" class="col-sm-2 col-form-label">${key}</label>
                                                            <div class="col-sm-8 append_input">
                                                                <input readonly name="specification[${data_key_import}][${key}][${key_v}][name]" type="text" value="${value_v['name'] ?? ''}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row col-md-6">
                                                            <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
                                                            <div class="col-sm-8">
                                                                <input readonly name="specification[${data_key_import}][${key}][${key_v}][note]" value="${value_v['note'] ?? ''}" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    `)
                                                })
                                            })
                                        })
                                }
                            })
                        })

                }

                function loadAttributeDevice(dom, val_id) {
                    $.ajax({
                            url: "../../t_device/get-device/attribute/" + val_id,
                        })
                        .done(function(data_attributes) {
                            console.log('dom', dom);
                            dom.children().remove();
                            $.each(data_attributes, function(key_attr, value_attr) {
                                dom.append(`
                            <div class="row">
                                <div class="form-group row col-md-6">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">${value_attr.name}</label>
                                    <div class="col-sm-8 add_input_js">
                                        <input name="specification[${count}][${value_attr.name}][0][name]" type="text" data-name="${value_attr.name}" value="" class="validate form-control" ${value_attr.is_required == 0 ? 'data-required="REQUIRED"' : ''}>
                                        <input name="specification[${count}][${value_attr.name}][0][required]" type="hidden" value="${value_attr.is_required}">
                                        <input name="specification[${count}][${value_attr.name}][0][mutiple]" type="hidden" value="${value_attr.is_mutiple}">
                                    </div>
                                    <div class="col-sm-2">` +
                                    (value_attr.is_mutiple == 0 ? '<button data-count="0" data-mutiple="' + value_attr.is_mutiple + '" data-required="' + value_attr.is_required + '" data-name-attr="' + value_attr.name + '" type="button" class="btn btn-primary btn-sm btn_add_js"><i class="fa fa-plus"></i></button>' : '') +
                                    `
                                    </div>
                                </div>
                                <div class="form-group row col-md-6">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
                                    <div class="col-sm-8 add_note_js">
                                        <input name="specification[${count}][${value_attr.name}][0][note]" value="" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            `);

                            });

                            dom.find('.btn_add_js').click(function() {
                                let data_name_attr = $(this).attr('data-name-attr')
                                let count_input = Number($(this).attr('data-count'));
                                let required_data = $(this).attr('data-required')
                                let mutiple_data = $(this).attr('data-mutiple')
                                $(this).parent().parent().find('.add_input_js').append(`
                                    <input style="margin-top:12px" name="specification[${count}][${data_name_attr}][${count_input + 1}][name]" type="text" value="" class="form-control">
                                    <input name="specification[${count}][${data_name_attr}][${count_input + 1}][required]" type="hidden" value="${required_data}">
                                    <input name="specification[${count}][${data_name_attr}][${count_input + 1}][mutiple]" type="hidden" value="${mutiple_data}">
                                `);
                                $(this).parent().parent().parent().find('.add_note_js').append(`
                                    <input style="margin-top:12px" name="specification[${count}][${data_name_attr}][${count_input + 1}][note]" type="text" value="" class="form-control">
                                `);
                                $(this).attr('data-count', count_input + 1);
                            })
                        });
                }




            });


    });


    // $('.slect_device_import').change(function() {
    //     var data_key_import = $(this).attr('data-key');
    //     var id_device_import = $(this).val();
    //     if (id_device_import == 'MANUALLY') {
    //         console.log('2', id_device_import)
    //         var id_device = $('.slect_type_' + data_key_import).val()
    //         $('.attribute_device_' + data_key_import).children().remove();
    //         $('.name_device_' + data_key_import).val('');
    //         $('.name_device_' + data_key_import).attr('readonly', false)
    //         loadAttributeDeviceFirst(id_device)

    //         $('.slect_type_' + data_key_import).change(function() {
    //             loadAttributeDeviceFirst($(this).val())
    //         })

    //         function loadAttributeDeviceFirst(id_device) {
    //             $.ajax({
    //                     url: "../../t_device/get-device/attribute/" + id_device,
    //                 })
    //                 .done(function(data_attributes) {
    //                     $('.attribute_device_' + data_key_import).children().remove();
    //                     $.each(data_attributes, function(key_attr, value_attr) {
    //                         $('.attribute_device_' + data_key_import).append(`
    //                         <div class="row">
    //                             <div class="form-group row col-md-6">
    //                                 <label for="staticEmail" class="col-sm-2 col-form-label">${value_attr.name}</label>
    //                                 <div class="col-sm-8 add_input_js">
    //                                     <input name="specification[${data_key_import}][${value_attr.name}][0][name]" data-name="${value_attr.name}" class="validate form-control" ${value_attr.is_required == 0 ? 'data-required="REQUIRED"' : ''} type="text" value="">
    //                                     <input name="specification[${data_key_import}][${value_attr.name}][0][required]" type="hidden" value="${value_attr.is_required}">
    //                                     <input name="specification[${data_key_import}][${value_attr.name}][0][mutiple]" type="hidden" value="${value_attr.is_mutiple}">
    //                                 </div>
    //                                 <div class="col-sm-2">` +
    //                             (value_attr.is_mutiple == 0 ? '<button data-count="0" data-mutiple="' + value_attr.is_mutiple + '" data-required="' + value_attr.is_required + '" data-name-attr="' + value_attr.name + '" type="button" class="btn btn-primary btn-sm btn_add_js"><i class="fa fa-plus"></i></button>' : '') +
    //                             `
    //                             </div>
    //                             </div>
    //                             <div class="form-group row col-md-6">
    //                                 <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
    //                                 <div class="col-sm-8 add_note_js">
    //                                     <input name="specification[${data_key_import}][${value_attr.name}][0][note]" type="text" class="form-control">
    //                                 </div>
    //                             </div>
    //                         </div>
    //                         `);

    //                     });

    //                     $('.attribute_device_' + data_key_import).find('.btn_add_js').click(function() {
    //                         let data_name_attr = $(this).attr('data-name-attr')
    //                         let count_input = Number($(this).attr('data-count'));
    //                         let required_data = $(this).attr('data-required')
    //                         let mutiple_data = $(this).attr('data-mutiple')
    //                         $(this).parent().parent().find('.add_input_js').append(`
    //                             <input style="margin-top:12px" name="specification[${data_key_import}][${data_name_attr}][${count_input + 1}][name]" type="text" value="" class="form-control">
    //                             <input name="specification[${data_key_import}][${data_name_attr}][${count_input + 1}][required]" type="hidden" value="${required_data}">
    //                             <input name="specification[${data_key_import}][${data_name_attr}][${count_input + 1}][mutiple]" type="hidden" value="${mutiple_data}">
    //                         `);
    //                         $(this).parent().parent().parent().find('.add_note_js').append(`
    //                             <input style="margin-top:12px" name="specification[${data_key_import}][${data_name_attr}][${count_input + 1}][note]" type="text" class="form-control">
    //                         `);
    //                         $(this).attr('data-count', count_input + 1);
    //                     })
    //                 });
    //         }
    //     } else {

    //         $.ajax({
    //                 url: '../../t_devices/get-device-import/' + id_device_import
    //             })
    //             .done(function(response) {
    //                 $('.attribute_device_' + data_key_import).children().remove();
    //                 $('.name_device_' + data_key_import).val(response.name);
    //                 $('.slect_type_' + data_key_import).val(response.device_type_id)
    //                 $.each(response.specification, function(key, value) {
    //                     $.each(value, function(key_v, value_v) {
    //                         $('.attribute_device_' + data_key_import).append(`
    //                         <div class="row">   
    //                             <div class="form-group row col-md-6 parent_attribute">
    //                                 <label for="staticEmail" class="col-sm-2 col-form-label">${key}</label>
    //                                 <div class="col-sm-8 append_input">
    //                                     <input readonly name="specification[${data_key_import}][${key}][${key_v}][name]" type="text" value="${value_v['name'] ?? ''}" class="form-control">
    //                                 </div>
    //                             </div>
    //                             <div class="form-group row col-md-6">
    //                                 <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
    //                                 <div class="col-sm-8">
    //                                     <input readonly name="specification[${data_key_import}][${key}][${key_v}][note]" value="${value_v['note'] ?? ''}" type="text" class="form-control">
    //                                 </div>
    //                             </div>
    //                         </div>
    //                         `)
    //                     })
    //                 })
    //             })
    //     }

    // })

    // $('.btn-remove-device').click(function() {
    //     let data_count = $(this).attr('data-count');
    //     $('.border_' + data_count).remove();
    // })

    // $('.add_input').each(function() {
    //     $(this).click(function() {
    //         let data_key_last = Number($(this).attr('data-key-last'));
    //         let data_name = $(this).attr('data-name')
    //         $(this).parent().parent().find('.append_input').append(
    //             `
    //             <input style="margin-top:12px" name="${data_name}[${data_key_last + 1}][name]" type="text" value="" class="form-control">
    //             `
    //         )
    //         $(this).attr('data-key-last', data_key_last + 1);
    //     })
    // })


    $("#formSubmit").submit(function(e) {

        //stop submitting the form to see the disabled button effect
        $.each($('#append_item').find('.validate'), function(key, value) {
            var error_name_attr = $(value).attr('data-name')

            if ($(value).attr('data-required') == 'REQUIRED') {
                if ($(value).val() == '') {
                    e.preventDefault();
                    $('.error').find('.error-notification').remove()
                    $('.error').append(`
                        <p class="text-danger error-notification">Bạn không được bỏ trồng trường ${error_name_attr}!</p>
                    `)
                }
            }
        })

        //disable the submit button
        $("#btn_submit").attr("disabled", true);

        return true;

    });


});