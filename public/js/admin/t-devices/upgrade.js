$(document).ready(function() {
    $('.add_input').each(function() {
        $(this).click(function() {
            let data_key_last = Number($(this).attr('data-key-last'));
            let data_name = $(this).attr('data-name')
            let required_data = $(this).attr('data-required')
            let mutiple_data = $(this).attr('data-mutiple')
            $(this).parent().parent().find('.append_input').append(
                `
                <input style="margin-top:12px" name="${data_name}[${data_key_last + 1}][name]" type="text" value="" class="form-control">
                <input name="${data_name}[${data_key_last + 1}][required]" type="hidden" value="${required_data}">
                <input name="${data_name}[${data_key_last + 1}][mutiple]" type="hidden" value="${mutiple_data}">
                `

            )

            $(this).parent().parent().parent().find('.append_note_input').append(
                `
                <input style="margin-top:12px" name="${data_name}[${data_key_last + 1}][note]" type="text" value="" class="form-control">
                `
            )
            $(this).attr('data-key-last', data_key_last + 1);
        })
    })


    $('.select_device').select2();


    $('.select_device').change(function() {
        var id_device_import = $(this).val();
        var id_device_attr = $('#device_type_id').val()

        $.ajax({
                url: '../../../t_devices/get-device-import/' + id_device_import
            })
            .done(function(response) {
                $('.attribute_device').children().remove();
                $('.name_device').val(response.name);
                $.each(response.specification, function(key, value) {
                    $.each(value, function(key_v, value_v) {
                        $('.attribute_device').append(`
                        <div class="form-group row col-md-6 parent_attribute">
                            <label for="staticEmail" class="col-sm-4 col-form-label">${key}</label>
                            <div class="col-sm-8 row append_input">
                                <input readonly name="specification[${key}][${key_v}][name]" type="text" value="${value_v['name'] ?? ''}" class="form-control">
                                <input name="specification[${key}][${key_v}][required]" type="hidden" value="${value_v['required'] ?? ''}">
                                <input name="specification[${key}][${key_v}][mutiple]" type="hidden" value="${value_v['mutiple'] ?? ''}">
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
                            <div class="col-sm-8">
                                <input readonly name="specification[${key}][${key_v}][note]" value="${value_v['note'] ?? ''}" type="text" class="form-control">
                            </div>
                        </div>
                    `)
                    })
                })
            })

    })

    $("#formSubmit").submit(function(e) {
        //stop submitting the form to see the disabled button effect
        $.each($('#find_validate').find('.validate'), function(key, value) {

            var error_name_attr = $(value).attr('data-name-error')
            if ($(value).attr('data-required') == ' REQUIRED ') {
                if ($(value).val() == '') {
                    e.preventDefault();

                    $('.error').find('.error-notification').remove()
                    $('.error').append(`
                        <p class="text-danger error-notification">Bạn không được bỏ trồng trường ${error_name_attr}!</p>
                    `)
                }
            }
        })

        //disable the submit button
        $("#btn_submit").attr("disabled", true);

        return true;

    });
})