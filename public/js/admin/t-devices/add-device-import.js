$(document).ready(function() {
    $('.user_device').select2();
    var count = 0;
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    };
    $('#add_device').click(function() {
        count++;

        var select = `<div class="border border-append border_${count}" data-count="">
        <div class="col-md-12" style="border: 1px solid #cccccc; padding-top: 12px; padding-bottom: 12px; margin-bottom: 12px">
            <div>
                <label for="staticEmail" class="">Thiết bị : <span class="name-type_device_${count}"></span></label>
                <button data-count="${count}" type="button" class="btn btn-danger btn-sm pull-right btn-remove-device"><i class="fa fa-times"></i></button>
            </div>
            <hr>
            <div class="row">
                <div class="form-group row col-md-6">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Loại thiết bị</label>
                    <div class="col-sm-8">
                        <select class="form-control select_device_append_${count}" required name="device_type_id[${count}]">

                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group row col-md-6">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Mã thiết bị</label>
                    <div class="col-sm-8">
                        <input type="text" name="code[${count}]" required class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group row col-md-6">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Số lượng</label>
                    <div class="col-sm-8">
                        <input name="sum_total[${count}]" required class="form-control input_just_number_${count}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group row col-md-6">
                    <label for="example-date-input" class="col-sm-3 col-form-label">Ngày mua</label>
                    <div class="col-sm-8">
                        <input name="date_import[${count}]" class="form-control" required type="date">
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group row col-md-6">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Bảo hành</label>
                    <div class="col-sm-8">
                        <input type="text" name="guarantee[${count}]" required class="form-control">
                    </div>
                </div>
            </div>
            <div class="append_attribute_${count}">

            </div>
        </div></div>`;





        $.ajax({
                url: "../../t_device/get-device",
            })
            .done(function(data) {
                $('#append_item').append(select);

                $('#append_item').find('.border-append').each(function() {
                    var remove = $(this);
                    $(this).find('.btn-remove-device').click(function() {
                        $(remove).remove();
                    })
                });
                $.each(data, function(key, value) {
                    $('.select_device_append_' + count).append(`<option value="${value.id}">${value.name}</option>`);
                });
                let dom_append_attribute = $('#append_item').find('.append_attribute_' + count);
                let val_option = $($('#append_item').find('.select_device_append_' + count), "option:selected").val();

                loadAttributeDevice(dom_append_attribute, val_option)
                $('#append_item').find('.select_device_append_' + count).change(function() {
                    let val_id_change = $(this).val();
                    loadAttributeDevice(dom_append_attribute, val_id_change)
                });

                $('#append_item').find('.input_just_number_' + count).inputFilter(function(value) {
                    console.log('value', value);
                    return /^\d*$/.test(value);
                });

                function loadAttributeDevice(dom, val_id) {
                    $.ajax({
                            url: "../../t_device/get-device/attribute/" + val_id,
                        })
                        .done(function(data_attributes) {
                            dom.children().remove();
                            $.each(data_attributes, function(key_attr, value_attr) {
                                dom.append(`
                            <div class="row">
                                <div class="form-group row col-md-6">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">${value_attr.name}</label>
                                    <div class="col-sm-8 add_input_js">
                                        <input name="specification[${count}][${value_attr.name}][0][name]" type="text" data-name="${value_attr.name}" value="" class="validate form-control" ${value_attr.is_required == 0 ? 'data-required="REQUIRED"' : ''}>
                                        <input name="specification[${count}][${value_attr.name}][0][required]" type="hidden" value="${value_attr.is_required}">
                                        <input name="specification[${count}][${value_attr.name}][0][mutiple]" type="hidden" value="${value_attr.is_mutiple}">
                                    </div>
                                    <div class="col-sm-1">` +
                                    (value_attr.is_mutiple == 0 ? '<button data-count="0" data-mutiple="' + value_attr.is_mutiple + '" data-required="' + value_attr.is_required + '" data-name-attr="' + value_attr.name + '" type="button" class="btn btn-primary btn-sm btn_add_js"><i class="fa fa-plus"></i></button>' : '') +
                                    `
                                    </div>
                                </div>
                                <div class="form-group row col-md-6">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Ghi chú</label>
                                    <div class="col-sm-8 add_note_js">
                                        <input name="specification[${count}][${value_attr.name}][0][note]" value="" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            `);

                            });

                            dom.find('.btn_add_js').click(function() {
                                let data_name_attr = $(this).attr('data-name-attr')
                                let count_input = Number($(this).attr('data-count'));
                                let required_data = $(this).attr('data-required')
                                let mutiple_data = $(this).attr('data-mutiple')
                                $(this).parent().parent().find('.add_input_js').append(`
                                    <input style="margin-top:12px" name="specification[${count}][${data_name_attr}][${count_input + 1}][name]" type="text" value="" class="form-control">
                                    <input name="specification[${count}][${data_name_attr}][${count_input + 1}][required]" type="hidden" value="${required_data}">
                                    <input name="specification[${count}][${data_name_attr}][${count_input + 1}][mutiple]" type="hidden" value="${mutiple_data}">
                                `);
                                $(this).parent().parent().parent().find('.add_note_js').append(`
                                    <input style="margin-top:12px" name="specification[${count}][${data_name_attr}][${count_input + 1}][note]" type="text" value="" class="form-control">
                                `);
                                $(this).attr('data-count', count_input + 1);
                            })
                        });
                }

            });


    });


    $('.btn-remove-device').click(function() {
        let data_count = $(this).attr('data-count');
        $('.border_' + data_count).remove();
    })

    $('.add_input').each(function() {
        $(this).click(function() {
            let data_key_last = Number($(this).attr('data-key-last'));
            let data_name = $(this).attr('data-name')
            $(this).parent().parent().find('.append_input').append(
                `
                <input style="margin-top:12px" name="${data_name}[${data_key_last + 1}][name]" type="text" value="" class="form-control">
                `
            )
            $(this).attr('data-key-last', data_key_last + 1);
        })
    })


    $("#formSubmit").submit(function(e) {

        //stop submitting the form to see the disabled button effect
        $.each($('#append_item').find('.validate'), function(key, value) {
            var error_name_attr = $(value).attr('data-name')

            if ($(value).attr('data-required') == 'REQUIRED') {
                if ($(value).val() == '') {
                    e.preventDefault();
                    $('.error').find('.error-notification').remove()
                    $('.error').append(`
                        <p class="text-danger error-notification">Bạn không được bỏ trồng trường ${error_name_attr}!</p>
                    `)
                }
            }
        })

        //disable the submit button
        $("#btn_submit").attr("disabled", true);

        return true;

    });


});