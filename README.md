# JVB - BBS: Laravel 5.6 integrated with Admin LTE

This project is **Laravel 5.6** integrated with **Admin LTE-2.3.11**

## How to Install

- Download this repo & extract or clone using Git
- Install project dependency by running `composer install`
- Copy `.env.example` to `.env`
- Create new database with name is bbs, execute bbs.sql file in database directory (mysql command line is recommendation)
- Run `php artisan migrate` to update database
- Run `php artisan db:seed` to create admin account (pw: admin@2022)
- Run `php artisan serve`
- Open `localhost:8000\admin`

## How to generate code

- Generate module: run `php artisan core:make-module {module name} {table name}`
- Generate model only: run `php artisan core:make-model {model name} {table name}`

## Deployment

### Create env file

```bash
cp .env.production .env
```

### Instalation

```bash
docker-compose up -d --build
docker-compose exec app sh -c "cp .env.production .env"
docker-compose exec app sh -c "composer install"
docker-compose exec app sh -c "php artisan key:generate"
docker-compose exec app sh -c "php artisan migrate"
docker-compose exec app sh -c "php artisan db:seed"
docker-compose exec app sh -c "npm install"
docker-compose exec app sh -c "npm run prod"
docker-compose restart app
```
