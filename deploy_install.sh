#!/bin/bash
set -xe

cp .env.production .env

composer install

php artisan key:generate --force
php artisan migrate --force
php artisan db:seed --force

php artisan optimize